!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
      SUBROUTINE ANALYSE ()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!     
      integer, dimension(:),     allocatable     :: num_nco
      logical, dimension(size(mol)-1)    :: new
      integer(kind=INT_LONG), dimension(:),allocatable:: ncomb_plus, ncomb_minus
      integer(kind=INT_LONG) :: ncomb_tot, AAN, BBN, CCN, i
      integer, dimension(:,:,:), allocatable     :: nco
!
      integer :: j,k, na, nao, nb, nbo ,n7, n8
      integer :: nmax, ncnt, ncut, nnpos
      integer (kind=INT_LONG) :: nct
!
      real, dimension(:,:), allocatable :: enlist
      integer, dimension(size(mol),4)       :: hkl      
      integer, dimension(size(mol),3:4)     :: hkl_old
      real            ::  entot, toten
      real            :: en_lowest = 1000000
      logical :: fflag
!
      real, dimension(max_npos)             :: en_up_to
!     
!     NUM_NCO(i) = NUMBER OF AA * ROTAMERS AT EACH POSITION (i)	
!     NMAX = MAX NUMBER ROTAMERS AT ANY POSITION
!     NCO(i, ncnt, 1-2) = STORES POINTERS FOR AA and ROTAMERS
!
      nnpos=size(mol)-1
      if (allocated(num_nco)) deallocate(num_nco)
      allocate(num_nco(nnpos))
      if (allocated(ncomb_plus)) deallocate(ncomb_plus)
      allocate(ncomb_plus(nnpos))
      if (allocated(ncomb_minus)) deallocate(ncomb_minus)
      allocate(ncomb_minus(nnpos))
!
!      allocate(num_nco(size(mol)-1))
!      allocate(ncomb_plus(size(mol)-1))
!      allocate(ncomb_minus(size(mol)-1))
!
      nmax=0
      fflag=.true.
!
      do i=1,nnpos
            num_nco(i)=0
            do j=1,mol(i+1)%res(1)%res_naa
               num_nco(i)=num_nco(i)+(mol(i+1)%res(1)%res_aas(j)%aa_nrt)
               if (nmax < num_nco(i)) then
                   nmax=num_nco(i)
               end if
               do k=1, mol(i+1)%res(1)%res_aas(j)%aa_nrt
!
         mol(i+1)%res(1)%res_aas(j)%aa_rots(k)%rot_flag=.false.
         mol(i+1)%res(1)%res_aas(j)%aa_rots(k)%rot_flag2=.false.
!
               end do
            end do
        end do
!
!      allocate(nco((size(num_nco)),nmax,2))
      allocate(nco(nnpos,nmax,2))
!
      do i=1,nnpos
            ncnt=0
            do j=1,mol(i+1)%res(1)%res_naa
                do k=1,mol(i+1)%res(1)%res_aas(j)%aa_nrt
               ncnt=ncnt+1
               nco(i,ncnt,1)=j
               nco(i,ncnt,2)=k
            end do
           end do
      end do
!
      ncomb_tot=1     
!
      do i=1, nnpos
             ncomb_minus(i)=  ncomb_tot
             ncomb_tot=       ncomb_tot*num_nco(i)
             ncomb_plus(i)=   ncomb_tot
      end do
!     
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '#    SUMMARY ANALYSE           #'
	PRINT*, '################################'
	PRINT*, '          '
!	
	PRINT*, '          '
	PRINT*, '  NUMBER OF COMBINATIONS TO TEST: '
	PRINT*, '          '
	DO i=1, nnpos
	PRINT*, '  COMBINATION LIMITS:  ',ncomb_minus(i), ncomb_plus(i)
	END DO
	PRINT*, '      ' 
	PRINT*, '  TOTAL NUMBER OF COMBINATIONS TO TEST: ', ncomb_tot 
	PRINT*, '      ' 
	PRINT*, '  LIMIT SPECIFIED IN INPUT FOR TESTING: MAXCOMB = ',mumbo_ana_mxc
	PRINT*, '  LIMIT SPECIFIED FOR SORTING: MAXSORT = ',mumbo_ana_mxs 
	PRINT*, '  LIMIT SPECIFIED FOR GLOBAL ENERGY MINIMUM: EN_BWR = ',mumbo_en_bwr 
!
	if (ncomb_tot > mumbo_ana_mxc) then
!
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>    '
	PRINT*, '>> NUMBER OF COMBINATIONS > MAXCOMB                      '
	PRINT*, '>> YOU MAY INCREASE MAXCOMB, BUT BE AWARE OF INCREASE    '
	PRINT*, '>> OF COMPUTER TIME NEEDED                               '
	PRINT*, '>> OR RERUN BRUT WITH A LOWER ENERGY_BRUT, KEEP - VALUE  '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>    '
!	
		stop
	end if
!
	allocate(enlist(mumbo_ana_mxs,2))
	do i= 1, mumbo_ana_mxs
	   enlist(i,1) = real(2 * ncomb_tot) 
	   enlist(i,2) = mumbo_en_bwr + 3 * abs(mumbo_en_bwr)
	end do   
!	
!
	do i=1,ncomb_tot
          do j=1,nnpos
!
              new(j)=.true.
!
              AAN = mod(i,ncomb_plus(j))
              if (AAN==0) then
                 AAN=ncomb_plus(j)
              end if
!
              BBN=int(AAN/(ncomb_minus(j)))
              CCN=mod(i,ncomb_minus(j))
                hkl(j,1)=j+1
                hkl(j,2)=1
              if (CCN > 0) then
                hkl(j,3)=nco(j,(BBN+1),1)
                hkl(j,4)=nco(j,(BBN+1),2)
              else
                hkl(j,3)=nco(j,(BBN),1)
                hkl(j,4)=nco(j,(BBN),2)
              end if
!
          end do
!
          do j=nnpos,1,-1
          na=hkl(j,3)
          nao=hkl_old(j,3)
          nb=hkl(j,4)
            nbo=hkl_old(j,4)
              if (na==nao.and.nb==nbo) then
                  new(j)=.false.
                  cycle
              else
                  goto 100
              end if
          end do
100      continue
!
          do j=1,nnpos
               hkl_old(j,3)=hkl(j,3)
               hkl_old(j,4)=hkl(j,4)
          end do      
!
          call tot_energy(hkl,new,entot,fflag,en_up_to)
          fflag=.false.
!          
          if (entot < en_lowest) then
            en_lowest = entot
          end if
!
	if (entot < mumbo_en_bwr) then 
	  ncut = mumbo_ana_mxs
	  do j = mumbo_ana_mxs,1,-1
	    if ( entot < enlist(j,2) ) then
	      ncut=j-1
	      cycle	
	    else 
	      exit
	    end if
	  end do
!
	  if (ncut <= (mumbo_ana_mxs-2)) then 
	     do j= mumbo_ana_mxs, (ncut+2), -1
		enlist(j,1) = enlist((j-1),1) 
		enlist(j,2) = enlist((j-1),2) 
	     end do
	  end if
!
	  if (ncut <= (mumbo_ana_mxs-1)) then 
		  enlist(ncut+1,1) = real(i)
		  enlist(ncut+1,2) = entot
	  end if
	end if 
!
	end do
!
!	do i = 1, mumbo_ana_mxs
!	   PRINT*, i, enlist(i,1), enlist(i,2)
!	end do
!
!	NOW STARTING TO ANALYSE THE VARIOUS ENERGIES
!	PER RESIDUE
!

!
	do i = 1, mumbo_ana_mxs
	  if (enlist(i,2) < mumbo_en_bwr) then
	    nct = int(enlist(i,1),kind=INT_LONG)
	    do j=1,nnpos
!
		AAN = mod(nct,ncomb_plus(j))
		if (AAN==0) then
		  AAN=ncomb_plus(j)
		end if
!
		BBN=int(AAN/(ncomb_minus(j)))
		CCN=mod(nct,ncomb_minus(j))
		hkl(j,1)=j+1
		hkl(j,2)=1
		if (CCN > 0) then
		   hkl(j,3)=nco(j,(BBN+1),1)
		   hkl(j,4)=nco(j,(BBN+1),2)
		else
		   hkl(j,3)=nco(j,(BBN),1)
		   hkl(j,4)=nco(j,(BBN),2)
		end if
!
 	    end do
!
!	HAVING DETERMINED THE CONFIGURATION, GET ALL THE ENERGIES AND WRITE TO FILE
!
		toten = enlist(i,2)
		call write_energies(toten,hkl)
!
            	do j=1,nnpos
            	    n7=hkl(j,3)
                    n8=hkl(j,4)
            	    mol(j+1)%res(1)%res_aas(n7)%aa_rots(n8)%rot_flag=.true.
            	end do
!
!	        FLAG THE BEST SOLUTION FOR WRITING
!
	        if (i==1) then
!
            do j=1,nnpos
            n7=hkl(j,3)
            n8=hkl(j,4)
            mol(j+1)%res(1)%res_aas(n7)%aa_rots(n8)%rot_flag2=.true.
            end do
!
		end if
!
	  else 
	    exit
	  end if 
!
	end do
!
	close(15)
	close(16)
	if (mumbo_log_ana_flag) close(17)
!
!
	if (en_lowest >  mumbo_en_bwr) then
            PRINT*,  '                                                  '
            PRINT*,  ' >>>>>>>>>>>>>>>>>>>>>>>>>>  BUMMER >>>>>>>>>>>>>>'
            PRINT*,  ' >>  NO COMBINATIONS FOUND WITH E < ',mumbo_en_bwr  
            PRINT*,  ' >>  LOWEST ENERGY FOUND = ', en_lowest 
            PRINT*,  ' >>                                               '
            PRINT*,  ' >>  NOTHING TO ANALAZE IN - ANA STEP -           ' 
            PRINT*,  ' >>  YOU MIGHT WANT TO INCREASE THE KEEP VALUE    '
            PRINT*,  ' >>  IN:  ENERGY_BRUT=   KEEP= -300               '
            PRINT*,  ' >>                                               '
            PRINT*,  ' >>  MUST STOP                                    '
            PRINT*,  ' >>>>>>>>>>>>>>>>>>>>>>>>>>  BUMMER >>>>>>>>>>>>>>'
            PRINT*,  '                                                  '
	    STOP
	end if
!
	PRINT*, '  ' 
	PRINT*, '  RESULTS WRITTEN TO FILE: mumbo.ene     '
	PRINT*, '  AND FILE: mumbo.lis ' 
	PRINT*, '  ' 
	PRINT*, '  ... FINISHED WITH ANALYSE   '
!
	END SUBROUTINE ANALYSE
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
      SUBROUTINE LOAD_REF_STRUCT 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
     USE MOLEC_M
     USE AA_DATA_M
     USE MUMBO_DATA_M
     USE MAX_DATA_M
     USE MOLECULE_SIMP_DATA_M
!     
     IMPLICIT NONE
!     
     INTEGER :: i,j,k,l
     INTEGER :: nres, nats, nat, ntot
     CHARACTER (LEN=1)   ::  cid
     LOGICAL :: flag
     REAL ::  xbfa, xx, xy, xz, xocc
!     
     CHARACTER(LEN=4) :: aanm, atnm, atnm1, atnm2
     LOGICAL:: strange_flag
!
     LOGICAL            :: cif_flag, pdb_flag
     CHARACTER(LEN=4)   :: ext
     CHARACTER(LEN=132) :: infile
     INTEGER            :: nn
!
!
     PRINT*, '        '
     PRINT*, '#######################################'
     PRINT*, '# STARTING LOADING REFERENCE STRUCT   #'
     PRINT*, '#######################################'
     PRINT*, '        '
!
     cif_flag = .false.
     pdb_flag = .false.
!
     do i=1,132
        infile(i:i) = ' '
     end do
     infile(1:132) = mumbo_ref_pdb(1:132)
!     
     ext = infile((len_trim(infile)-3):len_trim(infile))
     if ((ext.eq.'.pdb').or.(ext.eq.'.PDB')) then  
         print*,'  REFERENCE FILE IS ASSUMED TO BE A ** PDB ** FILE '
         print*,'  ',infile(1:len_trim(infile))
         print*,'  '
         cif_flag = .false.
         pdb_flag = .true.
     else if ((ext.eq.'.cif').or.(ext.eq.'.CIF')) then
         print*,'  REFERENCE FILE IS ASSUMED TO BE A ** CIF ** FILE '
         print*,'  ',infile(1:len_trim(infile))
         print*,'  '
         cif_flag = .true.
         pdb_flag = .false.
     else
        print*, '  NOT CLEAR WHETHER REFERENCE INPUT FILE = PDB OR CIF-FILE '
        print*, '  FILE EXTENSION NOT RECOGNIZED: ', ext
        print*, '  MUST QUIT'
        print*,  infile(1:len_trim(infile)) !
        stop    
     end if   
!
!    reading in structure files
!
     nn =1                       ! everything should be read into sim(1) 
     if (cif_flag) then 
         call cif_read_coords(infile,nn)
     else if (pdb_flag) then 
         call pdb_read_coords(infile,nn)
     end if
!
     nres = size(sim(nn)%mol_res)
     if (allocated(ori)) deallocate (ori)
     allocate (ori(1))
!     
     allocate (ori(1)%res(nres))
     ori(1)%mol_nrs=nres
!
     do,i=1,nres
       allocate(ori(1)%res(i)%res_aas(1))
       allocate(ori(1)%res(i)%res_aas(1)%aa_rots(1))
       ori(1)%res(i)%res_naa=1       
       ori(1)%res(i)%res_aas(1)%aa_nrt=1
       ori(1)%res(i)%res_mut=.false.
       ori(1)%res(i)%res_lig=.false.
       ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_prob= 1.0
!
       ori(1)%res(i)%res_aas(1)%aa_nam     = sim(nn)%mol_res(i)%res_name
       ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_flag= .false.
       ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_flag2=.false.
!
       ori(1)%res(i)%res_nold              = sim(nn)%mol_res(i)%res_nold
       ori(1)%res(i)%res_chid              = sim(nn)%mol_res(i)%res_chid 
       ori(1)%res(i)%res_num               = i
!  
       aanm= ori(1)%res(i)%res_aas(1)%aa_nam
!
       strange_flag =.true.
       do j = 1,(size(ps))
          if (ps(j)%p_aan == aanm) then
            strange_flag =.false.
            ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_flag2=.true.
            nats = ps(j)%p_at_ncnt
            allocate(ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(nats))
            ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat=nats
            do k=1, ps(j)%p_at_ncnt
              atnm= ps(j)%p_at_nam(k) 
              ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_nam  = atnm
              ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_flag =.false.
            end do
          end if
       end do
!
!      residues not present in parameter-file are allocated here
!
       if (strange_flag) then
          nat = size(sim(nn)%mol_res(i)%res_ats)
          allocate(ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(nat))
          ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat = nat
          ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_flag2=.false.
       end if
!
    end do
!
!      now filling up the coordinates
!
    do,i=1,nres
       if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_flag2) then 
         do j = 1, size(ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats)
           atnm1 = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam
           do k= 1, size(sim(nn)%mol_res(i)%res_ats)
             atnm2 = sim(nn)%mol_res(i)%res_ats(k)%at_name
             if (sim(nn)%mol_res(i)%res_name=='ILE '.and.atnm2=='CD  ') then
                  atnm2='CD1 '
             end if
!             
             if (atnm1==atnm2) then
!       
                atnm = atnm1
                xx   = sim(nn)%mol_res(i)%res_ats(k)%at_xyz(1)
                xy   = sim(nn)%mol_res(i)%res_ats(k)%at_xyz(2)
                xz   = sim(nn)%mol_res(i)%res_ats(k)%at_xyz(3)
                xocc = sim(nn)%mol_res(i)%res_ats(k)%at_occ
                xbfa = sim(nn)%mol_res(i)%res_ats(k)%at_bfa
!               
                ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam=atnm
                ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1)=xx
                ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2)=xy
                ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3)=xz
                ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ=xocc
                ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa=xbfa
                ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag=.true.
!
              exit
             end if
!             
           end do
         end do
       else
         nat = size(sim(nn)%mol_res(i)%res_ats)
         do j = 1, nat
           atnm = sim(nn)%mol_res(i)%res_ats(j)%at_name
           xx   = sim(nn)%mol_res(i)%res_ats(j)%at_xyz(1)
           xy   = sim(nn)%mol_res(i)%res_ats(j)%at_xyz(2)
           xz   = sim(nn)%mol_res(i)%res_ats(j)%at_xyz(3)
           xocc = sim(nn)%mol_res(i)%res_ats(j)%at_occ
           xbfa = sim(nn)%mol_res(i)%res_ats(j)%at_bfa
!          
           ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam=atnm
           ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1)=xx
           ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2)=xy
           ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3)=xz
           ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ=xocc
           ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa=xbfa
           ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag=.true.
         end do
       end if  
!
    end do
!
! CHECKING IF RESIDUE NUMBERS ARE UNIQUE IN INPUT PDB FILE
!
    do i=1,ori(1)%mol_nrs
       do j= i, ori(1)%mol_nrs
         if (i==j) then
            cycle
         else if (ori(1)%res(i)%res_nold == ori(1)%res(j)%res_nold) then
    
            if (ori(1)%res(i)%res_chid == ori(1)%res(j)%res_chid) then
    
            PRINT*, '    '
            PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
            PRINT*, '>>>>>>>  FOUND MORE THEN 1 RESIDUE WITH                '
            PRINT*, '>>>>>>>  RESIDUE NUMBER : ', ori(1)%res(i)%res_chid,   &
                ori(1)%res(i)%res_nold
            PRINT*, '>>>>>>>  IN REFERNCE COORDINATE FILE                      '
            PRINT*, '>>>>>>>  MUST EXIT                                     '
            PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
            PRINT*, '    '
    
            stop
            end if
         end if
       end do
    end do
!
    ntot = 0
    do i=1, ori(1)%mol_nrs
        ntot = ntot + size(ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats)
    end do    
!
    PRINT *, '  NUMBER OF ATOMS FOUND IN REFERENCE PDB FILE   :', ntot
!
    if (ntot == 0) then
       PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>'
       PRINT*, '>>>>>>>  FOUND NO ATOMS IN INPUT PDB               '
       PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
       PRINT*, '    '
       stop
    endif
!
!!   DEBUG
!    PRINT*,'***',ori(1)%mol_nrs 
!    do, i=1,ori(1)%mol_nrs
!     PRINT*,'****',ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!     do j=1, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!    PRINT*, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam
!    PRINT*, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1)
!    PRINT*, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2)
!    PRINT*, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3)
!    PRINT*, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ 
!    PRINT*, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa 
!     end do
!    end do
!!    
!!    Writing everything out 
!!    
!    open(15,file='z.lis',form='formatted',status='unknown')
!!    
!    nats=0
!    do i=1, ori(1)%mol_nrs 
!       do j=1, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!         nats=nats+1
!         if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag) then 
!          atnm=ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam
!          if (atnm(4:4)==' ') then
!!
!          write(15,fmt=2010)                                            &
!     &  'ATOM  ',                                                       &
!     &      nats,                                                       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_nam,                                &
!     &  ori(1)%res(i)%res_chid,                                         &
!     &  ori(1)%res(i)%res_nold,                                         &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa
!!
!          else
!!
!          write(15,fmt=2011)                                            &
!     &  'ATOM  ',                                                       &
!     &      nats,                                                       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_nam,                                &
!     &  ori(1)%res(i)%res_chid,                                         &
!     &  ori(1)%res(i)%res_nold,                                         &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa
!!
!          end if
!         end if
!        end do
!        end do
!!
!2010    format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
!2011    format(a6,i5,1x,a4,1x,a4,a,i4,4x,3f8.3,2f6.2)
!!
!        close(15)
!!
!!       END DEBUG
!
        return
!
	END SUBROUTINE LOAD_REF_STRUCT  
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE COMP_ANA_STRUCT 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
        USE WATER_DATA
!
	IMPLICIT NONE
!
	INTEGER :: i, j, k, l, m, nat1, nat2, nct
	INTEGER :: nat3, nat4, nats
	LOGICAl :: flag1, flag2, swap_flag1
	REAL :: rms1, rms2, sx, sy, sz, dis1, dis2, dis3
	REAL :: rms3, rms4
	REAL, DIMENSION(3) :: xat, yat
	REAL, DIMENSION(4) :: dis
	CHARACTER (LEN=4) :: atnm1, atnm2, aanm1, anam, aanm
	CHARACTER (LEN=4), DIMENSION(:), ALLOCATABLE :: aan_swap
	REAL, DIMENSION(:,:), ALLOCATABLE :: xyz_swap
!
	CHARACTER (LEN=132) :: string
	CHARACTER (LEN=80) ::  word(40), baff
        INTEGER            ::  nnchar, nw
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY  COMP_ANA_STRUCT     #'
	PRINT*, '################################'
	PRINT*, '        '
!
!	Checking if rotamers are unique in molecular structure
!
	flag1=.true.
	flag2=.true.
!
	if (.not.allocated(mol)) then
	  flag1=.false.
	end if 
!
	if (flag1) then 
	 do i=2,size(mol)
	    if (mol(i)%res(1)%res_naa==1) then
	       if (mol(i)%res(1)%res_aas(1)%aa_nrt/=1) then
                 flag2=.false.
	       end if
	    else
	       flag2=.false.
	    end if
	 end do
	end if
!
	if (.not.flag1) then
	  PRINT*, '        '
	  PRINT*, '  NO MOLECULAR STRUCTURE PRESENT                         '
	  PRINT*, '  CAN ONLY FOCUS ON REFERENCE STRUCTURE RIGHT NOW        '
	  PRINT*, '        '
	end if
!
	if (.not.flag2) then
	  PRINT*, '        '
	  PRINT*, '  MORE THEN ONE ROTAMER PRESENT AT CERTAIN POSITIONS     '
      PRINT*, '  CAN ONLY FOCUS ON REFERENCE STRUCTURE RIGHT NOW        '
	  PRINT*, '        '
	end if
!
!       mapping back the aa_nam of the solvated rotamers to unsolvated rotamers
!       and subsequently mapping back the aa_nam of the expanded histidines to HIS  
!
!       first read in the substitution tables in order to change for example HIS HID HIE back to HIS
!       in the final output-file (At present only the HIS HID HIE change back to HIS is implemented further 
!       below and makes sense (mumbosub(1) is checked). 
!       Further back-substitutions can be easily added if for example different protonations of ASP and GLU
!       are to be implemented)
!
        if (allocated(mumbosub)) deallocate(mumbosub)
        allocate(mumbosub(size(subs_instr)))
        do i=1,size(mumbosub)
            do j=1,132
               string(j:j)=' '
            end do
            nnchar=len(subs_instr(i))
            string(1:nnchar)=subs_instr(i)
	    call parser(string, word, nw)
	    call maxwords(word,nw)
!	        
	    baff=word(1)
	    mumbosub(i)%mut_key=baff(1:3)
	    allocate(mumbosub(i)%mut_rep(nw-2))
            do j=1,nw-2
                    baff=word(j+2)
                    mumbosub(i)%mut_rep(j)=baff(1:3)
              end do
        end do
!
!       now checking all molecules...
!
        PRINT*, '   '
!
	 do i=2,size(mol)
            do j=1,mol(i)%res(1)%res_naa
                anam=mol(i)%res(1)%res_aas(j)%aa_nam
!
!        mapping back the solvated rotamer names.. 
!
                do k=1,size(ASWNAMES)
                   if (anam.eq.ASWNAMES(k)) then
                        anam = ASNAMES(k)
                        exit
                   end if 
                end do
!
!        now mapping back the histidines..
!
                do k=1,1
                   do l=1,size(mumbosub(k)%mut_rep)
                     if (anam(1:3).eq.mumbosub(k)%mut_rep(l)) then
                         anam='    '
                         anam(1:3) = mumbosub(k)%mut_key
                     end if 
                   end do
                end do 
!
                if (mol(i)%res(1)%res_aas(j)%aa_nam/=anam) then 
	            PRINT*, '  BACK-MAPPING OF RESIDUE:   ',                    &
     &              mol(i)%res(1)%res_chid, mol(i)%res(1)%res_nold,             &
     &              mol(i)%res(1)%res_aas(j)%aa_nam, ' ==> ', anam
!
                    mol(i)%res(1)%res_aas(j)%aa_nam=anam
!
                end if 
            end do
          end do
!
        PRINT*, '   '
!
!   now pwforming the rms analysis
!   in a first step, swapping atoms is investigated 
!
	if (flag1.and.flag2) then
!
	 do i=1,ori(1)%mol_nrs
	   do j=2,size(mol)   
	      if ((ori(1)%res(i)%res_chid)==(mol(j)%res(1)%res_chid)) then
	      if ((ori(1)%res(i)%res_nold)==(mol(j)%res(1)%res_nold)) then
	      if ((ori(1)%res(i)%res_aas(1)%aa_nam)==(mol(j)%res(1)%res_aas(1)%aa_nam)) then
	        aanm1 = ori(1)%res(i)%res_aas(1)%aa_nam
		swap_flag1=.false.
	        if (allocated(aan_swap)) deallocate(aan_swap)
	        if (allocated(xyz_swap)) deallocate(xyz_swap)
		if (aanm1=='PHE '.or.aanm1=='TYR ') then	         
		  allocate (aan_swap(4))
		  allocate (xyz_swap(8,3))
		  aan_swap(1)='CD1'
		  aan_swap(2)='CD2'
		  aan_swap(3)='CE1'
		  aan_swap(4)='CE2'
		  swap_flag1=.true.
		else if (aanm1=='GLU ') then
		  allocate (aan_swap(2))
		  allocate (xyz_swap(4,3))
		  aan_swap(1)='OE1'
		  aan_swap(2)='OE2'
		  swap_flag1=.true.
		else if (aanm1=='ASP ') then
		  allocate (aan_swap(2))
		  allocate (xyz_swap(4,3))
		  aan_swap(1)='OD1'
		  aan_swap(2)='OD2'
		  swap_flag1=.true.
		end if
!
	        if (mumbo_swap_qnh_flag) then
		  if (aanm1=='GLN ') then
		    allocate (aan_swap(2))
		    allocate (xyz_swap(4,3))
		    aan_swap(1)='OE1'
		    aan_swap(2)='NE2'
		    swap_flag1=.true.
		  else if (aanm1=='ASN ') then
		    allocate (aan_swap(2))
		    allocate (xyz_swap(4,3))
		    aan_swap(1)='OD1'
		    aan_swap(2)='ND2'
		    swap_flag1=.true.
		  else if (aanm1=='HIS ') then
		    allocate (aan_swap(4))
		    allocate (xyz_swap(8,3))
		    aan_swap(1)='ND1'
		    aan_swap(2)='CD2'
		    aan_swap(3)='CE1'
		    aan_swap(4)='NE2'
		    swap_flag1=.true.
	          end if
		end if
!
	        if (swap_flag1) then
	          nct = size(aan_swap)
		  do k=1, nct
		    do m=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat 
		     if (aan_swap(k) == ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam) then
			 xyz_swap(k,1) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1) 
			 xyz_swap(k,2) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2) 
			 xyz_swap(k,3) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3) 
		     end if
		    end do
		    do m=1,mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_nat
		     if (aan_swap(k) == mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam) then
			 xyz_swap(nct+k,1) = mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1) 
			 xyz_swap(nct+k,2) = mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2) 
			 xyz_swap(nct+k,3) = mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3) 
		     end if
		    end do
		  end do
!
		  dis1 = 0
		  dis2 = 0
		  do k=1, nct  
		     dis3 = 0
		     dis3 = (xyz_swap(k,1) - xyz_swap(nct+k,1))**2
		     dis3 = dis3 + (xyz_swap(k,2) - xyz_swap(nct+k,2))**2
		     dis3 = dis3 + (xyz_swap(k,3) - xyz_swap(nct+k,3))**2
		     dis3 = sqrt(dis3)
		     dis1 = dis1 + dis3
		     dis3 = 0
		     if (mod(k,2)==1) then
		      dis3 = (xyz_swap(k,1) - xyz_swap(nct+k+1,1))**2
		      dis3 = dis3 + (xyz_swap(k,2) - xyz_swap(nct+k+1,2))**2
		      dis3 = dis3 + (xyz_swap(k,3) - xyz_swap(nct+k+1,3))**2
		      dis3 = sqrt(dis3)
		     else 
		      dis3 = (xyz_swap(k,1) - xyz_swap(nct+k-1,1))**2
		      dis3 = dis3 + (xyz_swap(k,2) - xyz_swap(nct+k-1,2))**2
		      dis3 = dis3 + (xyz_swap(k,3) - xyz_swap(nct+k-1,3))**2
		      dis3 = sqrt(dis3)
		     end if
		     dis2 = dis2 + dis3
		  end do
		  if (dis2 < dis1 ) then
	    PRINT*, '  SIDE CHAIN SWAPPING FOR RESIDUE:   ',      &
     &                          ori(1)%res(i)%res_aas(1)%aa_nam,            &
     &                          ori(1)%res(i)%res_chid, ori(1)%res(i)%res_nold
	    PRINT*, '  SUM_DIS1== ', dis1, 'SUM_DIS2== ', dis2
		    sx = 9999
		    sy = 9999
		    sz = 9999
		    do k = 1, nct  
		     if (mod(k,2)==1) then
		      sx = xyz_swap(k,1)
		      sy = xyz_swap(k,2)
		      sz = xyz_swap(k,3)
		      xyz_swap(k,1) = xyz_swap(k+1,1)
		      xyz_swap(k,2) = xyz_swap(k+1,2)
		      xyz_swap(k,3) = xyz_swap(k+1,3)
		     else 
		      xyz_swap(k,1) = sx
		      xyz_swap(k,2) = sy
		      xyz_swap(k,3) = sz
		     end if
                    end do
!
		    do k=1, nct
		      do m=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat 
		       if (aan_swap(k) == ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam) then
			 ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1) = xyz_swap(k,1) 
			 ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2) = xyz_swap(k,2)
			 ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3) = xyz_swap(k,3)
		       end if
		      end do
		    end do
		  end if
!
		end if  
	      end if
	      end if
	      end if
	  end do
	 end do
!
	PRINT*, '  '
!
!	Now calculating the actual rms deviations ....
!
    rms1=0         ! overall rmsd value
    nat1=0
!
    rms3=0         ! rmsd value of the protein part only
    nat3=0
!    
    rms4=0         ! rmsd value of the nucleic acid part only
    nat4=0
!
    do i=1, ori(1)%mol_nrs
       do j=1,3
          xat(j)=0.0
          yat(j)=0.0
       end do
       do j=1,4
          dis(j)=0.0
       end do
       rms2=0
       nat2=0
       do j=2,size(mol)   
          if ((ori(1)%res(i)%res_chid)==(mol(j)%res(1)%res_chid)) then
            if ((ori(1)%res(i)%res_nold)==(mol(j)%res(1)%res_nold)) then
              if ((ori(1)%res(i)%res_aas(1)%aa_nam)/=(mol(j)%res(1)%res_aas(1)%aa_nam)) then
!       
           PRINT*, 'MISMATCH AMINO ACID AT ',ori(1)%res(i)%res_aas(1)%aa_nam,    &
     &                      ori(1)%res(i)%res_chid, ori(1)%res(i)%res_nold
           PRINT*, '  AA IN MOLEC. STRUCT CALLED ', mol(j)%res(1)%res_aas(1)%aa_nam
                            cycle
              end if
!
              do k=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat 
                 ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_bfa=99.99
!
                 if (.not.mumbo_ref_beta_flag) then
                    if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_nam==cb_name) cycle
                 end if
!
                 if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_flag) then
!                 
              atnm1= ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_nam
              xat(1) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(1) 
              xat(2) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(2) 
              xat(3) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(3)
!
                    do l=1,mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_nat 
                       atnm2= mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(l)%at_nam
                       if (atnm1==atnm2) then
!
              yat(1) = mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(l)%at_xyz(1) 
              yat(2) = mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(l)%at_xyz(2) 
              yat(3) = mol(j)%res(1)%res_aas(1)%aa_rots(1)%rot_ats(l)%at_xyz(3)
!
              dis(1) = (xat(1) - yat(1))**2
              dis(2) = (xat(2) - yat(2))**2
              dis(3) = (xat(3) - yat(3))**2
              dis(4) = sqrt(dis(1) + dis(2) + dis(3))
!
              rms2=rms2 + (dis(4)**2)
              rms1=rms1 + (dis(4)**2)
              nat2=nat2 + 1
              nat1=nat1 + 1
!
              if      (mol(j)%res(1)%res_ntype.eq.2) then 
                rms3 = rms3 + (dis(4)**2)
                nat3 = nat3 +1
              else if (mol(j)%res(1)%res_ntype.eq.3) then
                rms4 = rms4 + (dis(4)**2)
                nat4 = nat4 +1
              end if              
!
              ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_bfa = dis(4)
!
                       end if
!
                    end do 

                 end if
!
              end do
              exit        ! hier neu eingefügt...(09.02.2023)
!
            end if
          end if
       end do 
!
       if (nat2 > 0) then
!
          rms2= rms2/nat2
          rms2= sqrt(rms2)
!
          write(*,'(A,2x,A4,2x,A1,2x,I5,A,F8.3,2x,A,I4,A)')                         &
     &    '   RMS DEVIATION SIDE CHAIN OF ',ori(1)%res(i)%res_aas(1)%aa_nam,    &
     &     ori(1)%res(i)%res_chid, ori(1)%res(i)%res_nold, ' == ', rms2,'( ', nat2,' )'
!
       else 
          write(*,'(A,2x,A4,2x,A1,2x,I5,A,A)')                                  &
     &    '   RMS DEVIATION SIDE CHAIN OF ',ori(1)%res(i)%res_aas(1)%aa_nam,    &
     &    ori(1)%res(i)%res_chid, ori(1)%res(i)%res_nold, ' == ','      --'
       end if
!
!      DONE WITH ANALYSING SINGLE SIDE-CHAINS / ROTAMERS
!
    end do
!
!      DONE WITH ANALYSING ALL SIDE-CHAINS / ROTAMERS
!
    PRINT*, '        '
    if (nat1 > 0) then
       rms1= rms1/nat1
       rms1= sqrt(rms1)
       WRITE(*,'(A,F7.3,2x,A,i6,A)')'NUM13: RMS DEVIATION ALL (SIDE CHAINS+NUCS)         ', rms1,'( ', nat1,' )'  
    else 
       WRITE(*,'(A,F7.3)')'NUM13: RMS DEVIATION ALL (SIDE CHAINS+NUCS)             == ' 
    end if
!    
    if (nat3 > 0) then
       rms3= rms3/nat3
       rms3= sqrt(rms3)
       WRITE(*,'(A,F7.3,2x,A,i6,A)')'NUM14: RMS DEVIATION ALL PROTEIN SIDE CHAINS        ', rms3,'( ', nat3,' )'  
    else 
       WRITE(*,'(A,F7.3)')'NUM14: RMS DEVIATION ALL PROTEIN SIDE CHAINS            == ' 
    end if
!    
    if (nat4 > 0) then
       rms4= rms4/nat4
       rms4= sqrt(rms4)
       WRITE(*,'(A,F7.3,2x,A,i6,A)')'NUM15: RMS DEVIATION ALL NUCLEIC ACID NUCLEOBASES   ', rms4,'( ', nat4,' )' 
    else 
       WRITE(*,'(A,F7.3)')'NUM15: RMS DEVIATION ALL NUCLEIC ACID NUCLEOBASES       == ' 
    end if
    PRINT*, '                                                            ( # atoms) '
    PRINT*, '        '
!
!
    end if      ! (flag1.and.flag2) enquiry above
!
!	Writing out everyrthing
!
!	open(15,file='y.lis',form='formatted',status='unknown')
!
!	nats=0
!	do i=1, ori(1)%mol_nrs 
!        do j=1, ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!          nats=nats+1
!	 if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag) then 
!          aanm=ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam
!          if (aanm(4:4)==' ') then
!
!          write(15,fmt=2010)                                            &
!     &  'ATOM  ',                                                       &
!     &      nats,                                                       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_nam,                                &
!     &  ori(1)%res(i)%res_chid,                                         &
!     &  ori(1)%res(i)%res_nold,                                         &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa
!
!          else
!
!          write(15,fmt=2011)                                            &
!     &  'ATOM  ',                                                       &
!     &      nats,                                                       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_nam,                                &
!     &  ori(1)%res(i)%res_chid,                                         &
!     &  ori(1)%res(i)%res_nold,                                         &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3),       &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ,          &
!     &  ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa
!
!          end if
!	 end if
!        end do
!	end do
!
!2010    format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
!2011    format(a6,i5,1x,a4,1x,a4,a,i4,4x,3f8.3,2f6.2)
!
!	close(15)
!
	END SUBROUTINE COMP_ANA_STRUCT   
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE PICKNAT_STRUCT 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: i, j, k, l, m, n, nat, nct, nlist, ndt, q, naver
	REAL :: rms, rmsmin, rmsaver
	REAL, DIMENSION(3) :: xat, yat
	REAL, DIMENSION(4) :: dis
	REAL, DIMENSION(:), ALLOCATABLE :: rotrms
	CHARACTER (LEN=4) :: atnm1, atnm2, atnm3, aanm1, aanm2
	CHARACTER (LEN=4), DIMENSION(:), ALLOCATABLE :: atomlist, aan_swap
	LOGICAL :: matchflag, swap_flag
!		
	CHARACTER (LEN=4), DIMENSION(4)  :: list_ats
	DATA   list_ats/"C1' ","C2' ","O2' ","O4' "/
!		
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY  PICKNAT_STRUCT      #'
	PRINT*, '################################'
	PRINT*, '        '
	PRINT*, '  PICKING THE BEST MATCH.....'
	PRINT*, '        '
	PRINT*, '   (Please note that sometimes more than one rotamer is retained. It is quite'
	PRINT*, '    possible that the rotamers only differ by the positions of the hydrogens.' 
	PRINT*, '    These might well be absent in the reference structure)'
	PRINT*, '        '
!
!       SET ALL ROTAMER FLAGS TO FALSE  
!
	do j=2,size(mol)  
	     do k= 1, mol(j)%res(1)%res_naa
	        do l=1,mol(j)%res(1)%res_aas(k)%aa_nrt
		  mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_flag=.false.
	        end do
	     end do
	end do
!
!    Unterer Code ermöglicht es nur bstimmte Atome der Nukleotide 
!    (z.B. entweder Zuckeratome oder Nukleobasenatome) für die Berechnng der
!    RMSD Werte zu verwenden. Code nützlich zur Evaluierung der Ausweitung von MUMBO 
!    auf Nucleotide
!
!    >>>>>>>>> Beginn Code
!
!
!    Wie sind die ori atome flag am Eingang der Subroutine gesetzt? 
!! 
!         do i=1,ori(1)%mol_nrs
!            print*, 'DEBUG_1 ', ori(1)%res(i)%res_chid, ori(1)%res(i)%res_nold, &
!      &                        ori(1)%res(i)%res_aas(1)%aa_nam
!              do j=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!                print*, 'DEBUG_1 ', ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam, '  ', &
!      &         ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag            
!              end do
!         end do
!!!!
!!!
!!       1) considering suggar atoms only
!!
!!        do i=1,ori(1)%mol_nrs
!!            do j=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!!                ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag = .false.
!!                do k=1, size(list_ats)
!!                
!!          if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam==list_ats(k)) then
!!            ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag = .true.                     
!!          end if
!!                end do
!!            end do
!!        end do
!!
!!       or 2) considering everything but suggar atoms 
!!
!        do i=1,ori(1)%mol_nrs
!            do j=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!                do k=1, size(list_ats)
!                
!          if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam==list_ats(k)) then
!            ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag = .false.                     
!          end if
!                end do
!            end do
!        end do
!!
!!       hier reporting the at_flag settings
!!
!        do i=1,ori(1)%mol_nrs
!            print*, 'DEBUG_2  ', ori(1)%res(i)%res_chid, ori(1)%res(i)%res_nold, &
!     &                         ori(1)%res(i)%res_aas(1)%aa_nam
!            do j=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
!               print*, 'DEBUG_2  ', ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam, '  ', &
!     &         ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag
!            end do
!        end do
!!
!!
!!      <<<<<<<<<<<< Ende Code
!
!
	rmsaver=0
	naver=0
	do i=1,ori(1)%mol_nrs
!
!	Taking care of possible atom swapping problems such as OD1 and OD2 in Asp etc.. 
!	Everything is duplicated even non-swapped residues. 
!	This should only slightly increase cpu time
!	  
	   nlist=ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat*2
	   if (allocated(atomlist)) deallocate(atomlist)
	   allocate(atomlist(nlist))
!	   
	   ndt=0
	   do j=1,2
	   do k=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat
	   ndt=ndt+1
	     atomlist(ndt)=ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_nam	   
	   end do
	   end do
!	   
	   aanm1=ori(1)%res(i)%res_aas(1)%aa_nam
	   swap_flag=.false.
!	   
	   if (allocated(aan_swap)) deallocate(aan_swap)
           allocate (aan_swap(1))
	   if (aanm1=='PHE '.or.aanm1=='TYR ') then
	          if (allocated(aan_swap)) deallocate(aan_swap)
		  allocate (aan_swap(4))
		  aan_swap(1)='CD1'
		  aan_swap(2)='CD2'
		  aan_swap(3)='CE1'
		  aan_swap(4)='CE2'
		  swap_flag=.true.
	   else if (aanm1=='GLU ') then
	          if (allocated(aan_swap)) deallocate(aan_swap)
		  allocate (aan_swap(2))
		  aan_swap(1)='OE1'
		  aan_swap(2)='OE2'
		  swap_flag=.true.
	   else if (aanm1=='ASP ') then
	          if (allocated(aan_swap)) deallocate(aan_swap)
		  allocate (aan_swap(2))
		  aan_swap(1)='OD1'
		  aan_swap(2)='OD2'
		  swap_flag=.true.
	   end if
!	   
	   if (mumbo_swap_qnh_flag) then
		if (aanm1=='GLN ') then
	          if (allocated(aan_swap)) deallocate(aan_swap)
		  allocate (aan_swap(2))
		  aan_swap(1)='OE1'
		  aan_swap(2)='NE2'
		  swap_flag=.true.
		else if (aanm1=='ASN ') then
	          if (allocated(aan_swap)) deallocate(aan_swap)
		  allocate (aan_swap(2))
		  aan_swap(1)='OD1'
		  aan_swap(2)='ND2'
		  swap_flag=.true.
		else if (aanm1=='HIS ') then
	          if (allocated(aan_swap)) deallocate(aan_swap)
		  allocate (aan_swap(4))
		  aan_swap(1)='ND1'
		  aan_swap(2)='CD2'
		  aan_swap(3)='CE1'
		  aan_swap(4)='NE2'
		  swap_flag=.true.
	        end if
	   end if
!
	   if (swap_flag) then
!
	   do j=((nlist/2)+1),nlist
	      atnm3=atomlist(j)
!
	      do k=1,size(aan_swap)
		if (atnm3==aan_swap(k)) then
!		
		    if (mod(k,2)==0) then 
		       atomlist(j)=aan_swap(k-1)
!		PRINT*, 'SWAPPING   ',aan_swap(k),aan_swap(k-1),' in ', aanm1  
		    else if (mod(k,2)==1) then 
		       atomlist(j)=aan_swap(k+1)
!		PRINT*, 'SWAPPING   ',aan_swap(k),aan_swap(k+1),' in ', aanm1  
		    end if
!		
		end if
	      end do
!
	   end do
!
	   end if
!	
	   do j=2,size(mol)   
	      if ((ori(1)%res(i)%res_chid)==(mol(j)%res(1)%res_chid)) then
	      if ((ori(1)%res(i)%res_nold)==(mol(j)%res(1)%res_nold)) then
!	      
	     matchflag=.false. 
!
	     do k= 1, mol(j)%res(1)%res_naa
!
	      aanm2 = mol(j)%res(1)%res_aas(k)%aa_nam
!
	      if (aanm1==aanm2) then
	      
	        matchflag=.true.
	        
	        if (aanm1==gly_name) then 
	           mol(j)%res(1)%res_aas(k)%aa_rots(1)%rot_flag=.true.
	 write(*,'(A,2x,A4,2x,A1,2x,I5,A)')                                         &
     &         '  BEST MATCH, SIDE CHAIN, RMS DEVIATION:', aanm2,                   &
     &          mol(j)%res(1)%res_chid, mol(j)%res(1)%res_nold,' ==    0.000  '
		  cycle
		end if
!
	        if (allocated(rotrms)) deallocate(rotrms)
		allocate (rotrms(mol(j)%res(1)%res_aas(k)%aa_nrt*2))
!		
! Now calculate rmsd for each rotamer
!
		do q=1,2
		do l=1,mol(j)%res(1)%res_aas(k)%aa_nrt
		rms = 0
		nat = 0
		do m=1,ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat 
	         if (ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_flag) then
		  atnm1= atomlist(((q-1)*(nlist/2))+m)
!
!		  atnm1= ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam
!
		  xat(1) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1) 
	          xat(2) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2) 
	          xat(3) = ori(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3)
!
                  do n=1,mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_nat 
	            atnm2= mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(n)%at_nam
		    
		    if (atnm1==atnm2) then
!
	yat(1) = mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(n)%at_xyz(1) 
	yat(2) = mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(n)%at_xyz(2) 
	yat(3) = mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(n)%at_xyz(3)
!
!DEBUG
!	PRINT*, xat
!	PRINT*, yat
!	PRINT*, aanm1, atnm1, atnm2
!		    
	dis(1) = (xat(1) - yat(1))**2
	dis(2) = (xat(2) - yat(2))**2
	dis(3) = (xat(3) - yat(3))**2
	dis(4) = sqrt(dis(1) + dis(2) + dis(3))
!
	rms=rms + (dis(4)**2)
	nat=nat + 1
!	
		    end if
		    
		  end do
		 end if
		end do 
		if (nat==0.and.aanm1/=gly_name) then 
	PRINT*,'  '
	PRINT*,'>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>                     '
	PRINT*,'>>>                                                              '
	PRINT*,'>>> NO MATCHING ATOMS FOUND ALTHOUGH RESIDUE NAMES ARE IDENTICAL '
	PRINT*,'>>> IN MUMBO STRUCTURE AND REFERENCE STRUCTURE                   '
	PRINT*,'>>>                MUST STOP    (SUBROUTINE PICK_NAT_STRCT)      '                            
	PRINT*,'>>>                                                              '
	PRINT*,'>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>                     '
	PRINT*,'  '
			stop
		else if (nat==0.and.aanm1==gly_name) then 
		rotrms(((q-1)*mol(j)%res(1)%res_aas(k)%aa_nrt)+l)=0
		else if (nat/=0) then 
		rotrms(((q-1)*mol(j)%res(1)%res_aas(k)%aa_nrt)+l)=sqrt(rms/nat)
		end if
	       end do
	       end do
!
! Now identify the rotamer with the lowest rmsd
!
		rmsmin = 999999
		do l=1,(mol(j)%res(1)%res_aas(k)%aa_nrt*2)
		  if (rotrms(l).lt.rmsmin) then  
		    rmsmin = rotrms(l) 
		  end if
		end do 
!
! Now flag the rotamers for writting out 
!
		rmsmin = rmsmin + picknat_rms_tolerance
		nct=0
		do q=1,2
		do l=1,mol(j)%res(1)%res_aas(k)%aa_nrt
		  nct=((q-1)*mol(j)%res(1)%res_aas(k)%aa_nrt)+l
		  if (rotrms(nct).le.rmsmin) then
!		  
		   mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_flag=.true.
		   rmsaver=rmsaver + rotrms(nct)
		   naver=naver+1
!
		    if (q==1) then 
	 write(*,'(A,2x,A4,2x,A1,2x,I5,A,F8.3)')                                    &
     &         '  BEST MATCH, SIDE CHAIN, RMS DEVIATION:', aanm2,                   &
     &          mol(j)%res(1)%res_chid, mol(j)%res(1)%res_nold,' == ', rotrms(nct)
		    else if (q==2.and.swap_flag) then
	 write(*,'(A,2x,A4,2x,A1,2x,I5,A,F8.3,A)')                                  &
     &         '  BEST MATCH, SIDE CHAIN, RMS DEVIATION:', aanm2,                   &
     &          mol(j)%res(1)%res_chid, mol(j)%res(1)%res_nold,' == ', rotrms(nct), &
     &         ' AFTER ATOM SWAPPING '
		    
		    end if
		  end if
		end do
		end do
!
	      end if
	      end do
!
	      if (matchflag.eqv..false.) then 
	PRINT*,'  '
	PRINT*,'>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>                     '
	PRINT*,'>>>                                                              '
	PRINT*,'>>> NO MATCHING RESIDUES / LIGANDS FOUND FOR POSITION            '
	PRINT*,'>>> ', mol(j)%res(1)%res_chid, mol(j)%res(1)%res_nold 
	PRINT*,'>>> PLEASE CHECK - REFERENCE STRUCTURE- OR -MUMBO.INP-           '
	PRINT*,'>>>                                                              '
	PRINT*,'>>>          MUST STOP                                           '                            
	PRINT*,'>>>                                                              '
	PRINT*,'>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>                     '
	PRINT*,'  '
	         stop
	      end if
!	      
	      end if
	      end if
	   end do 
	   end do
	PRINT*, '        '
	WRITE(*,'(A,2x,F8.3,x,A)') '   AVERAGE SIDE CHAIN RMS =',rmsaver/naver,'Angs.'
	PRINT*, '        '
	PRINT*, '        '
	PRINT*, '      .... DONE PICKING BEST MATCHES'
	PRINT*, '        '
!
	END SUBROUTINE PICKNAT_STRUCT   
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!


