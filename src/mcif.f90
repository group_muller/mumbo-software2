!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE SIMP_MOLECULE_REMOVE_HYD(nn)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MOLECULE_SIMP_DATA_M
!
    IMPLICIT NONE
!
    INTEGER           :: i, j, nn 
    CHARACTER(LEN=4)  :: atnm, atty
!    
    if (nn>size(sim)) then
      write(*,*)
      write(*,*)' Dimension of sim out-of-range in  '
      write(*,*)' SUBROUTINE SIMP_MOLECULE_REMOVE_ALT_CONFS '
      write(*,*)
      stop
    end if
!
    do i=1, size(sim(nn)%mol_res)
       do j=1, size(sim(nn)%mol_res(i)%res_ats)
           atnm = sim(nn)%mol_res(i)%res_ats(j)%at_name
!           atty = sim(nn)%mol_res(i)%res_ats(j)%at_typ
!           if (atty.eq.'H') then 
!              sim(nn)%mol_res(i)%res_ats(j)%at_flag=.false.
!              cycle
!           end if   
           if (atnm(1:1).eq.'H') then 
              sim(nn)%mol_res(i)%res_ats(j)%at_flag=.false.
              cycle
           end if
       end do
    end do        
!
    return
!    
    END SUBROUTINE SIMP_MOLECULE_REMOVE_HYD
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE SIMP_MOLECULE_RENAME_LEU(nn)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MOLECULE_SIMP_DATA_M
!
    IMPLICIT NONE
!
    INTEGER           :: i, j, nn 
    CHARACTER(LEN=4)  :: atnm
!    
    if (nn>size(sim)) then
      write(*,*)
      write(*,*)' Dimension of sim out-of-range in  '
      write(*,*)' SUBROUTINE SIMP_MOLECULE_REMOVE_ALT_CONFS '
      write(*,*)
      stop
    end if
!
    do i=1, size(sim(nn)%mol_res)
       do j=1, size(sim(nn)%mol_res(i)%res_ats)
           atnm = sim(nn)%mol_res(i)%res_ats(j)%at_name 
           if (sim(nn)%mol_res(i)%res_name=='ILE '.and.atnm=='CD  ') then
             atnm='CD1 '
             sim(nn)%mol_res(i)%res_ats(j)%at_name = atnm
           end if
       end do
    end do        
!
    return
!    
    END SUBROUTINE SIMP_MOLECULE_RENAME_LEU
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE SIMP_MOLECULE_RESET_BFAC(nn)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MOLECULE_SIMP_DATA_M
!
    IMPLICIT NONE
!
    INTEGER       :: i, j, nn 
    REAL          :: xbfa, xbfa_reset = 70
!    
    if (nn>size(sim)) then
      write(*,*)
      write(*,*)' Dimension of sim out-of-range in  '
      write(*,*)' SUBROUTINE SIMP_MOLECULE_REMOVE_ALT_CONFS '
      write(*,*)
      stop
    end if
!
    do i=1, size(sim(nn)%mol_res)
       do j=1, size(sim(nn)%mol_res(i)%res_ats)
!          xbfa = sim(nn)%mol_res(i)%res_ats(j)%at_bfa 
!          if (xbfa.gt.xbfa_reset) then 
!              sim(nn)%mol_res(i)%res_ats(j)%at_bfa = xbfa_reset  
!          end if 
          sim(nn)%mol_res(i)%res_ats(j)%at_bfa = xbfa_reset
       end do
    end do        
!
    return
!    
    END SUBROUTINE SIMP_MOLECULE_RESET_BFAC
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE RELOAD_COORD_FROM_SIM(mm,nn,nr)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MOLECULE_SIMP_DATA_M
    USE MOLEC_M
!
    IMPLICIT NONE
!
    INTEGER    :: mm, nn, nr
    INTEGER    :: i,j
    LOGICAl    :: flag
    CHARACTER(LEN=4) :: atnm1, atnm2, atnm
    REAL             :: xx, xy, xz, xocc, xbfa
!    
    flag = .false.
    if (mol(mm)%res(nr)%res_chid.eq.sim(nn)%mol_res(nr)%res_chid) then 
        if (mol(mm)%res(nr)%res_nold.eq.sim(nn)%mol_res(nr)%res_nold) then 
          flag=.true.
        end if
    end if
!    
    if (.not.flag) then
         PRINT*, '    '
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>   '
         PRINT*, '>>>>>>>  COULD NOT RETRIEVE COORDINATES IN              '
         PRINT*, '>>>>>>>  SUBROUTINE RELOAD_MOL_RESIDUE_COORD_FROM _SIM  '
         PRINT*, '>>>>>>>  '
         PRINT*, '>>>>>>>  MUST STOP '        
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
         PRINT*, '    '
         STOP
    end if 
!
    do i=1, size(mol(mm)%res(nr)%res_aas(1)%aa_rots(1)%rot_ats)
       mol(mm)%res(nr)%res_aas(1)%aa_rots(1)%rot_ats(i)%at_flag = .false.
       atnm1 = mol(mm)%res(nr)%res_aas(1)%aa_rots(1)%rot_ats(i)%at_nam
!       
       do j=1,size(sim(nn)%mol_res(nr)%res_ats)
         atnm2 = sim(nn)%mol_res(nr)%res_ats(j)%at_name
!        
         if (atnm1.eq.atnm2) then
          if (sim(nn)%mol_res(nr)%res_ats(j)%at_flag) then 
!       
             atnm = sim(nn)%mol_res(nr)%res_ats(j)%at_name
             xx   = sim(nn)%mol_res(nr)%res_ats(j)%at_xyz(1)
             xy   = sim(nn)%mol_res(nr)%res_ats(j)%at_xyz(2)
             xz   = sim(nn)%mol_res(nr)%res_ats(j)%at_xyz(3)
             xocc = sim(nn)%mol_res(nr)%res_ats(j)%at_occ
             xbfa = sim(nn)%mol_res(nr)%res_ats(j)%at_bfa
!       
             mol(mm)%res(nr)%res_aas(1)%aa_rots(1)%rot_ats(i)%at_nam=atnm
             mol(mm)%res(nr)%res_aas(1)%aa_rots(1)%rot_ats(i)%at_xyz(1)=xx
             mol(mm)%res(nr)%res_aas(1)%aa_rots(1)%rot_ats(i)%at_xyz(2)=xy
             mol(mm)%res(nr)%res_aas(1)%aa_rots(1)%rot_ats(i)%at_xyz(3)=xz
             mol(mm)%res(nr)%res_aas(1)%aa_rots(1)%rot_ats(i)%at_occ=xocc
             mol(mm)%res(nr)%res_aas(1)%aa_rots(1)%rot_ats(i)%at_bfa=xbfa
             mol(mm)%res(nr)%res_aas(1)%aa_rots(1)%rot_ats(i)%at_flag=.true.
!
           end if
         end if
!           
       end do
     end do    
!
   return
!                
    END SUBROUTINE RELOAD_COORD_FROM_SIM
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE WRITE_0ATOM_STRUCT(filename)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!   subroutine to write out mol(nn) into sim(nn) and then into either cif or pdb file 
!
    USE MOLECULE_SIMP_DATA_M
    USE MOLEC_M
!
    IMPLICIT NONE
!
    INTEGER    :: nn 
    CHARACTER(LEN=1000) filename
    CHARACTER(LEN=132)  fileout
!
    nn = 1
!
    fileout(1:132) = filename(1:132)
!
    if (allocated(sim)) deallocate(sim)
    allocate(sim(nn))
    call convert_mol_to_simp(nn)
!!
!    cif_flag = .false.
!    pdb_flag = .false.
!!    ext = mumbo_outpdb((len_trim(mumbo_outpdb)-3):len_trim(mumbo_outpdb))
!    if ((ext.eq.'.pdb').or.(ext.eq.'.PDB')) then  
!!       read in pdb file
!!        print*,'  '
!        print*,'  OUTPDB/OUTFILE IS ASSUMED TO BE A ** PDB ** FILE '
!        print*,'  ', mumbo_outpdb(1:len_trim(mumbo_outpdb))
!        print*,'  '
!        cif_flag = .false.
!        pdb_flag = .true.
!    else if ((ext.eq.'.cif').or.(ext.eq.'.CIF')) then
!!        print*,'  '
!        print*,'  OUTPDB/OUTFILE IS ASSUMED TO BE A ** CIF ** FILE '
!        print*,'  ',mumbo_outpdb(1:len_trim(mumbo_outpdb))
!        print*,'  '
!        cif_flag = .true.
!        pdb_flag = .false.
!    else
!       print*, '  NOT CLEAR WHETHER OUTPDB/OUTFILE IS MEANT TO BE PDB OR CIF-FILE '
!       print*, '  '
!       print*,  mumbo_outpdb(1:len_trim(mumbo_outpdb)) 
!       stop
!    end if   
!!   
!    if (cif_flag) then 
!         call cif_write_coords(fileout,nn)
!    end if
!
!    if (pdb_flag) then 
         call write_simp_pdb(fileout,nn)
!    end if
!
    if (allocated(sim)) deallocate(sim)
!
     return
!
      END SUBROUTINE WRITE_0ATOM_STRUCT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE CONVERT_MOL_TO_SIMP(nn)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MOLECULE_SIMP_DATA_M
    USE MOLEC_M
!
    IMPLICIT NONE
!
    INTEGER    :: nn, nr, nct, nats
    INTEGER    :: i, j 
    CHARACTER(LEN=4) ::atnm
    REAL       :: xx, xy, xz, xbfa, xocc
    LOGICAl    :: xflag
!
    if (nn>size(sim)) then
      write(*,*)
      write(*,*)' Dimension of sim out-of-range in '
      write(*,*)' SUBROUTINE CONVERT_SIMP_TO_MOL   '
      write(*,*)
      stop
    end if
!
    nr = size(mol(nn)%res)
!
    sim(nn)%mol_nrs            = nr
    sim(nn)%mol_flag           =.true.
    sim(nn)%mol_reference_flag =.false.
    
    if (allocated(sim(nn)%mol_res)) deallocate(sim(nn)%mol_res)
    allocate (sim(nn)%mol_res(nr))
!
    nct = 0
    do i=1,nr
!    
       sim(nn)%mol_res(i)%res_flag = .true.          
       sim(nn)%mol_res(i)%res_chid = mol(nn)%res(i)%res_chid          
       sim(nn)%mol_res(i)%res_name = mol(nn)%res(i)%res_aas(1)%aa_nam 
       sim(nn)%mol_res(i)%res_nold = mol(nn)%res(i)%res_nold          
!
       sim(nn)%mol_res(i)%res_nats = size(mol(nn)%res(i)%res_aas(1)%aa_rots(1)%rot_ats)   
       nats = sim(nn)%mol_res(i)%res_nats
       allocate(sim(nn)%mol_res(i)%res_ats(nats))
! 
       do j=1, nats
          nct = nct + 1
!
          atnm  = mol(nn)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam   
          xx    = mol(nn)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1)
          xy    = mol(nn)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2)
          xz    = mol(nn)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3)
          xocc  = mol(nn)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ   
          xbfa  = mol(nn)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa   
          xflag = mol(nn)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag  
!
          sim(nn)%mol_res(i)%res_ats(j)%at_name    = atnm 
          sim(nn)%mol_res(i)%res_ats(j)%at_xyz(1)  = xx   
          sim(nn)%mol_res(i)%res_ats(j)%at_xyz(2)  = xy   
          sim(nn)%mol_res(i)%res_ats(j)%at_xyz(3)  = xz   
          sim(nn)%mol_res(i)%res_ats(j)%at_occ     = xocc 
          sim(nn)%mol_res(i)%res_ats(j)%at_bfa     = xbfa 
          sim(nn)%mol_res(i)%res_ats(j)%at_flag    = xflag 
          sim(nn)%mol_res(i)%res_ats(j)%at_alt     = ' '
          sim(nn)%mol_res(i)%res_ats(j)%at_num     = nct
!
       end do
    end do    
!
    return
!
    END SUBROUTINE CONVERT_MOL_TO_SIMP 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE LOAD_STRUCT(infile)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MOLECULE_SIMP_DATA_M
    USE MOLEC_M
!
	IMPLICIT NONE
!
    LOGICAL            :: cif_flag, pdb_flag
    CHARACTER(LEN=4)   :: ext, atnm
    CHARACTER(LEN=132) :: infile, file
    REAL               :: xbfa
    INTEGER            :: i,j, nn, mm
!
    cif_flag = .false.
    pdb_flag = .false.
    ext = infile((len_trim(infile)-3):len_trim(infile))
    if ((ext.eq.'.pdb').or.(ext.eq.'.PDB')) then  
!       read in pdb file
!        print*,'  '
        print*,'  INPUT FILE IS ASSUMED TO BE A ** PDB ** FILE '
        print*,'  ',infile(1:len_trim(infile))
        print*,'  '
        cif_flag = .false.
        pdb_flag = .true.
    else if ((ext.eq.'.cif').or.(ext.eq.'.CIF')) then
!        print*,'  '
        print*,'  INPUT FILE IS ASSUMED TO BE A ** CIF ** FILE '
        print*,'  ',infile(1:len_trim(infile))
        print*,'  '
        cif_flag = .true.
        pdb_flag = .false.
    else
       print*, '  NOT CLEAR WHETHER INPUT FILE = PDB OR CIF-FILE '
       print*, '  MUST QUIT'
       print*,  infile(1:len_trim(infile)) !
       stop    
    end if   
!
!   reading in structure files
!
    nn =1                       ! everything should be read into sim(1) 
    if (cif_flag) then 
        call cif_read_coords(infile,nn)
    else if (pdb_flag) then 
        call pdb_read_coords(infile,nn)
    end if
! 
!   now doing some alterations
!
    call simp_molecule_remove_alt_confs(nn)
!    
    call simp_molecule_reset_bfac(nn)
!    
    call simp_molecule_rename_leu(nn)
!    
    call simp_molecule_remove_hyd(nn)
!
!!   DEBUG
!    do i=1,len(file)
!     file(i:i) = ' '
!    end do
!    file(1:5) = 'z.lis'
!    print*, file(1:len_trim(file))
!    call cif_write_coords(file,1)
!!    call write_simp_pdb(file,1)
!    stop
!!   END DEBUG
!!
!   here, setting up the mol/ori structure
!
    mm=1                       ! everything should be transferred into mol(1)
    call convert_simp_to_mol(nn,mm)
!
    return
!
    END SUBROUTINE LOAD_STRUCT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE CONVERT_SIMP_TO_MOL(nn,mm)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MOLECULE_SIMP_DATA_M
    USE MOLEC_M
    USE AA_DATA_M
!
    IMPLICIT NONE
!
    INTEGER    :: nn, mm, nres, nats
    INTEGER    :: i, j , k
    LOGICAl    :: strange_flag
    CHARACTER(LEN=4) :: aanm, atnm, atnm1, atnm2
    REAL       :: xx, xy, xz, xbfa, xocc
!
    if (nn>size(sim)) then
      write(*,*)
      write(*,*)' Dimension of sim out-of-range in '
      write(*,*)' SUBROUTINE CONVERT_SIMP_TO_MOL   '
      write(*,*)
      stop
    end if
!
    nres = size(sim(nn)%mol_res)
!
    allocate (mol(mm)%res(nres))
    mol(mm)%mol_nrs=nres
!
    do,i=1,nres
       allocate(mol(mm)%res(i)%res_aas(1))
       mol(mm)%res(i)%res_naa=1
       mol(mm)%res(i)%res_mut=.false.
       mol(mm)%res(i)%res_lig=.false.
       allocate(mol(mm)%res(i)%res_aas(1)%aa_rots(1))
       mol(mm)%res(i)%res_aas(1)%aa_nrt=1
       mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_prob= 1.0
    end do
!
    do i=1, nres
!    
       mol(mm)%res(i)%res_chid           = sim(nn)%mol_res(i)%res_chid 
       mol(mm)%res(i)%res_aas(1)%aa_nam  = sim(nn)%mol_res(i)%res_name
       mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_flag= .true.
       mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_flag2=.false.
       mol(mm)%res(i)%res_num            = i
       mol(mm)%res(i)%res_nold           = sim(nn)%mol_res(i)%res_nold
!
       aanm = mol(mm)%res(i)%res_aas(1)%aa_nam
       strange_flag = .true.       
       do j=1, (size(ps))
           if (ps(j)%p_aan == aanm) then
              strange_flag =.false.
              mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_flag2=.true.
              nats = ps(j)%p_at_ncnt
              allocate(mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(nats))
              mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_nat=nats
              do k=1, ps(j)%p_at_ncnt
                  atnm= ps(j)%p_at_nam(k) 
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_nam=atnm
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_flag=.false.
              end do
           end if
       end do
       if (strange_flag) then           ! residues not present in parameter file
          nats = size(sim(nn)%mol_res(i)%res_ats)
          allocate(mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(nats))
          mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_nat=nats
          mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_flag2=.false.
       end if
!       
    end do         
!
!   checking if residue numbers are unique in input pdb file
!
    do i=1,mol(mm)%mol_nrs
       do j= i,mol(mm)%mol_nrs
         if (i==j) then
            cycle
         else if (mol(mm)%res(i)%res_nold == mol(mm)%res(j)%res_nold) then
            if (mol(mm)%res(i)%res_chid == mol(mm)%res(j)%res_chid) then
       PRINT*, '    '
       PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CAUTION >>> '
       PRINT*, '>>>>>>>  FOUND MORE THEN 1 RESIDUE WITH                '
       PRINT*, '>>>>>>>  RESIDUE NUMBER : ', mol(mm)%res(i)%res_chid,   &
    &                                        mol(mm)%res(i)%res_nold
       PRINT*, mol(mm)%res(i)%res_aas(1)%aa_nam, mol(mm)%res(j)%res_aas(1)%aa_nam
       PRINT*, '>>>>>>>  IN COORDINATE FILE                            '
       PRINT*, '>>>>>>>  THIS MIGHT CAUSE PROBLEMS LATER                                              '
       PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CAUTION >>>'
       PRINT*, '    '
!               stop
            end if
         end if
       end do
    end do
!
    do i=1, nres
!   
       do j=1, size(mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats)
           mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag = .false.
           atnm1 = mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam
!
           if (mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_flag2) then 
!
             do k=1,size(sim(nn)%mol_res(i)%res_ats)
             atnm2 = sim(nn)%mol_res(i)%res_ats(k)%at_name
!             
                if (atnm1.eq.atnm2) then
!            
                  atnm = sim(nn)%mol_res(i)%res_ats(k)%at_name
                  xx   = sim(nn)%mol_res(i)%res_ats(k)%at_xyz(1)
                  xy   = sim(nn)%mol_res(i)%res_ats(k)%at_xyz(2)
                  xz   = sim(nn)%mol_res(i)%res_ats(k)%at_xyz(3)
                  xocc = sim(nn)%mol_res(i)%res_ats(k)%at_occ
                  xbfa = sim(nn)%mol_res(i)%res_ats(k)%at_bfa
!            
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam=atnm
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1)=xx
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2)=xy
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3)=xz
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ=xocc
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa=xbfa
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag=.true.
!                  
                  exit
!                  
                end if
!                
             end do
! 
          else 
                  atnm = sim(nn)%mol_res(i)%res_ats(j)%at_name
                  xx   = sim(nn)%mol_res(i)%res_ats(j)%at_xyz(1)
                  xy   = sim(nn)%mol_res(i)%res_ats(j)%at_xyz(2)
                  xz   = sim(nn)%mol_res(i)%res_ats(j)%at_xyz(3)
                  xocc = sim(nn)%mol_res(i)%res_ats(j)%at_occ
                  xbfa = sim(nn)%mol_res(i)%res_ats(j)%at_bfa
!            
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam=atnm
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1)=xx
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2)=xy
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3)=xz
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ=xocc
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa=xbfa
                  mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag=.true.
          end if 
!           
       end do
    end do
!
!!   DEBUG
!    do i=1, nres   
!      if(.not.mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_flag2) then
!         print*, 'HERE 1'
!         print*, mol(mm)%res(i)%res_nold,mol(mm)%res(i)%res_chid,mol(mm)%res(i)%res_aas(1)%aa_nam 
!      end if 
!    end do
!    do i=1, nres  
!      if(.not.mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_flag2) then
!         do j=1, size(mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats)
!         print*, mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag , &
!      &          mol(mm)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam, 'BOFF ' 
!         end do
!      end if 
!    end do
!!   DEBUG    
!
    return
!
    END SUBROUTINE CONVERT_SIMP_TO_MOL 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE CIF_PARSER(line,word,nw)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE CIF_DATA_M
!
    IMPLICIT NONE
!
	character*1   char,cboff, app_boff, ex_boff
	integer       nw, nch, i, j
	integer       nmaxchar 
	logical lchar, lword, app_flag, first_app, first_ex, ex_flag
!
	data nmaxchar/47/
	character*47  max_abc, min_abc
	character(len=cif_line_l)  line
	character(len=cif_word_l)  new, word(40), tempword, timpword
!
	data max_abc/"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-/!$_'?.=#"/
	data min_abc/"abcdefghijklmnopqrstuvwxyz0123456789.-/!$_'?.=#"/
!                 12345678901234567890123456789012345678901234567
!
	cboff=' '
	app_boff= "'"
	ex_boff = '"'
!	
!    print*, '#####',app_boff,'#####'	
!    print*, '#####',ex_boff,'#####'	
!
	do 10, j=1,len(new)
		new(j:j)=cboff
10	continue
!
	do 20, i=1,40
		word(i)=new
20       continue
!
	nw=0
	nch=0
	lword=.true.
	lchar=.false.
!
        app_flag     = .false.
        first_app    = .true.
        ex_flag      = .false.
        first_ex     = .true.
!
	do 30,i=1,cif_line_l
		read(line(i:i),'(a)',end=60) char
!
!               this bit should take care of the cif specific use of ' ' and "'"
                if ((char.eq.ex_boff).and.first_ex) then
                    ex_flag  = .true.
                    first_ex = .false.
                else if ((char.eq.ex_boff).and.(.not.first_ex)) then
                    ex_flag  = .false.
                    first_ex = .true.
                end if
!                
                if (.not.ex_flag) then                    
                   if ((char.eq.app_boff).and.first_app) then
                      app_flag     = .true.
                      first_app    = .false.
                   else if ((char.eq.app_boff).and.(.not.first_app)) then
                      app_flag     = .false.
                      first_app    = .true.
                   end if 
                end if
!                
!
		if ((char.eq.' ').and.(.not.app_flag)) then
		   lword=.true.
		   lchar=.false.
		   nch=0
		else if ((char.eq.' ').and.(app_flag)) then
		   lchar=.true.
		else if (char.eq.'"') then 
		   lchar=.true.
		else 
		   do 40,j=1,nmaxchar
		      if (char.eq.max_abc(j:j).or.char.eq.min_abc(j:j)) then
		         lchar=.true.
		      end if
40		   continue
		end if
!
		if (lchar)then
		    if(lword) then
				do 50, j=1,len(new)
					new(j:j)=cboff
50				continue
			nw=nw+1
			nch=nch+1
 	 		new(nch:nch)=char
			word(nw)=new
			lword=.false.
		    else 
			nch=nch+1
 	 		new(nch:nch)=char
			word(nw)=new
		    end if
		end if
!
30	continue
60	continue
!
!   getting rid of the apostrophe in some words
!
    do i=1,nw
      tempword=word(i)
      if (tempword(1:1).eq.app_boff) then
         do j=1,len(timpword)
           timpword(j:j)=' ' 
         end do
         timpword(1:(len_trim(tempword)-2)) = tempword(2:(len_trim(tempword)-1))
         word(i) = timpword
      end if
    end do 
!
!
!   getting rid of the " in some words
!
    do i=1,nw
      tempword=word(i)
      if (tempword(1:1).eq.ex_boff) then
         do j=1,len(timpword)
           timpword(j:j)=' ' 
         end do
         timpword(1:(len_trim(tempword)-2)) = tempword(2:(len_trim(tempword)-1))
         word(i) = timpword
      end if
    end do 
!
!
	return
!                
        END SUBROUTINE CIF_PARSER
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE CIF_INIT_LOOP_DATA(keyword)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
     USE CIF_DATA_M
!
     IMPLICIT NONE
!     
     CHARACTER(LEN=cif_line_l)  :: line
     CHARACTER(LEN=cif_word_l), DIMENSION(40)  ::   word
     CHARACTER(LEN=cif_word_l)   ::  tempword
     CHARACTER(LEN=10)   ::  keyword     
     INTEGER             ::  i,j,nw
!
     CHARACTER (LEN=50), DIMENSION(:), allocatable :: ciflabels      
!
!    setting up the cif item entries (item(i)) defined in CIF_DATA_M
!
     if (allocated(ciflabels)) deallocate(ciflabels)
!
     if (keyword.eq.'READCOORD ') then  
         allocate(ciflabels(size(coord_entries)))
         do i=1, size(ciflabels)         
            ciflabels(i) = coord_entries(i)
         end do
     else 
         PRINT*, '    '
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
         PRINT*, '>>>>>>>  DATA BLOCK TO READ IN                       '
         PRINT*, '>>>>>>>  IN SUBROUTINE CIF_READ_LOOP_DATA            '
         PRINT*, '>>>>>>>  IS NOT DEFINED                              '
         PRINT*, '>>>>>>>  '
         PRINT*, '>>>>>>>  MUST STOP '        
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
         PRINT*, '    '
         STOP
     end if
!
!    code below should work for all cif loop data blocks
!
     if (allocated(item)) deallocate(item)
     allocate(item(size(ciflabels)))
!
     do i=1,size(item)
        do j=1,len(line)
            line(j:j)=' '
        end do
        line(1:len(ciflabels(i))) = ciflabels(i)
        call parser(line,word,nw)
        call cif_minwords(word,nw)           !  in cif files, data category and data item names 
!                                        !  are not case sensitive. 
        do j=1,len(item(i)%itemlabel)
          item(i)%itemlabel(j:j) = ' '
        end do
        item(i)%itemlabel = word(1)
        tempword= word(2)
        if (tempword(1:1).eq.'t') then 
          item(i)%iflag=.true.
        else 
          item(i)%iflag=.false.        
        end if
        tempword= word(3)
        if (tempword(1:1).eq.'t') then 
          item(i)%rflag=.true.
        else 
          item(i)%rflag=.false.        
        end if
        tempword= word(4)
        if (tempword(1:1).eq.'t') then 
          item(i)%cflag=.true.
        else 
          item(i)%cflag=.false.        
        end if
     end do   
!
!!    DEBUG
!     do i=1,size(item)
!       print*, i, item(i)%itemlabel(1:len_trim(item(i)%itemlabel))
!     end do
!     print*, '  ' 
!!    END DEBU
!
    END SUBROUTINE CIF_INIT_LOOP_DATA
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE CIF_FIND_ITEMI(label,nit,flag)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE CIF_DATA_M
!
	IMPLICIT NONE
!
    CHARACTER(LEN=50) :: label
	character(LEN=cif_word_l), dimension(40) :: word
	character(LEN=cif_word_l) :: tempword
    INTEGER           :: i, nit, nw 
    LOGICAL           :: flag 
!
    do i=1, len(tempword)
      tempword(i:i) = ' ' 
    end do
    write (tempword(1:50),  '(A)') label
!
    flag = .true.
    nw = 1
    nit = 0
    word(1) = tempword
    call cif_minwords(word,nw)
    tempword = word(1)
!
    do i=1, size(item)
       if (tempword.eq.item(i)%itemlabel) then 
!          print*, i, item(i)%itemlabel, item(i)%present_flag
          flag = item(i)%present_flag
          nit = i
          exit
       end if
    end do 
!
	END SUBROUTINE CIF_FIND_ITEMI
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE CIF_READ_COORDS(INFILE,nn)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
     USE CIF_DATA_M
     USE MOLECULE_SIMP_DATA_M
!
     IMPLICIT NONE
!
     INTEGER             ::  nr, i, nn
     CHARACTER(LEN=132)  ::  infile
     CHARACTER(LEN=10)   ::  keyword = 'READCOORD '
!                                       1234567890     
     INTEGER, dimension(:), allocatable :: natom
     CHARACTER (LEN=4), dimension(:), allocatable :: cnam
!
     CALL cif_read_loop_data_from_cif(infile,keyword)
!     
     call cif_get_nresid(nr) 
!     
!!    DEBUG 
!     print*, '==> ', nr
!     print*, '  '
!!    END DEBUG
!     
     if (allocated(natom)) deallocate(natom)
     allocate (natom(nr))
     if (allocated(cnam)) deallocate(cnam)
     allocate (cnam(nr))
!     
     call cif_get_natom(nr,natom,cnam)
!
!!    DEBUG
!     do i=1,nr
!        print*, cnam(i), natom(i)
!     end do 
!     print*, '  '
!!    END DEBUG     
!
     if(allocated(sim)) deallocate(sim)
     allocate(sim(nn))
!
     print*, '  TOTAL NUMBER OF RESIDUES READ IN= ', nr
     print*, '   '
!
     call cif_loop_data_to_simp_molec(nn,nr,natom,cnam)
!
!    now cleaning up the entire item arrays
!
     if (allocated(item)) deallocate(item) 
!
      return
!
    END SUBROUTINE CIF_READ_COORDS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE CIF_READ_LOOP_DATA_FROM_CIF(infile,keyword)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
     USE CIF_DATA_M
!
     IMPLICIT NONE
!     
     CHARACTER(LEN=132)  :: infile
     CHARACTER(LEN=cif_line_l)  :: line
     CHARACTER(LEN=cif_word_l), DIMENSION(40)  ::   word
     CHARACTER(LEN=cif_word_l)   ::  tempword
     CHARACTER(LEN=10)   ::  keyword     
     INTEGER, DIMENSION(:), allocatable :: ncorres
     INTEGER             ::  i,j, na, nb, nc, ncount
     INTEGER             ::  nw, nloops, nline, ios, loop_number
     INTEGER             ::  ntot_data, nn, item_number
     INTEGER             ::  ntot_items, ncheck
     LOGICAL             ::  loop_flag, data_flag, item_flag, found_flag
     LOGICAL             ::  flag_all_absent, flag_all_present
     INTEGER             ::  max_lines_input = 100000
!
     CHARACTER (LEN=50), DIMENSION(:), allocatable :: ciflabels      
!
!!    DEBUG
!     do i=1,132
!       line(i:i) = ' '
!     end do
!     write(*,*) 'input line'     
!     read(*,'(A)') line
!!
!
!     call cif_parser(line,word,nw) 
!     print*, line
!     print*, nw
!     do i=1,nw
!        tempword= word(i)
!        print*, tempword(1:len_trim(tempword))
!     end do 
!     stop
!!    END DEBUG 

!    setting up the cif item entries (item(i)) defined in CIF_DATA_M
!
     if (allocated(ciflabels)) deallocate(ciflabels)
!
     if (keyword.eq.'READCOORD ') then  
         allocate(ciflabels(size(coord_entries)))
         do i=1, size(ciflabels)         
            ciflabels(i) = coord_entries(i)
         end do
     else 
         PRINT*, '    '
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
         PRINT*, '>>>>>>>  DATA BLOCK TO READ IN                       '
         PRINT*, '>>>>>>>  IN SUBROUTINE CIF_READ_LOOP_DATA_FROM_CIF   '
         PRINT*, '>>>>>>>  IS NOT DEFINED                              '
         PRINT*, '>>>>>>>  '
         PRINT*, '>>>>>>>  MUST STOP '        
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
         PRINT*, '    '
         STOP
     end if
!
!    code below should work for all cif loop data blocks
!
     if (allocated(item)) deallocate(item)
     allocate(item(size(ciflabels)))
!
     do i=1,size(item)
        do j=1,132
            line(j:j)=' '
        end do
        line(1:len(ciflabels(i))) = ciflabels(i)
        call parser(line,word,nw)
        call cif_minwords(word,nw)           !  in cif files, data category and data item names 
!                                        !  are not case sensitive. 
        do j=1,80
          item(i)%itemlabel(j:j) = ' '
        end do
        item(i)%itemlabel = word(1)
        tempword= word(2)
        if (tempword(1:1).eq.'t') then 
          item(i)%iflag=.true.
        else 
          item(i)%iflag=.false.        
        end if
        tempword= word(3)
        if (tempword(1:1).eq.'t') then 
          item(i)%rflag=.true.
        else 
          item(i)%rflag=.false.        
        end if
        tempword= word(4)
        if (tempword(1:1).eq.'t') then 
          item(i)%cflag=.true.
        else 
          item(i)%cflag=.false.        
        end if
     end do   
!
!!    DEBUG
!     do i=1,size(item)
!       print*, i, item(i)%itemlabel(1:len_trim(item(i)%itemlabel))
!     end do
!     print*, '  ' 
!!    END DEBU
!!
!    finding loop of interest in the cif_file
!
     open(14,file=infile,status='old',form='formatted',iostat=ios)
!
!    loop_number = number of the loop, the data block is part of
!    loop_flag   = .true. if any item(i)%itemlabel is part of a loop statement
!    found_flag  = .true. if correct loop has been identified
!
     loop_flag = .false.
     found_flag = .false.
     nloops = 0
     nline  = 0 
     do i=1, max_lines_input
          nline = nline + 1
          read(14,fmt='(a)',iostat=ios) line             
          if (ios.lt.0) exit
          if (i.eq.(max_lines_input)) then
              PRINT*, '    '
              PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
              PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
              PRINT*, '>>>>>>> ', infile(1:len_trim(infile))
              PRINT*, '>>>>>>>  IN SUBROUTINE CIF_READ_LOOP_DATA            '
              PRINT*, '>>>>>>>  POSSIBLY INCREASE MAX_LINES_INPUT           '
              PRINT*, '>>>>>>>  '
              PRINT*, '>>>>>>>  MUST STOP! '        
              PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
              PRINT*, '    '
              STOP
          end if
!     
          call cif_parser(line,word,nw)
          call cif_minwords(word,nw)
          if (nw.eq.0) cycle
          tempword=word(1)
          if (tempword(1:1)=='#') cycle
          if ((tempword(1:5)=='loop_').or.(tempword(1:5)=='LOOP_')) then 
             loop_flag = .true.
             nloops = nloops+1
             cycle
          end if
          if ((loop_flag).and.(tempword(1:1).ne.'_')) then 
             loop_flag = .false.
          end if
!                 
!         Correct loop will be identified while looking any item(i)%itemlabel 
!
          do j=1,size(item)
             if (tempword(1:len(item(j)%itemlabel)).eq.item(j)%itemlabel) then 
                  loop_number = nloops
                  found_flag = .true. 
                  exit
             end if
          end do
          if (found_flag) exit
!          
     end do
     close(14)
!
     if(.not.found_flag) then 
         PRINT*, '    '
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
         PRINT*, '>>>>>>>  NO LOOP DATA BLOCK FOUND                    '
         PRINT*, '>>>>>>>  IN SUBROUTINE CIF_READ_LOOP_DATA            '
         PRINT*, '>>>>>>>  CONTAINING THE REQUESTED LABELS:            '
         do i=1, size(item)
           PRINT*, '>>>>>>> ', item(i)%itemlabel(1:len_trim(item(i)%itemlabel))
         end do
         PRINT*, '>>>>>>>  '
         PRINT*, '>>>>>>>  MUST STOP! '        
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
         PRINT*, '    '
         STOP
     end if
!
!    now investigating the loop data block and 
!    trying to figure out which item lables are actually present
!
     open(14,file=infile,status='old',form='formatted',iostat=ios)
!     
     nloops = 0
     nline  = 0
     item_number = 0
     ntot_data = 0
     item_flag = .true.
     data_flag = .false.
     do i=1, max_lines_input
!     
          nline = nline + 1
          read(14,fmt='(a)',iostat=ios) line             
          if (ios.lt.0) exit
!     
          call cif_parser(line,word,nw)
          call cif_minwords(word,nw)
!          
          if (nw.eq.0) cycle
          tempword=word(1)
          if (tempword(1:1)=='#') cycle
!          
          if ((tempword(1:5)=='loop_').or.(tempword(1:5)=='LOOP_')) then 
             nloops = nloops+1 
          end if
          if (nloops.lt.loop_number) cycle
          if (nloops.gt.loop_number) exit
!
!         point only reached in correct loop_ block
!
          if (tempword(1:1)=='_'.and.item_flag) then 
              item_number = item_number + 1
              do j=1, size (item)
                 if (tempword(1:len(item(j)%itemlabel)).eq.item(j)%itemlabel) then 
                    item(j)%present_flag=.true. 
                    item(j)%item_seq= item_number
                 end if
              end do
              data_flag= .true.
              cycle
          else if (tempword(1:1)=='_'.and.(.not.item_flag)) then 
              exit
          end if
!
!         now trying to figure out the number of total data points in the datablock
!
          if ((tempword(1:1).ne.'_').and.data_flag) then
              item_flag = .false.
              ntot_data = ntot_data + nw
              cycle
          end if
!
     end do
     close(14)
!
!!    DEBUG
!     print*, ' ' 
!     print*, 'NTOT_DATAPOINTS= ',ntot_data, 'NTOT_CIF_LABELS= ', item_number 
!     print*, 'MODULO', modulo(ntot_data,item_number) 
!     print*, 'FLOOR',  floor(real(ntot_data)/real(item_number)) 
!     print*, ' '
!!    END DEBUG
!
!    checking whether # data points in loop is a multiple of # cif lables at the top 
!    of the loop
!
     ncheck = modulo(ntot_data,item_number)
!
     if (ncheck.ne.0) then  
        PRINT*, '    '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
        PRINT*, '>>>>>>>  NUMBER OF ITEMS IN LOOP DOES NOT            '
        PRINT*, '>>>>>>>  MATCH THE NUMBER OF DATA ENTRIES            '
        PRINT*, '>>>>>>>  NUMBER LOOP ITEMS: ', item_number
        PRINT*, '>>>>>>>  NUMBER OF DATA ENTRIES: ', ntot_data
        PRINT*, '>>>>>>>  QUOTIENT: ', real(ntot_data)/real(item_number)
        PRINT*, '>>>>>>>  '
        PRINT*, '>>>>>>>  MUST STOP! '        
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
        PRINT*, '    '
        STOP
     end if
!
!    DEBUG
!    print*, nline
!!     nn=0
!     do i=1, size(item)
!      if (item(i)%present_flag) then   
!       print*, i, item(i)%item_seq, item(i)%present_flag, item(i)%itemlabel(1:len_trim(item(i)%itemlabel))
!      else 
!       print*, i, ' NOT PRESENT ', item(i)%present_flag, item(i)%itemlabel(1:len_trim(item(i)%itemlabel))
!      end if      
!     end do  
!     print*, ' '      
!!    END DEBUG
!
     ntot_items = ntot_data/item_number
     do i=1, size(item)
       if (item(i)%present_flag) then 
!       
          if (allocated(item(i)%slabel)) deallocate(item(i)%slabel)
          allocate(item(i)%slabel(ntot_items))
!          
          if (allocated(item(i)%valpres)) deallocate(item(i)%valpres)
          allocate(item(i)%valpres(ntot_items))
!          
          if (allocated(item(i)%valomit)) deallocate(item(i)%valomit)
          allocate(item(i)%valomit(ntot_items))
!          
          item(i)%cflag  = .true.
!          
          if (item(i)%iflag) then 
             if (allocated(item(i)%valint)) deallocate(item(i)%valint)
             allocate(item(i)%valint(ntot_items))
!             
          else if (item(i)%rflag) then 
             if (allocated(item(i)%valreal)) deallocate(item(i)%valreal)
             allocate(item(i)%valreal(ntot_items))
          end if
!          
       end if       
     end do  
!
     if (allocated(ncorres)) deallocate(ncorres)
     allocate(ncorres(item_number))
!
!    now setting up a correspondence table
!
     do i=1, size(ncorres)
        ncorres(i) = 0
        do j=1, size(item)
           if (item(j)%present_flag.and.(item(j)%item_seq.eq.i)) then
              ncorres(i) = j
              cycle
           end if
        end do 
     end do
!
!!    DEBUG
!     do i=1, size(ncorres)
!        if (ncorres(i).eq.0) then 
!          print*, i, '      NOT TO BE READ' 
!        else 
!          nn = ncorres(i)
!          print*, i, nn, item(nn)%present_flag, item(nn)%itemlabel(1:len_trim(item(nn)%itemlabel))
!        end if
!     end do 
!     print*, ' '
!!    END DEBUG
!
!    now reading in the cif block data  
!
     open(14,file=infile,status='old',form='formatted',iostat=ios)
!     
     nloops = 0
     ntot_data = 0
     item_flag = .true.
     data_flag = .false.
!     
     do i=1, max_lines_input
!     
          read(14,fmt='(a)',iostat=ios) line             
          if (ios.lt.0) exit
!     
          call cif_parser(line,word,nw)
!         no need to minimize words when reading in cif block data 
!          
          if (nw.eq.0) cycle
          tempword=word(1)
          if (tempword(1:1)=='#') cycle
!          
          if ((tempword(1:5)=='loop_').or.(tempword(1:5)=='LOOP_')) then 
             nloops = nloops+1 
          end if
          if (nloops.lt.loop_number) cycle
          if (nloops.gt.loop_number) exit
!
!         this point should only be reached in the correct loop_ block
!
          if (tempword(1:1)=='_'.and.item_flag) then 
              data_flag= .true.
              cycle
          else if (tempword(1:1)=='_'.and.(.not.item_flag)) then 
              exit
          end if
!
!         now trying to figure out the number of items in the datablock
!
          if ((tempword(1:1).ne.'_').and.data_flag) then
!              print*, line
!          
              do j=1,nw           
                 ncount = ntot_data + j
                 na = modulo(ncount,item_number)
                 nc = floor(real(ncount)/real(item_number))
                 if (na.ne.0) nc = nc +1
                 if (na.eq.0) na = item_number
                 nb = ncorres(na)
!                 print*, nb, nc 
                 if ((nb.ne.0).and.(item(nb)%present_flag)) then
                     item(nb)%slabel(nc) = word(j)
                 end if    
              end do
              ntot_data = ntot_data + nw
              item_flag = .false.
              cycle
          end if
!
     end do
     close(14)
!     
!!    DEBUG
!     do i = 1, size(item(1)%slabel)
!         do j=1, size(ncorres)
!            if (ncorres(j).ne.0) then 
!               print*, item(ncorres(j))%slabel(i)
!            end if   
!         end do 
!         print*, '### BOFF ###'
!     end do
!     print*, ntot_data, item_number, real(ntot_data)/real(item_number)
!     print*, floor(real(ntot_data)/real(item_number))
!!
!     do i=1, size(item)
!          if (item(i)%present_flag) then
!          print*, i, '=== BOFF ===', size(item(i)%slabel),item(i)%itemlabel(1:len_trim(item(i)%itemlabel)) 
!          do j = 1, size(item(i)%slabel)
!            print*, item(i)%slabel(j)
!          end do  
!          end if
!     end do
!!    END DEBUG    
!!
!    
!    now transfering integer and real values from item(i)%slabel(j) data
!
     do i=1, size(item)
       if (item(i)%present_flag) then
!       
          do j=1,size(item(i)%slabel) 
               item(i)%valpres(j) = .true.
               item(i)%valomit(j) = .false.
               tempword = item(i)%slabel(j)
               if (tempword(1:1).eq.'?') then
                  item(i)%valpres(j) = .false.
                  cycle
               end if   
               if (tempword(1:1).eq.'.') then
                  item(i)%valpres(j) = .false.
                  item(i)%valomit(j) = .true.
                  cycle
               end if
               if (item(i)%iflag.and.item(i)%valpres(j)) then
                 read(tempword,*,iostat=ios) item(i)%valint(j) 
                  if (ios.gt.0) then
              PRINT*, '    '
              PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
              PRINT*, '>>>>>>>  ERROR READING IN INTEGER VALUE FROM         '
              PRINT*, '>>>>>>>  STRING= ', tempword(1:len_trim(tempword))
              PRINT*, '>>>>>>>  IN SUBROUTINE CIF_READ_LOOP_DATA.           '
              PRINT*, '>>>>>>>  CIF_ITEM_LABEL= ',item(i)%itemlabel(1:len_trim(item(i)%itemlabel)) 
              PRINT*, '>>>>>>>  MUST STOP '        
              PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
              PRINT*, '    '
              STOP
                  end if
               end if 
               if (item(i)%rflag.and.item(i)%valpres(j)) then
                 read(tempword,*,iostat=ios) item(i)%valreal(j) 
                  if (ios.gt.0) then
              PRINT*, '    '
              PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
              PRINT*, '>>>>>>>  ERROR READING IN REAL VALUE FROM         '
              PRINT*, '>>>>>>>  STRING= ', tempword(1:len_trim(tempword))
              PRINT*, '>>>>>>>  IN SUBROUTINE CIF_READ_LOOP_DATA            '
              PRINT*, '>>>>>>>  CIF_ITEM_LABEL= ',item(i)%itemlabel(1:len_trim(item(i)%itemlabel)) 
              PRINT*, '>>>>>>>  '
              PRINT*, '>>>>>>>  MUST STOP '        
              PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
              PRINT*, '    '
              STOP
                  end if
               end if 
          end do
!
       end if  
     end do    
!
!
!!    DEBUG
!     do i=1, size(item)
!          if (item(i)%present_flag) then
!          print*, i, '==> ', size(item(i)%slabel),item(i)%itemlabel(1:len_trim(item(i)%itemlabel)) 
!          do j = 1, size(item(i)%slabel)
!            print*, item(i)%slabel(j), item(i)%valpres(j), item(i)%iflag, item(i)%rflag
!          end do  
!          end if
!     end do
!!    END DO
!!
!
!    now cleaning up item(i) in case all data entries are empty
!
     do i=1, size(item)
       flag_all_absent = .true.
       if (item(i)%present_flag) then
!       
          do j=1,size(item(i)%slabel)
             if (item(i)%valpres(j)) then 
                 flag_all_absent = .false.
                 exit
             end if
          end do
!                    
          if (flag_all_absent) then
            item(i)%present_flag = .false.
            if (allocated(item(i)%slabel)) deallocate(item(i)%slabel)
          end if
!
       end if
     end do
!
!    now cleaning up item(i)%slabel in case all data entries have been transfered 
!    to item(i)%valreal and item(i)%valreal
!
     do i=1, size(item)
       flag_all_present = .true.
       if (item(i)%present_flag) then
!       
          do j=1,size(item(i)%slabel)
             if (.not.item(i)%valpres(j)) then 
                 flag_all_present = .false.
                 exit
             end if
          end do
!                    
          if (flag_all_present.and.(item(i)%iflag.or.item(i)%rflag)) then
             if (allocated(item(i)%slabel)) deallocate(item(i)%slabel)
          end if
!
       end if
     end do
!
!
!!    DEBUG         
!      print*, (item(17)%valint(i), i=1,size(item(17)%valint))
!     print*, (item(15)%valint(i), i=1,size(item(15)%valint))
!     print*, (item(16)%slabel(i), i=1,size(item(15)%slabel))
!!    END DEBUG
!
    END SUBROUTINE CIF_READ_LOOP_DATA_FROM_CIF
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE CIF_GET_NRESID(nr)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE CIF_DATA_M
!
	IMPLICIT NONE
!	
	integer :: i 
	integer :: nmax = 10000000
	integer :: nr, nw, nresid, nchainid, nitemi 
	integer :: nold
	character(LEN=cif_word_l) :: tempword
	character(LEN=2) :: chold, chid
	LOGICAL	:: chainid_flag, present_flag
!
!                                                                                             pdb:           
    CHARACTER(LEN=50) :: label_insert =  '_atom_site.pdbx_PDB_ins_code                      ' !insertion code
    CHARACTER(LEN=50) :: label_altconf = '_atom_site.label_alt_id                           ' !alt conf
    CHARACTER(LEN=50) :: label_chain  =  '_atom_site.auth_asym_id                           ' !chain identifier
    CHARACTER(LEN=50) :: label_number =  '_atom_site.auth_seq_id                            ' !residue number
!                                         12345678901234567890123456789012345678901234567890
!
!   starting doing checking: if an instertion code is being used then also stop
!                            if alternative conformations are present then stop
!
    call cif_find_itemi(label_insert,nitemi,present_flag) 
!
    if (present_flag) then 
        PRINT*, '    '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
        PRINT*, '>>>>>>>  INSERT CODE DETECTED IN ATOM RECORD IN CIF FILE'
        PRINT*, '>>>>>>>  THIS CAN NOT BE HANDLED YET                 '
        PRINT*, '>>>>>>>  PLEASE RENUMBER THE RESIDUES                '
        PRINT*, '>>>>>>>  '
        PRINT*, '>>>>>>>  MUST STOP                                   '        
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
        PRINT*, '    '
        STOP
    end if
!
    call cif_find_itemi(label_altconf,nitemi,present_flag) 
!
    if (present_flag) then 
         PRINT*, '  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WARNING >>'
         PRINT*, '  >>>>>>>  ALTERNATIVE SIDE CHAIN ORIENTATIONS DETECTED '
         PRINT*, '  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WARNING >>'
         PRINT*, '    '
!         STOP
    end if
!
!   looking up chain id and residue numbers
!
    call cif_find_itemi(label_chain,nchainid,present_flag) 
    if (.not.present_flag) chainid_flag = .false.
!
    call cif_find_itemi(label_number,nresid,present_flag) 
!
    nr=0
    nold=nmax
    chold= '  '
! 
!!   DEBUG
!    do i=1, size(item(nresid)%valpres)
!      print*, item(nchainid)%slabel(i)
!      print*, item(nresid)%valint(i)
!    end do
!!   END DEBUG
!!
!
    do i=1, size(item(nresid)%valpres)
!
            if (.not.(item(nresid)%valpres(i))) then 
               STOP 'SOMETHING TERRIBLY WRONG IN GET_NATOM'
            end if
!
           if (item(nchainid)%valpres(i)) then
               tempword = item(nchainid)%slabel(i)
               chid = tempword(1:2)
            else 
               chid = '  '
            end if
! 
            if ((item(nresid)%valint(i) /= nold).or.(chid /= chold)) then 
               nold =  item(nresid)%valint(i)
               chold = chid   
               nr = nr+1
            end if
!            
     end do
!
!
	END SUBROUTINE CIF_GET_NRESID
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE CIF_GET_NATOM(nr,natom,cnam)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE CIF_DATA_M
!
    IMPLICIT NONE
!    
    integer :: i 
    integer :: nmax = 10000000
    integer :: nresid, nchainid, nresnm, natnm, nitemi 
    integer :: nold, nr, nat
    character(LEN=cif_word_l) :: tempword
    character(LEN=2) :: chold, chid
    LOGICAL :: chainid_flag, present_flag
!    
    integer, dimension(nr) :: natom
    character(LEN=4), dimension(nr) :: cnam
!                                                                                               pdb code
    CHARACTER(LEN=50) :: label_chain  =   '_atom_site.auth_asym_id                           ' !chain identifier
    CHARACTER(LEN=50) :: label_number =   '_atom_site.auth_seq_id                            ' !residue number
    CHARACTER(LEN=50) :: label_res_name = '_atom_site.auth_comp_id                           ' !residue name
    CHARACTER(LEN=50) :: label_at_name =  '_atom_site.auth_atom_id                           ' !atom name
!                                          12345678901234567890123456789012345678901234567890
!
!   looking up chain id (nchainid), residue numbers(nresid), residue names (resnm), atom names (natnm)
!
!
    call cif_find_itemi(label_chain,nchainid,present_flag) 
    if (.not.present_flag) chainid_flag = .false.
!
    call cif_find_itemi(label_number,nresid,present_flag) 
!
    call cif_find_itemi(label_res_name,nresnm,present_flag) 
!
    call cif_find_itemi(label_at_name,natnm,present_flag) 
!
    do i=1,nr
         natom(i)=0
         cnam(i)='    '
    end do
!
    nold= nmax
    nr = 0
    nat = -100
    chid = '  '
!
    do i=1, size(item(nresid)%valpres)
!
            if((.not.(item(nresid)%valpres(i))).and.(.not.(item(nresnm)%valpres(i)))) then 
               STOP 'SOMETHING TERRIBLY WRONG IN GET_NATOM'
            end if
!
            if (item(nchainid)%valpres(i)) then
               tempword = item(nchainid)%slabel(i)
               chid = tempword(1:2)
            else 
               chid = '  '
            end if
! 
            if ((item(nresid)%valint(i) /= nold).or.(chid /= chold)) then 
               nold =  item(nresid)%valint(i)
               chold = chid   
               nr  = nr+1
               nat = 1
               natom(nr) = nat
               tempword = item(nresnm)%slabel(i)
               cnam(nr) = tempword(1:4)
            else if ((item(nresid)%valint(i)==nold).or.(chid==chold)) then 
               nat = nat+1
               natom(nr) = nat
            else
               STOP 'SOMETHING TERRIBLY WRONG IN GET_NATOM'
            end if
!            
     end do
!
     return
!
	END SUBROUTINE CIF_GET_NATOM
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE CIF_LOOP_DATA_TO_SIMP_MOLEC(nn,nr,natom,cnam)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
       USE MOLECULE_SIMP_DATA_M
       USE CIF_DATA_M
!
!
       IMPLICIT NONE
       INTEGER       :: nn      ! defines the molecule (mol(nn) to store the pdb in
       INTEGER       :: nr      ! nr = number of residues
       INTEGER       :: ni      ! ni = counter in itemis entries
       INTEGER       :: i, j, k
       integer, dimension(nr) :: natom
       character(LEN=4), dimension(nr) :: cnam
       REAL          :: x, y, z, bfa, occ
       INTEGER       :: atnum, resnum
       INTEGER       :: nresid, nchainid, nresnm, natnm, naltconf, nitemi 
       INTEGER       :: nx, ny, nz, nbfa, nocc
       CHARACTER(LEN=cif_word_l) :: tempword
       LOGICAL       :: present_flag
!                                                                                               pdb code
    CHARACTER(LEN=50) :: label_chain  =   '_atom_site.auth_asym_id                           ' !chain identifier
    CHARACTER(LEN=50) :: label_number =   '_atom_site.auth_seq_id                            ' !residue number
    CHARACTER(LEN=50) :: label_at_name =  '_atom_site.auth_atom_id                           ' !atom name
    CHARACTER(LEN=50) :: label_altconf =  '_atom_site.label_alt_id                           ' !alt conf
!                 
    CHARACTER(LEN=50) :: label_at_x =     '_atom_site.Cartn_x                                ' !atom x coord
    CHARACTER(LEN=50) :: label_at_y =     '_atom_site.Cartn_y                                ' !atom y coord
    CHARACTER(LEN=50) :: label_at_z =     '_atom_site.Cartn_z                                ' !atom z coord
    CHARACTER(LEN=50) :: label_at_bfa =   '_atom_site.B_iso_or_equiv                         ' !atom bfac
    CHARACTER(LEN=50) :: label_at_occ =   '_atom_site.occupancy                              ' !atom occ
!    
!                                                   12345678901234567890123456789012345678901234567890
!   looking up itemi's
!
!   looking up chain id (nchainid), residue numbers(nresid), atom names (natnm)
!
      call cif_find_itemi(label_chain,nchainid,present_flag) 
      call cif_find_itemi(label_number,nresid,present_flag) 
      call cif_find_itemi(label_at_name,natnm,present_flag) 
      call cif_find_itemi(label_altconf,naltconf,present_flag) 
!     
!     looking up x_coord (nx), y_coord (ny), z_coord (nz), bfac (nbfa), occ(nocc)
!     
      call cif_find_itemi(label_at_x,nx,present_flag) 
      call cif_find_itemi(label_at_y,ny,present_flag) 
      call cif_find_itemi(label_at_z,nz,present_flag) 
      call cif_find_itemi(label_at_bfa,nbfa,present_flag) 
      call cif_find_itemi(label_at_occ,nocc,present_flag) 
!
      if (nn>size(sim)) then
        write(*,*)
        write(*,*)'dimension of sim out-of-range in SUBROUTINE CIF_READ_STRUCT'
        write(*,*)
        stop
      end if
!
      sim(nn)%mol_nrs=nr
      if (allocated(sim(nn)%mol_res)) deallocate(sim(nn)%mol_res)
      allocate (sim(nn)%mol_res(nr))
!      
      do i=1,nr
        sim(nn)%mol_res(i)%res_nats=natom(i)
        if (allocated(sim(nn)%mol_res(i)%res_ats)) deallocate(sim(nn)%mol_res(i)%res_ats)
        allocate (sim(nn)%mol_res(i)%res_ats(natom(i)))
        sim(nn)%mol_res(i)%res_name= cnam(i)
        sim(nn)%mol_res(i)%res_flag= .true. 
!        
!          write(*,*) 'rest nr. ', i       
!          write(*,*) 'name     ', sim(nn)%mol_res(i)%res_name       
!          write(*,*) 'atome    ', sim(nn)%mol_res(i)%res_nats
      end do
!
      ni=0
      do i=1, nr
          j=0
          do while (j < sim(nn)%mol_res(i)%res_nats)
              ni = ni+1
!              
              if((.not.item(nresid)%valpres(ni)).and.(.not.item(natnm)%valpres(ni))) then
                 STOP 'SOMETHING TERRIBLY WRONG IN GET_NATOM'
              end if
!              
              if(.not.item(nx)%valpres(ni)) then 
                 STOP 'SOMETHING TERRIBLY WRONG IN GET_NATOM'
              end if
              if(.not.item(ny)%valpres(ni)) then
                 STOP 'SOMETHING TERRIBLY WRONG IN GET_NATOM'
              end if
              if(.not.item(nz)%valpres(ni)) then
                 STOP 'SOMETHING TERRIBLY WRONG IN GET_NATOM'
              end if
              if(.not.item(nbfa)%valpres(ni)) then
                 STOP 'SOMETHING TERRIBLY WRONG IN GET_NATOM'
              end if
              if(.not.item(nocc)%valpres(ni)) then
                 STOP 'SOMETHING TERRIBLY WRONG IN GET_NATOM'
              end if
!              
              j = j+1
!              
              if (item(nchainid)%valpres(ni)) then
                 tempword = item(nchainid)%slabel(ni)
                 sim(nn)%mol_res(i)%res_chid = tempword(1:1)
              else 
                 sim(nn)%mol_res(i)%res_chid = ' '
              end if
!              
              if (j==1) then
                 sim(nn)%mol_res(i)%res_nold=item(nresid)%valint(ni)
              end if
!             
              tempword = item(natnm)%slabel(ni)
              sim(nn)%mol_res(i)%res_ats(j)%at_name=tempword(1:4)
!
              if (item(naltconf)%valpres(ni)) then
                 tempword = item(naltconf)%slabel(ni)
                 sim(nn)%mol_res(i)%res_ats(j)%at_alt = tempword(1:1)
              else 
                 sim(nn)%mol_res(i)%res_ats(j)%at_alt = ' '
              end if
!
!             DEBUG
!              print*, ni, tempword(1:4), tempword(1:len_trim(tempword)) 
!             END DEBUG
!
              atnum = ni
              x     = item(nx)%valreal(ni)
              y     = item(ny)%valreal(ni)
              z     = item(nz)%valreal(ni)
              occ   = item(nocc)%valreal(ni)
              bfa   = item(nbfa)%valreal(ni)
!              
              sim(nn)%mol_res(i)%res_ats(j)%at_num = atnum
              sim(nn)%mol_res(i)%res_ats(j)%at_xyz(1) = x
              sim(nn)%mol_res(i)%res_ats(j)%at_xyz(2) = y
              sim(nn)%mol_res(i)%res_ats(j)%at_xyz(3) = z
              sim(nn)%mol_res(i)%res_ats(j)%at_bfa = bfa
              sim(nn)%mol_res(i)%res_ats(j)%at_occ = occ
!
              sim(nn)%mol_res(i)%res_ats(j)%at_flag = .true.
!
         end do
!
      end do 
!
        END SUBROUTINE CIF_LOOP_DATA_TO_SIMP_MOLEC
!        
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE CIF_WRITE_COORDS(outfile2,nmol)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
     USE CIF_DATA_M
     USE MOLECULE_SIMP_DATA_M
!
     IMPLICIT NONE
!
     INTEGER             ::  nmol
     CHARACTER(LEN=132)  ::  outfile2
     CHARACTER(LEN=10)   ::  keyword = 'READCOORD '
!                                       1234567890     
!
     CALL cif_init_loop_data(keyword)
!
     CALL cif_simp_molec_to_loop_data(nmol)
!
     CALL cif_write_loop_data_to_cif(outfile2,keyword)
!
    END SUBROUTINE CIF_WRITE_COORDS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE CIF_SIMP_MOLEC_TO_LOOP_DATA(nm)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
       USE MOLECULE_SIMP_DATA_M
       USE CIF_DATA_M
!
       IMPLICIT NONE
!       
       INTEGER       :: nm      ! defines the molecule (sim(nn) to tetrieve data from
       INTEGER       :: nr, ntot     ! nr = number of residues
       INTEGER       :: ni      ! ni = counter in itemis entries
       INTEGER       :: i, j, k, res_num
       REAL          :: x, y, z, bfa, occ
       INTEGER       :: atnum, resnum
       INTEGER       :: nresid, nchainid, nresnm, natnm, naltconf, nitemi 
       INTEGER       :: nx, ny, nz, nbfa, nocc
       CHARACTER(LEN=cif_word_l) :: tempword, timpword
       CHARACTER(LEN=1)  :: chid, altconf, capp= "'"
       LOGICAL       :: present_flag
!                                                                                               pdb code
    CHARACTER(LEN=50) :: label_at_name =  '_atom_site.auth_atom_id                           ' !atom name
    CHARACTER(LEN=50) :: label_res_name = '_atom_site.auth_comp_id                           ' !residue name
    CHARACTER(LEN=50) :: label_chain  =   '_atom_site.auth_asym_id                           ' !chain identifier
    CHARACTER(LEN=50) :: label_number =   '_atom_site.auth_seq_id                            ' !residue number
    CHARACTER(LEN=50) :: label_altconf =  '_atom_site.label_alt_id                           ' !alt conf    
!
!    CHARACTER(LEN=50) :: label_insert =   '_atom_site.pdbx_PDB_ins_code                      ' !insertion code
!    CHARACTER(LEN=50) :: label_altconf =  '_atom_site.label_alt_id                           ' !alt conf
!
    CHARACTER(LEN=50) :: label_at_x =     '_atom_site.Cartn_x                                ' !atom x coord
    CHARACTER(LEN=50) :: label_at_y =     '_atom_site.Cartn_y                                ' !atom y coord
    CHARACTER(LEN=50) :: label_at_z =     '_atom_site.Cartn_z                                ' !atom z coord
    CHARACTER(LEN=50) :: label_at_occ =   '_atom_site.occupancy                              ' !atom occ
    CHARACTER(LEN=50) :: label_at_bfa =   '_atom_site.B_iso_or_equiv                         ' !atom bfac
!                                          12345678901234567890123456789012345678901234567890
!
!   count total number of atoms present in sim(nn)
!
    ntot = 0
    do i=1, size(sim(nm)%mol_res)
        if (.not.sim(nm)%mol_res(i)%res_flag) cycle
        do j= 1, size(sim(nm)%mol_res(i)%res_ats)
           if (sim(nm)%mol_res(i)%res_ats(j)%at_flag) then
             ntot = ntot + 1 
           end if 
        end do    
    end do     
!
!!   DEBUG
!     print*, ntot
!!   END DEBUG    
!
    do i=1,size(item)
       if (allocated(item(i)%slabel))  deallocate(item(i)%slabel) 
!       if (allocated(item(i)%llabel))  deallocate(item(i)%llabel) 
       if (allocated(item(i)%valint))  deallocate(item(i)%valint) 
       if (allocated(item(i)%valreal)) deallocate(item(i)%valreal) 
       if (allocated(item(i)%valpres)) deallocate(item(i)%valpres)
       if (allocated(item(i)%valomit)) deallocate(item(i)%valomit)
       if (allocated(item(i)%valapp)) deallocate(item(i)%valapp)
!       
       allocate (item(i)%slabel(ntot))
!       allocate (item(i)%llabel(ntot))
       allocate (item(i)%valint(ntot))
       allocate (item(i)%valreal(ntot))
       allocate (item(i)%valpres(ntot))
       allocate (item(i)%valomit(ntot))
       allocate (item(i)%valapp(ntot))
!
       item(i)%nitems = ntot
     end do       
!
      call cif_find_itemi(label_at_name,natnm,present_flag) 
      call cif_find_itemi(label_res_name,nresnm,present_flag) 
      call cif_find_itemi(label_chain,nchainid,present_flag) 
      call cif_find_itemi(label_number,nresid,present_flag)
      call cif_find_itemi(label_altconf,naltconf,present_flag)
      call cif_find_itemi(label_at_x,nx,present_flag) 
      call cif_find_itemi(label_at_y,ny,present_flag) 
      call cif_find_itemi(label_at_z,nz,present_flag) 
      call cif_find_itemi(label_at_occ,nocc,present_flag) 
      call cif_find_itemi(label_at_bfa,nbfa,present_flag) 
!
      do i=1, len(tempword)
         tempword(i:i) = ' '
         timpword(i:i) = ' '
      end do 
!
      ni = 0
!
      do i=1, size(sim(nm)%mol_res)
        if (.not.sim(nm)%mol_res(i)%res_flag) cycle
        do j= 1, size(sim(nm)%mol_res(i)%res_ats)
          if (sim(nm)%mol_res(i)%res_ats(j)%at_flag) then 
           ni = ni +1 
!
              item(natnm)%slabel(ni)    = tempword
              item(nresnm)%slabel(ni)   = tempword
              item(nchainid)%slabel(ni) = tempword
              item(nresid)%slabel(ni)   = tempword
              item(naltconf)%slabel(ni) = tempword
              item(nx)%slabel(ni)       = tempword     
              item(ny)%slabel(ni)       = tempword     
              item(nz)%slabel(ni)       = tempword     
              item(nbfa)%slabel(ni)     = tempword   
              item(nocc)%slabel(ni)     = tempword   
!
              timpword = tempword
              timpword(1:4) = sim(nm)%mol_res(i)%res_ats(j)%at_name     
              item(natnm)%slabel(ni)=  timpword
              item(natnm)%valpres(ni)= .true.
              item(natnm)%valomit(ni)= .false.              
!
              timpword = tempword
              timpword(1:4) = sim(nm)%mol_res(i)%res_name     
              item(nresnm)%slabel(ni)  = timpword               
              item(nresnm)%valpres(ni) = .true.               
              item(nresnm)%valomit(ni) = .false.               
!
              chid = sim(nm)%mol_res(i)%res_chid
              if (chid==' ') then
                 item(nchainid)%valpres(ni) = .false.
                 item(nchainid)%valomit(ni) = .false.
              else 
                 timpword = tempword
                 timpword(1:1) = sim(nm)%mol_res(i)%res_chid
                 item(nchainid)%slabel(ni)  = timpword
                 item(nchainid)%valpres(ni) = .true.
                 item(nchainid)%valomit(ni) = .false.
              end if
!
              altconf = sim(nm)%mol_res(i)%res_ats(j)%at_alt
              if (altconf==' ') then
                 item(naltconf)%valpres(ni) = .false.
                 item(naltconf)%valomit(ni) = .false.
              else 
                 timpword = tempword
                 timpword(1:1) = sim(nm)%mol_res(i)%res_ats(j)%at_alt
                 item(naltconf)%slabel(ni)  = timpword
                 item(naltconf)%valpres(ni) = .true.
                 item(naltconf)%valomit(ni) = .false.
              end if
!
              resnum = sim(nm)%mol_res(i)%res_nold
!
              x      =   sim(nm)%mol_res(i)%res_ats(j)%at_xyz(1) 
              y      =   sim(nm)%mol_res(i)%res_ats(j)%at_xyz(2) 
              z      =   sim(nm)%mol_res(i)%res_ats(j)%at_xyz(3) 
              occ    =   sim(nm)%mol_res(i)%res_ats(j)%at_occ    
              bfa    =   sim(nm)%mol_res(i)%res_ats(j)%at_bfa    
!
              item(nresid)%valint(ni) =    resnum
              item(nx)%valreal(ni)    =    x     
              item(ny)%valreal(ni)    =    y     
              item(nz)%valreal(ni)    =    z     
              item(nbfa)%valreal(ni)  =    bfa   
              item(nocc)%valreal(ni)  =    occ   
!              
              item(nresid)%valpres(ni) =    .true.
              item(nx)%valpres(ni)     =    .true.     
              item(ny)%valpres(ni)     =    .true.     
              item(nz)%valpres(ni)     =    .true.     
              item(nbfa)%valpres(ni)   =    .true.   
              item(nocc)%valpres(ni)   =    .true.   
!      
              item(nresid)%valomit(ni) =    .false.
              item(nx)%valomit(ni)     =    .false.     
              item(ny)%valomit(ni)     =    .false.     
              item(nz)%valomit(ni)     =    .false.     
              item(nbfa)%valomit(ni)   =    .false.   
              item(nocc)%valomit(ni)   =    .false.   
!
          end if
        end do 
      end do       
!
!     now checking whether atom names with apostrophes are present (O5' etc..)
!     these have to be written out later as "O5'" in cif files
!
      do i =1, size(item(natnm)%slabel)
         timpword = item(natnm)%slabel(i)
         do j=1, len_trim(item(natnm)%slabel(i))
            if  (timpword(j:j) == capp) then 
               item(natnm)%valapp(i) = .true.
            else   
               item(natnm)%valapp(i) = .false.
            end if
         end do
      end do
!
!!     DEBUG
!      do i =1, size(item(natnm)%slabel)
!         if (item(natnm)%valapp(i)) then 
!            timpword = item(natnm)%slabel(i)
!            print*, timpword(1:len_trim(timpword)) 
!         end if   
!      end do
!!     END DEBUG
!
        END SUBROUTINE CIF_SIMP_MOLEC_TO_LOOP_DATA
!        
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE CIF_WRITE_LOOP_DATA_TO_CIF(filenm,keyword)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
       USE CIF_DATA_M
!
       IMPLICIT NONE
!       
       INTEGER       :: ni      ! ni = total count of items entries
       INTEGER       :: i, j, k, res_num, ios, nct
       REAL          :: x, y, z, bfa, occ
       INTEGER       :: atnum, resnum
       INTEGER       :: nresid, nchainid, nresnm, natnm, naltconf, nitemi 
       INTEGER       :: nx, ny, nz, nbfa, nocc
       CHARACTER(LEN=cif_word_l) :: word_blank, word_temp
       CHARACTER(LEN=cif_word_l), DIMENSION(1) :: word
!
       CHARACTER(LEN=10) :: str_natoms, str_nresid, str_x, str_y, str_z, str_occ, str_bfa
       CHARACTER(LEN=10) :: str_attyp, str_atnm, str_chid, str_resnm, str_altconf, str_blank
!       
       INTEGER       :: maxrr, ncl_natoms, ncl_nresid, ncl_nx, ncl_ny, ncl_nz, ncl_nocc, ncl_nbfa
       INTEGER       :: ncl_atnm
       REAL          :: rr
!
       CHARACTER(LEN=1)  :: chid, cpar = '"'
       LOGICAL           :: present_flag
!       
       CHARACTER(LEN=132)  ::  filenm
       CHARACTER(LEN=10)   ::  keyword
       CHARACTER (LEN=50), DIMENSION(:), allocatable :: ciflabels
       CHARACTER (LEN=50)                            :: temp      
!                                                                                               pdb code
    CHARACTER(LEN=50) :: label_at_name =  '_atom_site.auth_atom_id                           ' !atom name
    CHARACTER(LEN=50) :: label_res_name = '_atom_site.auth_comp_id                           ' !residue name
    CHARACTER(LEN=50) :: label_chain  =   '_atom_site.auth_asym_id                           ' !chain identifier
    CHARACTER(LEN=50) :: label_number =   '_atom_site.auth_seq_id                            ' !residue number
    CHARACTER(LEN=50) :: label_altconf =  '_atom_site.label_alt_id                           ' !alt conf
!    CHARACTER(LEN=50) :: label_insert =   '_atom_site.pdbx_PDB_ins_code                      ' !insertion code
    CHARACTER(LEN=50) :: label_at_x =     '_atom_site.Cartn_x                                ' !atom x coord
    CHARACTER(LEN=50) :: label_at_y =     '_atom_site.Cartn_y                                ' !atom y coord
    CHARACTER(LEN=50) :: label_at_z =     '_atom_site.Cartn_z                                ' !atom z coord
    CHARACTER(LEN=50) :: label_at_occ =   '_atom_site.occupancy                              ' !atom occ
    CHARACTER(LEN=50) :: label_at_bfa =   '_atom_site.B_iso_or_equiv                         ' !atom bfac
!                                          12345678901234567890123456789012345678901234567890
!
    CHARACTER(LEN=cif_line_l)                 ::   line
    CHARACTER(LEN=cif_word_l), DIMENSION(40)  ::   wird
    CHARACTER(LEN=cif_word_l)                 ::   tempword
    INTEGER                                   ::   nw
!
     if (allocated(ciflabels)) deallocate(ciflabels)
!
     if (keyword.eq.'READCOORD ') then  
         allocate(ciflabels(size(coord_entries)))
         do i=1, size(ciflabels)
            do j=1,len(line)
              line(j:j)=' '
            end do
            temp = coord_entries(i)
            line(1:len(temp)) = temp
            call cif_parser(line,wird,nw)
            tempword = wird(1)
            temp = tempword(1:len(temp))
            ciflabels(i) = temp
!!       DEBUG
!            print*, 'BEEN HERE XX'
!            print*, ciflabels(i)
!!
!!       END DEBUG
         end do
!         
     else 
         PRINT*, '    '
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
         PRINT*, '>>>>>>>  DATA BLOCK TO WRITE OUT IN                  '
         PRINT*, '>>>>>>>  IN SUBROUTINE CIF_WRITE_LOOP_DATA_TO_CIF    '
         PRINT*, '>>>>>>>  IS NOT DEFINED                              '
         PRINT*, '>>>>>>>  '
         PRINT*, '>>>>>>>  MUST STOP '        
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
         PRINT*, '    '
         STOP
     end if
!
      open(14,file=filenm,status='unknown',form='formatted',iostat=ios)   
!
      write(14,'(A)') "data_XXX"
      write(14,'(A)') "#"
      write(14,'(A)') "loop_"
!
      do i=1, len(word_blank)
         word_blank(i:i) = ' ' 
      end do 
!
      do i=1, size(ciflabels)
         word_temp = word_blank
         read(ciflabels(i),'(A)') word_temp(1:len(ciflabels(i)))      
         word(1) = word_temp
         call cif_minwords(word,1)
         word_temp = word(1) 
         write(14,'(A)') word_temp(1:len_trim(word_temp)) 
      end do 
!
      call cif_find_itemi(label_at_name,natnm,present_flag) 
      call cif_find_itemi(label_res_name,nresnm,present_flag) 
      call cif_find_itemi(label_chain,nchainid,present_flag) 
      call cif_find_itemi(label_number,nresid,present_flag) 
      call cif_find_itemi(label_altconf,naltconf,present_flag)
      call cif_find_itemi(label_at_x,nx,present_flag) 
      call cif_find_itemi(label_at_y,ny,present_flag) 
      call cif_find_itemi(label_at_z,nz,present_flag) 
      call cif_find_itemi(label_at_occ,nocc,present_flag) 
      call cif_find_itemi(label_at_bfa,nbfa,present_flag) 
!
!     figuring out the number of columns required to write out each value
!
      ni = size(item(nx)%valreal)         ! total number of atoms
!
!!     
!      print*, 'maximum (abolute) values and estimation of number of '
!      print*, 'columns needed for writing out these values into a CIF file: '
!      print*, ' ' 
!!
!      print*, '_atom_site.id' 
      maxrr = ni
      rr = maxrr
      nct=0       
      do while (rr.ge.1)
         rr = rr/10
         nct = nct + 1
      end do   
      ncl_natoms = nct
!      print*, 'MAX_value= ', maxrr, ' #columns needed= ',ncl_natoms
!      print*, ' ' 
!!
!
!      print*, item(nresid)%itemlabel(1:len_trim(item(nresid)%itemlabel))
      maxrr = maxval(abs(item(nresid)%valint))
      rr = maxrr
      nct=0       
      do while (rr.ge.1)
         rr = rr/10
         nct = nct + 1
      end do   
      ncl_nresid = nct
!      print*, 'MAX_value= ', maxrr, ' #columns needed= ',ncl_nresid
!      print*, ' ' 
!!
!
!      print*, item(nx)%itemlabel(1:len_trim(item(nx)%itemlabel))
      maxrr = maxval(nint(abs(item(nx)%valreal)))
      rr = maxrr
      nct=0       
      do while (rr.ge.1)
         rr = rr/10
         nct = nct + 1
      end do   
      ncl_nx = nct
!      print*, 'MAX_abs_value= ', maxrr, ' #columns needed= ',ncl_nx+5,' -xx.xxx'
!      print*, ' ' 
!!
!
!      print*, item(ny)%itemlabel(1:len_trim(item(ny)%itemlabel))
      maxrr = maxval(nint(abs(item(ny)%valreal)))
      rr = maxrr
      nct=0       
      do while (rr.ge.1)
         rr = rr/10
         nct = nct + 1
      end do   
      ncl_ny = nct
!      print*, 'MAX_abs_value= ', maxrr, ' #columns needed= ',ncl_ny+5,' -xx.xxx'
!      print*, ' ' 
!!
!
!      print*, item(nz)%itemlabel(1:len_trim(item(nz)%itemlabel))
      maxrr = maxval(nint(abs(item(nz)%valreal)))
      rr = maxrr
      nct=0       
      do while (rr.ge.1)
         rr = rr/10
         nct = nct + 1
      end do   
      ncl_nz = nct
!      print*, 'MAX_abs_value= ', maxrr, ' #columns needed= ',ncl_nz+5,' -xx.xxx'
!      print*, ' ' 
!!
!
!      print*, item(nocc)%itemlabel(1:len_trim(item(nocc)%itemlabel))
      maxrr = maxval(nint(abs(item(nocc)%valreal)))
      rr = maxrr
      nct=0       
      do while (rr.ge.1)
         rr = rr/10
         nct = nct + 1
      end do   
      ncl_nocc = nct
!      print*, 'MAX_value= ', maxrr, ' #columns needed= ',ncl_nocc+3,' x.xx'
!      print*, ' ' 
!!
!
!      print*, item(nbfa)%itemlabel(1:len_trim(item(nbfa)%itemlabel))
      maxrr = maxval(nint(abs(item(nbfa)%valreal)))
      rr = maxrr
      nct=0       
      do while (rr.ge.1)
         rr = rr/10
         nct = nct + 1
      end do   
      ncl_nbfa = nct
!     print*, 'MAX_value= ', maxrr, ' #columns needed= ',ncl_nbfa+3, ' xxx.xx' 
!     print*, ' ' 
!
!     checking whether atom names with apostrophes are present (O5' etc..)
!
      ncl_atnm = 0
      do i=1, ni
         if (item(natnm)%valapp(i)) then 
           ncl_atnm = 2
           exit
         else
           ncl_atnm  = 0
         end if  
      end do 
!
!     now preparing substrings for writing everything out
!
      do i=1, len(word_temp)
         word_temp(i:i) = ' '       
      end do 
!
      do i=1,10
        str_blank(i:i) = ' '
      end do
!
      nct = 0
      do i=1, ni
             nct = nct +1
             str_natoms = str_blank
              str_attyp = str_blank
               str_atnm = str_blank 
              str_resnm = str_blank 
               str_chid = str_blank 
             str_nresid = str_blank
            str_altconf = str_blank
                  str_x = str_blank 
                  str_y = str_blank 
                  str_z = str_blank 
                str_occ = str_blank 
                str_bfa = str_blank 
!         
            write(str_natoms,'(I10)') nct
!
            if (item(natnm)%valpres(i)) then              
                word_temp = item(natnm)%slabel(i)
                str_attyp(1:1) = word_temp(1:1) 
            else 
                str_attyp(1:1) = '?'
            end if 
!
            if (item(natnm)%valpres(i)) then              
               word_temp = item(natnm)%slabel(i)
               if ((ncl_atnm.gt.0).and.(item(natnm)%valapp(i))) then            !  taking care of "O5'" in cif files
                 str_atnm(1:1) = cpar
                 str_atnm(2:(1+len_trim(word_temp))) = word_temp(1:len_trim(word_temp))
                 str_atnm((2+len_trim(word_temp)):(2+len_trim(word_temp))) = cpar
               else 
                  write(str_atnm,'(A4)') word_temp(1:4)
               end if 
            else 
                write(str_atnm(3:3),'(A1)'  ) '?'
            end if    
!
            if (item(nresnm)%valpres(i)) then
                word_temp = item(nresnm)%slabel(i)
                write(str_resnm,'(A4)') word_temp(1:4)
            else 
                write(str_resnm(3:3),'(A1)'  ) '?'
            end if    
!
            if (item(nchainid)%valpres(i)) then
                word_temp = item(nchainid)%slabel(i)
                write(str_chid,'(A1)') word_temp(1:4)
            else 
                write(str_chid(1:1),'(A1)'  ) '?'
            end if
!            
            if (item(naltconf)%valpres(i)) then
                word_temp = item(naltconf)%slabel(i)
                write(str_altconf,'(A1)') word_temp(1:1)
            else 
                write(str_altconf(1:1),'(A1)'  ) '?'
            end if
!
            if (item(nresid)%valpres(i)) then
                write(str_nresid,'(I10)') item(nresid)%valint(i)
            else 
                write(str_nresid(10:10),'(A1)'  ) '?'
            end if    
!
            if (item(nx)%valpres(i)) then
                write(str_x,'(F10.3)') item(nx)%valreal(i)
            else 
                write(str_x(10:10),'(A1)'  ) '?'
            end if    
!
            if (item(ny)%valpres(i)) then
                write(str_y,'(F10.3)') item(ny)%valreal(i)
            else 
                write(str_y(10:10),'(A1)'  ) '?'
            end if    
!
            if (item(nz)%valpres(i)) then
                write(str_z,'(F10.3)') item(nz)%valreal(i)
            else 
                write(str_z(10:10),'(A1)'  ) '?'
            end if    
!
            if (item(nocc)%valpres(i)) then
                write(str_occ,'(F10.2)') item(nocc)%valreal(i)
            else 
                write(str_occ(10:10),'(A1)'  ) '?'
            end if    
!
            if (item(nbfa)%valpres(i)) then
                write(str_bfa,'(F10.2)') item(nbfa)%valreal(i)
            else 
                write(str_bfa(10:10),'(A1)'  ) '?'
            end if    
!
            write(14,'(21(A,X))')                             &
    &       'ATOM'                                        ,   &  ! _atom_site.group_PDB         
    &       str_natoms((10-(ncl_natoms-1)):10)            ,   &  ! _atom_site.id                
    &       str_attyp(1:1)                                ,   &  ! _atom_site.type_symbol    
    &       str_atnm(1:(4+ncl_atnm))                      ,   &  ! _atom_site.label_atom_id     
    &       str_altconf(1:1)                              ,   &  ! _atom_site.label_alt_id        
    &       str_resnm(1:4)                                ,   &  ! _atom_site.label_comp_id     
    &       str_chid(1:1)                                 ,   &  ! _atom_site.label_asym_id     
    &       '?'                                           ,   &  ! _atom_site.label_entity_id   
    &       str_nresid((10-(ncl_nresid-1)):10)            ,   &  ! _atom_site.label_seq_id      
    &       '?'                                           ,   &  ! _atom_site.pdbx_PDB_ins_code 
    &       str_x((6-(ncl_nx)):10)                        ,   &  ! _atom_site.Cartn_x           
    &       str_y((6-(ncl_nx)):10)                        ,   &  ! _atom_site.Cartn_y           
    &       str_z((6-(ncl_nx)):10)                        ,   &  ! _atom_site.Cartn_z           
    &       str_occ((7-(ncl_nocc-1)):10)                  ,   &  ! _atom_site.occupancy         
    &       str_bfa((7-(ncl_nbfa-1)):10)                  ,   &  ! _atom_site.B_iso_or_equiv    
    &       '?'                                           ,   &  ! _atom_site.pdbx_formal_charge
    &       str_nresid((10-(ncl_nresid-1)):10)            ,   &  ! _atom_site.auth_seq_id         
    &       str_resnm(1:4)                                ,   &  ! _atom_site.auth_comp_id        
    &       str_chid(1:1)                                 ,   &  ! _atom_site.auth_asym_id        
    &       str_atnm(1:(4+ncl_atnm))                      ,   &  ! _atom_site.auth_atom_id        
    &       '?'                                                  ! _atom_site.pdbx_PDB_model_num  
!                        
      end do 
!
      write(14,'(A)') "#"
!
      close(14)
!
      if (allocated(item)) deallocate(item)
      if (allocated(ciflabels)) deallocate(ciflabels)
!
      return
!        
        END SUBROUTINE CIF_WRITE_LOOP_DATA_TO_CIF
!        
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE PDB_READ_COORDS(filenm,nn)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
       USE MOLECULE_SIMP_DATA_M
!
       IMPLICIT NONE
!
       INTEGER  :: max_lines_input=100000000
       INTEGER  :: i, nr, nn, nfile
!
       CHARACTER (LEN=132) :: filenm
!
       INTEGER, dimension(:), allocatable :: natom
       CHARACTER (LEN=4), dimension(:), allocatable :: cnam
       CHARACTER (LEN=5)                  :: label
!
       if (allocated(sim)) deallocate(sim)
       allocate(sim(nn))
!
       sim(nn)%mol_reference_flag = .false.
       sim(nn)%mol_flag           = .true.
!
       call pdb_get_nresid(filenm,nr,max_lines_input)  
       if (allocated(natom)) deallocate(natom)
       allocate (natom(nr))
       if (allocated(cnam)) deallocate(cnam)
       allocate (cnam(nr))
       call pdb_get_natom(filenm,nr,natom,cnam,max_lines_input)
!
       print*, '  TOTAL NUMBER OF RESIDUES READ IN= ', nr
       print*, '   '
!       
!       print*, (natom(i), i=1,size(natom))
!       print*, (cnam(i),  i=1,size(cnam))
!
       nfile = nn
       call pdb_read_pdb(filenm,nfile,nr,natom,cnam,max_lines_input)
!
       return
!
    END SUBROUTINE PDB_READ_COORDS          
!          
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE PDB_GET_NRESID(filenm,nr,nmax)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
     USE CIF_DATA_M
!
     IMPLICIT NONE
!
     character(LEN=132) :: filenm
     integer :: nr, n5, nold, nw, i, nmax, io_err, ncode, ios
     character(LEN=cif_line_l) :: string
     character(LEN=cif_word_l), dimension(40) :: word
     character(LEN=cif_word_l) :: boff
     character(LEN=1) :: cold= ' ', cnow=' '
     LOGICAL	:: chainid_flag
!
!
     open(14,file=filenm,status='old',form='formatted',iostat=ios)
     if(ios .gt. 0) then
           write(*,*)
           write(*,*)'error open file ', filenm(1:len_trim(filenm))
           write(*,*)
           stop
     end if
!
     nr=0
     nold=nmax
     ncode=0
!
     do i=1, nmax
          read(14,fmt='(a)',iostat=ios) string
          if (ios .lt.0) exit
          if (i.eq.nmax) then
             PRINT*, '    '
             PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
             PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
             PRINT*, '>>>>>>> ', filenm(1:len_trim(filenm))
             PRINT*, '>>>>>>>  IN SUBROUTINE GET_NRESID                    '
             PRINT*, '>>>>>>>  POSSIBLY INCREASE MAX_LINES_INPUT           '
             PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
             PRINT*, '    '
             STOP
          end if
!
          if (string(1:6)=='ATOM  '.or.string(1:6)=='HETATM') then
!
             call pdb_parser_explicit(string, word, nw)
!   
!   ATOM OR HETATM RECORD              = word(1)
!   ATOM SERIAL NUMBER                 = word(2)
!   ATOM NAME                          = word(3)
!   ALTERNATIVE CONFORMATION INDICATOR = word(4)
!   RESIDUE NAME                       = word(5)
!   CHAIN IDENTIFIER                   = word(6)
!   RESIDUE SEQUENCE NUMBER            = word(7)
!   CODE FOR INSERTION OF RESIDUES     = word(8)
!   ORTHOGONAL X-COORDINATE            = word(9)
!   ORTHOGONAL Y-COORDINATE            = word(10)
!   ORTHOGONAL Z-COORDINATE            = word(11)
!   OCCUPANCY                          = word(12)
!   TEMPERATURE FACTOR                 = word(13)
!   ELEMENT SYMBOL                     = word(14)
!   CHARGE ON THE ATOM                 = word(15)
!
             boff = word(6)
             cnow = boff(1:1)
             read(word(7),*,iostat=io_err) n5
             if (io_err > 0 ) then
                  ncode=3
                  call flame(string,ncode)
             end if
!            
             if (n5 /= nold .or. cnow /= cold) then
                 nold = n5
                 cold = cnow   
                 nr = nr+1
             end if
          end if
     end do
!
     close(14)
     return
!
     END SUBROUTINE PDB_GET_NRESID
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE PDB_GET_NATOM(filenm,nr,natom,cnam,nmax)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE CIF_DATA_M
!
    IMPLICIT NONE
    integer :: nr, i, nw, n5, nat, nx, nmax, ios
    integer :: nold, io_err, ncode
    logical :: chainid_flag
    integer, dimension(nr) :: natom
    character(LEN=4), dimension(nr) :: cnam
    character(LEN=cif_word_l):: boff, baff
    character(LEN=132) :: filenm
    character(LEN=cif_line_l) :: string
    character(LEN=cif_word_l), dimension(40) :: word
    character(LEN=1) :: cold=' ', cnow=' '
!
    open(14,file=filenm,status='old',form='formatted')
!
    do i=1,nr
         natom(i)=0
         cnam(i)='    '
    end do
!
    nold= nmax
    nx = 0
    nat = -100
    ncode = 0
!
    do i=1,nmax
         read(14,fmt='(a)',iostat=ios) string
          if (ios .lt.0) exit
          if (i.eq.nmax) then
             PRINT*, '    '
             PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
             PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
             PRINT*, '>>>>>>> ', filenm(1:len_trim(filenm))
             PRINT*, '>>>>>>>  IN SUBROUTINE GET_NRESID                    '
             PRINT*, '>>>>>>>  POSSIBLY INCREASE MAX_LINES_INPUT           '
             PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
             PRINT*, '    '
             STOP
          end if
!
          if (string(1:6)=='ATOM  '.or.string(1:6)=='HETATM') then
!
            call pdb_parser_explicit(string, word, nw)
!   
!   ATOM OR HETATM RECORD              = word(1)
!   ATOM SERIAL NUMBER                 = word(2)
!   ATOM NAME                          = word(3)
!   ALTERNATIVE CONFORMATION INDICATOR = word(4)
!   RESIDUE NAME                       = word(5)
!   CHAIN IDENTIFIER                   = word(6)
!   RESIDUE SEQUENCE NUMBER            = word(7)
!   CODE FOR INSERTION OF RESIDUES     = word(8)
!   ORTHOGONAL X-COORDINATE            = word(9)
!   ORTHOGONAL Y-COORDINATE            = word(10)
!   ORTHOGONAL Z-COORDINATE            = word(11)
!   OCCUPANCY                          = word(12)
!   TEMPERATURE FACTOR                 = word(13)
!   ELEMENT SYMBOL                     = word(14)
!   CHARGE ON THE ATOM                 = word(15)
!
!
            boff= word(5)
            baff= word(6)
            cnow= baff(1:1)
            read(word(7),*,iostat=io_err) n5
!            
            if (io_err > 0 ) then
                 ncode=3
                 call flame(string,ncode)
            end if
!            
            if (n5 /= nold .or. cnow /= cold) then
              nold=n5
              cold=cnow   
              nx=nx+1
              nat=1
              natom(nx)=nat
              cnam(nx)=boff(1:4)
!            
            else if (n5 == nold .and. cnow == cold) then
              nat=nat+1
              natom(nx)=nat
!            
            else 
              STOP 'SOMETHING TERRIBLY WRONG IN GET_NATOM'
!            
            end if
!
          end if
    end do
!
    close(14)
!
    return
!
	END SUBROUTINE PDB_GET_NATOM
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE PDB_READ_PDB(filenm,nn,nr,natom,cnam,nmax)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
       USE MOLECULE_SIMP_DATA_M
       USE CIF_DATA_M
!
       IMPLICIT NONE
       INTEGER       :: nn                ! defines the molecule (sim(nn) to store the pdb on
       INTEGER       :: nmax, nr, ios     ! nr = number of resdues
       INTEGER       :: i, j, k
       character(LEN=132) :: filenm
       integer, dimension(nr) :: natom
       character(LEN=4), dimension(nr) :: cnam
       REAL          :: x, y, z, bfa, occ
       INTEGER       :: atnum, resnum
!
       INTEGER                                     :: nw
       CHARACTER(LEN=cif_word_l)                   :: wtemp
       CHARACTER(LEN=cif_word_l), DIMENSION(40)    :: word
       CHARACTER(LEN=cif_line_l)                   :: string
       LOGICAL                                     :: chainid_flag
!        
        if (nn>size(sim)) then
          write(*,*)
          write(*,*)'dimension of sim out of range in SUBROUTINE READPDB'
          write(*,*)
          stop
        end if
!
        sim(nn)%mol_nrs=nr
        if (allocated(sim(nn)%mol_res)) deallocate(sim(nn)%mol_res)
        allocate (sim(nn)%mol_res(nr))
!        
        do i=1,nr
          sim(nn)%mol_res(i)%res_nats=natom(i)
          sim(nn)%mol_res(i)%res_flag=.true.
          if (allocated(sim(nn)%mol_res(i)%res_ats)) deallocate(sim(nn)%mol_res(i)%res_ats)
          allocate (sim(nn)%mol_res(i)%res_ats(natom(i)))
          sim(nn)%mol_res(i)%res_name=cnam(i)
!            write(*,*) 'rest nr. ', i       
!            write(*,*) 'name     ', sim(nn)%mol_res(i)%res_name       
!            write(*,*) 'atome    ', sim(nn)%mol_res(i)%res_nats
        end do
!
        open(15,file=filenm,form='formatted',status='unknown', iostat=ios)
!
        do i=1, nr
          j=0
          do while (j < sim(nn)%mol_res(i)%res_nats) 
            read(15,'(a)',iostat=ios) string
            if (string(1:6)/='ATOM  '.and.string(1:6)/='HETATM') then
                 cycle      
            end if
!       
            j = j+1
            call pdb_parser_explicit(string, word, nw)
!
!   ATOM OR HETATM RECORD              = word(1)
!   ATOM SERIAL NUMBER                 = word(2)
!   ATOM NAME                          = word(3)
!   ALTERNATIVE CONFORMATION INDICATOR = word(4)
!   RESIDUE NAME                       = word(5)
!   CHAIN IDENTIFIER                   = word(6)
!   RESIDUE SEQUENCE NUMBER            = word(7)
!   CODE FOR INSERTION OF RESIDUES     = word(8)
!   ORTHOGONAL X-COORDINATE            = word(9)
!   ORTHOGONAL Y-COORDINATE            = word(10)
!   ORTHOGONAL Z-COORDINATE            = word(11)
!   OCCUPANCY                          = word(12)
!   TEMPERATURE FACTOR                 = word(13)
!   ELEMENT SYMBOL                     = word(14)
!   CHARGE ON THE ATOM                 = word(15)
!
            wtemp = word(6)
            sim(nn)%mol_res(i)%res_chid=wtemp(1:1)
!       
            if (j==1) then
               read(word(7),*) resnum
               sim(nn)%mol_res(i)%res_nold=resnum
            end if
!       
            wtemp=word(3)
            sim(nn)%mol_res(i)%res_ats(j)%at_name=wtemp(1:4)
!
            wtemp=word(4)
            sim(nn)%mol_res(i)%res_ats(j)%at_alt=wtemp(1:1)
!            
            read(word(2),*)   atnum
            read(word(9),*)   x
            read(word(10),*)  y
            read(word(11),*)  z
            read(word(12),*)  occ
            read(word(13),*)  bfa
!    
            sim(nn)%mol_res(i)%res_ats(j)%at_num = atnum
            sim(nn)%mol_res(i)%res_ats(j)%at_xyz(1) = x
            sim(nn)%mol_res(i)%res_ats(j)%at_xyz(2) = y
            sim(nn)%mol_res(i)%res_ats(j)%at_xyz(3) = z
            sim(nn)%mol_res(i)%res_ats(j)%at_bfa = bfa
            sim(nn)%mol_res(i)%res_ats(j)%at_occ = occ
!
            sim(nn)%mol_res(i)%res_ats(j)%at_flag = .true.
!                   
          end do
        end do 
!
        close(15)
! 
        return
!        
        END SUBROUTINE PDB_READ_PDB
!        
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE PDB_PARSER_EXPLICIT(LINE,WORD,NW)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE CIF_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER(LEN=1)                           ::   cblank=' '
	CHARACTER(LEN=cif_line_l)                  ::   line
	CHARACTER(LEN=cif_word_l)                  ::   new, nword
	CHARACTER(LEN=cif_word_l), DIMENSION(40)   ::   word
	LOGICAL, DIMENSION(40)                     ::   empty
	INTEGER          ::   nw, i, j, nblank
	LOGICAL, SAVE                              ::   first = .true. 
	LOGICAL                                    ::   flag_alt 
!
!'ATOM   5661 AOP1 NAP B1318      -8.357 -33.199  26.448  1.00 51.58           O'
!'123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
!'         1         2         3         4         5         6         7         8         9'
!
! word(1)  =  ATOM OR HETATM RECORD              
! word(2)  =  ATOM SERIAL NUMBER                 
! word(3)  =  ATOM NAME                          
! word(4)  =  ALTERNATIVE CONFORMATION INDICATOR 
! word(5)  =  RESIDUE NAME                       
! word(6)  =  CHAIN IDENTIFIER                   
! word(7)  =  RESIDUE SEQUENCE NUMBER            
! word(8)  =  CODE FOR INSERTION OF RESIDUES     
! word(9)  =  ORTHOGONAL X-COORDINATE            
! word(10) =  ORTHOGONAL Y-COORDINATE            
! word(11) =  ORTHOGONAL Z-COORDINATE            
! word(12) =  OCCUPANCY                          
! word(13) =  TEMPERATURE FACTOR                 
! word(14) =  ELEMENT SYMBOL                     
! word(15) =  CHARGE ON THE ATOM                 
!
!
	do i=1,len(new)
        new(i:i)  = cblank
	    nword(i:i)= cblank
        end do
!
	do i=1,size(word)
	    word(i)=new
	    empty=.true.
	end do
!
!	CHECKING A FEW THINGS
    flag_alt = .false.
	if (line(17:17).ne.' ') then
      flag_alt = .true.
	end if
!
	if (line(27:27).ne.' ') then
	  PRINT*,'>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>>>'
	  PRINT*,'>>> SIDE CHAIN INSERTION CODE FOUND'
	  PRINT*,'>>> PLEASE RENUMBER PDB BEFORE CONTINUING'
	  PRINT*,'>>> MUST EXIT'
	  stop
	end if
!
!   ATOM OR HETATM RECORD 
	nword=new
	nword(1:6)=line(1:6)
	word(1)=nword
!
!	ATOM SERIAL NUMBER
	nword=new
	nword(1:5)=line(7:11)
	word(2)=nword
!
!	ATOM NAME
	nword=new
	nword(1:4)=line(13:16)
	word(3)=nword
!
!	ALTERNATIVE LOCATION INDICATOR
	nword=new
	nword(1:1)=line(17:17)
	word(4)=nword
!
!	RESIDUE NAME
	nword=new
	nword(1:3)=line(18:20)
	word(5)=nword
!
!	CHAIN IDENTIFIER
	nword=new
	nword(1:1)=line(22:22)
	word(6)=nword
!
!	RESIDUE SEQUENCE NUMBER
	nword=new
	nword(1:4)=line(23:26)
	word(7)=nword
!
!	CODE FOR INSERTION OF RESIDUES
	nword=new
	nword(1:1)=line(27:27)
	word(8)=nword
!
!	ORTHOGONAL X-COORDINATE
	nword=new
	nword(1:8)=line(31:38)
	word(9)=nword
!
!	ORTHOGONAL Y-COORDINATE
	nword=new
	nword(1:8)=line(39:46)
	word(10)=nword
!
!	ORTHOGONAL Z-COORDINATE
	nword=new
	nword(1:8)=line(47:54)
	word(11)=nword
!
!	OCCUPANCY
	nword=new
	nword(1:6)=line(55:60)
	word(12)=nword
!
!	TEMPERATURE FACTOR
	nword=new
	nword(1:6)=line(61:66)
	word(13)=nword
!
!	ELEMENT SYMBOL
	nword=new
	nword(1:2)=line(77:78)
          word(14)=nword
!
!	CHARGE ON THE ATOM 
	nword=new
	nword(1:2)=line(79:80)
	word(15)=nword
	nword=new	
!
!   Now get rid of leading blanks
!
	do i=1,size(word)
	   nword=word(i)
	   nblank=0
	   do j=1,len(nword)
	     if(nword(j:j)==cblank)then 
	       nblank=nblank+1
	       cycle
	     else
	       exit
	     end if
	   end do
!
	   if(nblank==len(nword)) then
	     empty(i)=.true.
	   else
	     empty(i)=.false.
	     do j=1,(len(nword)-nblank)
	       nword(j:j)=nword((nblank+j):(nblank+j))
	     end do
	   end if
!	   
	   word(i)=nword
	end do 
!
!      Now get rid of empty words
!!
!       nw = 0
!       do i= (nwnum-1), 1, -1
!          if (empty(i) .eqv. .true.) then
!	    do j=i,(nwnum-1)
!	     word(j)=word(j+1)
!	     empty(j)=empty(j+1)
!	    end do
!	  else 
!	    nw = nw+1
!	  end if
!       end do
!!
!			
!	PRINT*,'DEBUG3', nw
!	PRINT*, line
!	do i=1,nwnum
!	PRINT*, i, empty(i)
!	PRINT*,'##',word(i)
!	end do
!
    if (flag_alt.and.first) then
      first=.false.
	  PRINT*,'  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WARNING >>>>>>'
	  PRINT*,'  >>> ALTERNATIVE SIDE CHAIN ORIENTATIONS DETECTED          '
	  PRINT*,'  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WARNING >>>>>>'
	  PRINT*,'     '
    end if
!
    return
!
      END SUBROUTINE PDB_PARSER_EXPLICIT
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE SIMP_MOLECULE_REMOVE_ALT_CONFS(nn)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MOLECULE_SIMP_DATA_M
!
    IMPLICIT NONE
!
    INTEGER       :: i, j, k 
    INTEGER       :: nn, nalt, na, nct
    INTEGER       :: nmax_lab_alt = 10                 ! maximum of alternative conformations to 
                                                       ! be (un)reasonably expectable
    INTEGER, DIMENSION(:), allocatable :: list
!    
    LOGICAL            :: alt_flag, flag_new, first
    CHARACTER(LEN=1)   :: cblank = ' ' , ref_label
    CHARACTER(LEN=1), DIMENSION(:), allocatable  :: lab_alt
!
    CHARACTER(LEN=30)  :: string
    CHARACTER(LEN=4)   :: ref_atom
    REAL               :: ref_occ
!    
    TYPE (AT_T), DIMENSION(:), allocatable :: back            
!
    if (nn>size(sim)) then
      write(*,*)
      write(*,*)' Dimension of sim out-of-range in  '
      write(*,*)' SUBROUTINE SIMP_MOLECULE_REMOVE_ALT_CONFS '
      write(*,*)
      stop
    end if
!
!   determening the number of residues with alternative 
!
    nalt = 0 
    do i=1, size(sim(nn)%mol_res)
       alt_flag = .false.
       do j=1, size(sim(nn)%mol_res(i)%res_ats)
         if (sim(nn)%mol_res(i)%res_ats(j)%at_alt.ne.cblank) then     
           alt_flag = .true.
           exit
         end if 
       end do
       if (alt_flag) nalt = nalt + 1 
    end do        
!
!
    if (allocated(list)) deallocate(list)
    allocate(list(nalt))
!
    nalt = 0 
    do i=1, size(sim(nn)%mol_res)
       alt_flag = .false.
       do j=1, size(sim(nn)%mol_res(i)%res_ats)
         if (sim(nn)%mol_res(i)%res_ats(j)%at_alt.ne.cblank) then     
           alt_flag = .true.
           exit
         end if 
       end do
       if (alt_flag) then 
          nalt = nalt + 1 
          list(nalt) = i
       end if
    end do        
!
    if (nalt.ne.0) then 
      print*, '  ELIMINATING ALTERNATIVE CONFORMATIONS:   '
      print*, '  '
    end if
!
    na  = 0 
    do i=1,size(list)
!    
         na = list(i)
         print*, '  RESIDUE: ',sim(nn)%mol_res(na)%res_nold, sim(nn)%mol_res(na)%res_chid
         if (allocated(back)) deallocate(back)
         allocate(back(size(sim(nn)%mol_res(na)%res_ats)))         
!        copying up all residue/atom information                 
         do j=1,size(back)
             back(j)%at_name   = sim(nn)%mol_res(na)%res_ats(j)%at_name 
             back(j)%at_alt    = sim(nn)%mol_res(na)%res_ats(j)%at_alt  
             back(j)%at_xyz(1) = sim(nn)%mol_res(na)%res_ats(j)%at_xyz(1)  
             back(j)%at_xyz(2) = sim(nn)%mol_res(na)%res_ats(j)%at_xyz(2)  
             back(j)%at_xyz(3) = sim(nn)%mol_res(na)%res_ats(j)%at_xyz(3)  
             back(j)%at_bfa    = sim(nn)%mol_res(na)%res_ats(j)%at_bfa  
             back(j)%at_occ    = sim(nn)%mol_res(na)%res_ats(j)%at_occ  
             back(j)%at_num    = sim(nn)%mol_res(na)%res_ats(j)%at_num  
             back(j)%at_flag   = sim(nn)%mol_res(na)%res_ats(j)%at_flag 
         end do 
!
!        figuring out how many alternative conformations are there
!
         if (allocated(lab_alt)) deallocate(lab_alt)
         allocate(lab_alt(nmax_lab_alt))
         lab_alt = ' '
!
         nalt = 0
         do j=1,size(back)
            if (back(j)%at_alt.ne.cblank) then
               flag_new = .true.
               do k = 1, nalt
                  if (back(j)%at_alt.eq.lab_alt(k)) then 
                    flag_new = .false.
                    cycle
                  end if
               end do    
               if (flag_new) then       
                  nalt = nalt+1
                  lab_alt(nalt) = back(j)%at_alt
               end if   
            end if
         end do 
!
         string= '                              '
!                 123456789012345678901234567890
!
         do k= 1, nalt
           string(((2*k)-1):((2*k)-1)) = lab_alt(k) 
         end do
!                  
         write(*,'(2A)') '   CONFORMATIONS OBSERVED: ', string
!
!        figuring out the conformation with the highest occupancy
!
!         do j=1,size(back)
!            print*, back(j)%at_name, back(j)%at_occ, back(j)%at_alt
!         end do 
!
         ref_atom  = '    '
         ref_occ   = 99
         ref_label = '&'
         first = .true.
         do j=1, nalt
             do k=1, size(back)
                if (back(k)%at_alt.eq.lab_alt(j)) then
                   if (first) then                  ! pick the first atom of the first conform.
                      ref_atom  = back(k)%at_name
                      ref_occ   = back(k)%at_occ
                      ref_label = back(k)%at_alt
                      first=.false.
                      exit
                   end if   
                   if (back(k)%at_name == ref_atom) then ! search for the identical atom in other conf.
                      if (back(k)%at_occ.gt.ref_occ) then
                         ref_label = back(k)%at_alt
                         ref_occ   = back(k)%at_occ
                         exit
                      end if   
                   end if
                end if  
             end do
         end do
!
!        flagging the rotamers that have to go and keeping the selected one
!
         write(*,'(3A,F5.2)') '   KEEPING CONFORMATION ', ref_label,'; OCCUPANCY= ', ref_occ 
         print*, '   ' 
!
         do j=1, nalt
             do k=1, size(back)
                if (back(k)%at_alt.eq.lab_alt(j)) then
                   if (back(k)%at_alt.eq.ref_label) then 
                      back(k)%at_alt = ' '
                      back(k)%at_flag = .true.
                   else
                      back(k)%at_flag = .false.
                   end if
                end if  
             end do
         end do
!
!       
        nct = 0
        do j=1, size(back)
          if (back(j)%at_flag) nct = nct+1
        end do
!
        if (allocated(sim(nn)%mol_res(na)%res_ats)) deallocate(sim(nn)%mol_res(na)%res_ats)
        allocate(sim(nn)%mol_res(na)%res_ats(nct))
        sim(nn)%mol_res(na)%res_nats = nct
!
!       copying everything back
!
        nct = 0 
        do j=1, size(back) 
            if (back(j)%at_flag) then 
               nct = nct+1
               sim(nn)%mol_res(na)%res_ats(nct)%at_name   = back(j)%at_name  
               sim(nn)%mol_res(na)%res_ats(nct)%at_alt    = back(j)%at_alt   
               sim(nn)%mol_res(na)%res_ats(nct)%at_xyz(1) = back(j)%at_xyz(1)
               sim(nn)%mol_res(na)%res_ats(nct)%at_xyz(2) = back(j)%at_xyz(2)
               sim(nn)%mol_res(na)%res_ats(nct)%at_xyz(3) = back(j)%at_xyz(3)
               sim(nn)%mol_res(na)%res_ats(nct)%at_bfa    = back(j)%at_bfa   
               sim(nn)%mol_res(na)%res_ats(nct)%at_occ    = back(j)%at_occ   
               sim(nn)%mol_res(na)%res_ats(nct)%at_num    = back(j)%at_num   
               sim(nn)%mol_res(na)%res_ats(nct)%at_flag   = back(j)%at_flag  
            end if
        end do
!         
      end do   
!
!!     DEBUG
!      do i=1, size(sim(nn)%mol_res)
!         print*, sim(nn)%mol_res(i)%res_nold, sim(nn)%mol_res(i)%res_name, sim(nn)%mol_res(i)%res_flag, size(sim(nn)%mol_res(i)%res_ats)            
!      end do
!!     END DEBUG
!    print*, list
!
!    stop
!    
    END SUBROUTINE SIMP_MOLECULE_REMOVE_ALT_CONFS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE WRITE_SIMP_PDB(file,i)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MOLECULE_SIMP_DATA_M
!
    IMPLICIT NONE
!    
    CHARACTER(LEN=132)  :: file
    CHARACTER(LEN=60)   :: fm1,fm2,fm3
    CHARACTER(LEN=80)   :: string
    CHARACTER(LEN=6)    :: atom
    CHARACTER(LEN=4)    :: name_in, name_out    
    INTEGER             :: i, k, j, l, nat
!
    fm1="(a6,i5,2x,a3,a1,a3,x,a,i4,4x,3f8.3,2f6.2)" ! pdb-format for formatted reading
    fm2="(a6,i5,x,a4,a1,a3,x,a,i4,4x,3f8.3,2f6.2)"  ! pdb-format for formatted reading
    fm3="(a,10x,a1,x,i5,x,a3)"
!    fm3="(a1,2x,i4,2x,a3)" ! format of neigh.lis
    atom="ATOM  "
!
    open(15,file=file,form='formatted',status='unknown')    
!
    nat = 0
    do k=1, size(sim(i)%mol_res)
     if (.not.sim(i)%mol_res(k)%res_flag) cycle
!     
     name_in  = sim(i)%mol_res(k)%res_name
     name_out = '    '     
     if (len_trim(name_in)==1) then 
         name_out(3:3) = name_in(1:1)
     else if (len_trim(name_in)==2) then
         name_out(2:3) = name_in(1:2)
     else     
         name_out(1:4) = name_in(1:4)
     end if
!     
     do j=1,sim(i)%mol_res(k)%res_nats
      if (sim(i)%mol_res(k)%res_ats(j)%at_flag) then
       nat = nat + 1
       if (sim(i)%mol_res(k)%res_ats(j)%at_name(4:4)==' ') then
!       write(15,fmt=fm1) atom,sim(i)%mol_res(k)%res_ats(j)%at_num            &
       write(15,fmt=fm1) atom, nat                                           &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_name(1:3)    &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_alt(1:1)     &
!        &                      ,sim(i)%mol_res(k)%res_name                   &
        &                      ,name_out                                     &
        &                      ,sim(i)%mol_res(k)%res_chid                   &
        &                      ,sim(i)%mol_res(k)%res_nold                   &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_xyz(1)       &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_xyz(2)       &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_xyz(3)       &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_occ          &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_bfa 
       else 
!       write(15,fmt=fm2) atom,sim(i)%mol_res(k)%res_ats(j)%at_num            &
       write(15,fmt=fm2) atom, nat                                           &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_name(1:4)    &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_alt(1:1)     &
!        &                      ,sim(i)%mol_res(k)%res_name                   &
        &                      ,name_out                   &
        &                      ,sim(i)%mol_res(k)%res_chid                   &
        &                      ,sim(i)%mol_res(k)%res_nold                   &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_xyz(1)       &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_xyz(2)       &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_xyz(3)       &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_occ          &
        &                      ,sim(i)%mol_res(k)%res_ats(j)%at_bfa 
       end if
      end if 
     end do
    end do 
    write(15,fmt="(A3)")'TER'
    close(15)
!    
    END SUBROUTINE WRITE_SIMP_PDB
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    SUBROUTINE CIF_MINWORDS(word,nw)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE CIF_DATA_M
!
	IMPLICIT NONE
!	
	integer       nw, i, j, k
	integer       nmaxchar
	character*42  max_abc, min_abc
	character(len=cif_word_l)  new, word(nw)
!
	data nmaxchar/42/
!
	data max_abc/'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-/!$_'/
	data min_abc/'abcdefghijklmnopqrstuvwxyz0123456789.-/!$_'/
!	              1234567890123456789012345678901234567890123
!
	do i=1,nw
	  do j=1,len(new)
            new(j:j)=' '
	  end do
 	  new=word(i)
	  do j=1,len(new)
	     do k=1,nmaxchar
	        if ( new(j:j)== max_abc(k:k)) then
                     new(j:j)= min_abc(k:k)
	        end if
          end do
	  end do
	  word(i)=new
	end do
!	
	END SUBROUTINE CIF_MINWORDS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
