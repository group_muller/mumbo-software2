!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
        SUBROUTINE ENERGIZE(set1,set2,en,first1,first2)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	real, dimension(15) :: en       
	integer, dimension(4)  :: set1, set2
	logical:: first1, first2
!
	integer,   dimension (:),   allocatable   ::  mlc
!
	integer,   dimension (:,:),   allocatable   ::  hd
	integer,   dimension (:,:),   allocatable   ::  ab
!
	real,      dimension (:),   allocatable   ::  xyz1, xyz2, xyz3
	real,      dimension (:),   allocatable   ::  rprob 
	real,      dimension (:),   allocatable   ::  q 
	real,      dimension (:),   allocatable   ::  eps, sig
	real,      dimension (:),   allocatable   ::  sgr, sgf, svo, sla
	real,      dimension (:),   allocatable   ::  ede
!
	integer, dimension(:,:), allocatable :: con_at
	integer, dimension(:,:), allocatable :: p_int
	real, dimension(:,:), allocatable :: p_real
!
	integer    :: na1,  na2, ntot, n
	integer    :: n1, n2, n3, n4, m1, m2, m3, m4, o1, o2, o3, o4, o5
	integer    :: i, j, k, num1, num2
!
	character (len=4) :: aan1
	character (len=4) :: atn2
	character (len=4) :: c_at, c_ab
	character (len=1) :: c_hb
	logical :: cflag, hd1_flag, hd2_flag, ab1_flag, ab2_flag, hflag, aflag
	logical :: check = .false.
!
	check=.false.
!
	m1=set1(1)
	m2=set1(2)
	m3=set1(3)
	m4=set1(4)
!
	n1=set2(1)
	n2=set2(2)
	n3=set2(3)
	n4=set2(4)
!
!DEBUG
!	print*, 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
!	print*, m1, m2, m3, m4, first1, first2
!
!       call dump_pdb (m1, m2, m3, m4)
!
!	print*, '-----------------------------------'
!	print*, n1, n2, n3, n4
!
!	call dump_pdb (n1, n2, n3, n4)
!!
!	print*, 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
!
!	if (m1==2.and.m2==1.and.m3==1.and.m4==1) then
!	   if (n1==1.and.n2==40.and.n3==1.and.n4==1) then
!	      check=.true.
!	   print*, 'check=', check
!	   print*, '     '
!	   call dump_pdb_atflag(m1,m2,m3,m4)
!	   print*, '     '
!	   call dump_pdb_atflag(n1,n2,n3,n4)
!	   print*, '     '
!           end if
!	end if
!
!!	if (m1==4.and.m2==1.and.m3==1.and.m4==1) then
!	   if (n1==1.and.n2==1.and.n3==1.and.n4==1) then
!	      check=.true.
!	   print*, 'check=', check
!        end if
!	end if
!DEBUG
!
	na1  =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	na2  =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat
	ntot =  na1+na2
!
	if (allocated(mlc)) deallocate(mlc)
	allocate(mlc(ntot))
!
	if (allocated(hd)) deallocate(hd)
	allocate(hd(ntot,3))
	if (allocated(ab)) deallocate(ab)
	allocate(ab(ntot,3))
!
	if (allocated(rprob)) deallocate(rprob)
	allocate(rprob(ntot))
	if (allocated(q))     deallocate(q)
	allocate(q(ntot))
	if (allocated(eps))   deallocate (eps)
	allocate(eps(ntot))
	if (allocated(sig))   deallocate(sig)
	allocate(sig(ntot))
	if (allocated(sgr))   deallocate(sgr)
	allocate(sgr(ntot))
	if (allocated(sgf))   deallocate(sgf)
	allocate(sgf(ntot))
	if (allocated(svo))   deallocate(svo)
	allocate(svo(ntot))
	if (allocated(sla))   deallocate(sla)
	allocate(sla(ntot))
	if (allocated(xyz1))  deallocate(xyz1)
	allocate(xyz1(ntot))
	if (allocated(xyz2))  deallocate(xyz2)
	allocate(xyz2(ntot))
	if (allocated(xyz3))  deallocate(xyz3)
	allocate(xyz3(ntot))
	if (allocated(ede))   deallocate(ede)
	allocate(ede(ntot))
!
	if (allocated(con_at)) deallocate(con_at)
	allocate(con_at(ntot,ntot))
! 
	n=0
	do i=1, na1
	  n= n+1
	  mlc(n)=   m1
	  rprob(n)= mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_prob
	  q(n)=     mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_cha
	  eps(n)=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_eps
	  sig(n)=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_sig
	  sgr(n)=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_sgr
	  sgf(n)=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_sgf
	  svo(n)=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_svo
	  sla(n)=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_sla
	  xyz1(n)=  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	  xyz2(n)=  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	  xyz3(n)=  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
	  ede(n)=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_ede
	end do 
!
	do i=1, na2
	  n= n+1
	  mlc(n)=   n1
	  rprob(n)= mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_prob
	  q(n)=     mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_cha
	  eps(n)=   mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_eps
	  sig(n)=   mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_sig
	  sgr(n)=   mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_sgr
	  sgf(n)=   mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_sgf
	  svo(n)=   mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_svo
	  sla(n)=   mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_sla
	  xyz1(n)=  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_xyz(1)
	  xyz2(n)=  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_xyz(2)
	  xyz3(n)=  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_xyz(3)
	  ede(n)=   mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_ede
	end do 
!
!	Now create a complete connectivity table
!
	do i=1, ntot
	 do j=1, ntot
	   con_at(i,j) = 9
	   if (i==j) con_at(i,j) = 1 
	 end do
	end do
!
!!DEBUG
!    if (check) then
!      PRINT*, 'CON_ALL-1', set1, set2
!      do i=1,ntot
!        write(*,'(80i2)')(con_at(i,j),j=1,ntot)
!      end do
!    end if
!!DEBUG
!
	do i=1, ntot
	if (i <= na1) then 
	  do k=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_n14
	   o1= mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_c14(k,1)
	   o2= mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_c14(k,2)
	   o3= mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_c14(k,3)
	   o4= mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_c14(k,4)
	   o5= mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_ats(i)%at_c14(k,5)
	   if (o1==m1.and.o2==m2.and.o3==m3) then
	      con_at(i,o4) = o5
	   end if 
	   if (o1==n1.and.o2==n2.and.o3==n3) then
	      con_at(i,(na1+o4)) = o5 
	   end if
	  end do
	else 
	  do k=1,mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_n14
	   o1= mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_c14(k,1)
	   o2= mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_c14(k,2)
	   o3= mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_c14(k,3)
	   o4= mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_c14(k,4)
	   o5= mol(n1)%res(n2)%res_aas(n3)%aa_rots(1)%rot_ats(i-na1)%at_c14(k,5)
	   if (o1==m1.and.o2==m2.and.o3==m3) then
	      con_at(i,o4) = o5
	   end if 
	   if (o1==n1.and.o2==n2.and.o3==n3) then
	      con_at(i,(na1+o4)) = o5 
	   end if
	  end do
	end if 
	end do
!
	cflag=.false.
	do i = 1,ntot
	  do j =i,ntot
	    if (con_at(i,j) /= con_at(j,i)) cflag=.true.
	  end do
	end do
!	
	if (cflag) then 
	  PRINT*, '          '
	  PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>> '
	  PRINT*, '   CONNECTIVITY TABLE IS NOT SYMMETRIC    '
	  PRINT*, '   FOR INTERACTION BETWEEN :              '
	  PRINT*, '   ',m1, m2, m3, m4  
	  PRINT*, '   ',n1, n2, n3, n4  
	  PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>> '
 	  PRINT*, '          '
	  do i=1,ntot
!GFORTRAN    write(*,'(<ntot>i2)')(con_at(i,j),j=1,ntot)
 	     write(*,*)(con_at(i,j),j=1,ntot)
	  end do
	  STOP
	end if
! 
!!DEBUG
!      if (check) then 
!	     PRINT*, 'CON_ALL_0'
!	     do i=1,ntot
!	       write(*,'(80i2)')(con_at(i,j),j=1,ntot)
!	     end do
!	  end if
!!DEBUG
!
!
     if (check) then 
	   print*, '     '
	   call dump_pdb_atflag(m1,m2,m3,m4)
	   print*, '     '
	   call dump_pdb_atflag(n1,n2,n3,n4)
	   print*, '     '
     end if
!
!	Now figuring out h-bond stuff
!	Two arrays have to be filled, one related to the hydrogen, donor, donor-1,
!	the second related to acceptor, acceptor-1, acceptor-2 	  
!
	do i=1, ntot
	  do j=1,3
	    hd(i,j) = 0
	    ab(i,j) = 0
	  end do
	end do
!
	hflag=.false.
	aflag=.false.
!
	if (hbond_flag) then
!
	 do i=1,ntot
	  if (i<=na1) then
	      c_hb= mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_hb 
	  else
	      c_hb= mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i-na1)%at_hb 
	  end if 
!
!     if (check) then
!      PRINT*,  h_sp_name, 'XX', h2_sp_name, 'XX', h3_sp_name    
!     end if
!
      if (c_hb == h_sp_name .or. c_hb == h2_sp_name .or. c_hb == h3_sp_name ) then
        hd(i,1) = 1
        hd(i,2) = 0
        hd(i,3) = 0
        hflag=.true.
      else if (c_hb == d_sp3_name) then 
        hd(i,1) = 0 
        hd(i,2) = 1
        hd(i,3) = 0
      else if (c_hb == d_sp2_name) then
        hd(i,1) = 0 
        hd(i,2) = 1
        hd(i,3) = 1
      else if (c_hb == a_sp3_name) then 
        ab(i,1) = 1
        ab(i,2) = 0
        ab(i,3) = 0
        aflag=.true.
      else if (c_hb == a_sp2_name) then 
        ab(i,1) = 1
        ab(i,2) = 1
        ab(i,3) = 0
        aflag=.true.
      else if (c_hb == b_sp3_name) then 
        hd(i,1) = 0 
        hd(i,2) = 1
        hd(i,3) = 0
        ab(i,1) = 1
        ab(i,2) = 0
        ab(i,3) = 0
        aflag=.true.
      else if (c_hb == b_sp2_name) then 
        hd(i,1) = 0 
        hd(i,2) = 1
        hd(i,3) = 1
        ab(i,1) = 1
        ab(i,2) = 1
        ab(i,3) = 0
        aflag=.true.
      end if 
     end do 
!
	end if           ! hbond_flag
!
! No need for further processing if no polar hydrogens or potential acceptors are present
! Figuring out the position of donor, donor-1, acceptor, acceptor-1, acceptor-2
!
!!   DEBUG
!    if (check) then
!      do i=1, ntot
!      print *, i, hd(i,1), hd(i,2), hd(i,3)
!!      print *, i, ab(i,1), ab(i,2), ab(i,3)
!      end do
!    end if
!!   END DEBUG    
!
    if (hflag.and.aflag) then 
!
    do i=1,ntot
      if (hd(i,1)==1) then
        if (check) print*, i
        hd1_flag=.true.
        hd2_flag=.true.
        do j=1,ntot
         if (hd(j,1)==0.and.hd(j,2)==1.and.con_at(i,j)==2) then
!         
           if (hd(j,3)==1) then               !    looking up donor -1 atom
!           
             if (j<= na1) then                !    looking up donor -1 in first set
               c_ab = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_ab
!               print*,' DEBUG21 ',  c_ab, ' C_HD'
               do k=1,na1 
                 c_at = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_nam
!                print*, c_at, c_ab, con_at(i,k)
                 if (con_at(i,k)==3.and.c_ab==c_at) then
                     hd(i,3)= k
                     hd2_flag=.false.
                 end if 
               end do
             else                             !     looking up donor-1 in second set
               c_ab = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j-na1)%at_ab
!               print*, c_ab, ' C_HD'
               do k=1,na2 
                 c_at = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(k)%at_nam
!                print*, c_at, c_ab, con_at(i,na1+k)
                 if (con_at(i,(na1+k))==3.and.c_ab==c_at) then
                     hd(i,3)= k+na1
                     hd2_flag=.false.
                 end if
               end do
             end if
!             
             hd(i,2)=j
             hd1_flag=.false.
!             
           else                              ! only donor atom is needed; no need to search for donor -1
             hd(i,2)=j
             hd(i,3)=0
             hd1_flag=.false.
             hd2_flag=.false.
           end if
!           
         end if
        end do
!
	    if (hd1_flag.or.hd2_flag) then 
	  PRINT*, '          '
	  PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>    '
	  PRINT*, '   FAILED TO FIND DONOR, DONOR-1 FOR HYDROGEN    '
	  PRINT*, '   ATOM: ', i, ' EITHER IN ', hd1_flag, hd2_flag 
	  PRINT*, '   ',m1, m2, m3, m4  , ' OR '
	  PRINT*, '   ',n1, n2, n3, n4 , 'IN RESIDUE:  '
	  PRINT*, '   ',mol(m1)%res(m2)%res_aas(m3)%aa_nam, ' OR '
	  PRINT*, '   ',mol(n1)%res(n2)%res_aas(n3)%aa_nam
 	  PRINT*, '          '
 	  PRINT*, '   CHECK CONNECTIVITY OR RESIDUE PARAMETER FILE '
	  PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>> '
 	  PRINT*, '          '
		STOP 
	    end if
!
	   end if
	   if (ab(i,1) == 1) then 
	     ab1_flag=.true.
	     ab2_flag=.true.
	     if (ab(i,2) == 0) then 
		  do j=1,ntot
!                   HERE THE FOLLOWING LINE WAS EDITED SO THAT WATER MOLECULES DON'T NEED TO BE ATTACHED TO ROTAMERS IN THE CONNECTIVITY FILE ANYMORE...	  
!		    if (con_at(i,j) == 2 .and. hd(j,1)==0) then 
		    if (con_at(i,j) == 2) then 
		      ab(i,2) = j
			ab1_flag=.false.
		    end if
		  end do
		  ab(1,3) = 0
!MARK:		  ab(1,3) = 0!????????????????FALSCH!?  YAM: Why???  
		  ab2_flag=.false.
	     else if (ab(i,2) == 1) then
		  if(i<=na1) then 
		    c_ab = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_ab
!		    print*, c_ab, ' C_AB'
		    do k=1,na1 
			  c_at = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_nam
!		          print*, c_at, c_ab, con_at(i,k)
			  if (con_at(i,k)==2.and.c_ab==c_at) then
			    ab(i,2)= k
			    ab1_flag=.false.
			  end if 
		    end do
		  else 
		    c_ab = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i-na1)%at_ab
!		    print*, c_ab, ' C_AB'
		    do k=1,na2 
			  c_at = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(k)%at_nam
!		          print*, c_at, c_ab, con_at(i,na1+k)
			  if (con_at(i,(na1+k))==2.and.c_ab==c_at) then
			    ab(i,2)= na1+k
			    ab1_flag=.false.
			  end if
		    end do
		  end if
		  do k=1,ntot
		    if (con_at(i,k)==3.and.hd(k,1)==0) then
			  ab(i,3) = k
			  ab2_flag=.false.
		    end if
		  end do
	     end if
	     if (ab1_flag.or.ab2_flag) then 
	  PRINT*, '          '
	  PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>    '
	  PRINT*, '   FAILED TO FIND ACCEPTOR-1, ACCEPTOR-2 FOR     '
	  PRINT*, '   ATOM: ', i, ' EITHER IN ' 
	  PRINT*, '   ',m1, m2, m3, m4  , ' OR '
	  PRINT*, '   ',n1, n2, n3, n4, 'IN RESIDUE:  '
	  PRINT*, '   ',mol(m1)%res(m2)%res_aas(m3)%aa_nam, ' OR '
	  PRINT*, '   ',mol(n1)%res(n2)%res_aas(n3)%aa_nam
 	  PRINT*, '          '
 	  PRINT*, '   CHECK CONNECTIVITY OR RESIDUE PARAMETER FILE '
	  PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>> '
 	  PRINT*, '          '
		  STOP 
	     end if
	  end if
	  end do
!
	end if   ! hflag and aflag
!
!   HAVING IDENTIFIED ALL ATOMS SOME FINE TUNING OF THE CONECTIVITY TABLE  
!   IS NEEDED BEFORE MOVING ON 
!
!   In case PRO is at the position to be mumboed then Gly H 
!   has to be removed from any con_at interaction list 
!
!DEBUG
!	if (n1==1.and.m1==2.and.m4==1) then
!	  PRINT*, 'CON_ALL_1'
!	  do i=1,ntot
!	    write(*,'(80i2)')(con_at(i,j),j=1,ntot)
!	  end do
!	end if
!DEBUG

!
	num1 =  mol(m1)%res(m2)%res_num
	aan1 =  mol(m1)%res(m2)%res_aas(m3)%aa_nam	
	num2 =  mol(n1)%res(n2)%res_num
	if (n1==1.and.aan1==pro_name.and.num1==num2) then 
	  do i=1,na2
	    atn2= mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_nam
	    if (atn2 == h_name) then
		do j=1,ntot
		  con_at(j,na1+i)= 1
		  con_at(na1+i,j)= 1
		end do
	    end  if
	  end do 
	end if 
!
!
!DEBUG
!	if (n1==1.and.m1==2.and.m4==1) then
!	  PRINT*, 'CON_ALL_2'
!	  do i=1,ntot
!	    write(*,'(80i2)')(con_at(i,j),j=1,ntot)
!	  end do
!	end if
!DEBUG
!
!
!  Remove any intra-molecular interactions within the second 'molecule' in order to avoid 
!  double counting or calculating intra-constant-part-interactions...
!
	do i=na1+1, ntot
	  do j=na1+1, ntot
	    con_at(i,j) =1
	  end do
	end do
!
!  Intramolecular interactions in the first molecule only have to be calculated once 
!  namely when interactions with the template are calculated (n1=1) and only in the very 
!  first call, namely when first1 = true
!
	if (.not.first1.or.n1/=1) then
	 do i=1, na1
	  do j=1, na1
	     con_at(i,j) = 1
	  end do
	 end do
	end if
!
!  This is needed in the analyse step, when calculating the rotamer self-energy
!  separately .... 
!
	if (first2) then
	 do i=1, ntot
	  do j=1, ntot
	     if (i > na1 .or. j > na1) then 
	       con_at(i,j) = 1
	     end if
	  end do
	 end do
	end if
!
!  Before moving on,check if there are still potential hydrogen-bonds left, if not no 
!  need to attempt calculating those
!
	if (hflag.and.aflag) then 
	  hflag=.false. 
	  aflag=.false.
	  do i=1,ntot
	   if (hd(i,1)==1) then 
		do j=1,ntot
		  if (ab(j,1)==1.and.con_at(i,j)>1) then 
			hflag=.true.
			aflag=.true.
		  end if 
		end do
	   end if 
	  end do 
	end if 
!
!	Now packing everything neatly up 
!
	if (allocated(p_int)) deallocate(p_int)
	allocate(p_int(ntot,7))
	if (allocated(p_real)) deallocate(p_real)
	allocate(p_real(ntot,12))
!
	do i=1,ntot
	  p_int(i,1) = mlc(i)
	  p_int(i,2) = hd(i,1)
	  p_int(i,3) = hd(i,2)
	  p_int(i,4) = hd(i,3)
	  p_int(i,5) = ab(i,1)
	  p_int(i,6) = ab(i,2)
	  p_int(i,7) = ab(i,3)
	  p_real(i,1)  = rprob(i)
	  p_real(i,2)  = q(i)    
	  p_real(i,3)  = eps(i)  
	  p_real(i,4)  = sig(i)  
	  p_real(i,5)  = sgr(i)   
	  p_real(i,6)  = sgf(i)   
	  p_real(i,7)  = svo(i)   
	  p_real(i,8)  = sla(i)   
	  p_real(i,9)  = xyz1(i)   
	  p_real(i,10) = xyz2(i) 
	  p_real(i,11) = xyz3(i)  
	  p_real(i,12) = ede(i)   
	end do
!
	do i=1,size(en)
	en(i)= 0
	end do
!
	if (hflag.and.aflag) then
		call calc_hbonds(ntot,p_int,p_real,con_at,en)
	end if
!
!	Now having calculated h-bonds interaction list needs further manipulation
!	for further processing 
!
	if (n1==1.and.first1) then
!
	 do i=1, ntot
	  do j=1, ntot
	   if (i>=j) con_at(i,j) = 1 
	  end do
	 end do
!
	end if 
!
	do i= (na1+1), ntot
	 do j= 1, ntot
	   con_at(i,j) = 1 
	 end do
	end do
!
!!DEBUG
!	if (n1==1.and.m1==2.and.m4==1) then
!	  PRINT*, 'CON_ALL_3'
!	  do i=1,ntot
!	    write(*,'(80i2)')(con_at(i,j),j=1,ntot)
!	  end do
!	end if
!!END DEBUG
!!
!!DEBUG
!   if (n1/=1.and.m1/=1) then 
!       print*, 'ENTERING ', set1, '   ##########   ',  set2
!       do i=1, ntot
!         print*, ' ##### ', i
!         print*, ' MLC',  p_int(i,1) 
!         print*, ' HD ',  p_int(i,2) 
!         print*, ' HD ',  p_int(i,3) 
!         print*, ' HD ',  p_int(i,4) 
!         print*, ' AB ',  p_int(i,5) 
!         print*, ' AB ',  p_int(i,6) 
!         print*, ' AB ',  p_int(i,7) 
!         print*, ' RPROP ',  p_real(i,1) 
!         print*, ' Q ',  p_real(i,2)    
!         print*, ' EPS ',  p_real(i,3)   
!         print*, ' SIG ',  p_real(i,4)   
!         print*, ' SGR ',  p_real(i,5)    
!         print*, ' SGF ',  p_real(i,6)    
!         print*, ' SVO ',  p_real(i,7)    
!         print*, ' SLA ',  p_real(i,8)    
!         print*, ' X1  ',  p_real(i,9)     
!         print*, ' X2  ',  p_real(i,10) 
!         print*, ' X3  ',  p_real(i,11)  
!         print*, ' EDE ',  p_real(i,12)
!       end do  
!     end if
!!       
!!END DEBUG
!!
	call calc_inter(ntot,na1,p_int,p_real,con_at,en)
!
!!DEBUG
!    if (n1/=1.and.m1/=1) print*, en
!    if (n1/=1.and.m1/=1) print*, 'EXITING ', set1, '   ##########   ', set2
!     print*, en
!     print*, 'EXITING ', set1, '   ##########   ', set2
!     
!    en(1) = 0
!    en(2) = 0
!    en(3) = en_vdw
!    en(4) = en_elec 
!    en(5) = en_rprob
!    en(6) = en_solv
!    en(7) = en_solv_ref
!    en(8) = en_xray
!    en(10) = en_solv_ana
!     
!!END DEBUG
!
	if (n1==1.and.first1) then
	    en(5)=en(5)
	    en(7)=en(7)
	    en(8)=en(8)
	else
	    en(5)=0
	    en(7)=0
	    en(8)=0
	end if		
!
	END SUBROUTINE ENERGIZE
!
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
        SUBROUTINE CALC_INTER(ntot,na1,p_int,p_real,con_at,en)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	real, dimension(15) :: en       
	integer    :: na1, ntot
!
	integer,   dimension(ntot)  ::  mlc
!
	real, dimension (ntot)  ::  xyz1, xyz2, xyz3
	real, dimension (ntot)  ::  rprob 
	real, dimension (ntot)  ::  q 
	real, dimension (ntot)  ::  eps, sig
	real, dimension (ntot)  ::  sgr, sgf, svo, sla
	real, dimension (ntot)  ::  ede
!
	integer, dimension(ntot,ntot)  :: con_at
!
	integer, dimension(ntot,7)  :: p_int
	real, dimension(ntot,12)  :: p_real
!
	real	:: en_vdw, en_elec, en_rprob, en_solv, en_solv_ref, en_solv_ana
	real	:: en_xray
	real  :: ap_dis, ap_sig, ap_eps, ap_elec, ap_vdw, ap_solv, ap_solv_ana
	real  :: xt, yt, zt, rt, st, tt
!	real, parameter :: pi = 3.1415927, p4s = 22.27331 
	real, parameter :: p4s = 22.27331 
	real, dimension(3) :: dis
	real    :: x1, x2, x3, x4, x5, rele
	real    :: beta, sigbet
	real, parameter   :: xi=1.5, mu=2.0 
!
    real    :: rdebug
!
	integer    :: n1 
	integer    :: i, j
!
!#######################################################################	
!VDW_SIG 


real               ::      ap_vdw_rmin
real               ::      ap_vdw_atr
real               ::      ap_vdw_rep

real               ::      vdw_switch_lin2norm_reci         !reciprocal (1/X)
real               ::      vdw_line_m                       !slope      (y=mx+b)
real               ::      vdw_line_b                       !intercept  (y=mx+b)
real               ::      vdw_line_y                       !

real               ::      vdw_norm2cos_dis                 !vdw_switch_norm2cos_sig * ap_sig
real               ::      vdw_stop_cos_dis                 !vdw_stop_cos_sig * ap_sig 


!ELEC
real, parameter    ::      PI = 3.14159265358979323846264338327950288419716939937510
real               ::      lambda
real               ::      ap_dis_reci                  !reciprocal (1/X)
real               ::      elec_con2cos_dis
real               ::      elec_cos2norm_dis
real               ::      ap_elec_temp
real               ::      qij                          !qij = q(i)*q(j)

!Sigma & eps werte mit Rosetta abgleichen ! MARK
!###################################################################
!
!	logical :: check
!
!
!	Initialising parameters which might never get initialised else (GFORTRAN)
!
	X2=0.0
	X4=0.0
	ap_vdw=0.0
!
!	Unpacking everything
!
	do i=1,ntot
	   mlc(i) =   p_int(i,1)  
	   rprob(i) = p_real(i,1)   
	   q(i)  =    p_real(i,2)    
	   eps(i)=    p_real(i,3)    
	   sig(i) =   p_real(i,4)   
	   sgr(i) =   p_real(i,5)    
	   sgf(i) =   p_real(i,6)    
	   svo(i) =   p_real(i,7)    
	   sla(i) =   p_real(i,8)    
	   xyz1(i) =  p_real(i,9)    
	   xyz2(i) =  p_real(i,10)  
	   xyz3(i) =  p_real(i,11)   
	   ede(i)  =  p_real(i,12)   
	end do
!
	if (na1==ntot) then
	     n1 = 0
	else if (na1.lt.ntot) then 
	     n1= mlc(na1+1)
	else 
	     PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   '   
	     PRINT*, ' >>>>>>> MUST STOP BECAUSE ntot <  na1 in calc_inter >>>>>>   '
	     PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   '   
	     STOP
	end if
!
!	NOW CALCULATE ENERGIES
!
	en_vdw   = 0
	en_elec  = 0
	en_rprob = 0
	en_solv  = 0
	en_solv_ref = 0
	en_solv_ana = 0
	en_xray = 0
!
!	EPROB EXRAY AND EN_SOLV_REF ONLY NEED TO BE CALCULATED ONCE NAMELY WHEN 
!	CALLED FROM MC_INTERACTIONS
!
	if (n1==1) then
!
!	THIS SHOULD BE MORE LIKE E = RT ln(Prob)....
!
	  if (rprob_flag) then
		en_rprob = -log(rprob(1))    ! HIER SOLLTE WAS GEMACHT WERDEN! So werden kleine AS bevorzugt! MARK
	  else
		en_rprob = 0
	  end if
!
	  if (solv_flag) then
	     do i=1, na1
		    en_solv_ref =  en_solv_ref + sgr(i)
	     end do	
	     en_solv_ref =  mumbo_en_solgref * en_solv_ref
	  else 
	     en_solv_ref = 0
	  end if
!
	  if (xray_flag) then
	     do i=1, na1
		    en_xray = en_xray + ede(i)
	     end do	
	     en_xray = - en_xray
	  else 
	     en_xray = 0
	  end if
!
	end if
!
!	DONE WITH EN_XRAY, EN_SOLV_REF, EN_RPROB
!
!	CALCULATING ATOM - ATOM INTERACTIONS NOW
!
	  if (elec_flag.and.eswitch_flag) then
	     x1= (mumbo_en_ctonnb - mumbo_en_ctofnb) 
	     x2= x1*x1*x1
	     x4= (3 * mumbo_en_ctonnb)
             x4= x4 - mumbo_en_ctofnb
	  end if
!
	do i=1,na1
!
	  do j= i+1, ntot
!
	   if (con_at(i,j) > 1) then
!
	     ap_dis = 0
	     dis(1) = xyz1(i) - xyz1(j)
	     dis(2) = xyz2(i) - xyz2(j)
	     dis(3) = xyz3(i) - xyz3(j)
!
	     ap_dis = sqrt(dot_product(dis,dis))
!
       if (vdw_flag) then
!        
         vdw_switch_lin2norm_reci = 1.0/vdw_switch_lin2norm     !ZEILE GEANDET MARK!
	     
         if (con_at(i,j) > mumbo_en_it_vdw) then
!         
!         safenet necessary for entries with sig(i) and eps(i) = 0
!
          if (((sig(i).lt.0.0001).and.(eps(i).lt.0.0001)).or.((sig(j).lt.0.0001).and.(eps(j).lt.0.0001))) then 
             ap_vdw = 0
          else      
!
           ap_sig = (sig(i) + sig(j)) / 2  
           ap_sig =  mumbo_en_svdw  * ap_sig
!
           ap_eps =  eps(i) * eps(j)
           ap_eps =  sqrt(ap_eps)
!
           if (mumbo_en_vdw_soft) then 
!
            beta = 2**0.166666666666
            beta = beta - 1
            beta = 1 - xi * beta
            sigbet = ap_sig*beta 
! 
             if (ap_dis .le. sigbet) then
! 
		   xt = 1/(beta**6)
		   yt = xt**2
		   ap_vdw = (24*ap_eps*xt*ap_dis) / sigbet
		   ap_vdw = ap_vdw - ((48*ap_eps*yt*ap_dis) / sigbet)
		   ap_vdw = ap_vdw + 52*ap_eps*yt - mumbo_en_vdw_reponly*28*ap_eps*xt
!
             else if ((ap_dis .gt. sigbet) .and. (ap_dis .lt. ap_sig)) then
!
		   xt = ap_sig/ap_dis
		   yt = xt**6
		   zt = yt**2
		   ap_vdw = 4 * ap_eps * (zt - mumbo_en_vdw_reponly*yt)
! 
             else if (ap_dis .gt. ap_sig) then
!
		   xt = ap_sig/ap_dis
		   yt = xt**6
		   zt = yt**2
		   ap_vdw = 4 * mu * ap_eps * (zt - mumbo_en_vdw_reponly*yt)
!   
             end if
!
!!!MARK########################################################################################                 
!                 
           else if (mumbo_en_vdw_sig) then

            ap_vdw_rep = 0.0
            ap_vdw_atr = 0.0
            ap_vdw_rmin = (ap_sig * (2.0**(1.0/6.0))) 

!##########REPULSION#############    
! 
            if (ap_dis <= (vdw_switch_lin2norm * ap_vdw_rmin)) then
!
                !!!######## LINE => y=mx+b                           #############!!!
                !!!######## m = f'(x) für x=switch_lin2norm*rmin     #############!!!
                vdw_line_m = -12.0* (ap_eps/ap_vdw_rmin) * ((vdw_switch_lin2norm_reci**13) - (vdw_switch_lin2norm_reci**7 )) 
                !!!######## b=y-mx für x=switch_lin2norm*rmin        #############!!!
                vdw_line_y= (vdw_switch_lin2norm_reci**12) - 2.0 * (vdw_switch_lin2norm_reci**6)                
                vdw_line_b = ap_eps * vdw_line_y-  vdw_line_m* ap_vdw_rmin * vdw_switch_lin2norm                
                !!!######## LINE => y=mx+b                           #############!!!                
                ap_vdw_rep = (vdw_line_m* ap_dis) + vdw_line_b                                          
!
            else if (((vdw_switch_lin2norm * ap_vdw_rmin) < ap_dis) .and. (ap_dis <= ap_vdw_rmin)) then
!            
                ap_vdw_rep = ap_eps * ((((ap_vdw_rmin/ap_dis)**12)-(2*((ap_vdw_rmin/ap_dis)**6)))+1)                
!            
            else if (ap_vdw_rmin < ap_dis) then
!
                ap_vdw_rep = 0.0
!   
            else
                print*, 'ERROR with mumbo_en_vdw_sig--##########REPULSION#############'
            end if
! 
!##########ATTRACTION#############
!
            vdw_norm2cos_dis = vdw_switch_norm2cos_sig * ap_sig 
            vdw_stop_cos_dis = vdw_stop_cos_sig * ap_sig 

    
            if (ap_dis <= ap_vdw_rmin) then
!
                ap_vdw_atr = - ap_eps
!    
            else if (( ap_vdw_rmin < ap_dis) .and. (ap_dis <= (vdw_norm2cos_dis) )) then
!
                ap_vdw_atr = ap_eps * (((ap_vdw_rmin/ap_dis)**12)-(2*((ap_vdw_rmin/ap_dis)**6)))
!             
            else if ((vdw_norm2cos_dis < ap_dis) .and. (ap_dis <= vdw_stop_cos_dis )) then 
!
                ap_vdw_atr = ap_eps  * (((ap_vdw_rmin/ap_dis)**12)-(2*((ap_vdw_rmin/ap_dis)**6)))
                ap_vdw_atr = ap_vdw_atr*((cos((1.0-((vdw_stop_cos_dis-ap_dis)/(vdw_stop_cos_dis-vdw_norm2cos_dis)))*PI)/2)+0.5)
                
!
            else if (vdw_stop_cos_dis < ap_dis) then
                ap_vdw_atr = 0.0

            else
              print*, 'ERROR with mumbo_en_vdw_sig--##########ATTRACTION#############'
            end if

        ap_vdw = ap_vdw_atr + mumbo_we_vdw_rep * ap_vdw_rep
                      
!!!SEHR VIELE KONSTANTE WERTE IN BERECHUNGEN!        Mark
!!!Ab hier ist alles beim Alten...                   Mark   
!####################################################################        
!
           else 
!
		xt =(ap_sig/ap_dis)
		yt = xt**6
		zt = yt**2
		ap_vdw= 4 * ap_eps * (zt - mumbo_en_vdw_reponly*yt)
!
           end if
!                             
           end if          !  end if from if clause checking whether sig and eps = 0
!
!DEBUG
!	       if (check.eqv..true.) then
!                  write(*,'(2(X,I3),X,4(4X,F7.3))') i, j, ap_dis, ap_vdw, ap_sig, ap_eps
!	       end if
!DEBUG
!
!
		en_vdw = en_vdw + ap_vdw
!
	      end if
	     end if
!
	     if (elec_flag) then
	       if (con_at(i,j) > mumbo_en_it_ele) then
!        
!!!MARK########################################################################################                 
!!!
             if (mumbo_en_ele_sig .eqv. .false.) then
!!!
!!!MARK########################################################################################                         
!           
               rele= 1.0
               if (eshift_flag) then                      ! electrostatics continously shifting to 0 and reaching  
                   if (ap_dis > mumbo_en_ctofnb) then     ! 0 at distance mumbo_en_ctofnb
                       rele = 0.0
                   else if (mumbo_en_ctofnb > 0.0) then 
                       x1 = (ap_dis/mumbo_en_ctofnb)**2
                       x1 = 1 - x1
                       rele = x1**2
                   else 
                       rele = 1.0
                   end if
               end if
               if (eswitch_flag) then                      ! electrostatics switching to 0 between 
                   if (ap_dis <= mumbo_en_ctonnb) then     ! mumbo_en_ctonnb and mumbo_en_ctofnb
                       rele= 1.0
                   else if (ap_dis >= mumbo_en_ctofnb) then
                       rele= 0.0
                   else 
                       x3= (ap_dis - mumbo_en_ctofnb)**2     ! here switching is accomplished
                       x5= x4 - (2 * ap_dis)
                       rele = (x3 * x5) / x2 
                   end if
               end if
               if (edist_flag) then                    ! electrostatics scaled in a distance-dependent
                   if (ap_dis <= 1.0 ) then            ! manner. 
                       rele = 1.0
                   else 
                       rele= 1/ap_dis
                   end if 
               end if
               if (econt_flag) then                  ! electrostatics kept unaltered... 
                   rele = 1.0
               end if
!           
!        setting electrostatic interactions to a constant value if atoms are
!        closer than mumbo_en_ecstdis. Constant value is obatined by calculating 
!        electrostatic interaction energy at distance = mumbo_en_ecstdis
!           
               if (ap_dis.lt.mumbo_en_ecstdis) then
                       ap_dis = mumbo_en_ecstdis
               end if
!
!        now calculating the electrostatic interaction 
!
               if (rele > 0.0) then
                   ap_elec = q(i)*q(j)
                   ap_elec = ap_elec/ap_dis
                   ap_elec = 332 * ap_elec
                   ap_elec = ap_elec * rele
                   ap_elec = ap_elec / mumbo_en_sdiel
               else
                   ap_elec = 0.0
               end if
!
!
!!!MARK########################################################################################                 
!!!AB Hier sind Sachen geaendert
!!!Sollten wir doch ein Abstandsabhäniges mumbo_en_sdiel einbauen?

             else if (mumbo_en_ele_sig) then
!             
                ap_elec                 = 0.0
                ap_sig                  = ((sig(i) + sig(j)) / 2)
                if (ap_sig.lt.0.0001)   ap_sig = 0.0001                  ! this is to take care of entries where sig(i) and sig(j) = 0
                elec_con2cos_dis        = elec_switch_con2cos * ap_sig * (2.0**(1.0/6.0))                    
                elec_cos2norm_dis       = ap_sig 
                qij = q(i)*q(j)
!                      
!                elec_norm2cos_dis   = ap_sig * elec_norm2cos_dis_sig ! koennte man auch sigma abhaenig machen MK
!                elec_cos2stop_dis   = ap_sig * elec_cos2stop_dis_sig ! koennte man auch sigma abhaenig machen MK
!
                if ((abs(qij) > 0.00001)) then
                    if (ap_dis < elec_con2cos_dis) then
                        ap_dis_reci  = 1/elec_con2cos_dis
                        ap_elec = qij
                        ap_elec = ap_elec*ap_dis_reci
                        ap_elec = ap_elec * 332
                        ap_elec = ap_elec / mumbo_en_sdiel
!
                    else if ((elec_con2cos_dis <= ap_dis) .AND. (ap_dis < elec_cos2norm_dis)) then
                        ap_dis_reci = 1/ap_dis
                        ap_elec = qij
                        ap_elec = ap_elec*ap_dis_reci
                        ap_elec = ap_elec * 332
                        ap_elec = ap_elec / mumbo_en_sdiel
                        ap_elec_temp = ap_elec
                        ap_elec = 0.0
                        ap_dis_reci = 1/elec_con2cos_dis
                        ap_elec = qij
                        ap_elec = ap_elec*ap_dis_reci
                        ap_elec = ap_elec * 332
                        ap_elec = ap_elec / mumbo_en_sdiel
                        lambda = 0.0
!                        
                        lambda=(cos((((elec_cos2norm_dis-ap_dis)/(elec_cos2norm_dis-elec_con2cos_dis))**elec_cos_pow)*PI)/2)+0.5
                        ap_elec = ap_elec_temp * lambda + ap_elec * (1.0-lambda) 
!
                    else if ((elec_cos2norm_dis<= ap_dis) .AND. (ap_dis < elec_norm2cos_dis )) then 
                        ap_dis_reci = 1/ap_dis
                        ap_elec = qij
                        ap_elec = ap_elec * ap_dis_reci
                        ap_elec = ap_elec * 332
                        ap_elec = ap_elec / mumbo_en_sdiel
!
                    else if ((elec_norm2cos_dis <= ap_dis) .AND. (ap_dis < elec_cos2stop_dis )) then 
                        ap_dis_reci  = 1/ap_dis
                        ap_elec = qij
                        ap_elec = ap_elec * ap_dis_reci
                        ap_elec = ap_elec * 332
                        ap_elec = ap_elec / mumbo_en_sdiel
!                  
                        lambda = 0.0
                        lambda=(cos(((((elec_norm2cos_dis-ap_dis)/(elec_norm2cos_dis-elec_cos2stop_dis)))**elec_cos_pow)*PI)/2)
                        lambda=lambda+0.5
                        ap_elec = ap_elec * lambda 
!
                    else if (ap_dis >= elec_cos2stop_dis ) then
                        ap_elec = 0.0
                    else
                        print*, 'ERROR with mumbo_en_elec_sig'
                    end if

                else if ((abs(qij) <= 0.00001)) then   
                        ap_elec = 0.0
                else                    
                    print*, 'ERROR with mumbo_en_elec_sig_2'                                        
                end if   
             else
                print*, 'ERROR with mumbo_en_elec_sig_3'
             end if

!!!BIS Hier sind Sachen geaendert
!!!MARK########################################################################################                 
!
		     en_elec = en_elec + ap_elec
!
	       end if
	     end if
!
! 	FOLLOWING THE FORMALISM OF LAZARIDIS AND KARPLUS 
!
      if (solv_flag) then
       if (con_at(i,j) > mumbo_en_it_sol) then
!
        ap_solv = 0
        ap_solv_ana = 0
!                                                               
        if (sig(i).lt.0.0001)  sig(i) = 0.0001                  ! this is to take care of entries where sig(i) and sig(j) = 0
        if (sig(j).lt.0.0001)  sig(j) = 0.0001                  ! at present it is not clear if such entries make sense or not !!!!
!!
!       the following lines were commented out because they introduced changes in comparison to OLD feb22 mumbo version 
!
!        if (sgr(i).lt.0.0001)  sgr(i) = 0.0001 
!        if (sgr(j).lt.0.0001)  sgr(j) = 0.0001 
!!
!        if (sgf(i).lt.0.0001)  sgf(i) = 0.0001 
!        if (sgf(j).lt.0.0001)  sgf(j) = 0.0001 
!!        
!        if (svo(i).lt.0.0001)  svo(i) = 0.0001 
!        if (svo(j).lt.0.0001)  svo(j) = 0.0001 
!!
!        if (sla(i).lt.0.0001)  sla(i) = 0.0001 
!        if (sla(j).lt.0.0001)  sla(j) = 0.0001 
!!
!       If atoms are too close then it is better to replace ap_dis by
!         ap_sig = (sig(i) + sig(j)) / 2 because else the solvatisation becomes
!         artificially low
!
        ap_sig = (sig(i) + sig(j)) / 2 
!
        if (ap_sig.gt.ap_dis) then
            ap_dis = ap_sig
        end if
! 
        xt = 0
        yt = 0
        zt = 0
        tt = 0
!
!       Considering atoms surrounding i
!
        xt = mumbo_en_svdw * sig(i) / 2
        xt = ap_dis - xt
        xt = xt / (sla(i) * mumbo_en_sollam)
!
        xt = xt**2
!
!       The following line is needed to avoid the following warning in the gfortran compiled program: 
!                  Note: The following floating-point exceptions are signalling: IEEE_UNDERFLOW_FLAG IEEE_DENORMAL
!       For -xt = -40 : yt = 4.24835413E-18 
!
        rdebug = xt
        if (xt.gt.40) xt = 40
! 
        xt = - xt
        yt = exp(xt)
!        
        zt = (ap_dis)**2
        zt = zt * sla(i) * p4s
        zt = 1/zt 
!        
        zt = - (zt * 2* sgf(i) * mumbo_en_solgfree * svo(j) * yt)        
!
!  DEBUG
!        print*, 'BEEN_HERE_2', rdebug, xt, yt, zt
!  END DEBUG
!
!       Considering atom j only when calculating E(i,j) and not when calculating E(i)
!
!       AENDERUNG HIER 14.11.05
!
           if (n1==1) then
               tt = 0
           else
               rt = mumbo_en_svdw * sig(j) / 2
               rt = ap_dis - rt
               rt = rt / (sla(j) * mumbo_en_sollam)
!          
               rt = rt**2
!
               if (rt.gt.40) rt = 40       ! same safenet as above
!                              
!               
               rt = - rt
               st = exp(rt)
!          
               tt = (ap_dis)**2
               tt = tt * sla(j) * p4s
               tt = 1/tt 
               tt = - (tt * 2* sgf(j) * mumbo_en_solgfree * svo(i) * st)
           end if
!          
           ap_solv =     zt + tt
           ap_solv_ana = zt 
!          
!          
           en_solv = en_solv + ap_solv
           en_solv_ana = en_solv_ana + ap_solv_ana
!          
         end if
       end if
!
	   end if
!
	   end do
	end do
!
	en_vdw =       mumbo_we_vdw    * en_vdw
	en_elec =      mumbo_we_elec   * en_elec
	en_rprob =     mumbo_we_rprob  * en_rprob
	en_solv =      mumbo_we_solv   * en_solv 
	en_solv_ana =  mumbo_we_solv   * en_solv_ana   ! needed in the ana step
	en_solv_ref =  mumbo_we_solv   * en_solv_ref
	en_xray =      mumbo_we_xray   * en_xray
!
	en(1) = 0
	en(2) = 0
	en(3) = en_vdw
	en(4) = en_elec 
	en(5) = en_rprob
	en(6) = en_solv
	en(7) = en_solv_ref
	en(8) = en_xray
	en(10) = en_solv_ana
!
!    DEBUG
!    print*, en
!    END DEBUG
!
	END SUBROUTINE CALC_INTER
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
        SUBROUTINE CALC_HBONDS(ntot,p_int,p_real,con_at,en)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	real, dimension(15) :: en       
	integer,  intent(in)   :: ntot
!
	real,      dimension(ntot)  ::    en_hb
	integer,   dimension(ntot,3)  ::  hd
	integer,   dimension(ntot,3)  ::  ab
!
	real, dimension (ntot)  ::  xyz1, xyz2, xyz3
	integer, dimension(ntot,ntot)  :: con_at
!
	integer, dimension(ntot,7), intent(in)  :: p_int
	real, dimension(ntot,12), intent(in)    :: p_real
!
	real, parameter :: PI = 3.1415927
	integer :: i, j, k, nh , na , nd
	logical :: asp_flag, dsp_flag
	real, dimension(3) :: dis, hy, af, as , at, df, ds, xx, yy, nn
	real :: ap_dis, dis_ha, dis_da
	real :: an, phi, tet, ang
	real :: AXP, BXP, CXP, DXP, XXP, EHB
!
	do i=1,ntot
	   hd(i,1)= p_int(i,2) 
	   hd(i,2)= p_int(i,3) 
	   hd(i,3)= p_int(i,4) 
	   ab(i,1)= p_int(i,5) 
	   ab(i,2)= p_int(i,6) 
	   ab(i,3)= p_int(i,7) 
!
	   xyz1(i) =  p_real(i,9)    
	   xyz2(i) =  p_real(i,10)  
	   xyz3(i) =  p_real(i,11)   
!
	   en_hb(i) = 0.0
	end do
!
	do i=1,ntot
	  if(hd(i,1) == 1) then
	   do j=1,ntot
	     if (ab(j,1) == 1 .and. con_at(i,j)> mumbo_en_it_hbd) then
	       ap_dis = 0
	       dis(1) = xyz1(i) - xyz1(j)
	       dis(2) = xyz2(i) - xyz2(j)
	       dis(3) = xyz3(i) - xyz3(j)
	       do k=1,3
		 dis(k)= dis(k)**2
		 ap_dis = ap_dis + dis(k)
	       end do
 	       ap_dis = sqrt(ap_dis)
!
	       if (ap_dis > mumbo_en_hbd_ctof ) then
		 cycle
	       end if
!
!	Now starting some geometric calculations
!
	       hy(1)=xyz1(i)
	       hy(2)=xyz2(i)
	       hy(3)=xyz3(i)
!
	       df(1)= xyz1(hd(i,2))
	       df(2)= xyz2(hd(i,2))
	       df(3)= xyz3(hd(i,2))
!
	       af(1) = xyz1(j)
	       af(2) = xyz2(j)
	       af(3) = xyz3(j)
!
	       as(1) = xyz1(ab(j,2))
	       as(2) = xyz2(ab(j,2))
	       as(3) = xyz3(ab(j,2))
!
	       if (hd(i,3)/= 0) then
	        dsp_flag=.true.
	        ds(1)= xyz1(hd(i,3))
	        ds(2)= xyz2(hd(i,3))
	        ds(3)= xyz3(hd(i,3))
	       else
	        dsp_flag=.false.
	       end if
!
	       if (ab(j,3)/= 0) then
	        asp_flag=.true.
	        at(1)= xyz1(ab(j,3))
	        at(2)= xyz2(ab(j,3))
	        at(3)= xyz3(ab(j,3))
	       else
	        asp_flag=.false.
	       end if
!
	       call angle(af,hy,df,tet)
	       call angle(as,af,hy,phi)
!
	      if (mumbo_en_hbd_geoflag) then
!
		dis_da = 0
		do k=1,3
		   dis_da = dis_da + (df(k)-af(k))**2
		end do
		dis_da = sqrt(dis_da)
!
		if (dsp_flag.and.asp_flag) then 
		   call vekpro(ds,df,hy,xx)
		   call vekpro(at,as,af,yy)
		   do k=1,3
		     nn(k)=0 
		   end do
		   call angle (xx,nn,yy,an)
		   if (an < (pi/2)) then
		      an = pi - an
		   end if 
		   if (an < phi) an = phi 
		end if
!
!     Now calculating A, XXP for different cases according to 
!     Gohlke et al., Proteins 56, 322-337 (2004) 	
!
		AXP = (cos(tet))**2
		BXP = (pi - tet)**6
		CXP = exp ( BXP * (-1) ) 
		DXP = (109.5 / 180) * pi 
		if (.not.dsp_flag) then 
		   if (.not.asp_flag) then
		    XXP = phi - DXP 
		    XXP = (cos(XXP))**2
		    XXP = XXP * AXP * CXP
		   else if (asp_flag) then 
		    XXP = (cos(phi))**2
		    XXP = XXP * AXP * CXP
		   end if
		else if (dsp_flag) then 
		   if (.not.asp_flag) then
                    AXP = AXP * AXP
		    XXP = exp ( BXP * (-2) ) 
		    XXP = XXP * AXP
		   else if (asp_flag) then 
		    XXP = (cos(an))**2
		    XXP = XXP * AXP * CXP
		   end if
		end if 
!
!	Now calculating some energies...
!
		AXP = mumbo_en_hbd_dzero / dis_da
		BXP = AXP**12
		BXP = BXP * 5
		CXP = AXP**10
		CXP = CXP * 6
!
		EHB =  mumbo_en_hbd_ezero * XXP * (BXP - CXP)
!
	     else if (mumbo_en_hbd_empflag) then
 	         dis_ha = ap_dis
		   ang= 0
		   ehb= 0
		   if (asp_flag) then 
	            call dihedral(hy,af,as,at,ang)
		   end if
		   tet = (tet / pi) * 180
		   phi = (phi / pi) * 180
		   call calc_hbd_exp(dis_ha,tet,phi,ang,asp_flag,ehb)
 	     end if
!
	     if (EHB < 0 .and. EHB < en_hb(i)) then 
		  en_hb(i) = ehb
		  hd(i,1) = ntot + j
!
!		  print*,'ENERGY STORED ',i, en_hb(i)
!		  if (mumbo_en_hbd_geoflag) then
!	          print*,'ANGLES, PHI, TET', (phi*180/pi), (tet*180/pi)
!		  else
!	          print*,'ANGLES, PHI, TET', phi, tet
!		  end if
!	          print*,'DIS DA ', dis_da
!		  print*,'AN ', (an*180/pi)
!		  print*,'AF',af
!		  print*,'HY',hy
!	          print*,'DF',df
!	          print*,'DS',ds
!	          print*,'AS',as
!		  print*,'AT',at
!	          call dihedral(df,hy,af,as,ang)
!		  print*,'DIHER', ang 
!
!	          do k=1, ntot
!		   print*, xyz1(k), xyz2(k), xyz3(k)
!	          end do
!
	     end if
!	     	
	     end if
	   end do
	   end if
	end do
!
	do i=1,ntot
	  en(9) = en(9) +  en_hb(i)
	end do
!
	en(9) = mumbo_we_hbond * en(9)
!
!	print*, 'ENERGY RETURNED2: ', en(9)
!
!	Before returning the connectivity table is cleaned up for further 
!	processing, namely removing any additional interactions between atoms
!	Hydrogen and Donor and Acceptor 
!
	do i=1,ntot
	  if (hd(i,1) > ntot) then
	   nh = i 
	   na = hd(i,1) - ntot
	   nd = hd(i,2)
	   con_at(nh,na)=1
	   con_at(na,nh)=1
	   con_at(na,nd)=1
	   con_at(nd,na)=1
	   con_at(nh,nd)=1
	   con_at(nd,nh)=1
	  end if
	end do
!
	END SUBROUTINE CALC_HBONDS
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE CALC_HBD_EXP(DIS,TET,PSI,CHI,FLAG,EHB)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	IMPLICIT NONE
	REAL :: DIS, TET, PSI, CHI, EHB
	LOGICAL :: FLAG 
!
	REAL :: eh_d, eh_p, eh_t, eh_c, rh
	INTEGER :: nh
!
	REAL :: ED2(31),ED3(28)
	REAl :: ECS2(18),EPS2(11),ETS2(8),ECL2(18),EPL2(12),ETL2(10)
	REAl :: EPS3(10),ETS3(8),EPL3(12),ETL3(10)
!
	REAL :: dis_switch = 2.1 
!
	REAL :: NS_ED2 = 1.45, NE_ED = 3.00, NI_ED = 0.05
	REAL :: NS_ED3 = 1.60 
!
	INTEGER :: NS_ECS2 = 0  , NE_ECPT = 180 , NI_ECPT = 10
	INTEGER :: NS_EPS2 = 70 
	INTEGER :: NS_ETS2 = 100
	INTEGER :: NS_ECL2 = 0
	INTEGER :: NS_EPL2 = 60 
	INTEGER :: NS_ETL2 = 80  
!
	INTEGER :: NS_EPS3 = 80 
	INTEGER :: NS_ETS3 = 100 
	INTEGER :: NS_EPL3 = 60 
	INTEGER :: NS_ETL3 = 80 
!
	DATA  ED2 /2.99,1.50,1.50,0.52,0.37,-0.10,-0.83,-0.71,-1.21,-1.29,-1.25,    &
     &		-1.18,-0.72,-0.24,-0.28,0.02,0.16,0.43,0.60,0.53,0.76,0.69,     &
     &            1.06,1.38,1.02,1.20,1.08,1.04,1.47,1.17,1.22/
	DATA  ED3 /1.30,0.90,0.45,-0.43,-0.34,-0.67,-1.14,-1.24,-1.34,-0.84,-0.55,  &
     &            -0.37,-0.34,-0.24,0.20,0.61,0.45,0.45,0.20,0.90,0.69,0.61,0.61, &
     &            0.69,0.32,0.79,0.69,0.61/
!
	DATA  ECS2 /-0.11,0.01,0.07,0.18,0.38,0.39,0.53,0.90,0.80,0.82,0.61,0.40,   &
     &             0.05,-0.18,-0.40,-0.58,-0.67,-0.82/
	DATA  EPS2 /2.89,1.61,0.15,-1.33,-2.13,-2.05,-1.76,-1.47,-0.81,-0.37,-0.11/
	DATA  ETS2 /3.93,3.04,1.66,0.33,-0.98,-1.70,-2.37,-2.78/
!
	DATA  ECL2 /0.60,0.61,0.41,0.28,0.26,0.22,0.14,0.13,0.10,-0.04,-0.03,-0.27, &
     &            -0.35,-0.27,-0.30,-0.19,-0.29,-0.23/
	DATA  EPL2 /0.76,-0.48,-0.81,-1.06,-1.34,-1.46,-1.62,-1.38,-1.26,-1.13,     &
     &             -0.95,-0.89/
	DATA  ETL2 /0.71,0.19,-0.18,-0.62,-1.04,-1.60,-1.72,-1.71,-1.77,-1.93/
!
	DATA  EPS3 /1.72,-0.49,-1.53,-2.03,-2.16,-1.94,-1.25,-0.65,0.63,0.94/
	DATA  ETS3 /3.27,1.97,1.04,-0.39,-1.16,-1.86,-2.36,-2.62/
!
	DATA  EPL3/1.77,0.63,-0.30,-0.89,-1.16,-1.67,-1.66,-1.76,-1.38,-1.20,-0.96,-0.74/
	DATA  ETL3/0.32,-0.06,-0.84,-1.09,-1.30,-1.55,-1.51,-1.86,-1.67,-1.46/
!
!
	if (tet < 0 .or. tet > 180) then 
	  STOP 'UNEXPECTED VALUE FOR THETA IN CALC_HBD_EXP'
	else if (psi < 0 .or. psi > 180) then 
	  STOP 'UNEXPECTED VALUE FOR PSI IN CALC_HBD_EXP'
	else if (flag) then 
	   if (chi < 0) chi = -chi
	   if (chi > 180) then 
	  STOP 'UNEXPECTED VALUE FOR DIHEDRAL CHI IN CALC_HBD_EXP'
	   end if  
	end if
!
	  eh_d= 0.0
	  eh_t= 0.0
	  eh_p= 0.0
	  eh_c= 0.0
!
	if (flag) then 
!
	 if (dis < ns_ed2 .or. dis > ne_ed ) then 
	   ehb = 0
	   return
	 else 
	   rh = (dis - ns_ed2)/ ni_ed
	   nh = int(rh)
	   if (nh == size(ed2)) nh = nh - 1
	   nh = nh +1
	   eh_d = ED2(nh)
!
	 end if
!
	 if (dis < dis_switch) then 
	   if (tet < NS_ETS2 .or. tet > NE_ECPT) then 
	      ehb = 0
	      return
	   else
	      rh = (tet - ns_ets2)/ ni_ecpt
	      nh = int(rh)
 	      if (nh== size(ets2)) nh = nh - 1
	      nh = nh +1
	      eh_t = ETS2(nh)
	   end if
!
	   if (psi < NS_EPS2 .or. psi > NE_ECPT) then
	      ehb = 0
	      return
	   else
	      rh = (psi - ns_eps2) / ni_ecpt
	      nh = int(rh)
	      if (nh== size(eps2)) nh = nh - 1
	      nh = nh +1
	      eh_p = EPS2(nh)
	   end if
!
	   if (chi < NS_ECS2 .or. chi > NE_ECPT) then
	      ehb = 0
	      return
	   else
	      rh = (chi - ns_ecs2) / ni_ecpt
	      nh = int(rh) 
	      if (nh== size(ecs2)) nh = nh - 1
	      nh = nh +1
	      eh_c = ECS2(nh)
	   end if
!
	  else if (dis >= dis_switch) then 
!
	   if (tet < NS_ETL2 .or. tet > NE_ECPT) then 
	      ehb = 0
	      return
	   else
	      rh = (tet - ns_etl2)/ ni_ecpt
	      nh = int(rh)
	      if (nh== size(etl2)) nh = nh - 1
	      nh = nh +1
	      eh_t = ETL2(nh)
	   end if
!
	   if (psi < NS_EPL2 .or. psi > NE_ECPT) then
	      ehb = 0
	      return
	   else
	      rh = (psi - ns_epl2) / ni_ecpt
	      nh = int(rh)
	      if (nh== size(epl2)) nh = nh - 1
	      nh = nh +1
	      eh_p = EPL2(nh)
	   end if
!
	   if (chi < NS_ECL2 .or. chi > NE_ECPT) then
	      ehb = 0
	      return
	   else
	      rh = (chi - ns_ecL2) / ni_ecpt
	      nh = int(rh)
	      if (nh== size(ecl2)) nh = nh - 1
	      nh = nh +1
	      eh_c = ECL2(nh)
	   end if
!
	  end if
!
!	now looking up the case when the acceptor is sp3
!
	else if (.not.flag) then 
!
	 if (dis < ns_ed3 .or. dis > ne_ed ) then 
	   ehb = 0
	   return
	 else 
	   rh = (dis - ns_ed3)/ ni_ed
	   nh = int(rh)
	   if (nh== size(ed3)) nh = nh - 1
	   nh = nh +1
	   eh_d = ED3(nh)
	 end if
!
	 if (dis < dis_switch) then 
	   if (tet < NS_ETS3 .or. tet > NE_ECPT) then 
	      ehb = 0
	      return
	   else
	      rh = (tet - ns_ets3)/ ni_ecpt
	      nh = int(rh) 
	      if (nh == size(ets3)) nh = nh - 1
	      nh = nh +1
	      eh_t = ETS3(nh)
	   end if
!
	   if (psi < NS_EPS3 .or. psi > NE_ECPT) then
	      ehb = 0
	      return
	   else
	      rh = (psi - ns_eps3) / ni_ecpt
	      nh = int(rh)
	      if (nh == size(eps3)) nh = nh - 1
	      nh = nh +1
	      eh_p = EPS3(nh)
	   end if
!
	  else if (dis >= dis_switch) then 
!
	   if (tet < NS_ETL3 .or. tet > NE_ECPT) then 
	      ehb = 0
	      return
	   else
	      rh = (tet - ns_etl3)/ ni_ecpt
	      nh = int(rh) 
	      if (nh == size(etl3)) nh = nh - 1
	      nh = nh +1
	      eh_t = ETL3(nh)
	   end if
!
	   if (psi < NS_EPL3 .or. psi > NE_ECPT) then
	      ehb = 0
	      return
	   else
	      rh = (psi - ns_epl3) / ni_ecpt
	      nh = int(rh)
	      if (nh == size(epl3)) nh = nh - 1
	      nh = nh +1
	      eh_p = EPL3(nh)
	   end if
!
	  end if
!
	end if
!
	ehb = eh_d + eh_t + eh_p + eh_c
!
!	PRINT*, 'eh_d + eh_t + eh_p + eh_c', eh_d, eh_t, eh_p, eh_c
!
	END SUBROUTINE CALC_HBD_EXP
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE GET_ATOM_DENS(s1,s2,s3,rho)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER		:: ntitle
	REAL, DIMENSION(6) :: dcell
	INTEGER		:: i
	REAL, PARAMETER :: twopi= 6.28319
	REAL, DIMENSION(3):: xyz,rxyz
	REAL :: ae,be,ce,astar,bstar,cstar,cbstar,cgstar,vo
	REAL :: s1, s2, s3, rho
!
	REAL, DIMENSION(:,:,:), allocatable, save :: map
	LOGICAL, SAVE 	:: first=.true. 
	REAL, SAVE:: SA,SB,SG,CA,CB,CG,VF,CASTAR
	INTEGER, DIMENSION(3), SAVE:: mygrid,ioxyz,iendpoint,imxyz
	REAL, DIMENSION(6), SAVE :: cell
!
	if (first) then 
!
	PRINT*, '    STARTING READING IN ELECTRON DENSITY MAP ' 
!
	    if (mumbo_map_type(1:3)=='CNS') then
!
		call cns_open_head(ntitle,cell,mygrid,ioxyz,iendpoint)
!
		do i = 1,3
			imxyz(i) = iendpoint(i)-ioxyz(i) + 1
		end do
!
		if (allocated(map)) deallocate(map)
		allocate(map(imxyz(1),imxyz(2),imxyz(3)))
!
		call cns_open_map(ntitle,map,imxyz(1),imxyz(2),imxyz(3))
	    else
		PRINT*, '    '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '>>>>>>>  UNRECOGNISED TYPE OF ELECTRON DENSITY       '
		PRINT*, '>>>>>>> ', mumbo_map_type
		PRINT*, '>>>>>>>  CNS TYPE REQUIRED                           '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		PRINT*, '    '
		STOP
	    end if
!
	    ae  = cell(1)
	    be  = cell(2)
	    ce  = cell(3)
	    dcell(4)= (cell(4)/360)*twopi
	    dcell(5)= (cell(5)/360)*twopi
	    dcell(6)= (cell(6)/360)*twopi
	    sa = sin(dcell(4))
	    sb = sin(dcell(5))
	    sg = sin(dcell(6))
	    ca = cos(dcell(4))
	    cb = cos(dcell(5))
	    cg = cos(dcell(6))
	    vf = 1.0/sqrt(1.0 - ca**2 - cb**2 - cg**2 + 2.0*ca*cb*cg)
	    vo =  ae*be*ce/vf
	    astar  =  sa*vf/ae
	    bstar  =  sb*vf/be
	    cstar  =  sg*vf/ce
	    castar = (cb*cg - ca)/(sb*sg)
	    cbstar = (cg*ca - cb)/(sg*sa)
	    cgstar = (ca*cb - cg)/(sa*sb)
!
	    first = .false.
!
	end if
!
	xyz(3) = (s3*SG*VF)/cell(3)
	xyz(2) = (s2 + xyz(3)*cell(3)*SB*CASTAR)/(cell(2)*SG)
	xyz(1) = (s1 - xyz(2)*cell(2)*CG - xyz(3)*cell(3)*CB)/cell(1)
!
	rxyz(1) = xyz(1) * mygrid(1)
	rxyz(2) = xyz(2) * mygrid(2)
	rxyz(3) = xyz(3) * mygrid(3)
!
	call get_map_dens(map,imxyz(1),imxyz(2),imxyz(3),rxyz,ioxyz,rho,s1,s2,s3)
!
!	print*, s1, s2, s3, rho
!
	END SUBROUTINE GET_ATOM_DENS
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE GET_MAP_DENS(map,n1,n2,n3,rxyz,ioxyz,ede,s1,s2,s3)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!  SOME CHANGES HERE
!
!
	IMPLICIT NONE
!
	INTEGER		:: n1,n2,n3,na,nb,nc
	INTEGER		:: i 
	INTEGER, DIMENSION(3)  :: nxyz, ioxyz, ng
	REAL, DIMENSION(n1,n2,n3)  :: map
	REAL, DIMENSION(3)  :: rxyz,  dxyz
	REAL, DIMENSION(6)  :: rho(6)
	REAL 		:: ede, s1, s2, s3
	LOGICAL :: flag
!
	ng(1)=n1
	ng(2)=n2
	ng(3)=n3
!
	flag=.true.
	do i=1,3
	    nxyz(i)= int(rxyz(i)-ioxyz(i)+1)
	    dxyz(i)=(rxyz(i)-ioxyz(i)+1)-nxyz(i)
!
	    if (nxyz(i) > (ng(i)-1) .or. nxyz(i) < 1) then
		flag=.false.
	    end if
	end do
!
	if (.not.flag) then 
         PRINT*, '    '
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
         PRINT*, '>>>  MAP DOES NOT COVER ALL ATOMS OF ALL         '
         PRINT*, '>>>  ROTAMERS: INCREASE MAP BOX SIZE             '    
	   PRINT*, '>>>  AT_COORD:   ',s1, s2, s3 
	   PRINT*, '>>>  GRID_COORD: ',rxyz(1), rxyz(2), rxyz(3)
	   PRINT*, '>>>  NEAREST MAP POINT: ',nxyz(1), nxyz(2), nxyz(3)                      
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
         PRINT*, '    '
	   ede=0
	   return		
	end if 
!
!     Interpolation (Rossman et al, J. Appl. Cryst (1992) 25, 166-180)
!
	na=nxyz(1)
	nb=nxyz(2)
	nc=nxyz(3)
!
	rho(1)=map(na,nb,nc)    +                       &
     &        (map(na+1,nb,nc)    -map(na,nb,nc))    *dxyz(1)
	rho(2)=map(na,nb+1,nc)  +                       &
     &        (map(na+1,nb+1,nc)  -map(na,nb+1,nc))  *dxyz(1)
	rho(3)=map(na,nb,nc+1)  +                       &
     &        (map(na+1,nb,nc+1)  -map(na,nb,nc+1))  *dxyz(1)
	rho(4)=map(na,nb+1,nc+1)+                       &
     &        (map(na+1,nb+1,nc+1)-map(na,nb+1,nc+1))*dxyz(1)
	rho(5)=rho(1) +(rho(2)-rho(1))* dxyz(2)
	rho(6)=rho(3) +(rho(4)-rho(3))* dxyz(2)
	ede  =rho(5) +(rho(6)-rho(5))*  dxyz(3)
!
	END SUBROUTINE GET_MAP_DENS
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE cns_open_head(ntitle,cell,mygrid,ioxyz,iendpoint)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MUMBO_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER (LEN=132), DIMENSION(:), ALLOCATABLE :: title
	INTEGER		:: ntitle
	INTEGER, DIMENSION(3) :: mygrid,ioxyz,iendpoint, imxyz
	REAL, DIMENSION(6) :: CELL
	CHARACTER (LEN=3)     :: mode
	INTEGER		:: i,j
!
!
	open(11,form='formatted',file=mumbo_map_in,status='old')
!
	READ(11,'(/I8)') NTITLE
!
	if (allocated(title)) deallocate(title) 
	allocate(title(ntitle))
!
	READ(11,'(A)') (TITLE(J),J=1,NTITLE)
	write(6,'(//)')
	write(6,'(A)') (TITLE(J),J=1,NTITLE)
	read(11,'(9I8)')(mygrid(i),ioxyz(i),iendpoint(i),i=1,3)
	read (11,'(6E12.5)')(cell(i),i=1,6)
	read (11,'(3A)') mode
!
	if (mode.eq.'ZYX') then
		write(*,802) mode
	else
		write(*,803)
		stop
	end if
!
	do i = 1,3
		imxyz(i) = iendpoint(i)-ioxyz(i) + 1
	end do
!
	write (6,805)(mygrid(i),ioxyz(i),imxyz(i),i=1,3)
	write(6,804) cell
!
	close(11)
!
802         FORMAT(1X,'SECTIONING DIRECTIONS READ AS :',A4)
803         format (/,2x,'Map format not accepted ! ')
804   format (//,2x,' cell parameter from input map',   &
     &        6(2x,f6.2),/)
805   format (/,/,2x,'Map parameters read from input stream.',/,           &
     &       2x,' number of grid points along cell edge A ',i5,            &
     &       1x,'lowest x point in map ',i5,' number of x points',         &
     &       ' in map ',i5,/,                                              &
     &       2x,' number of grid points along cell edge B ',i5,            &
     &       1x,'lowest y point in map ',i5,' number of y points',         &
     &       ' in map ',i5,/,                                              &
     &       2x,' number of grid points along cell edge C ',i5,            &
     &       1x,'lowest z point in map ',i5,' number of z points',         &
     &        ' in map ',i5)
!
	END SUBROUTINE cns_open_head
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE cns_open_map(ntitle,map,n1,n2,n3)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MUMBO_DATA_M
!
	IMPLICIT NONE
!
	INTEGER		:: ntitle,n1,n2,n3
	INTEGER		:: i,j,k,l
	REAL, DIMENSION(n1,n2,n3)  :: map
	CHARACTER (LEN=132) :: string
!
	open(11,form='formatted',file=mumbo_map_in,status='old')
!
	do, j=1,(ntitle+4)
		read(11,*) string
	end do
!
	do i=1,n3
		read(11,'(i8)')l
		read(11,'(6E12.5)')((map(j,k,i),j=1,n1),k=1,n2)
		write(*,'("SECTION READ",2x,i3)') l
	end do
!
	PRINT*, '..... ' 
	PRINT*, '..... DONE READING IN ELECTRON DENSITY MAP ......' 
!
	close(11)
!
	END SUBROUTINE cns_open_map
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!


