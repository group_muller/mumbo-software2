!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	PROGRAM MAIN
!
	USE MOLEC_M
	USE EP_DATA_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
	USE WATER_DATA
!
	IMPLICIT NONE
	INTEGER :: I,K
!
	print*,'     '
	print*, mumbo_ver
	print*, mumbo_ref
	print*,'     '
!
	CALL CPU_TIME(mumbo_start)
!
	CALL GET_JOB_DESCRIPTION
	CALL READ_NBOND
	CALL READ_CONNEC
	CALL READ_PARAMS
!
	DO I=1, MUMBO_NJOBS
!
	MUMBO_PART_JOB = MUMBO_JOBS(I)
!
	  IF (MUMBO_PART_JOB(1:4)=='INIT') THEN 
	    MUMBO_PROC='INIT-----'
	    CALL ANNOUNCE_START
	    CALL READ_GEOM 
	    CALL BUILD_NEW
	    CALL FILL_IN_MISSING_DATA
	    CALL EXPAND_LIGAND
	    CALL EXPAND_HYDROROTAMS
	    CALL EXPAND_BACKRUB
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!
	  ELSE IF (MUMBO_PART_JOB(1:7)=='PICKNAT') THEN
	    MUMBO_PROC='PICKNAT--'
	    CALL ANNOUNCE_START
	    CALL LOAD_REF_STRUCT
	    CALL LOAD_PREV_STRUCT 
	    CALL PICKNAT_STRUCT
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!
	  ELSE IF (MUMBO_PART_JOB(1:3)=='DEL') THEN    
	    MUMBO_PROC='DEL------'
	    CALL ANNOUNCE_START
	    CALL LOAD_PREV_STRUCT
	    CALL FILL_IN_MISSING_DATA
        CALL CHECK_ROTAMER_UNIQUENESS	    
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!	    
	   ELSE IF (MUMBO_PART_JOB(1:5)=='WATER') THEN 
	    MUMBO_PROC='WATER----'
	    CALL ANNOUNCE_START
	    CALL LOAD_PREV_STRUCT 
	    CALL FILL_IN_MISSING_DATA 
!	    
	    CALL CHECK_PRESENCE_NUCLEOTIDES     ! in the presence of nucleotides, waters 
!	                                          can't be built yet
	    CALL WATER
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    MUMBO_PROC='WATERMC--'
	    CALL LOAD_PREV_STRUCT 
	    CALL FILL_IN_MISSING_DATA
	    CALL MC_INTERACTION 
	    CALL FLAG_ROTAMER
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!
	  ELSE IF (MUMBO_PART_JOB(1:2)=='MC') THEN 
	    MUMBO_PROC='MC-------'
	    CALL ANNOUNCE_START
	    CALL LOAD_PREV_STRUCT 
	    CALL FILL_IN_MISSING_DATA
	    CALL MC_INTERACTION 
	    CALL FLAG_ROTAMER
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!
	  ELSE IF (MUMBO_PART_JOB(1:3)=='DEE') THEN 
	    MUMBO_PROC='DEE------'
	    CALL ANNOUNCE_START
	    CALL LOAD_PREV_STRUCT 
	    CALL FILL_IN_MISSING_DATA
	    CALL MC_INTERACTION
	    CALL CALC_PAIRWISE_INT
	    CALL DEAD_END_ELIM
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!
	  ELSE IF (MUMBO_PART_JOB(1:4)=='PDEE') THEN 
	    MUMBO_PROC='PDEE-----'
	    CALL ANNOUNCE_START
	    CALL LOAD_PREV_STRUCT 
	    CALL FILL_IN_MISSING_DATA
	    CALL MC_INTERACTION
	    CALL P_DEAD_END_ELIM
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!
	  ELSE IF (MUMBO_PART_JOB(1:4)=='GOLD') THEN 
	    MUMBO_PROC='GOLD-----'
	    CALL ANNOUNCE_START
	    CALL LOAD_PREV_STRUCT
	    CALL FILL_IN_MISSING_DATA
	    CALL MC_INTERACTION
	    CALL CALC_PAIRWISE_INT
	    CALL GOLDSTEIN
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!    
	  ELSE IF (MUMBO_PART_JOB(1:5)=='PGOLD') THEN 
	    MUMBO_PROC='PGOLD----'
	    CALL ANNOUNCE_START
	    CALL LOAD_PREV_STRUCT
	    CALL FILL_IN_MISSING_DATA
	    CALL MC_INTERACTION
	    CALL P_GOLDSTEIN
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!
	  ELSE IF (MUMBO_PART_JOB(1:5)=='SPLIT') THEN
	   DO K =1, MUMBO_SPLIT_NCYC
	    MUMBO_PROC='SPLIT1---'
	    CALL ANNOUNCE_START
	    CALL LOAD_PREV_STRUCT
	    CALL FILL_IN_MISSING_DATA
	    CALL MC_INTERACTION
	    CALL CALC_PAIRWISE_INT
	    CALL MB_SPLIT
	    CALL CONF_SPLIT1
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
	    IF (MUMBO_CONV_FLAG) EXIT
           END DO
           DO K =1, MUMBO_SPLIT_NCYC
	    MUMBO_PROC='SPLIT2---'
	    CALL ANNOUNCE_START
        CALL LOAD_PREV_STRUCT
        CALL FILL_IN_MISSING_DATA
        CALL MC_INTERACTION
        CALL CALC_PAIRWISE_INT
        CALL MB_SPLIT
        CALL CONF_SPLIT2
        CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
            IF (MUMBO_CONV_FLAG) EXIT
           END DO
!
	  ELSE IF (MUMBO_PART_JOB(1:4)=='DOUB') THEN
	   DO K =1, MUMBO_DB_MAXIT
	    MUMBO_PROC='DOUB-----'
	    CALL ANNOUNCE_START
	    CALL LOAD_PREV_STRUCT
	    CALL FILL_IN_MISSING_DATA
	    CALL MC_INTERACTION
	    CALL CALC_PAIRWISE_INT
	    CALL MB
	    CALL MB_DOUBLES
	    CALL GOLDSTEIN_DOUBLES
	    CALL GOLDSTEIN_DB
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
            IF (MUMBO_CONV_FLAG) EXIT
	   END DO 
!
	  ELSE IF (MUMBO_PART_JOB(1:5)=='MONSA') THEN
	    MUMBO_PROC='MONSA----'
	    CALL ANNOUNCE_START
        CALL LOAD_PREV_STRUCT
        CALL FILL_IN_MISSING_DATA
        CALL MC_INTERACTION
        CALL CALC_PAIRWISE_INT
        CALL SIM_ANNEAL
	    CALL FLAG_ROTAM_SIM
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!
	  ELSE IF (MUMBO_PART_JOB(1:4)=='BRUT') THEN
	    MUMBO_PROC='BRUT-----'
	    CALL ANNOUNCE_START
	    CALL LOAD_PREV_STRUCT 
	    CALL FILL_IN_MISSING_DATA 
	    CALL MC_INTERACTION 
	    CALL CALC_PAIRWISE_INT
	    CALL ALL_EXPLICIT 
	    CALL WRITE_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!
	  ELSE IF (MUMBO_PART_JOB(1:3)=='ANA') THEN
	    MUMBO_PROC='ANA------'
	    CALL ANNOUNCE_START
	    CALL LOAD_PREV_STRUCT 
	    CALL FILL_IN_MISSING_DATA 
	    CALL MC_INTERACTION 
	    CALL CALC_PAIRWISE_INT
	    CALL ANALYSE 
	    CALL WRITE_STRUCT
	    CALL APPLY_BACKRUB_MAINCHAIN
	    CALL WRITE_FINAL
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!
	  ELSE IF (MUMBO_PART_JOB(1:4)=='COMP') THEN
	    MUMBO_PROC='COMP-----'
	    CALL ANNOUNCE_START
	    CALL LOAD_REF_STRUCT
	    CALL LOAD_PREV_STRUCT 
	    CALL COMP_ANA_STRUCT
	    CALL CLEAN_UP
	    CALL ANNOUNCE_END
!
      END IF
!
	END DO
!
	CALL cpu_time(mumbo_end)
!
	PRINT*, '     '
	PRINT*, '#######################################'
	PRINT*, '#                                     #'
	PRINT*, '#   MUMBO COMPLETED SUCCESSFULLY      #'
	PRINT*, '#                                     #'
	PRINT*, '#######################################'
	PRINT*, '   '
	PRINT*, '  ELAPSED TOTAL TIME: ', mumbo_end - mumbo_start, ' SECONDS'
	PRINT*, '   '
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	END program main
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
