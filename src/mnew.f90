!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE BUILD_NEW
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MOLECULE_SIMP_DATA_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: i,j,k,l,m
	INTEGER :: n1, n2, n3
	INTEGER :: nres, nw, ios, io_err, nn, ntmol, ncode, ntype
        INTEGER :: nrib, ntot, num, nrot, num_rot
!
        INTEGER, DIMENSION(4)  :: set
!	
	CHARACTER (LEN=132) :: string, infile
	CHARACTER (LEN=80) ::  word(40), boff
	CHARACTER (LEN=1) :: chid, cid
	CHARACTER (LEN=10) :: echo
	CHARACTER (LEN=10) :: label_d
	CHARACTER (LEN=15) :: label_p
	CHARACTER (LEN=30) :: label_ee
!
	INTEGER, DIMENSION(:), allocatable :: natom
	CHARACTER(LEN=4), DIMENSION(:), allocatable :: cnam
	CHARACTER(LEN=4) :: aanm, atnm, ctype, mcnm, aanm_conf, aanam
!
	REAL::  xbfa, xx, xy, xz, xocc
!
	LOGICAL :: chainid_flag, flag, flag1, flag2, checkflag
	LOGICAL, DIMENSION(:), allocatable :: strange_flag
	LOGICAL, DIMENSION(5):: check_flag
!	
    INTEGER, DIMENSION(:), allocatable :: nn_mcs
!
	if (allocated(natom)) deallocate (natom)
	if (allocated(cnam))  deallocate (cnam)
	if (allocated(strange_flag)) deallocate (strange_flag)
	if (allocated(mol))  deallocate (mol)
!
	call get_mumbo_data
!
	PRINT*, '        '
	PRINT*, '###################################'
	PRINT*, '#  READING IN STARTING PDB-FILE   #'
	PRINT*, '###################################'
	PRINT*, '        '
    PRINT*, ' PLEASE NOTE: '
	PRINT*, '    Most problems with MUMBO occur while reading the original PDB file.        '
!	PRINT*, '    MUMBO expects all items in the PDB file to be separated by at least one    '
!	PRINT*, '    blank/empty space. Therfore MUMBO does NOT LIKE the presence of alternative'
!	PRINT*, '    conformations as in:                                                       '
!	PRINT*, '    ATOM     47  OD1AASP A  63     -11.378  32.034   0.167  0.50 16.89         '
!	PRINT*, '    or residue numbers > 999 as in:                                            '
!	PRINT*, '    ATOM     47  OD1 ASP A1001     -11.378  32.034   0.167  1.00 16.89         '
!	PRINT*, '    or B-factors higher then 99.99 as in:                                      '
!	PRINT*, '    ATOM     47  OD1 ASP A  63     -11.378  32.034   0.167  1.00120.00         '
!	PRINT*, '    and so forth.                                                              '
	PRINT*, '    For reasons unknown to the authors, MUMBO will not accept negative         '
	PRINT*, '    residue numbers. If MUMBO stops before the next step then please take a    '
	PRINT*, '    close look at the input PDB file.                                          '
	PRINT*, '        '
	
	ncode=0
!
!   IF LIGAND OPTIMISATION REQUESTED THEN CREATE SPACE FOR LIGAND
!
    if (mumbo_lig_flag) then 	
      allocate (mol(size(mumbo)+2))
    else 
      allocate (mol(size(mumbo)+1))
    end if
    mol_nmls = size(mol)
!
!   NOW READING IN PDB/CIF FILE INTO MOL(1)
!
    infile = mumbo_inpdb(1:132)
    call load_struct(infile)
!
!    
    PRINT*, '  READING IN INPUT PDB-FILE ..... '
    PRINT*, '                            ..... DONE'!
!
!   LOOKING UP RESIDUES TO BE MUMBOED AND REPLACE GLY WITH ALA IF REQUIRED
!   THIS IS NEEDED TO GENERATE A CB POSITION FOR ROTAMER BUILDING
!
     n1=0
     chid=' '
     do i=1, mol(1)%mol_nrs
         n1  = mol(1)%res(i)%res_nold
         chid= mol(1)%res(i)%res_chid	     
         do j=1,(size(mumbo))
           if (n1==(mumbo(j)%mut_nold).and.chid==(mumbo(j)%mut_chid)) then
              mumbo(j)%mut_flag=.true.
              mol(1)%res(i)%res_mut=.true.
              mumbo(j)%mut_nnew=i
!
             if (mol(1)%res(i)%res_aas(1)%aa_nam == gly_name) then
!                 call dump_pdb(1,i,1,1)
                 PRINT*, '    '
                 PRINT*, '>>> '
                 PRINT*, '>>>  GLY FOUND AT POSITION TO BE MUMBOED: ', chid, n1
                 PRINT*, '>>>  GLY WILL BE MUTATED AUTOMATICALLY TO ALANINE '
                 PRINT*, '>>> '
                 PRINT*, '    '
                 mol(1)%res(i)%res_aas(1)%aa_nam=ala_name
                 aanm=mol(1)%res(i)%res_aas(1)%aa_nam
                 call init_aminoacid(1,i,1,1,aanm)
!                 call dump_pdb(1,i,1,1)
!                recalling missing atom coordinates back from sim molecule ... 
                 call reload_coord_from_sim(1,1,i)
!                 call dump_pdb(1,i,1,1)
             end if
!
         end if
       end do
     end do
!
! CHECKING IF RESIDUES REQUESTED TO BE MUMBOED ARE ALL PRESENT IN INPUT FILE
!
    do i=1, size(mumbo)
      if (.not.(mumbo(i)%mut_flag)) then
!
        PRINT*, '    '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>'
        PRINT*, '>>>>>>>  COULD NOT LOCATE RESIDUE == ',            &
     &                 mumbo(i)%mut_chid, mumbo(i)%mut_nold
        PRINT*, '>>>>>>>  IN INPUT COORDINATE FILE     '
        PRINT*, '>>>>>>>  MUST EXIT                 '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
        PRINT*, '    '
!
        STOP
      end if
    end do
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# STARTING BUILDING ROTAMERS   #'
	PRINT*, '################################'
	PRINT*, '        '
!
!  NOW ADDING MISSING ATOMS TO GLYS MUTATED TO ALA BEFORE
!
!
    checkflag=.true.
    do i=1,mol(1)%mol_nrs
         if (mol(1)%res(i)%res_mut) then
            if (mol(1)%res(i)%res_aas(1)%aa_nam==ala_name) then
!
              call add_groups(1,i,1,1,g2a_name,checkflag)
!
            end if
        end if
    end do
!
!
!  CHECKING WHETHER RESIDUES IN INPUT-FILE ARE EITHER 
!  UNDEFINED (ntype=1) AMINO ACIDS (=2) OR NUCLEOTIDES (=3) 
!
   do i=1,mol(1)%mol_nrs
      set=1
      set(2)=i
      call check_residue_type_set(set,ntype,ctype)
      mol(1)%res(i)%res_ntype= ntype
      mol(1)%res(i)%res_aas(1)%aa_ntype= ntype
!      
!      print*, ntype, ctype
!      if (ntype.eq.1) then
!          print*, mol(1)%res(i)%res_nold, mol(1)%res(i)%res_chid, '  ', mol(1)%res(i)%res_aas(1)%aa_nam 
!      end if
   end do
!
!   NOW CALCULATING THE RIBOSE PSEUDOROTATION ANGLE IN 
!   NUCLEOTIDE RESIDUES BEFORE ATOMS ARE BEING DELETED
!
    do i=1,mol(1)%mol_nrs
      if ((mol(1)%res(i)%res_mut).and.(mol(1)%res(i)%res_ntype.eq.3)) then
         call CALC_PSEUDO_P(i)
      end if
    end do  
!
!  NOW BACKING UP MAIN CHAIN ATOMS OF RES= NRES INTO MOL(NRES+1) 
!  MAIN CHAIN ATOMS = res_type(1)%atm_nams IN CASE OF AMINO ACIDS +  SKIPPING H-atoms 
!  MAIN CHAIN ATOMS = res_type(2)%atm_nams IN CASE OF NUCLEOTIDES +  SKIPPING H-atoms 
!
!  main chain atoms are defined in temporary arrays (res_type(i)%atm_nams(nw))
!  these arrays have been defined/data-filled in  
!  subroutine check_residue_type_set 
!
!  its OK to look for CB since Glycines that are intended to be mutated have been converted to Ala 
!  before (see above)
!
!  counting number of main chain atoms while skippen H-atoms 
!
    if (allocated(nn_mcs)) deallocate(nn_mcs)
    allocate(nn_mcs(size(res_type)))  
!
    do i=1, size(res_type)
       nn_mcs(i)= 0
       do j=1, size(res_type(i)%atm_nams)
          mcnm=res_type(i)%atm_nams(j)
          if (mcnm(1:1)=='H') then
            cycle
          end if
          nn_mcs(i)=nn_mcs(i)+1
       end do
    end do
!
!    print*, nn_mcs
!
    nres=0
    do i=1,mol(1)%mol_nrs
!
       if (mol(1)%res(i)%res_mut) then
!
          nres=nres+1
          ntype=mol(1)%res(i)%res_ntype  
!          
          if (ntype==1) then
!          
        PRINT*, '    '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
        PRINT*, '>>>>>>>  TRYING TO MUTATE RESIDUE:                     '
        PRINT*, '>>>>>>> ', mol(1)%res(i)%res_chid,   &
    &       mol(1)%res(i)%res_nold
        PRINT*, '>>>>>>>  HOWEVER MUMBO FAILED TO DETERMINE WHICH TYPE  '
        PRINT*, '>>>>>>>  OF RESIDUE (AA or NUCLEOTIDE) IS PRESENT AT   '
        PRINT*, '>>>>>>>  THIS POSITON. MOST LIKELY SOME MAIN CHAIN     '
        PRINT*, '>>>>>>>  ATOMS ARE MISSING FOR PROPER IDENTIFICATION   '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
        PRINT*, '    '
        STOP
!   
          end if
!                    
          allocate(mol(nres+1)%res(1))
          mol(nres+1)%mol_nrs=1
          mol(nres+1)%res(1)%res_lig=   .false.
          mol(nres+1)%res(1)%res_num=   mol(1)%res(i)%res_num          
          mol(nres+1)%res(1)%res_nold=  mol(1)%res(i)%res_nold
          mol(nres+1)%res(1)%res_chid=  mol(1)%res(i)%res_chid
          mol(nres+1)%res(1)%res_ntype= mol(1)%res(i)%res_ntype
!
          if (mol(nres+1)%res(1)%res_ntype.eq.3) then 
              mol(nres+1)%res(1)%res_ribflag=  mol(1)%res(i)%res_ribflag
              mol(nres+1)%res(1)%res_ribconf=  mol(1)%res(i)%res_ribconf
              mol(nres+1)%res(1)%res_ribang=   mol(1)%res(i)%res_ribang
              mol(nres+1)%res(1)%res_ribtor=   mol(1)%res(i)%res_ribtor
          end if
! 
!          print*, 'DEBUG 44', mol(nres+1)%res(1)%res_ntype
!
          ntype=ntype-1                  ! correcting for off_set in res_type(i) 
          allocate(mol(nres+1)%res(1)%res_atom_mc(nn_mcs(ntype)))
!
          flag1=.true.
          nn=0
          do j=1, size(res_type(ntype)%atm_nams)          
!          
              mcnm=res_type(ntype)%atm_nams(j)
              if (mcnm(1:1)=='H') then
                cycle
              end if
!              
              nn=nn+1
!              
              flag1=.true.
              flag2=.false.
!              
              do k=1, mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat 
!              
                flag2=.false.                
                atnm = mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_nam 
                if (atnm==mcnm)then
!
          mol(nres+1)%res(1)%res_atom_mc(nn)%at_nam =                     &
        &      mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_nam
          mol(nres+1)%res(1)%res_atom_mc(nn)%at_xyz(1)=                   &
        &      mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(1)
          mol(nres+1)%res(1)%res_atom_mc(nn)%at_xyz(2)=                   &
        &      mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(2)
          mol(nres+1)%res(1)%res_atom_mc(nn)%at_xyz(3)=                   &
        &      mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(3)
          mol(nres+1)%res(1)%res_atom_mc(nn)%at_occ =                     &
        &      mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_occ
          mol(nres+1)%res(1)%res_atom_mc(nn)%at_bfa =                     &
        &      mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_bfa
          mol(nres+1)%res(1)%res_atom_mc(nn)%at_flag =                    &
        &      mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_flag
!
                   flag2=.true.
                   exit
                end if
              end do  ! loop over all atoms in structure
!              
              if (.not.flag2) then 
                    flag1=.false.
              end if
!              
          end do ! loop over all mc atoms
!
          if (.not.flag1) then        ! 
        PRINT*, '    '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
        PRINT*, '>>>>>>>  DID NOT FIND ALL MAIN CHAIN ATOMS FOR         '
        PRINT*, '>>>>>>>  RESIDUES TO BE MUMBOED, MISSING ATOMS IN :    '
        PRINT*, '>>>>>>> ', mol(1)%res(i)%res_chid,   &
        &                   mol(1)%res(i)%res_nold
        PRINT*, '>>>>>>>  MUST EXIT                                     '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
        PRINT*, '    '
             STOP
          end if
        
!
       end if   
    end do  ! loop over all residues in structure       
!
!
!   ALL MAIN-CHAIN ATOMS HAVE BEEN STORED NOW (INCLUDING CB IN CASE OF AAS)
!
!
! print*, 'DEBUG8'
!  do i=2, size(mol)-1   ! if ligand is present, just for debugging purposes 
!     print*, mol(i)%res(1)%res_chid, mol(i)%res(1)%res_nold
!     print*, size(mol(i)%res(1)%res_atom_mc) 
!     do j=1, size(mol(i)%res(1)%res_atom_mc)
!        print*, mol(i)%res(1)%res_atom_mc(j)%at_nam, mol(i)%res(1)%res_atom_mc(j)%at_xyz
!     end do
!  end do 
!  print*, 'END DEBUG8'
!
!   NOW MUTATING AMINO ACIDS RESIDUES TO BE MUMBOED TO XAA (TO PROTEIN BACKBONE ATOMS MINUS CB)
!   AND NUCLEOTIDES TO NUC RESIDUES (TO NUCLEOTIDE BACKBONE ATOMS AS DEFINED IN RESIDUE NUC)
!
    do i=1,mol(1)%mol_nrs
      if (mol(1)%res(i)%res_mut) then
!
        if (mol(1)%res(i)%res_ntype.eq.2) then 
!            mol(1)%res(i)%res_aas(1)%aa_nam=gly_name         ! name has been changed for consistency with nucs
            mol(1)%res(i)%res_aas(1)%aa_nam=xaa_name            
        else if (mol(1)%res(i)%res_ntype.eq.3) then
            mol(1)%res(i)%res_aas(1)%aa_nam=nuc_name
        end if
!
        aanm= mol(1)%res(i)%res_aas(1)%aa_nam
        call init_aminoacid(1,i,1,1,aanm)
        call reload_coord_from_sim(1,1,i)
!        
!        call dump_pdb(1,i,1,1)
!
      end if
    end do 
!
!
! NOW ADDING GROUPS TO MAIN CHAIN ATOMS / CONSTANT PARTS OF PROTEIN
! (THIS IS MAINLY ADDING HYDROGEN ATOMS) 
! (ALSO WORKS WITH NUCLEOTIDES IF RESIDUE NUC IS PROPERLY DEFINED 
!  IN LIBRARIES)
!
    checkflag=.true.
    do i=1,mol(1)%mol_nrs
         aanm=mol(1)%res(i)%res_aas(1)%aa_nam
         call add_groups(1,i,1,1,aanm,checkflag)
    end do
!
!   print*, 'DEBUG92'
!	do i=1,mol(1)%mol_nrs 
!	   print*, mol(1)%res(i)%res_ntype, mol(1)%res(i)%res_chid, mol(1)%res(i)%res_nold
!	end do
!   DEBUG
!
! NOW ADDING PEPTIDE-H TO MAIN CHAIN ATOMS 
!
    flag=.false.
    do i=1,size(geo)
       if (geo(i)%g_aan==pep_name.and.geo(i)%g_ngr>0) then
         flag=.true.
       end if
    end do
    if (flag) then
      checkflag=.true.
      do i=1,mol(1)%mol_nrs
 !        only adding peptide-H if ntype=2 = AAs
         if (mol(1)%res(i)%res_ntype==2) then 
           aanm=pep_name
           call add_groups(1,i,1,1,aanm,checkflag)
         end if
      end do
    end if
!
! NOW ADDING CA-H ATOM TO MAIN CHAIN ATOMS FOR THE AMBER ALL H ATOM FORCE FIELD
! (ONLY ADDED IF CAH-ENTRY IS DEFINED IN MUMBO_SPECIFIC ROTAMER-DATABASE)
!
   flag=.false.
   do i=1,size(geo)
      if (geo(i)%g_aan==cah_name.and.geo(i)%g_ngr>0) then
         flag=.true.
      end if
   end do
   if (flag) then
      checkflag=.true.
      do i=1,mol(1)%mol_nrs
!
!      only adding CA-H if ntype=2 = AAs
!
       if (mol(1)%res(i)%res_ntype==2) then 
          aanm=cah_name
          call add_groups(1,i,1,1,aanm, checkflag)
       end if
! 
     end do
   end if
!
! DEBUG10
!	print*, 'DEBUG10 MAIN CHAIN BUILDING NOW COMPLETED' 
!	do i=1, mol(1)%mol_nrs
!	  call dump_pdb(1,i,1,1)
!	end do
!	print*, 'END DEBUG10'
! DEBUG10
!
!
!  FROM HERE ON, NO NEED FOR SIM... ANYMORE 
!
     if(allocated(sim)) deallocate(sim)
!
!
!  NOW STARTING BUILDING ROTAMERS.....
!
!  FIRST CALCULATE PHI-PSI FOR AAS TO BE MUMBOED
!  AND O3'-C3'-C4'-C5' DIHEDRAL ANGLE FOR NUCLEOTIDES TO BE MUMBOED
!
!
    if (mumbo_lig_flag) then 	
      ntmol= size(mol)-1
    else 
      ntmol= size(mol)
    end if
!
    do i=2,ntmol
        mol(i)%res(1)%res_lig=.false.
        mol(i)%res(1)%res_pps=.false.
        mol(i)%res(1)%res_endo_exo=.false.
        mol(i)%res(1)%res_endo2=.false.
        mol(i)%res(1)%res_endo3=.false.
        mol(i)%res(1)%res_bbr=.false.
    end do
!
!   amino acid reporting
!
    PRINT*, '        '
    PRINT*, '  PHI-PSI VALUES AT POSITIONS TO BE MUMBOED: '
    PRINT*, '         '
    PRINT*, '  POSITION        PHI            PSI           '
    PRINT*, '  --------        ---            ---           '
!
    do i=2,ntmol
      if (mol(i)%res(1)%res_ntype.ne.2) cycle
      call calc_phi_psi(i)
      if(mol(i)%res(1)%res_pps) then
            WRITE(*,'(2A,I5,F14.2,a,F14.2)') '    ',mol(i)%res(1)%res_chid,   & 
    &             mol(i)%res(1)%res_nold,                                     &
    &             mol(i)%res(1)%res_phi_psi(1),' ', mol(i)%res(1)%res_phi_psi(2)
      end if
    end do
!
!   nucleotide reporting
!
    PRINT*, '        '
    PRINT*, '        '
    PRINT*, "  2'- ENDO/3'-ENDO-LIKE CONFORMATION AT POSITION TO BE MUMBOED: "
    PRINT*, '        '
    PRINT*, "    ( DEDUCED FROM THE PSEUDOROTATIONAL ANGLE ) "
    WRITE(*, '(A,F6.1,A)') '     ( pseudo rotational ang. < ', psdorot_angle_lim, " ==> 3'endo, B-form )" 
    WRITE(*, '(A,F6.1,A)') '     ( pseudo rotational ang. > ', psdorot_angle_lim, " ==> 2'endo, B-form )" 
    PRINT*, '         '
    PRINT*, "  POSITION    DELTA ANGLE    PSEUDOROTATION ANGLE "
    PRINT*, '  --------    -----------    -------------------- '
!
    do i=2,ntmol
       if (mol(i)%res(1)%res_ntype.ne.3) cycle
       mol(i)%res(1)%res_endo_exo = .false.
       call calc_ribose_delta(i)
       label_d = '          '
!                 1234567890  
       if (mol(i)%res(1)%res_deltaflag) then
           write(label_d,'(F10.2)') mol(i)%res(1)%res_rib_delta
       else
          write(label_d,'(A)') ' not det. '
!                               1234567890  
       end if
!       
       label_p = '               '
!                 123456789012345  
       if (mol(i)%res(1)%res_ribflag) then
           write(label_p,'(F10.2,X,A4)') mol(i)%res(1)%res_ribang, mol(i)%res(1)%res_ribconf 
       else
          write(label_p,'(A)') ' not det.      '
!                               123456789012345  
       end if
!
       label_ee = '                              '
!                  123456789012345678901234567890  
!
!       defining endo/exo based on pseudoratotion angle criterium 
!
          if (mol(i)%res(1)%res_ribang.gt.(psdorot_angle_lim+180)) then 
                mol(i)%res(1)%res_ribang = mol(i)%res(1)%res_ribang - 360
          end if
!
         if (mol(i)%res(1)%res_ribang.gt.psdorot_angle_lim) then
           mol(i)%res(1)%res_endo2 = .true.
           mol(i)%res(1)%res_endo_exo = .true.
           write(label_ee,'(A)') " ==>  2'endo-like, B-form     "
!                                 123456789012345678901234567890  
!         
         else if (mol(i)%res(1)%res_ribang.le.psdorot_angle_lim) then
           mol(i)%res(1)%res_endo3 = .true.      
           mol(i)%res(1)%res_endo_exo = .true.
           write(label_ee,'(A)') " ==>      3'endo-like, A-form "
!                                 123456789012345678901234567890  
         end if   
!
         WRITE(*,'(2A,I5,4X, A, 6X, A, 4X, A)') '    ',mol(i)%res(1)%res_chid,   & 
     &             mol(i)%res(1)%res_nold, label_d, label_p, label_ee
!
    if (.not.mol(i)%res(1)%res_endo_exo) then
           PRINT*, '    '
           PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
           PRINT*, ">>>>>>>  NEITHER  2'endo, B-form - NOR - 3'endo, A-form  "
           PRINT*, '>>>>>>>  IDENTIFIED AT ',mol(i)%res(1)%res_chid, mol(i)%res(1)%res_nold
           PRINT*, '>>>>>>>  THIS WILL CAUSE PROBLEMS                      '
           PRINT*, '>>>>>>>  MUST EXIT                                     '
           PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
           PRINT*, '    '
           STOP
    end if
!       
    end do
!
    PRINT*, '        '
!    
!   RESETTING PHI_PSI IN CASE OVERALL PHIPSI_FLAG = FALSE FOR ALL RESIDUES
!   THIS IS FINE EVEN WITH NUCLEOTIDES BEING PRESENT
!
	if (.not.phipsi_flag) then
	    do i=2,ntmol
	       mol(i)%res(1)%res_pps=.false.
	    end do
	end if
!
!DEBUG
!	print*, size(mumbo)
!	do i=1,size(mumbo)
!	  print*, mumbo(i)%mut_nold		
!	  print*, mumbo(i)%mut_nnew		
!	  print*, mumbo(i)%mut_flag	
!	  print*, mumbo(i)%mut_chid	
!	  do j=1,size(mumbo(i)%mut_aas)
!		print*, mumbo(i)%mut_aas(j)		
!	  end do
!	end do
!DEBUG
!
!
!  NOW BUILDING THE AMINO ACID PART (CODE ONLY EXECUTED FOR AMONIACIDS)
!
!  NOW ALLOCATE SPACE FOR NUMBER OF AAS AS REQUESTED FOR A POSITION IN MUMBO
!  AND NUMBER OF ROTAMERS AS DEFINED IN GEOM_DATA
!
   do i=2,ntmol
!                    
    if (mol(i)%res(1)%res_ntype.ne.2) cycle
!
    do j=1, size(mumbo)
      if (mumbo(j)%mut_nnew == mol(i)%res(1)%res_num) then
!      
        if (mumbo(j)%mut_noveralltype.ne.mol(i)%res(1)%res_ntype) then 
          PRINT*, '    '
          PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>             '
          PRINT*, '>>>>>>>  MISMATCH BETWEEN REQUESTED TYPE OF RESIDUE TO BE BULD AND '
          PRINT*, '>>>>>>>  AND TYPE OF RESIDUE PRESENT AT THE REQUESTED POSITION     '
          PRINT*, '>>>>>>>  POSITION TO BEING CONSIDERED: ',mol(i)%res(1)%res_chid,'  ', mol(i)%res(1)%res_nold 
          PRINT*, '>>>>>>>  THE FOLLOWING RESIDUE IS NOT AN AMINO ACID: ',mumbo(j)%mut_aas(1)
          PRINT*, '>>>>>>>                                                            '
          PRINT*, '>>>>>>>  MUST STOP                                                 '
          PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
          PRINT*, '    '
          STOP
        end if
!           
        n1 = size(mumbo(j)%mut_aas)
!                                                    allocate number of different amino acids        
        allocate (mol(i)%res(1)%res_aas(n1))
        mol(i)%res(1)%res_naa=n1
        do k=1,n1
           aanm=mumbo(j)%mut_aas(k)
           mol(i)%res(1)%res_aas(k)%aa_nam=aanm
!                                                     allocate number of rotamers    
           if (aanm.eq.ala_name) then
               n2=1
           else if (aanm.eq.gly_name) then
               n2=1
           else
               flag=.false.
               do l=1,size(geo)
                  if (geo(l)%g_aan==aanm) then
                     if (geo(l)%g_ndh > 0) then
                        n2=geo(l)%dh(1)%at_nrt
                        flag=.true.
                     end if
                  end if
               end do
               if (.not.flag) then
       PRINT*, '    '
       PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
       PRINT*, '>>>>>>>  NO IDEA HOW TO BUILD REQUESTED RESIDUE:       '
       PRINT*, '>>>>>>> ',aanm
       PRINT*, '>>>>>>>  IN BUILD_NEW                                  '
       PRINT*, '>>>>>>>  MUST EXIT                                     '
       PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
       PRINT*, '    '
        stop
               end if
           end if
!
!                                               allocate number of atoms
           allocate(mol(i)%res(1)%res_aas(k)%aa_rots(n2))
           mol(i)%res(1)%res_aas(k)%aa_nrt=n2
           aanm=mol(i)%res(1)%res_aas(k)%aa_nam
!
!           if (aanm==gly_name) then 
!            print*, '88##########'
!            call dump_pdb(i,1,k,n2)
!           end if 
!
           call init_aminoacid(i,1,k,n2,aanm)
!           
!           if (aanm==gly_name) then 
!            print*, '99##########'
!            call dump_pdb(i,1,k,n2)
!           end if 
!!
        end do
      end if
    end do
   end do
!
!
!    CHECK HERE WHETHER ALL RESIDUES AT A GIVEN POSITION ARE DEFINED IN THE PHI-PSI-DEPENDENT LIBRARY
!    IF NOT THEN ONLY PHI_PSI INDEPENDENT ROTAMERS WILL BE BUILD AT THAT POSITION 
!
!    FIRST CHECK WHICH OF THE REQUESTED RESIDUE TYPES ARE PRESENT IN THE PHI-PSI-DEPENDENT LIBRARY
!
     if (phipsi_flag) then
         do i=1,size(reslst)
            aanm='    '
            flag=.false.
            aanm(1:3)=reslst(i)%aas
            if (aanm.eq.gly_name.or.aanm.eq.ala_name) then
               flag=.true.
            else 
               call check_phi_psi_lib(aanm,flag)
            end if
            reslst(i)%flag=flag
         end do
!
! NOW FLAG POSITIONS THAT CONTAIN RESIDUES THAT ARE ABSENT IN THE PHI-PSI-DEPENDENT LIBRARY (ALA AND GLY ARE EXEMPT)
! CODE IS SAVE FOR NUCLEOTIDES AS WELL SINCE THEN: mol(i)%res(1)%res_pps=.false.
!
         do i=2, ntmol   
           if (.not.(mol(i)%res(1)%res_pps)) then
                  cycle
           else
             do j=1,mol(i)%res(1)%res_naa
               aanm=mol(i)%res(1)%res_aas(j)%aa_nam
               do k=1,size(reslst) 
                  if (aanm(1:3).eq.reslst(k)%aas) then
                    if (.not.reslst(k)%flag) then  
                        mol(i)%res(1)%res_pps=.false.
                        cycle
                    end if    
                  end if
               end do
               if (.not.(mol(i)%res(1)%res_pps)) then
                      cycle
               end if
             end do
           end if
         end do 
!
     end if
!       
!
! NOW BUILD AA-ROTAMERS THAT ARE NOT PHI-PSI DEPENDENT
!     (NUCLEOTIDES ARE OMITTED (mol(i)%res(1)%res_ntype.ne.2) at this stage)
!       ... ONLY AMINO ACIDS ARE BEING BUILD 
!
    num_rot=0
    do i=2,ntmol
!
      if (mol(i)%res(1)%res_ntype.ne.2) cycle
!      
      if (.not.(mol(i)%res(1)%res_pps)) then
        do j=1,mol(i)%res(1)%res_naa
         aanm=mol(i)%res(1)%res_aas(j)%aa_nam
         do k=1,mol(i)%res(1)%res_aas(j)%aa_nrt
           mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag=.true.  
           mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag2=.false.  
!
!       (FIRST COPY MAIN CHAIN ATOMS BACK ==> ACTUALLY ONLY CB IS BEING COPIED BACK)
!
          do l=1,mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_nat
            atnm=mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_nam
            do m=1,size(mol(i)%res(1)%res_atom_mc)                             
              if (mol(i)%res(1)%res_atom_mc(m)%at_nam==atnm) then
!
     mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(1) =         &
         &   mol(i)%res(1)%res_atom_mc(m)%at_xyz(1) 
     mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(2) =         &
         &   mol(i)%res(1)%res_atom_mc(m)%at_xyz(2) 
     mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(3) =         &
         &   mol(i)%res(1)%res_atom_mc(m)%at_xyz(3) 
     mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_occ =            &
         &   mol(i)%res(1)%res_atom_mc(m)%at_occ
     mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_bfa =            &
         &   mol(i)%res(1)%res_atom_mc(m)%at_bfa 
     mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_flag =           &
         &   mol(i)%res(1)%res_atom_mc(m)%at_flag 
!    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_flag = .true.
!
              end if
            end do
          end do
!
          num_rot=k
          call build_rotamer(aanm,i,1,j,k,num_rot)
!
         end do
        end do
      end if
    end do
!
!  NOW BUILD ROTAMERS THAT ARE PHI_PSI DEPENDENT (ONLY IN CASE OF AMINO ACIDS)
!
   if (phipsi_flag) then
    PRINT*, '        '
    PRINT*, '  READING IN PHI-PSI-DEPENDENT ROTAMER LIBRARY: '
    PRINT*, '        ',mumbo_libpps(1:len_trim(mumbo_libpps))
    PRINT*, '        '
!
    do i=2,ntmol
!                    only for amino acids... 
      if (mol(i)%res(1)%res_ntype.ne.2) cycle      
!      
      if (.not.mol(i)%res(1)%res_pps) then
!
    PRINT*, '        '
    PRINT*, '>>>  NOT POSSIBLE TO USE PHI-PSI-DEPENDENT ROTAMERS FOR POSITION: '
    PRINT*, '>>>  ',mol(i)%res(1)%res_chid,  mol(i)%res(1)%res_nold 
    PRINT*, '>>>  USING INDEPENDENT ROTAMERS INSTEAD FROM FILE:  '
    PRINT*, '>>>  ',mumbo_librot(1:len_trim(mumbo_librot))
    PRINT*, '        '
!
      else
!
        do j=1,mol(i)%res(1)%res_naa
          aanm=mol(i)%res(1)%res_aas(j)%aa_nam
          if (aanm == ala_name) then
              n2=1
          else if (aanm == gly_name) then
              n2=1
          else
!
            call phi_psi_dep_geom(aanm,i)
!
            flag=.false.
            do l=1,size(geo)
               if (geo(l)%g_aan==aanm) then
               n1=l
                   if (geo(l)%g_ndh > 0) then
                     n2=geo(l)%dh(1)%at_nrt
                     flag=.true.
                   end if
               end if
            end do
            if (.not.flag) then
    PRINT*, '    '
    PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
    PRINT*, '>>>>>>>  NO IDEA HOW TO BUILD REQUESTED RESIDUE:       '
    PRINT*, '>>>>>>> ',aanm
    PRINT*, '>>>>>>>  PHI PSI DEPENDENT                             '
    PRINT*, '>>>>>>>  MUST EXIT                                     '
    PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
    PRINT*, '    '
    stop
            end if
          end if
!
          allocate(mol(i)%res(1)%res_aas(j)%aa_rots(n2))
          mol(i)%res(1)%res_aas(j)%aa_nrt=n2
!          
          call init_aminoacid(i,1,j,n2,aanm)
!
! NOW AGAIN COPY MAIN CHAIN ATOMS BACK ==> ACTUALLY ONLY CB IS BEING COPIED BACK
!
          do k=1,n2
             mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag=.true. 
             mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag2=.false. 
             do l=1,mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_nat
              atnm=mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_nam
              do m=1,size(mol(i)%res(1)%res_atom_mc)   
                if (mol(i)%res(1)%res_atom_mc(m)%at_nam==atnm) then
!
        mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(1) =         &
            &   mol(i)%res(1)%res_atom_mc(m)%at_xyz(1) 
        mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(2) =         &
            &   mol(i)%res(1)%res_atom_mc(m)%at_xyz(2) 
        mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(3) =         &
            &   mol(i)%res(1)%res_atom_mc(m)%at_xyz(3) 
        mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_occ =            &
            &   mol(i)%res(1)%res_atom_mc(m)%at_occ
        mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_bfa =            &
            &   mol(i)%res(1)%res_atom_mc(m)%at_bfa 
        mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_flag =            &
            &   mol(i)%res(1)%res_atom_mc(m)%at_flag 
!      mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_flag = .true.
!
                end if
              end do
             end do
!      
             num_rot=k
             call build_rotamer(aanm,i,1,j,k,num_rot)
          end do
!
        end do
      end if
    end do
!
   end if
!
!   NOW REMOVING CB FROM THE MAIN CHAIN PROTEIN PART 
!
    do i=2,ntmol
      if (mol(i)%res(1)%res_ntype.ne.2) cycle
      do j=1,size(mol(i)%res(1)%res_atom_mc)   
         if (mol(i)%res(1)%res_atom_mc(j)%at_nam=='CB  ') then
           mol(i)%res(1)%res_atom_mc(j)%at_flag=.false.
         end if
      end do 
    end do
!
!   END OF PROTEIN PART BUILDING 
!
!
!
!   HERE NUCLEOTIDE BUILDING IS BEING ACHIEVED...
!   WITH ALL ITS FACETS....
!
!
    call init_rib_conformers  ! number of ribose conformers to build is being initialized
!
!      print*, 'DEBUG140' 
!      do i=1, size(rib_conf)
!        print*, rib_conf(i)%ctype, size(rib_conf(i)%cfnm)
!        do j= 1, size(rib_conf(i)%cfnm)
!           print*, rib_conf(i)%cfnm(j)
!        end do 
!      end do
!     DEBUG140 
!
    nrib=1
    nrot=0
    ntot=0
! 
!
    do i=2,ntmol
!
       if (mol(i)%res(1)%res_ntype.ne.3) cycle
!       
       if      (mol(i)%res(1)%res_endo_exo.and.mol(i)%res(1)%res_endo2) then 
           nrib=size(rib_conf(1)%cfnm)                     
       else if (mol(i)%res(1)%res_endo_exo.and.mol(i)%res(1)%res_endo3) then
           nrib=size(rib_conf(2)%cfnm)  
       else
           PRINT*, '    '
           PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
           PRINT*, '>>>>>>>  NO IDEA HOW TO ADD RIBOSE MOIETY TO:             '
           PRINT*, '>>>>>>> ',mol(i)%res(1)%res_chid,  mol(i)%res(1)%res_nold 
           PRINT*, ">>>>>>>  INFORMATION REGARDING 2'-ENDO AND 3'-ENDO IS UNCLEAR "
           PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
           PRINT*, '    '
           STOP
       end if
!
       do j=1, size(mumbo)
         if (mumbo(j)%mut_nnew == mol(i)%res(1)%res_num) then
!         
           if (mumbo(j)%mut_noveralltype.ne.mol(i)%res(1)%res_ntype) then 
             PRINT*, '    '
             PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>             '
             PRINT*, '>>>>>>>  MISMATCH BETWEEN REQUESTED TYPE OF RESIDUE TO BE BULD AND '
             PRINT*, '>>>>>>>  AND TYPE OF RESIDUE PRESENT AT THE REQUESTED POSITION     '
             PRINT*, '>>>>>>>  POSITION TO BEING CONSIDERED: ',mol(i)%res(1)%res_chid,'  ', mol(i)%res(1)%res_nold 
             PRINT*, '>>>>>>>  THE FOLLOWING RESIDUE IS NOT A NUCLEOTIDE: ',mumbo(j)%mut_aas(1)
             PRINT*, '>>>>>>>                                                            '
             PRINT*, '>>>>>>>  MUST STOP                                                 '
             PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
             PRINT*, '    '
             STOP
           end if
!         
           n1 = size(mumbo(j)%mut_aas)
           allocate (mol(i)%res(1)%res_aas(n1))
!           
           mol(i)%res(1)%res_naa=n1
           do k=1,n1
              aanm=mumbo(j)%mut_aas(k)
              mol(i)%res(1)%res_aas(k)%aa_nam=aanm
!              
              flag=.false.
              do l=1,size(geo)
                   if (geo(l)%g_aan==aanm) then
                      flag=.true.
                      if (geo(l)%g_ndh.eq.0) then 
        PRINT*, '    '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
        PRINT*, '>>>>>>>  ERROR IN mnew.f90                             '
        PRINT*, '>>>>>>>  EXPECTING geo(l)%g_ndh > 0                    '   
        PRINT*, '>>>>>>>  MUST STOP                                     '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
        PRINT*, '    '
                        stop
                      end if
                      nrot=geo(l)%dh(1)%at_nrt    
                      ntot=nrot*nrib
                      exit
                  end if
              end do
              if (.not.flag) then
        PRINT*, '    '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
        PRINT*, '>>>>>>>  NO IDEA HOW TO BUILD REQUESTED RESIDUE:       '
        PRINT*, '>>>>>>> ',aanm
        PRINT*, '>>>>>>>  IN BUILD_NEW                                  '
        PRINT*, '>>>>>>>  MUST EXIT                                     '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>'
        PRINT*, '    '
                  stop
              end if
!                
              allocate(mol(i)%res(1)%res_aas(k)%aa_rots(ntot))  
              mol(i)%res(1)%res_aas(k)%aa_nrt=ntot
              aanm=mol(i)%res(1)%res_aas(k)%aa_nam
              call init_aminoacid(i,1,k,ntot,aanm)
!          
           end do
         end if
       end do
    end do
!
!   print*, 'DEBUG100'
!	do i=2,ntmol
!	   if (mol(i)%res(1)%res_ntype.ne.3) cycle
!       do j=1,size(mol(i)%res(1)%res_aas)
!         print*, 'DEBUG 100 ', mol(i)%res(1)%res_aas(j)%aa_nam
!         call dump_pdb_atflag(i,1,j,1)
!       end do
!    end do
!    DEBUG100
!
!   NOW COPYING MAIN CHAIN ATOMS BACK
!
	do i=2,ntmol
        if (mol(i)%res(1)%res_ntype.ne.3) cycle
!      
	    do j=1,mol(i)%res(1)%res_naa
		aanm=mol(i)%res(1)%res_aas(j)%aa_nam
		do k=1,mol(i)%res(1)%res_aas(j)%aa_nrt
		   mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag=.true.  
		   mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag2=.false.  
!
		  do l=1,mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_nat
		  atnm=mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_nam
			do m=1,size(mol(i)%res(1)%res_atom_mc)
			if (mol(i)%res(1)%res_atom_mc(m)%at_nam==atnm) then
!
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(1) =         &
     &   mol(i)%res(1)%res_atom_mc(m)%at_xyz(1) 
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(2) =         &
     &   mol(i)%res(1)%res_atom_mc(m)%at_xyz(2) 
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(3) =         &
     &   mol(i)%res(1)%res_atom_mc(m)%at_xyz(3) 
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_occ =            &
     &   mol(i)%res(1)%res_atom_mc(m)%at_occ
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_bfa =            &
     &   mol(i)%res(1)%res_atom_mc(m)%at_bfa 
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_flag =           &
     &   mol(i)%res(1)%res_atom_mc(m)%at_flag 
!	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_flag = .true.
!
			end if
			end do
		  end do
!
		end do
	    end do
	end do
!
!   NOW ADDING RIBOSE ATOMS AND STARTING BUILDING CONFOMERS
!
    num=0
    nrib=0
    num_rot=0
    checkflag=.false.
    do i=2,ntmol
        if (mol(i)%res(1)%res_ntype.ne.3) cycle
        if (mol(i)%res(1)%res_endo_exo.and.mol(i)%res(1)%res_endo2) then 
            nrib=size(rib_conf(1)%cfnm)        
            num=1
        else if (mol(i)%res(1)%res_endo_exo.and.mol(i)%res(1)%res_endo3) then
            nrib=size(rib_conf(2)%cfnm)        
            num=2
        end if
!
        do j=1,mol(i)%res(1)%res_naa
           aanm=mol(i)%res(1)%res_aas(j)%aa_nam
!           
           do k=1,size(geo)           
               if (geo(k)%g_aan==aanm) then           
                  nrot=geo(k)%dh(1)%at_nrt               
                  exit
               end if
           end do        
! 
           nn=0
           do k=1, nrib
              aanm_conf = rib_conf(num)%cfnm(k)
              do l=1, nrot
                 num_rot=l
                 nn=nn+1
!      
                 call add_groups(i,1,j,nn,aanm_conf,checkflag)
!                 
                 call add_groups(i,1,j,nn,aanm,checkflag)   ! adding missing atoms (N1 and N9) of nucleobase
                                                            ! before building rotamers
!
                 call build_rotamer(aanm,i,1,j,nn,num_rot)  ! chi-torsion-angle
!
              end do
           end do              
!
        end do
    end do
!
!   print*, 'DEBUG200'
!	do i=2,ntmol
!	   if (mol(i)%res(1)%res_ntype.ne.3) cycle
!       do j=1, size(mol(i)%res(1)%res_aas)
!         print*, 'DEBUG200', mol(i)%res(1)%res_aas(j)%aa_nam
!           do k=1, mol(i)%res(1)%res_aas(j)%aa_nrt
!              print*, 'DEBUG***'
!              call dump_pdb_atflag(i,1,j,k)
!           end do
!       end do
!    end do
!   DEBUG200
!
!   NOW DONE WITH BUILDING NUCLEOTIDES
!
!
!   NOW READING IN LIGAND CONFORMATIONS/ORIENTATIONS
!
    if (mumbo_lig_flag) then 
      call init_ligand 
    end if
!
!  NOW ADDING ALL REMAINING GROUPS TO ALL RESIDUES IN ALL MOLECULES
!  (THIS WORKS FIND FOR AMINO ACIDS, NUCLEOTIDES, LIGANDS etc...)
!
    checkflag=.true.
	do i=2, size(mol)
	   do j=1,mol(i)%res(1)%res_naa
	     aanm=mol(i)%res(1)%res_aas(j)%aa_nam
	     do k=1,mol(i)%res(1)%res_aas(j)%aa_nrt
	     
		   call add_groups(i,1,j,k,aanm,checkflag)
		   
	     end do
	   end do
	end do
!
!!  DEBUG
!    do i=1,mol(1)%mol_nrs
!         print*,'1', i
!         call dump_pdb(1,i,1,1)
!    end do
!!   END DEBUG
!
!   FOR MFILL_MISSING DATA IT IS IMPORTANT TO ALSO KNOW THE TYPE OF RESIDUES PRESENT IN mol(1)
!
    do i=1,1
      do j=1,mol(i)%mol_nrs
        do k=1,1            ! it should be perfectly fine to check just one residue
             aanam = mol(i)%res(j)%res_aas(k)%aa_nam
             call check_residue_type_byname(aanam,ntype,ctype)                 
             mol(i)%res(j)%res_ntype = ntype
!             print*, 'MNEW: ',i, j, k, ' ntype= ', ntype, ctype, mol(i)%res(j)%res_nold, mol(i)%res(j)%res_chid
!        
        end do
      end do
    end do
!
! NOW FINISHING SETTING UP A FEW THINGS
!
	do i=1,size(mol)
      do j=1,mol(i)%mol_nrs
         do k=1,mol(i)%res(j)%res_naa
            do l=1, mol(i)%res(j)%res_aas(k)%aa_nrt
                 do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
		    mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_n12 = 0
		    mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_n14 = 0
                 end do
	       end do 
	     end do
	   end do
	end do
!
!
	PRINT*, '        '
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY BUILD-NEW            #'
	PRINT*, '################################'
	PRINT*, '        '
!
	PRINT*, '  READ IN', mol(1)%mol_nrs,' RESIDUES FROM FILE '
	PRINT*, '     ',mumbo_inpdb(1:len_trim(mumbo_inpdb))
!
	n1=mol(1)%mol_nrs
	PRINT*, '  '
	PRINT*, '  POSITIONS TO BE MUMBOED IDENTIFIED  '
	PRINT*, "  RES. NUM.  INTER. NUM.  PHI-PSI-DEP  NUCLEOT.  2'endo    3'endo "
	PRINT*, '  ---------  -----------  -----------  --------  -------   -------'
	nres=0
	do j=2,ntmol
		nres=nres+1
		write(*,'(4X,A1,X,I5,5X,I5,7X,A,X,3A)')                                               &
    &          mol(j)%res(1)%res_chid, mol(j)%res(1)%res_nold,                        &
    &          mol(j)%res(1)%res_num, echo(mol(j)%res(1)%res_pps),                    &
    &          echo(mol(j)%res(1)%res_endo_exo),                                      &
    &     echo(mol(j)%res(1)%res_endo2),         echo(mol(j)%res(1)%res_endo3)
!    
	end do
!
	PRINT*, '   '
	if (nres /= (ntmol-1)) then
	PRINT*, '                                   >>>>>> BUMMER >>>>>>'
	end if
	PRINT*, '  NUMBER OF POSITIONS REQUESTED:  ',ntmol-1
	PRINT*, '  NUMBER OF POSITIONS FOUND:      ',nres
	if (nres /= (ntmol-1)) then
	PRINT*, '                                   >>>>>> BUMMER >>>>>>'
	PRINT*, '   '
	end if
!
	if (mumbo_lig_flag) then
	  n1= mol(ntmol+1)%res(1)%res_aas(1)%aa_nrt
 	PRINT*, '   '
	PRINT*, '  READ IN', n1 ,' LIGAND ORIENTATIONS FROM FILE '
	PRINT*, '     ',mumbo_lig_in(1:len_trim(mumbo_lig_in))
	PRINT*, '   '
	end if
!
	PRINT*, '   '
	PRINT*, '  ... FINISHED BUILDING SIDE-CHAIN ROTAMERS / LIGANDS '
	PRINT*, '   '
!
!
	END SUBROUTINE BUILD_NEW
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE INIT_AMINOACID (m1,m2,m3,m4,aanm)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!     subroutine to initialize all atoms in new amino acid consisting of m4 rotamer
!     the new amono acid and the number of rotamers has already been initialized before...  
!
!
    USE MOLEC_M
    USE AA_DATA_M
    USE MUMBO_DATA_M
    USE MAX_DATA_M
    USE WATER_DATA
!
    IMPLICIT NONE
!
    INTEGER :: m1,m2,m3,m4, n1
    INTEGER :: i, j, k
    INTEGER :: nres, n_xaa_nuc, natms, nwat, nprev
    LOGICAL :: flag, wat_flag, flag1, flag2
    CHARACTER (LEN=4) :: atnm, aanm, res_name, aanm_alt, atnm1, atnm2
!
!	GFORTRAN
    nres=0
    n_xaa_nuc=0
!	GFORTRAN
!
! FIRST LOOK UP RESIDUE IN mumbo_lib_par
!
!    aanm=mol(m1)%res(m2)%res_aas(m3)%aa_nam
!
     flag1=     .false.
     flag2=     .false.
     wat_flag = .false.
     nwat = 0
!     
     do i=1,(size(ps))
          if (ps(i)%p_aan == aanm) then
             nres = i
             flag1= .true.
         end if
     end do
!
     if (.not.flag1) then                  ! trying to figure out if a solvated residue is requested  
!                                          ! looking up the solvated and corresponding non-solvated residue names 
        wat_flag = .false. 
        do i = 1,size(ASWNAMES)
         if (aanm.eq.ASWNAMES(i)) then        
              aanm_alt = ASNAMES(i)        
              wat_flag   = .true.        
              exit        
         end if         
        end do        
!
        if (wat_flag) then
           do i=1,(size(ps))
              if (ps(i)%p_aan == aanm_alt) then
                 nres = i
                 flag1= .true.
                 exit
              end if
           end do
           do i=1,(size(ps))
              if (ps(i)%p_aan == WHOH_name) then
                 nwat = i
                 flag2= .true.
                 exit
              end if
           end do
        end if 
!
        if (.not.(flag1.and.flag2)) then 
!
     PRINT*, '          '
     PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  '
     PRINT*, ' >>>>'
     PRINT*, ' >>>>  COULD NOT FIND PARAMETERS FOR RESIDUE      '
     PRINT*, ' >>>>',  mol(m1)%res(m2)%res_aas(m3)%aa_nam 
     PRINT*, ' >>>>',  m1 , m2, m3, ' IN INIT RESIDUE           '
     PRINT*, ' >>>>  IN FILE: ', mumbo_libpar(1:len_trim(mumbo_libpar))
     PRINT*, ' >>>>  RESIDUE LEFT UNCHANGED AT THIS STAGE       '
     PRINT*, ' >>>>'
     PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> '
     PRINT*, '          '
        end if
!        
     end if        
!
     if (wat_flag) then
          natms = ps(nres)%p_at_ncnt + ps(nwat)%p_at_ncnt
     else 
          natms = ps(nres)%p_at_ncnt
     end if
!
!
     if (m1/=1) then
!
! LOOK UP XAA (IN CASE OF AAs) AND NUCS (Nucleotides) IN MUMBO_LIB_PAR
! FOR ATOMS TO BE SUBSTRACTED IN ORDER TO GET ROTAMER
! ATOMS WITHOUT MAIN CHAIN ATOMS 
!
       flag=.false.
       res_name = xaa_name                    ! this doesn't harm in general
!       
       if (mol(m1)%res(m2)%res_ntype.eq.2) then 
          res_name = xaa_name 
       else if (mol(m1)%res(m2)%res_ntype.eq.3) then
          res_name = nuc_name
       end if
!      
       do i=1,(size(ps))
          if (ps(i)%p_aan == res_name) then
            n_xaa_nuc = i
            flag = .true.
          end if
       end do
!      
       if (.not.flag) then
    PRINT*, '          '
    PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>      '
    PRINT*, ' >>>>                                                 '
    PRINT*, ' >>>>  COULD NOT FIND PARAMETERS FOR RESIDUE          '
    PRINT*, ' >>>>  XAA/NUC NEEDED FOR SUBSTRACTION IN INIT_RESI   '
    PRINT*, ' >>>>  IN FILE: ', mumbo_libpar(1:len_trim(mumbo_libpar))
    PRINT*, ' >>>>  RESIDUE LEFT UNCHANGED IN ADD_MISS_ATOMS       '
    PRINT*, ' >>>>                                                 '
    PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>     ' 
    PRINT*, '          '
        stop
       end if
!
!  REMOVE ATOMS IN COMMON
!
       do i=1, ps(nres)%p_at_ncnt
         do j=1, ps(n_xaa_nuc)%p_at_ncnt
           if (ps(n_xaa_nuc)%p_at_nam(j) == ps(nres)%p_at_nam(i)) then
             natms=natms-1
           end if
         end do
       end do
!      
       if (wat_flag) then   ! also for water molecules, remove any atoms in common, this should however never be the case 
!       
            do i=1, ps(nwat)%p_at_ncnt
              do j=1, ps(n_xaa_nuc)%p_at_ncnt
                if (ps(n_xaa_nuc)%p_at_nam(j) == ps(nwat)%p_at_nam(i)) then
                  natms=natms-1
                end if
              end do
            end do
!       
       end if 
!
     end if
!
!  ALLOCATE NUMBER OF ATOMS IN EACH AMINOACID AND IN EACH ROTAMER
!  (NUMBER OF ROTAMERS ALREADY ALLOCATED BEFORE)
!
     do i=1, m4
       allocate(mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(natms))
       mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_nat=natms
       mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_flag=.true.
       mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_flag2=.false.
     end do
     
!   NOW COPY ATOM NAMES 
!   
     do i=1,m4
        n1=0
        do j=1, ps(nres)%p_at_ncnt
           atnm= ps(nres)%p_at_nam(j) 
           if (m1==1) then
             n1 = n1 + 1
             mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(n1)%at_nam=atnm
             mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(n1)%at_flag=.false.
           else
             flag=.true.
             do k=1,ps(n_xaa_nuc)%p_at_ncnt
                 if (atnm==ps(n_xaa_nuc)%p_at_nam(k)) then
                   flag=.false.
                   exit
                 end if
             end do
             if (flag) then
                n1=n1+1
                mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(n1)%at_nam=atnm
                mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(n1)%at_flag=.false.
             end if
           end if
        end do
     end do
!   
     nprev = n1
!   
     if (wat_flag) then        ! adding the missing water atoms in case of solvated residues  
!    
        do i=1,m4
           n1=0
           do j=1, ps(nwat)%p_at_ncnt
              atnm= ps(nwat)%p_at_nam(j) 
              if (m1==1) then
                n1 = n1 + 1
                mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(nprev+n1)%at_nam=atnm
                mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(nprev+n1)%at_flag=.false.
              else
                flag=.true.
                do k=1,ps(n_xaa_nuc)%p_at_ncnt
                    if (atnm==ps(n_xaa_nuc)%p_at_nam(k)) then
                      flag=.false.
                      exit
                    end if
                end do
                if (flag) then
                   n1=n1+1
                   mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(nprev+n1)%at_nam=atnm
                   mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(nprev+n1)%at_flag=.false.
                end if
              end if
           end do
        end do
!        
     end if
!
!    now checking whether atom names are unique
!
     do i=1, m4
       natms = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_nat
       do j=1,natms
         atnm1 = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_nam
         do k= j+1, natms
           atnm2 = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(k)%at_nam
           if (atnm1==atnm2) then 
    PRINT*, '          '
    PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>      '
    PRINT*, ' >>>>                                                 '
    PRINT*, ' >>>>  ATOM NAMES ARE NOT UNIQUE IN RESIDUE: ', aanm           
    PRINT*, ' >>>>  PROBLEMATIC ATOM: ', atnm1 
    PRINT*, ' >>>>  THIS WILL CAUSE SERIOUS PROBLEMS LATER         '
    PRINT*, ' >>>>         '
    PRINT*, ' >>>>  A POSSIBLE REASON IS THAT WATER ATOMS WERE     '
    PRINT*, ' >>>>  AUTOMATICALLY ADDED TO RESIDUE                 '
    PRINT*, ' >>>>         '
    PRINT*, ' >>>>  MUST STOP!                                     '
    PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>     ' 
    PRINT*, '          '
        stop
           end if
         end do
       end do
     end do   
!
!
!!    DEBUG     
!     do i=1,1 
!         print*, mol(m1)%res(m2)%res_aas(m3)%aa_rots(1)%rot_nat
!         print*, wat_flag, nres, nwat, aanm, aanm_alt
!         call dump_pdb(m1,m2,m3,1)
!     end do
!!    END DEBUG     
!
!   if (aanm.eq.'GLY ') then
!     do i=1,m4
!     print*,m1,m2,m3,i
!     call dump_pdb(m1,m2,m3,i)
!     print*, 'done in INIT_AMINOACID'
!     end do
!   end if
!!
	END SUBROUTINE INIT_AMINOACID
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE BUILD_ROTAMER(aanm,m1,m2,m3,m4,num_rot)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE AA_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: m1,m2,m3,m4
	INTEGER :: i, j, k, l
	INTEGER :: ng, na, nb, nr, num_rot
	LOGICAL :: flag
	LOGICAL, DIMENSION(4) :: bfg
	LOGICAL, DIMENSION(:), allocatable :: flag_d
	CHARACTER (LEN=4) :: aanm, atnm
	CHARACTER (LEN=4), DIMENSION(4) :: bnm
	REAL, DIMENSION(4) :: aco1, aco2, aco3
	REAL, DIMENSION(3) :: xat1, xat2, xat3, xat4 
	REAL :: rdih, rang, rbon, rprob
!
!	GFORTRAN
	ng=0
!	GFORTRAN
!
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_prob = 1.0
!
! FIND BUILDING INSTRUCTIONS FIRST IN GEOM_DATA
!
	 flag =.false.
	 do i=1,(size(geo))
	      if (geo(i)%g_aan == aanm) then
		ng = i
		flag  = .true.
	     end if
	 end do
!
	 if (.not.flag) then
	PRINT*, '          '
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>         '
	PRINT*, ' >>>>'
	PRINT*, ' >>>>  COULD NOT FIND BUILDING INSTRUCTIONS FOR RESIDUE  '
	PRINT*, ' >>>>', mol(m1)%res(m2)%res_aas(m3)%aa_nam   
	PRINT*, ' >>>>  IN FILE: ', mumbo_libpar(1:len_trim(mumbo_libpar)) 
	PRINT*, ' >>>>  MUST EXIT                                         '
	PRINT*, ' >>>>'
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        '
	PRINT*, '          '
	   stop
	 end if
!
	na=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	nb=size(mol(m1)%res(m2)%res_atom_mc)
!	nr=m4
	nr=num_rot
!
	if (allocated(flag_d)) deallocate(flag_d)
	allocate(flag_d(geo(ng)%g_ndh))
!
	do i=1, geo(ng)%g_ndh 
		flag_d(i)=.false.
	end do
!
! LOOP OVER ALL DIHEDRAL ATOMS TO BE ADDED
!
	do i=1,geo(ng)%g_ndh
	  do j=1,geo(ng)%g_ndh
!
	    if (flag_d(j)) then
	      cycle
	    end if
!
!
! FIND ATOM COORDINATES FIRST
!
	    do k=1,4
	       bfg(k)=.false.
	       bnm(k)= geo(ng)%dh(j)%at_dih(k)
	    end do
!
	    do k=1,3
	       do l=1,na
	  atnm= mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_nam
	  flag = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_flag
		   if (flag) then
		     if (bnm(k)==atnm) then
	  aco1(k)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_xyz(1)  
	  aco2(k)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_xyz(2)  
	  aco3(k)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_xyz(3)  
		        bfg(k)=.true.
		        exit
		     end if
		   end if
	       end do
!
	       if (bfg(k)) then 
		    cycle
	       else
		   do l=1,size(mol(m1)%res(m2)%res_atom_mc)
		      atnm= mol(m1)%res(m2)%res_atom_mc(l)%at_nam
		      flag = mol(m1)%res(m2)%res_atom_mc(l)%at_flag
		      if (flag) then
		         if (bnm(k)==atnm) then
		              aco1(k)=mol(m1)%res(m2)%res_atom_mc(l)%at_xyz(1)
		              aco2(k)=mol(m1)%res(m2)%res_atom_mc(l)%at_xyz(2)
		              aco3(k)=mol(m1)%res(m2)%res_atom_mc(l)%at_xyz(3)
		              bfg(k)=.true.	
		              exit
		         end if
		      end if
		   end do
	       end if
	    end do
!
! NOW CHECK bfg(k)
!
	    do k=1,3
		if (.not.bfg(k)) then
	PRINT*, '          '
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>         '
	PRINT*, ' >>>>'
	PRINT*, ' >>>>  COULD NOT FIND ', aanm, 'DIHEDRAL', j , 'bfg()', k            
	PRINT*, ' >>>>'
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        '
	PRINT*, '          '
	    STOP
		end if
	    end do
!
	    if(.not.bfg(1)) then
	       cycle
	    end if
	    if(.not.bfg(2)) then
	       cycle
	    end if
	    if(.not.bfg(3)) then
	       cycle
	    end if
!
! NOW FIND CORRESPONDING BOND LENGHTS
!
! DEBUG1
!	   PRINT*, 'DEBUG1'
!	   PRINT*, bnm(1),bnm(2), bnm(3), bnm(4)
!	   PRINT*, geo(ng)%dh(j)%at_dil(nr)
!
! DEBUG1
!
	    do k=1,geo(ng)%g_nbo 
	      if (bnm(3)== geo(ng)%bo(k)%at_bod(1)) then
		   if (bnm(4)== geo(ng)%bo(k)%at_bod(2)) then
!
		   rbon= geo(ng)%bo(k)%at_bol

! DEBUG2
!	   PRINT*, 'DEBUG2'
!	   PRINT*, geo(ng)%bo(k)%at_bod(1), geo(ng)%bo(k)%at_bod(2)
!	   PRINT*,  rbon
! DEBUG2
		   end if
	      end if
	    end do
!
! DEBUG2
!	   PRINT*, 'DEBUG2bis'
!	   PRINT*,  rbon
! DEBUG2
!
! NOW FIND CORRESPONDING BOND ANGLES
!
	    do k=1,geo(ng)%g_nan 
	      if (bnm(2)== geo(ng)%an(k)%at_ang(1)) then
		   if (bnm(3)== geo(ng)%an(k)%at_ang(2)) then
		     if (bnm(4)== geo(ng)%an(k)%at_ang(3)) then
!
		   rang= geo(ng)%an(k)%at_agl
! DEBUG3
!	   PRINT*, 'DEBUG3'
!	   PRINT*, geo(ng)%an(k)%at_ang(1),geo(ng)%an(k)%at_ang(2),geo(ng)%an(k)%at_ang(3)  
!	   PRINT*, rang
! DEBUG3
!
		     end if
		   end if
	      end if
	    end do
! DEBUG3
!	   PRINT*, 'DEBUG3bis'
!	   PRINT*, rang
! DEBUG3
!
! NOW FIND CORRSPONDING DIHEDRAL VALUE
!
	    rdih = geo(ng)%dh(j)%at_dil(nr)
	    rprob= geo(ng)%dh(j)%at_dpr(nr)
	    if (rprob < 0.01) then
		rprob= 0.01
	    end if
!
! DEBUG4
!	   PRINT*, 'DEBUG4'
!	   PRINT*, rdih, rprob
!	   PRINT*, '###########################'
! DEBUG4
!
! NOW CALL BUILD ATOM
!
	    xat1(1)=aco1(2)
	    xat1(2)=aco2(2)
	    xat1(3)=aco3(2)
!
	    xat2(1)=aco1(3)
	    xat2(2)=aco2(3)
	    xat2(3)=aco3(3)
!
	    xat3(1)=aco1(1)
	    xat3(2)=aco2(1)
	    xat3(3)=aco3(1)
!
	    call build_atom(xat1,xat2,xat3,xat4,rbon,rang,rdih)
!
	    aco1(4)= xat4(1)
	    aco2(4)= xat4(2)
	    aco3(4)= xat4(3)
!
! NOW MERGE ATOMS
!
	    do k=1, mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	        atnm = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_nam
	        flag  = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_flag
		if (.not.flag) then
		   if (atnm==bnm(4)) then
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(1)= aco1(4)
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(2)= aco2(4)
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(3)= aco3(4)
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_bfa= 20.00
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_occ= 1.00
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_flag=.true.
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_prob = rprob
		   end if
		end if
	     end do
!
! ALL DONE
	    flag_d(j)=.true.
!
! FINAL DOUBLE END DO
	  end do
	end do
!
	END SUBROUTINE BUILD_ROTAMER
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE ADD_GROUPS(m1,m2,m3,m4,aanm,checkflag)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE AA_DATA_M
	USE MAX_DATA_M
	USE MUMBO_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: m1,m2,m3,m4
	CHARACTER(LEN=4) :: aanm, atnm, boff
	INTEGER :: ng
	INTEGER :: i,j,k,l
	INTEGER :: nbuild, nms
	REAl, DIMENSION(3) :: xat1, xat2, xat3, xat4
	REAL, DIMENSION(3,3) :: rmat, umat
	REAL, DIMENSION(3) :: tra, notra
	LOGICAL, DIMENSION(3) :: present
	LOGICAL :: flag, checkflag
	LOGICAL, DIMENSION(:), ALLOCATABLE :: flag_g
	REAL, DIMENSION(:), ALLOCATABLE :: xyz1, xyz2, xyz3
!
!	GFORTRAN
	ng=-100
	nbuild=0
!
    flag = .false. 
	do i=1,size(geo)
	   if (geo(i)%g_aan==aanm) then
		ng=i
		flag = .true.
	   end if
	end do
!
    if (.not.flag) then 
      return
    end if
!
	if(allocated(flag_g)) deallocate(flag_g)
	allocate(flag_g(geo(ng)%g_ngr))
!
	do i=1, geo(ng)%g_ngr
	  flag_g(i)=.false.
	end do	
!
	do j= 1,geo(ng)%g_ngr 
	do i= 1,geo(ng)%g_ngr
!
	  if (flag_g(i)) then
		cycle
	  end if
!
!	  FIRST IDENTIFY COMMON ATOMS IN GROUP
!
	   present(1) =  .false.
	   present(2) =  .false.
	   present(3) =  .false.
	   do k=1, geo(ng)%gr(i)%at_num
	        if (geo(ng)%gr(i)%at_com(2)==geo(ng)%gr(i)%at_nam(k)) then
		      xat1(1)=geo(ng)%gr(i)%at_co1(k)  
		      xat1(2)=geo(ng)%gr(i)%at_co2(k)  
		      xat1(3)=geo(ng)%gr(i)%at_co3(k)
	              present(1) = .true.
	        else if (geo(ng)%gr(i)%at_com(1)==geo(ng)%gr(i)%at_nam(k)) then
		      xat2(1)=geo(ng)%gr(i)%at_co1(k)  
		      xat2(2)=geo(ng)%gr(i)%at_co2(k)  
		      xat2(3)=geo(ng)%gr(i)%at_co3(k)
	              present(2) = .true.
	        else if (geo(ng)%gr(i)%at_com(3)==geo(ng)%gr(i)%at_nam(k)) then
		      xat3(1)=geo(ng)%gr(i)%at_co1(k)  
		      xat3(2)=geo(ng)%gr(i)%at_co2(k)  
		      xat3(3)=geo(ng)%gr(i)%at_co3(k)
	              present(3) = .true.
	        end if
	   end do
!
	   do k=1,3
		if(.not.present(k)) then
	PRINT*, '          '
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   '
	PRINT*, ' >>>>                                              '    
	PRINT*, ' >>>>  INCONSISTENCY IN GROUP DEFINITIONS          '
	PRINT*, ' >>>>  ', mol(m1)%res(m2)%res_chid, mol(m1)%res(m2)%res_nold
	PRINT*, ' >>>>  COULD NOT ADD GROUP                         '
	PRINT*, ' >>>>  ', geo(ng)%g_aan    
	PRINT*, ' >>>>                                              '
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  '
	PRINT*, '          '
		end if
	   end do
!
	   call align_plane(xat1,xat2,xat3,rmat,tra)
	   call inimat(umat)
!
	
	   notra(1)=0.0
	   notra(2)=0.0
	   notra(3)=0.0
!
	   if (allocated(xyz1)) deallocate(xyz1)
	   if (allocated(xyz2)) deallocate(xyz2)
	   if (allocated(xyz3)) deallocate(xyz3)
!
	   allocate(xyz1(geo(ng)%gr(i)%at_num))
	   allocate(xyz2(geo(ng)%gr(i)%at_num))
	   allocate(xyz3(geo(ng)%gr(i)%at_num))
!
!      now transform all atoms in the group to the special origine/reference frame..
!
	   do k = 1, geo(ng)%gr(i)%at_num
!
		xat4(1)=geo(ng)%gr(i)%at_co1(k)
		xat4(2)=geo(ng)%gr(i)%at_co2(k)
		xat4(3)=geo(ng)%gr(i)%at_co3(k)
! 
		call applytransform(xat4,umat,tra)
		call applytransform(xat4,rmat,notra)
!
		xyz1(k)=xat4(1)
		xyz2(k)=xat4(2)
		xyz3(k)=xat4(3)
!
	   end do
!
!	   NOW IDENTIFY COMMON ATOMS IN RESIDUE/MOLECULE
!
	   present(1) =  .false.
	   present(2) =  .false.
	   present(3) =  .false.
!
	   do k =1, mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	      atnm = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_nam
	      flag = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_flag
	      if (flag) then
	        if (geo(ng)%gr(i)%at_com(2)== atnm) then
	  xat1(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(1)  
	  xat1(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(2)  
	  xat1(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(3)  
	  present(1) = .true.
	        end if
	        if (geo(ng)%gr(i)%at_com(1)== atnm) then
	  xat2(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(1)  
	  xat2(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(2)  
	  xat2(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(3)  
	  present(2) = .true.
	        end if
	        if (geo(ng)%gr(i)%at_com(3)== atnm) then
	  xat3(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(1)  
	  xat3(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(2)  
	  xat3(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(3)  
	  present(3) = .true.
	        end if
	      end if
	   end do
!
	   if (aanm==pep_name) then
!
!      in PEP_GROUPS, atom C has to be retrieved from the previous residue instead
!      present(3), xat3  relates to the third atom in common atoms = C
!
!      PEP_GROUPS
!      CA  N   C
!      ATOM      1  CA  PEP     1       1.728  -1.884  -2.807  1.00 20.00
!      ....
!
!      WS2122
!
		present(3)=.false.
!
		   if (m1==1.and.m2==1) then 
		       xat3(1)=999.9  
		       xat3(2)=999.9
		       xat3(3)=999.9
		       present(3) = .true.
!		       print*, 'M1M2', m1,m2, mol(m1)%res(m2)%res_chid, mol(m1)%res(m2)%res_nold
		   else if ((m1==1).and.(m2.ne.1).and.(mol(m1)%res(m2-1)%res_ntype.ne.2)) then 
		       xat3(1)=999.9
		       xat3(2)=999.9
		       xat3(3)=999.9
		       present(3) = .true.
!		       print*, 'M1NTYPE', m1,m2, mol(m1)%res(m2)%res_chid, mol(m1)%res(m2)%res_nold
		   else
!		   
		     do k =1, mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_nat
	atnm = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_nam
	flag = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_flag
		       if (flag) then
		         if (geo(ng)%gr(i)%at_com(3)== atnm) then
	xat3(1)=mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(1)  
	xat3(2)=mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(2)  
	xat3(3)=mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(k)%at_xyz(3)  
		            present(3) = .true.
!		            print*, 'ELSE', m1,m2, mol(m1)%res(m2)%res_chid, mol(m1)%res(m2)%res_nold
!
		         end if
		       end if
		     end do
		   end if
	   end if
!  
!     end of (if (aanm==pep_name) then)
!
! IF NEEDED LOOK UP IN ADDITION MAIN CHAIN ATOMS
!
	   if (m1/=1)then
	    if (.not.mol(m1)%res(m2)%res_lig) then 
	    do k =1, size(mol(m1)%res(m2)%res_atom_mc)
	    
	      atnm = mol(m1)%res(m2)%res_atom_mc(k)%at_nam
	      flag = mol(m1)%res(m2)%res_atom_mc(k)%at_flag
	     
	      if (flag) then

		    if(.not.present(1)) then
		      if (geo(ng)%gr(i)%at_com(2)== atnm) then
		          xat1(1)=mol(m1)%res(m2)%res_atom_mc(k)%at_xyz(1)
		          xat1(2)=mol(m1)%res(m2)%res_atom_mc(k)%at_xyz(2)
		          xat1(3)=mol(m1)%res(m2)%res_atom_mc(k)%at_xyz(3)
		          present(1) = .true.
		      end if
		    end if
		    if(.not.present(2)) then
		      if (geo(ng)%gr(i)%at_com(1)== atnm) then
		          xat2(1)=mol(m1)%res(m2)%res_atom_mc(k)%at_xyz(1)
		          xat2(2)=mol(m1)%res(m2)%res_atom_mc(k)%at_xyz(2)
		          xat2(3)=mol(m1)%res(m2)%res_atom_mc(k)%at_xyz(3)
		          present(2) = .true.
		      end if
		    end if
		    if(.not.present(3)) then
		      if (geo(ng)%gr(i)%at_com(3)== atnm) then
		          xat3(1)=mol(m1)%res(m2)%res_atom_mc(k)%at_xyz(1)
		          xat3(2)=mol(m1)%res(m2)%res_atom_mc(k)%at_xyz(2)
		          xat3(3)=mol(m1)%res(m2)%res_atom_mc(k)%at_xyz(3)
		          present(3) = .true.
		      end if
		    end if
	      end if
	    end do
	    end if
	   end if
!
	   do k=1,3
		if(.not.present(k)) then
		 if(checkflag) then 
		  if (mol(m1)%res(m2)%res_aas(m3)%aa_nam /= pro_name) then
	PRINT*, '          '
	PRINT*, ' >>>>>>> WARNING >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  '
	PRINT*, ' >>>>                                              '    
	PRINT*, ' >>>>  INCONSISTENCY IN GROUP DEFINITIONS          '
	PRINT*, ' >>>>  ', geo(ng)%g_aan
	PRINT*, ' >>>>  COULD NOT FIND COMMON ATOMS IN RESIDUE      '
	PRINT*, ' >>>>  ', mol(m1)%res(m2)%res_chid, mol(m1)%res(m2)%res_nold
	PRINT*, ' >>>>                                              '    
	PRINT*, ' >>>>>>> WARNING >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> '
	PRINT*, '          '
		  end if
		 end if 
		end if
	   end do
!
	    if(.not.present(1)) then
	       cycle
	    end if
	    if(.not.present(2)) then
	       cycle
	    end if
	    if(.not.present(3)) then
	       cycle
	    end if
!
	   call inimat(rmat)
	   call align_plane(xat1,xat2,xat3,rmat,tra)
	   call invomat(rmat)             
!
	   do k=1,3
		tra(k)= -tra(k)
	   end do	
!
	   do k = 1, geo(ng)%gr(i)%at_num
!
		xat4(1)=xyz1(k)
		xat4(2)=xyz2(k)
		xat4(3)=xyz3(k)
!
		call applytransform(xat4,rmat,tra)
!
		xyz1(k)=xat4(1)
		xyz2(k)=xat4(2)
		xyz3(k)=xat4(3)
!
	   end do
!
!	   NOW MERGE ATOMS
!
	  do k=1, geo(ng)%gr(i)%at_num
	     do l=1, mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	        boff = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_nam
	        flag = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_flag
		if (.not.flag) then
		   if (boff==geo(ng)%gr(i)%at_nam(k)) then
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_xyz(1)=     &
     &     xyz1(k)  
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_xyz(2)=     &
     &     xyz2(k) 
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_xyz(3)=     &
     &     xyz3(k)
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_bfa= 20.00
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_occ= 1.00
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(l)%at_flag=.true.
	nbuild=nbuild+1
		   end if
		end if
	     end do
	  end do
!
	flag_g(i)=.true.
!
	end do
	end do
!
!   here checking whether all missing atoms have been added
!
    if (checkflag) then
!
	  nms=0
	  if (m1/=1) then
	     do i=1, mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	       flag = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_flag
	       if (.not.flag) then
	          nms=nms+1
	       end if
	     end do
	  end if
!     
	  if (nms > 0) then
	    PRINT*, '          '
	    PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    '
	    PRINT*, ' >>>>                                               '
	    PRINT*, ' >>>>  STILL MISSING COORDINATES FOR: ',nms,' ATOMS '
	    PRINT*, ' >>>>  IN: ', aanm, m1, m2, m3, m4,'                '
	    PRINT*, ' >>>>  IN ADD GROUPS                                '
!	    PRINT*, ' >>>>  NOT WORTH CONTINUING, MUST EXIT  !!!!!!!     '
	    PRINT*, ' >>>>                                               '
	    PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   '
	    PRINT*, '          '
	    stop
	  end if
!	
	end if
!
	END SUBROUTINE ADD_GROUPS
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE PHI_PSI_DEP_GEOM(AANM,MX)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: mx, i, j, k, ios, nphi, npsi
	CHARACTER (LEN=4) :: aanm
	REAL :: rphi, rpsi, fphi, fpsi
	INTEGER :: ndh, ng, nrt, nw, nres, nnpt, npp
	LOGICAL flag
!
	CHARACTER (LEN=132) :: string
	CHARACTER (LEN=80)  :: word(40) 
	CHARACTER (LEN=3)   :: keyword
!
	INTEGER :: noff, nnp, nfine, nstp, nmi, nmo, nrot, ntot, nt
	REAL, DIMENSION(:), ALLOCATABLE :: xdil, xdpr
	INTEGER :: m,n,o,p
!	
        REAL, PARAMETER :: err2 = 0.01
!
!
!	GFORTRAN
	ng=0
	noff=0
!	GFORTRAN
!
	nres= mol(mx)%res(1)%res_num
	rphi= mol(mx)%res(1)%res_phi_psi(1)
	rpsi= mol(mx)%res(1)%res_phi_psi(2)
!
	nphi=nint(rphi/10)
	rphi=nphi*10
!
	npsi=nint(rpsi/10)
	rpsi=npsi*10
!
	do i=1,size(geo)
	   if (geo(i)%g_aan==aanm) then
		ng=i
	   end if
	end do
!
	ndh=geo(ng)%g_ndh
!
	PRINT*,'     '
	PRINT*, '  ',aanm, rphi, rpsi,  & 
     &           mol(mx)%res(1)%res_nold, mol(mx)%res(1)%res_chid
!	PRINT*,'     '
!
	keyword=aanm(1:3)
!
	open(11,file=mumbo_libpps,form='formatted',status='old')
	flag = .false.
	nrt = 0
	do j=1, max_lines_input
		read(11,fmt='(a)',iostat=ios) string
!
		if (ios==eo_file) then
!		  print*,'END-OF-FILE REACHED IN FILE: '
!		  print*, mumbo_libpps
!		  print*,'NO HARM DONE                 '
		  exit
		end if
!
		if (j==max_lines_input) then
		  PRINT*, '    '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED IN:                 '
		  PRINT*, '>>>>>>> ', mumbo_libpps(1:len_trim(mumbo_libpps))
		  PRINT*, '>>>>>>>  INCREASE MAX_LINE_INPUT                     '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '    '
		  STOP
		end if
!
		if (string(1:3)/=keyword) then
		   cycle
		else 
		   read(string,'(4x,f5.0)') fphi
!		   if (fphi/=rphi) then
		   if (abs(fphi-rphi).gt.err2) then
		     cycle
		   else
		     read(string,'(9x,f5.0)') fpsi
!		     if (fpsi/=rpsi) then
		     if (abs(fpsi-rpsi).gt.err2) then
		         cycle
		     else
!		        print*,string(1:len_trim(string))
			nrt=nrt+1
		     end if
		   end if
		end if	
!
	end do
	close(11)
!
	if (nrt == 0) then
		do j=1, ndh 
	           geo(ng)%dh(j)%at_nrt = nrt
		end do
		  PRINT*, '    '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '>>>>>>>  NO PHI PSI DEPENDENT ROTAMERS FOUND IN:     '
		  PRINT*, '>>>>>>> ', mumbo_libpps(1:len_trim(mumbo_libpps))
		  PRINT*, '>>>>>>>  FOR RESIDUE: ', aanm                         
		  PRINT*, '>>>>>>>  PHI= ', rphi                                 
		  PRINT*, '>>>>>>>  PSI= ', rpsi                                 
		  PRINT*, '>>>>>>>  AS REQUESTED FOR:                           '
		  PRINT*, '>>>>>>>  ',mol(1)%res(nres)%res_chid,            &
     &                           mol(1)%res(nres)%res_nold                           
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '    '
		stop
	end if
!
	if (fine_flag) then
	
		do j= (mumbo_fine_nstp + 1), 1, -1
		       nstp = j - 1
		       nfine = (2 * nstp ) + 1
		       
		       if (chi_all_flag.and.(.not.chi_one_flag))      then
		          noff = nfine**(geo(ng)%g_ndh)
		          ntot = noff * nrt
		       else if (chi_one_flag.and.(.not.chi_all_flag)) then
		          noff = nfine
		          ntot = noff * nrt
		       else
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>'
	PRINT*, '>> SOMETHING MESSED UP IN PHI_PSI_DEP                       '  
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
		          stop
		       end if
!		       noff = nfine**(geo(ng)%g_ndh)
!		       ntot = noff * nrt
		       if (ntot <= mumbo_fine_limit) then
		         exit
		       end if
	PRINT*, '  FINE_NSTP AUTOMATICALLY REDUCED FOR AMINO_ACID  ',geo(ng)%g_aan  
		end do 	  
!
	else
		    noff = 1
		    nfine = 1
		    ntot = nrt
	end if
!
!
	nt = geo(ng)%g_ndh
	PRINT*, '  NUMBER OF ROTAMERS FOUND FOR: ', geo(ng)%g_aan, nrt 
		if (fine_flag) then
		    if (chi_all_flag.and.(.not.chi_one_flag))      then
	WRITE(*,'(A16,I2,A2,I2,A2,I3,A1,I5)')                          &
     &              '   EXPANDED TO:(',nfine,'**',nt,')*',nrt,'=',ntot
		    else if (chi_one_flag.and.(.not.chi_all_flag)) then
	WRITE(*,'(A16,I3,A3,I3,A3,I5)')                          &
     &              '   EXPANDED TO: ',nrt,' * ',nfine,' = ',ntot
		    end if
		end if
!
	do j=1, ndh 
	    geo(ng)%dh(j)%at_nrt= ntot
	    allocate(geo(ng)%dh(j)%at_dil(ntot))
	    allocate(geo(ng)%dh(j)%at_dpr(ntot))
	end do
!
	open(11,file=mumbo_libpps,form='formatted',status='old')
	flag = .false.
	nrt = 0
	do j=1, max_lines_input
		read(11,fmt='(a)',iostat=ios) string
		if (ios==eo_file) then
		  exit
		end if
		if (string(1:3)/=keyword) then
		   cycle
		else 
		   read(string,'(4x,f5.0)') fphi
!		   if (fphi/=rphi) then
		   if (abs(fphi-rphi).gt.err2) then
		     cycle
		   else
		     read(string,'(9x,f5.0)') fpsi
!		     if (fpsi/=rpsi) then
 		     if (abs(fpsi-rpsi).gt.err2) then
		         cycle
		     else
			nrt=nrt+1
		        call parser (string, word, nw)
		   	nnp = (noff*(nrt-1)) + 1
		        do k= 1, ndh
		           read(word(9),*)  geo(ng)%dh(k)%at_dpr(nnp)
			   read (word(k+9),*) geo(ng)%dh(k)%at_dil(nnp) 
		        end do
		     end if
		   end if
		end if	
!
	end do
	close(11)
!
!DEBUG
!	    if (geo(ng)%g_aan =='TYR ') then 
!	  	do k = 1, geo(ng)%g_ndh
!		   do m = 1, size(geo(ng)%dh(k)%at_dpr)
!		    print *, geo(ng)%dh(k)%at_dil(m), geo(ng)%dh(k)%at_dpr(m), k
!		   end do
!		end do
!	    end if
!DEBUG
!
	if (fine_flag) then
		nstp = (nfine +1)/2 
	        nrot = geo(ng)%dh(1)%at_nrt / noff 
		if (allocated(xdil)) deallocate(xdil)
		if (allocated(xdpr)) deallocate(xdpr)
		allocate(xdil(nfine))
		allocate(xdpr(nfine))
		
	     if (chi_all_flag.and.(.not.chi_one_flag))      then
!
	        nnp = 1
	        do p = 1, nrot 
	          nmo = noff / nfine
	          nmi = 1
	          do k = geo(ng)%g_ndh, 1, -1
                     nnp = (noff*(p-1)) + 1 
		     do m = 1, nfine
		 xdil(m)= geo(ng)%dh(k)%at_dil(nnp) + ((m-nstp)*  mumbo_fine_ndeg)
		 xdpr(m) = geo(ng)%dh(k)%at_dpr(nnp)
		        if (xdil(m) < -180) then
			  xdil(m) = xdil(m) + 360
			else if (xdil(m) > 180) then
			  xdil(m) = xdil(m) - 360
			end if
		     end do
		     do m = 1, nmo
			do n = 1, nfine
			   do o = 1, nmi
!
			    geo(ng)%dh(k)%at_dil(nnp) = xdil(n)
			    geo(ng)%dh(k)%at_dpr(nnp) = xdpr(n)
!
			    nnp = nnp +1
!
			   end do
			end do
		     end do
		     nmo = nmo / nfine
		     nmi = nmi *  nfine
		   end do
		end do
		
!
	      else if (chi_one_flag.and.(.not.chi_all_flag)) then
!
		 npp=1
	         do k = 1, geo(ng)%g_ndh
		   do p = 1, nrot
		     npp = (p-1) * nfine + 1
!
		     do m = 1, nfine
!
		       if (k==1) then
!		     
		 xdil(m) = geo(ng)%dh(k)%at_dil(npp) + ((m-nstp)*  mumbo_fine_ndeg)
		 xdpr(m) = geo(ng)%dh(k)%at_dpr(npp)
!		 
		          if (xdil(m) < -180) then
			    xdil(m) = xdil(m) + 360
			  else if (xdil(m) > 180) then
			    xdil(m) = xdil(m) - 360
			  end if
		       else
		 xdil(m) = geo(ng)%dh(k)%at_dil(npp)
		 xdpr(m) = geo(ng)%dh(k)%at_dpr(npp)
		       end if
		     end do
!
		     do m = 1, nfine
		 nnpt = npp + m - 1
		 geo(ng)%dh(k)%at_dil(nnpt) = xdil(m)
		 geo(ng)%dh(k)%at_dpr(nnpt) = xdpr(m)
		     end do
		   end do
		 end do
!
	    end if
		
!
	end if
!
!
!DEBUG
! 	    if (geo(ng)%g_aan =='TYR ') then 
!	  	do k = 1, geo(ng)%g_ndh
!		   do m = 1, size(geo(ng)%dh(k)%at_dpr)
!		    print *, geo(ng)%dh(k)%at_dil(m), geo(ng)%dh(k)%at_dpr(m), k
!		   end do
!		end do
!	    end if
!DEBUG

!	do i=1, ndh
!	   do j=1,geo(ng)%dh(i)%at_nrt
!	    print*, geo(ng)%dh(i)%at_dpr(j)
!	    print*, geo(ng)%dh(i)%at_dil(j)
!	   end do
!	end do
!
	END SUBROUTINE PHI_PSI_DEP_GEOM
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE CHECK_PHI_PSI_LIB(AANM,FLAG)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER             :: i,ios  
	CHARACTER (LEN=132) :: string
	CHARACTER (LEN=3)   :: keyword
	CHARACTER (LEN=4)   :: aanm
	LOGICAL             :: flag
!
        flag=.false.
!
	keyword=aanm(1:3)
!
	open(11,file=mumbo_libpps,form='formatted',status='old')
!
	do i=1, max_lines_input
		read(11,fmt='(a)',iostat=ios) string
!
		if (ios==eo_file) then
!		  print*,'END-OF-FILE REACHED IN FILE: '
!		  print*, mumbo_libpps
!		  print*,'NO HARM DONE                 '
		  exit
		end if
!
		if (i==max_lines_input) then
		  PRINT*, '    '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED IN:                 '
		  PRINT*, '>>>>>>> ', mumbo_libpps(1:len_trim(mumbo_libpps))
		  PRINT*, '>>>>>>>  INCREASE MAX_LINE_INPUT                     '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '    '
		  STOP
		end if
!
		if (string(1:3)/=keyword) then
		   cycle
		else 
		   flag=.true.
		   exit
		end if	
	end do
	close(11)
!
	END SUBROUTINE CHECK_PHI_PSI_LIB
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE CHECK_NUC_BREAK(n1,n2,break_flag)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!   THIS SUBROUTINE INVESTIGATES WHETHER IN mol(1)
!                                   ATOM O3' FROM RESIDUE n1 IS CONNECTED TO 
!                                   ATOM P   FROM RESIDUE n2 
!                                   (USUALLY n2 WOULD BE EQUAL TO n1+1)
!
!                                   THE DISTANCE CRITERIUM IS SPECIFIED IN ch_break_dis
!
	USE MOLEC_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: n1, n2, m1, m2, m3, m4
	INTEGER :: i 
	REAL, DIMENSION(3) :: xat1, xat2
	REAL :: dx , dy, dz, dr
	LOGICAL :: flag, break_flag
	LOGICAL, DIMENSION(2) :: present
	CHARACTER (LEN=4) :: atnm
!
    break_flag=.true.
    present(1)=.false.
    present(2)=.false.
!
!	Gfortran asks for an initialisation
	do i=1,3
	  xat1(i)=9999.99
	  xat2(i)=9999.99
	end do
!
    m1=1
    m2=n1
    m3=1
    m4=1
!
    do i=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	    atnm=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_nam
	    flag=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_flag
        if (flag) then
	    	     if (atnm==o_nuc_name) then
	    xat1(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	    xat1(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	    xat1(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		      present(1)=.true.
	    	     end if
	    end if
	end do
!
    m1=1
    m2=n2
    m3=1
    m4=1
!
    do i=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	    atnm=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_nam
	    flag=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_flag
        if (flag) then
	    	     if (atnm==p_nuc_name) then
	    xat2(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	    xat2(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	    xat2(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		      present(2)=.true.
	     	     end if
        end if
    end do
!
		flag=.true.
		do i=1,2
		  if (.not.present(i)) then
		     flag=.false.
		  end if
		end do
!
    if (.not.flag) then 
!
	PRINT*, '          '
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   '
	PRINT*, ' >>>>                                              '
	PRINT*, ' >>>>  COULD NOT CACULATE MATCHING ATOMS IN        '
	PRINT*, ' >>>>  ',mol(1)%res(n1)%res_chid, mol(1)%res(n1)%res_nold
	PRINT*, ' >>>>  ',mol(1)%res(n2)%res_chid, mol(1)%res(n2)%res_nold
	PRINT*, ' >>>>  IN SUBROUTINE CHECK_NUC_BREAK              '
	PRINT*, ' >>>>                                              '
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  '
	PRINT*, '          '
!
    else
!
        dx=xat1(1)-xat2(1)
        dy=xat1(2)-xat2(2)
        dz=xat1(3)-xat2(3)
!
        dr=(dx**2)+(dy**2)+(dz**2)
        dr=sqrt(dr)
!
        if(dr.le.(ch_break_dis)) then
    	    break_flag=.false.
        end if
!    
!        print*, 'DEBUG11'
!        call dump_pdb_atflag(1,n1,1,1)
!        print*, 'DEBUG12'
!        call dump_pdb_atflag(1,n2,1,1)
!        print*, 'DISTANCE:', o_nuc_name, p_nuc_name, ' =' , dr, ch_break_dis
!
    end if
!
	END SUBROUTINE CHECK_NUC_BREAK
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE CHECK_PROT_BREAK(n1,n2,break_flag)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!   THIS SUBROUTINE INVESTIGATES WHETHER IN mol(1)
!                                   ATOM C   FROM RESIDUE n1 IS CONNECTED TO 
!                                   ATOM N   FROM RESIDUE n2 
!                                   (USUALLY n2 WOULD BE EQUAL TO n1+1)
!
!                                   THE DISTANCE CRITERIUM IS SPECIFIED IN ch_break_dis
!
!
	USE MOLEC_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: n1, n2, m1, m2, m3, m4
	INTEGER :: i 
	REAL, DIMENSION(3) :: xat1, xat2
	REAL :: dx , dy, dz, dr
	LOGICAL :: flag, break_flag
	LOGICAL, DIMENSION(2) :: present
	CHARACTER (LEN=4) :: atnm
!
    break_flag=.true.
    present(1)=.false.
    present(2)=.false.
!
!	Gfortran asks for an initialisation
	do i=1,3
	  xat1(i)=9999.99
	  xat2(i)=9999.99
	end do
!
    m1=1
    m2=n1
    m3=1
    m4=1
!
    do i=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	    atnm=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_nam
	    flag=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_flag
        if (flag) then
	    	     if (atnm==c_name) then
	    xat1(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	    xat1(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	    xat1(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		      present(1)=.true.
	    	     end if
	    end if
	end do
!
    m1=1
    m2=n2
    m3=1
    m4=1
!
    do i=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	    atnm=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_nam
	    flag=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_flag
        if (flag) then
	    	     if (atnm==n_name) then
	    xat2(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	    xat2(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	    xat2(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		      present(2)=.true.
	     	     end if
        end if
    end do
!
		flag=.true.
		do i=1,2
		  if (.not.present(i)) then
		     flag=.false.
		  end if
		end do
!
    if (.not.flag) then 
!
	PRINT*, '          '
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   '
	PRINT*, ' >>>>                                              '
	PRINT*, ' >>>>  COULD NOT CACULATE MATCHING ATOMS IN        '
	PRINT*, ' >>>>  ',mol(1)%res(n1)%res_chid, mol(1)%res(n1)%res_nold
	PRINT*, ' >>>>  ',mol(1)%res(n2)%res_chid, mol(1)%res(n2)%res_nold
	PRINT*, ' >>>>  IN SUBROUTINE CHECK_PROT_BREAK              '
	PRINT*, ' >>>>                                              '
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  '
	PRINT*, '          '
!
    else
!
        dx=xat1(1)-xat2(1)
        dy=xat1(2)-xat2(2)
        dz=xat1(3)-xat2(3)
!
        dr=(dx**2)+(dy**2)+(dz**2)
        dr=sqrt(dr)
!
        if(dr.le.(ch_break_dis)) then
    	    break_flag=.false.
        end if
!    
!        print*, 'DEBUG11'
!        call dump_pdb_atflag(1,n1,1,1)
!        print*, 'DEBUG12'
!        call dump_pdb_atflag(1,n2,1,1)
!        print*, 'DISTANCE:', o_nuc_name, p_nuc_name, ' =' , dr, ch_break_dis
!
    end if
!
	END SUBROUTINE CHECK_PROT_BREAK
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE CALC_PHI_PSI(MX)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: mx, nr
	INTEGER :: m1,m2,m3,m4
	INTEGER :: i, j 
	REAL, DIMENSION(3) :: xat1, xat2, xat3, xat4, xat5
	REAL, DIMENSION(3) :: yat1, yat2, yat3, yat4      
	REAL :: dx , dy, dz, dr, da
	REAL, PARAMETER :: pi = 3.1415927
	LOGICAL, DIMENSION(5) :: present
	LOGICAL :: flag
	CHARACTER (LEN=4) :: atnm
!
	REAL, DIMENSION(3,3) :: rmat, umat
	REAL, DIMENSION(3) :: tra, notra
!	
	REAL, PARAMETER :: err3 = 0.001
!
!	Gfortran asks for an initialisation
	do i=1,3
	  xat1(i)=9999.99
	  xat2(i)=9999.99
	  xat3(i)=9999.99
	  xat4(i)=9999.99
	  xat5(i)=9999.99
	end do
!
	nr= mol(mx)%res(1)%res_num
!
	m1=1
	m2=nr
	m3=1
	m4=1
!
	mol(mx)%res(1)%res_pps=.false.
!
!    print*, 'mol= ', mx, 'residue number= ', nr
!    print*, 'm1,m2,m3,m4'
!    call dump_pdb(m1,m2,m3,m4)
!
!
	if (m2==1) then                                 ! first residue
		mol(mx)%res(1)%res_pps=.false.
	else if (m2==mol(m1)%mol_nrs) then              ! last residue 
		mol(mx)%res(1)%res_pps=.false.
!
!   to account for the possibility of the presence of nucleotides
!
	else if (mol(m1)%res(m2-1)%res_ntype.ne.2) then 	
        mol(mx)%res(1)%res_pps=.false.
	else if (mol(m1)%res(m2+1)%res_ntype.ne.2) then 	
        mol(mx)%res(1)%res_pps=.false.
!
	else 
!	
		do i=1,5
		   present(i)=.false.
		end do
!
		do i=1,mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_nat
	atnm=   mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_nam
	flag=   mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_flag
		   if (flag) then
		     if (atnm==c_name) then
	xat1(1)=mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	xat1(2)=mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	xat1(3)=mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		      present(1)=.true.
		     end if
		   end if
		end do
!
		do i=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	atnm=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_nam
	flag=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_flag
		   if (flag) then
		     if (atnm==n_name) then
	xat2(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	xat2(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	xat2(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		      present(2)=.true.
		     end if
		     if (atnm==ca_name) then
	xat3(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	xat3(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	xat3(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		      present(3)=.true.
		     end if
		     if (atnm==c_name) then
	xat4(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	xat4(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	xat4(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		      present(4)=.true.
		     end if
		   end if
		end do
!
		do i=1,mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_nat
	atnm=   mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_nam
	flag=   mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_flag
		   if (flag) then
		     if (atnm==n_name) then
	xat5(1)=mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	xat5(2)=mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	xat5(3)=mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		      present(5)=.true.
		     end if
		   end if
		end do
!
		flag=.true.
		do i=1,5
		  if (.not.present(i)) then
		     flag=.false.
		  end if
		end do
!
		if (.not.flag) then
!
	PRINT*, '          '
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   '
	PRINT*, ' >>>>                                              '
	PRINT*, ' >>>>  COULD NOT CACULATE PHI PSI FOR RESIDUE      '
	PRINT*, ' >>>>  ',mol(m1)%res(m2)%res_chid, mol(m1)%res(m2)%res_nold
	PRINT*, ' >>>>  ATOMS NEEDED TO DO SO NOT FOND              '
	PRINT*, ' >>>>                                              '
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  '
	PRINT*, '          '
!	
		else
		  do i=1,2
		    if (i==1) then
			do j=1,3 
			  yat1(j)=xat1(j)
			  yat2(j)=xat2(j)
			end do
		    else 
			do j=1,3 
			  yat1(j)=xat4(j)
			  yat2(j)=xat5(j)
			end do
		    end if
!
		    dx=yat1(1)-yat2(1)
		    dy=yat1(2)-yat2(2)
		    dz=yat1(3)-yat2(3)
!
		    dr=(dx**2)+(dy**2)+(dz**2)
		    dr=sqrt(dr)
!
		    if(dr.gt.(ch_break_dis)) then
			mol(mx)%res(1)%res_pps=.false.
			flag=.false.
		    end if
		  end do
		end if
!
		if (flag) then
		  do i=1,2
		    if (i==1) then
			do j=1,3 
			  yat4(j)=xat1(j)
			  yat2(j)=xat2(j)
			  yat1(j)=xat3(j)
			  yat3(j)=xat4(j)
			end do
		    else
			do j=1,3 
			  yat3(j)=xat2(j)
			  yat1(j)=xat3(j)
			  yat2(j)=xat4(j)
			  yat4(j)=xat5(j)
			end do
		    end if
!
		    call align_plane(yat1,yat2,yat3,rmat,tra)
		    call inimat(umat)
!
		    notra(1)=0.0
		    notra(2)=0.0
		    notra(3)=0.0
!
		    call applytransform(yat1,umat,tra)
		    call applytransform(yat1,rmat,notra)
		    call applytransform(yat2,umat,tra)
		    call applytransform(yat2,rmat,notra)
		    call applytransform(yat3,umat,tra)
		    call applytransform(yat3,rmat,notra)
		    call applytransform(yat4,umat,tra)
		    call applytransform(yat4,rmat,notra)
!
		    dy= yat4(2)
		    dz= yat4(3)
!
!                   if (dz==0) then
		    if (abs(dz).lt.err3) then
		      	if (dy > 0) then
			 da= 90
			else
			 da= -90
			end if
		    else
		      da=atan(dy/dz)
		      da=(da*180)/pi
			if (dz > 0) then
			  da = da
			else if (dz < 0) then
			  da = 180 + da 
			end if
		    end if
!
		    if (da > 180) then
			da = da - 360 
		    end if
!
		    if (da < -180) then
			da = da + 360 
		    end if
!
		    mol(mx)%res(1)%res_phi_psi(i)=da
		    mol(mx)%res(1)%res_pps=.true.
!
		  end do
		end if
	end if
!        
!
	if (.not.mol(mx)%res(1)%res_pps) then
	  PRINT*, '          '
	  PRINT*, ' >>> PHI PSI FOR RESIDUE: ', mol(m1)%res(m2)%res_chid,  &
     &           mol(m1)%res(m2)%res_nold           
	  PRINT*, ' >>> COULD NOT BE CALCULATED                       '
	  PRINT*, ' >>> EITHER FIRST OR LAST RESIDUE OR CHAIN BREAK   '
	  PRINT*, '          '
	end if
!
	END SUBROUTINE CALC_PHI_PSI 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE CALC_PSEUDO_P(MX)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!   subroutine to calculate the pseudorotation angle W for the ribose ring
!   pseudorotation angle characterizes puckering
!
!   subroutine returns the  pseudorotation angle  ..%res_ribang
!   its acronyme                                  ..%res_ribconf
!   and all five individual torsion angles        ..%res_ribtor(i)
!   subroutine also returns   ..%res_ribflag = .true. if successful
!
!   Torsion angles to investigate: 
!
!   t1 = C1'-C2'-C3'-C4'
!   t2 = C2'-C3'-C4'-O4'
!   t3 = C3'-C4'-O4'-C1'
!   t4 = C4'-O4'-C1'-C2'
!   t5 = O4'-C1'-C2'-C3'
!
!   P=atan[((t3+t5)-(t2+t4))/(2*t1*(sin(Pi/5) + sin(2Pi/5)))]
!
!   Following the formalism and conventions of 
!   C. Altona and M. Sundralingam, J. Am. Chem. Soc. 1972, 94:8205
!   PMID: 5079964  DOI: 10.1021/ja00778a043 
!
!   and as illustrated in 
!   Liljas, Liljas, Piskur et al., Textbook of Structural Biology, World Scientific
!   (however the latter does contain some errors in the angle definition)
!
	USE MOLEC_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
     CHARACTER(LEN=20), PARAMETER    :: rib_name_string=  "C1' C2' C3' C4' O4' "   
!                                                          12345678901234567890
     CHARACTER(LEN=70), PARAMETER ::                       &
     &     rib_conf_string= "3T2 3E 3T4 E4 OT4 OE OT1 E1 2T1 2E 2T3 E3 4T3 4E 4TO OE 1TO 1E 1T2 E2 "   
!                            1234567890123456789012345678901234567890123456789012345678901234567890
    LOGICAL, SAVE                   :: first = .true.    
    CHARACTER(LEN=4), ALLOCATABLE, DIMENSION(:), SAVE   :: fatnm     ! furanose atom names
    CHARACTER(LEN=4), ALLOCATABLE, DIMENSION(:), SAVE   :: fconf     ! furanose atom names
    CHARACTER(LEN=4) :: at1nm, at2nm, at3nm, at4nm     
    CHARACTER(LEN=132) :: string
    CHARACTER(LEN=80), DIMENSION(40)  :: word
    CHARACTER(LEN=80)                 :: boff
    INTEGER                           :: nn, nm
    REAL, ALLOCATABLE, DIMENSION(:)   :: tord, torr
    REAL                              :: rr, pseuP
!
	INTEGER :: mx, nr, nw
	INTEGER :: m1,m2,m3,m4
	INTEGER :: i, j 
	REAL, DIMENSION(3) :: xat1, xat2, xat3, xat4
	REAL, DIMENSION(3) :: yat1, yat2, yat3, yat4      
	REAL :: dx , dy, dz, dr, da
	REAL, PARAMETER :: pi = 3.1415927
	LOGICAL, DIMENSION(4) :: present
	LOGICAL :: flag, overall_flag
	CHARACTER (LEN=4) :: atnm
	REAL, DIMENSION(3,3) :: rmat, umat
	REAL, DIMENSION(3) :: tra, notra
	REAL, PARAMETER :: err3 = 0.001
!
    if (first) then 
!    
       do i=1,132
            string(i:i)= ' '
       end do 
       write(string(1:20),'(a)') rib_name_string
       call parser(string,word,nw)
       if (allocated(fatnm)) deallocate(fatnm)
       allocate(fatnm(nw))
!           
       do i=1,nw
            boff=word(i)
            fatnm(i)=boff(1:4)
       end do
!       
       do i=1,132
            string(i:i)= ' '
       end do 
       write(string(1:70),'(a)') rib_conf_string
       call parser(string,word,nw)
       if (allocated(fconf)) deallocate(fconf)
       allocate(fconf(nw))
!           
       do i=1,nw
            boff=word(i)
            fconf(i)=boff(1:4)
       end do
!
       first= .false.
    end if
!
!    print*, fatnm, 'FIRST= ', first
!
!	Gfortran asks for an initialisation
	do i=1,3
	  xat1(i)=9999.99
	  xat2(i)=9999.99
	  xat3(i)=9999.99
	  xat4(i)=9999.99
	end do
!
	m1=1
	m2=mx
	m3=1
	m4=1
!
    mol(m1)%res(m2)%res_ribconf='    '
    mol(m1)%res(m2)%res_ribflag=.false.
!
!
    if (allocated(tord)) deallocate(tord)
    allocate(tord(size(fatnm)))
!    
    if (allocated(torr)) deallocate(torr)
    allocate(torr(size(fatnm)))
!
    overall_flag=.true.
    do i= 1,size(fatnm)
!    
         nn = modulo(i,size(fatnm))
         if (nn.eq.0) then 
            nn = size(fatnm)
         end if
         at1nm = fatnm(nn)
!         
         nn = modulo(i+1,size(fatnm))
         if (nn.eq.0) then 
            nn = size(fatnm)
         end if
         at2nm = fatnm(nn)
!         
         nn = modulo(i+2,size(fatnm))
         if (nn.eq.0) then 
            nn = size(fatnm)
         end if
         at3nm = fatnm(nn)
!         
         nn = modulo(i+3,size(fatnm))
         if (nn.eq.0) then 
            nn = size(fatnm)
         end if
         at4nm = fatnm(nn)
!         
!         print*, 'HERE= ', i, at1nm, at2nm, at3nm, at4nm
!         
		 do j=1,4
		   present(j)=.false.
		 end do
!
		 do j=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	atnm=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_nam
	flag=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_flag
!	
		   if (flag) then
!		   
		     if (atnm==at1nm) then
	xat1(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(1)
	xat1(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(2)
	xat1(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(3)
		       present(1)=.true.
		      end if
		     if (atnm==at2nm) then
	xat2(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(1)
	xat2(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(2)
	xat2(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(3)
		      present(2)=.true.
		     end if
		     if (atnm==at3nm) then
	xat3(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(1)
	xat3(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(2)
	xat3(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(3)
		      present(3)=.true.
		     end if
		     if (atnm==at4nm) then
	xat4(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(1)
	xat4(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(2)
	xat4(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(3)
		      present(4)=.true.
		     end if
		   end if
		end do
!
		flag=.true.
		do j=1,4
		  if (.not.present(j)) then
		     flag=.false.
		     overall_flag= .false.
		  end if
		end do
!
		if (flag) then
			do j=1,3 
			  yat4(j)=xat1(j)
			  yat2(j)=xat2(j)
			  yat1(j)=xat3(j)
			  yat3(j)=xat4(j)
			end do
!
		    call align_plane(yat1,yat2,yat3,rmat,tra)
		    call inimat(umat)
!
		    notra(1)=0.0
		    notra(2)=0.0
		    notra(3)=0.0
!
		    call applytransform(yat1,umat,tra)
		    call applytransform(yat1,rmat,notra)
		    call applytransform(yat2,umat,tra)
		    call applytransform(yat2,rmat,notra)
		    call applytransform(yat3,umat,tra)
		    call applytransform(yat3,rmat,notra)
		    call applytransform(yat4,umat,tra)
		    call applytransform(yat4,rmat,notra)
!
		    dy= yat4(2)
		    dz= yat4(3)
!
		    if (abs(dz).lt.err3) then
		      	if (dy > 0) then
			        da= 90
                else
			        da= -90
			    end if
		    else
		        da=atan(dy/dz)
		        da=(da*180)/pi
			    if (dz > 0) then
			      da = da
			    else if (dz < 0) then
			      da = 180 + da 
			    end if
		    end if
!
		    if (da > 180) then
                da = da - 360 
		    end if
!
		    if (da < -180) then
			    da = da + 360 
		    end if
!
            tord(i) = da
!
!            print*, 'HERE2= ', tord(i), at1nm, at2nm, at3nm, at4nm
!
	       end if
	end do        
!!
   if (.not.overall_flag) then
!
	PRINT*, '          '
    PRINT*, ' >>> RIBOSE PSEUDOROTATION ANGLE W FOR NUCLEOTIDE: ', &
    &         mol(m1)%res(m2)%res_chid, mol(m1)%res(m2)%res_nold
    PRINT*, ' >>> COULD NOT BE CALCULATED BECAUSE OF MISSING ATOMS  '
	PRINT*, '          '
!	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   '
!	PRINT*, ' >>>>                                              '
!	PRINT*, ' >>>>  COULD NOT CACULATE PSEUDOROTATION ANGLE W FOR NUCLEOTIDE  '
!	PRINT*, ' >>>>  ',mol(m1)%res(m2)%res_chid, mol(m1)%res(m2)%res_nold
!	PRINT*, ' >>>>  ATOMS NEEDED TO DO SO NOT FOND              '
!	PRINT*, ' >>>>                                              '
!	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  '
!	PRINT*, '          '
	mol(m1)%res(m2)%res_ribflag=.false.
!   
   else
!
     do i=1,5 
!       if (tord(i).lt.0) then
!           tord(i) = tord(i)+360
!       end if
       torr(i)= (tord(i)/180)*pi
     end do
!
     rr = sin(pi/5)+sin(2*pi/5)
     rr = rr * 2 * torr(1)
     rr =  (torr(3)+torr(5)-torr(2)-torr(4)) / rr
     pseuP =  atan(rr)          ! returns a value between -180° and 180°
!
     pseuP = (pseuP *180)/pi    ! convert into degrees
!
     if (tord(1).lt.0) then 
         pseuP = pseuP + 180    ! see J. Am. Chem. Soc. 1972, 94:8205
     end if
!
     if (pseuP.lt.0) then
         pseuP = pseuP + 360
     end if
!
     mol(m1)%res(m2)%res_ribflag=.true.
     mol(m1)%res(m2)%res_ribang= pseuP
     do i=1,5
       mol(m1)%res(m2)%res_ribtor(i)=tord(i) 
     end do
!     
     if ((pseuP+9).gt.360) then
        pseuP = pseuP-360
     end if
!     
     nm = int(pseuP+9)/18
     mol(m1)%res(m2)%res_ribconf=fconf(nm+1)
!
!    write(*,'(A,I4,A,F7.2,3A,5F7.2)') 'residue= ', m2, ' PseuP= ', pseuP, ' ', fconf(nm+1), & 
!    &       ' Tor_ang= ', tord(1), tord(2), tord(3), tord(4), tord(5)     
!    
!!	
    end if
!
!!  for testing purposes... 
!    tord(1) =  -32.6
!    tord(2) =   18.0
!    tord(3) =   5.0
!    tord(4) =  -26.7
!    tord(5) =   36.9
!!
!     do i=1,5 
!!       if (tord(i).lt.0) then
!!           tord(i) = tord(i)+360
!!       end if
!       torr(i)= (tord(i)/180)*pi
!     end do
!!
!     rr = sin(pi/5)+sin(2*pi/5)
!     rr = rr * 2 * torr(1)
!     rr =  (torr(3)+torr(5)-torr(2)-torr(4)) / rr
!     pseuP =  atan(rr)
!!
!     pseuP = (pseuP *180)/pi
!!
!     if (pseuP.lt.0) then
!         pseuP = pseuP + 360
!     end if
!!
!     if (pseuP.gt.180) then
!         pseuP = pseuP - 180
!     end if
!!
!    write(*,'(A,F7.2,A,5F7.2)') '***PseuP= ', pseuP, ' Tor_ang= ', tord(1), tord(2), tord(3), tord(4), tord(5)     
!!    
!!!	
!!
	END SUBROUTINE CALC_PSEUDO_P 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE CALC_RIBOSE_DELTA(MX)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
!  Subroutine to calculate the ribose delta torsion angle (C5'-C4'-C3'-O3')
!  returned as ....%res_rib_delta 
!
!  used to to infer 2'end and 3'endo conformation from this angle 
!  
!
!
	USE MOLEC_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER :: mx, nr
	INTEGER :: m1,m2,m3,m4
	INTEGER :: i, j 
	REAL, DIMENSION(3) :: xat1, xat2, xat3, xat4
	REAL, DIMENSION(3) :: yat1, yat2, yat3, yat4      
	REAL :: dx , dy, dz, dr, da
	REAL, PARAMETER :: pi = 3.1415927
	LOGICAL, DIMENSION(4) :: present
	LOGICAL :: flag
	CHARACTER (LEN=4) :: atnm
!
	CHARACTER (LEN=4) :: at1nm="O3' "
	CHARACTER (LEN=4) :: at2nm="C3' "
	CHARACTER (LEN=4) :: at3nm="C4' "
	CHARACTER (LEN=4) :: at4nm="C5' "
!
	REAL, DIMENSION(3,3) :: rmat, umat
	REAL, DIMENSION(3) :: tra, notra
!	
	REAL, PARAMETER :: err3 = 0.001
!
!	Gfortran asks for an initialisation
	do i=1,3
	  xat1(i)=9999.99
	  xat2(i)=9999.99
	  xat3(i)=9999.99
	  xat4(i)=9999.99
	end do
!
	nr= mol(mx)%res(1)%res_num
!
	m1=1
	m2=nr
	m3=1
	m4=1
!
	mol(mx)%res(1)%res_deltaflag=.false.
!
!   DEBUG WS2122
!
!    print*, 'mol= ', mx, 'residue number= ', nr
!    print*, 'm1,m2,m3,m4'
!    call dump_pdb(m1,m2,m3,m4)
!
!   DEBUG WS2122
!
		do i=1,4
		   present(i)=.false.
		end do
!
		do i=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	atnm=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_nam
	flag=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_flag
!	
		   if (flag) then
!		   
		     if (atnm==at1nm) then
	xat1(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	xat1(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	xat1(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		       present(1)=.true.
		      end if
		     if (atnm==at2nm) then
	xat2(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	xat2(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	xat2(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		      present(2)=.true.
		     end if
		     if (atnm==at3nm) then
	xat3(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	xat3(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	xat3(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		      present(3)=.true.
		     end if
		     if (atnm==at4nm) then
	xat4(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)
	xat4(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)
	xat4(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)
		      present(4)=.true.
		     end if
		   end if
		end do
!
		flag=.true.
		do i=1,4
		  if (.not.present(i)) then
!
	PRINT*, '          '
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   '
	PRINT*, ' >>>>                                              '
	PRINT*, ' >>>>  COULD NOT CACULATE ENDO/EXO DIHEDRAL ANGLE FOR NUCLEOTIDE  '
	PRINT*, ' >>>>  ',mol(m1)%res(m2)%res_chid, mol(m1)%res(m2)%res_nold
	PRINT*, ' >>>>  ATOMS NEEDED TO DO SO NOT FOND              '
	PRINT*, ' >>>>                                              '
	PRINT*, ' >>>>>>> BUMMER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  '
	PRINT*, '          '
		     flag=.false.
		  end if
		end do
!
		if (flag) then
			do j=1,3 
			  yat4(j)=xat1(j)
			  yat2(j)=xat2(j)
			  yat1(j)=xat3(j)
			  yat3(j)=xat4(j)
			end do
!
		    call align_plane(yat1,yat2,yat3,rmat,tra)
		    call inimat(umat)
!
		    notra(1)=0.0
		    notra(2)=0.0
		    notra(3)=0.0
!
		    call applytransform(yat1,umat,tra)
		    call applytransform(yat1,rmat,notra)
		    call applytransform(yat2,umat,tra)
		    call applytransform(yat2,rmat,notra)
		    call applytransform(yat3,umat,tra)
		    call applytransform(yat3,rmat,notra)
		    call applytransform(yat4,umat,tra)
		    call applytransform(yat4,rmat,notra)
!
		    dy= yat4(2)
		    dz= yat4(3)
!
		    if (abs(dz).lt.err3) then
		      	if (dy > 0) then
			        da= 90
                else
			        da= -90
			    end if
		    else
		        da=atan(dy/dz)
		        da=(da*180)/pi
			    if (dz > 0) then
			      da = da
			    else if (dz < 0) then
			      da = 180 + da 
			    end if
		    end if
!
		    if (da > 180) then
                da = da - 360 
		    end if
!
		    if (da < -180) then
			    da = da + 360 
		    end if
!
		    mol(mx)%res(1)%res_rib_delta=da
		    mol(mx)%res(1)%res_deltaflag=.true.
!
	end if
!
	if (.not.mol(mx)%res(1)%res_deltaflag) then
	  PRINT*, '          '
	  PRINT*, ' >>> RIBOSE DIHEDRAL ANGLE: ', mol(m1)%res(m2)%res_chid,  &
     &           mol(m1)%res(m2)%res_nold           
	  PRINT*, ' >>> COULD NOT BE CALCULATED                       '
	  PRINT*, '          '
	end if
!
	END SUBROUTINE CALC_RIBOSE_DELTA 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE EXPAND_HYDROROTAMS()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	REAL :: chi, cho
	REAL, dimension(3) :: yat1, yat2, yat3, xat3, tra, notra
	REAL, dimension(3,3) :: rmat, unit
!
	INTEGER, dimension(4) :: set
!
	INTEGER :: nh2, nh3, nht
	INTEGER :: nmult, nupper, nlower, n4
	INTEGER :: ncrt , ncnt
	INTEGER :: n11, n12, n13, n14, n15
	INTEGER :: n21, n22, n23, n24, n25
	INTEGER :: n31, n32, n33, n34, n35
	INTEGER :: n51, n52, n53, n54, n55, n56
	INTEGER :: n5
!
	INTEGER :: i, j, k, l, m, n, o, p
	CHARACTER (LEN=1) :: c_hb
!
	LOGICAL :: flag
!
	REAL, PARAMETER :: PI = 3.1415927
	DATA   notra/0.0,0.0,0.0/
!
	if (.not.hbond_flag) then
	  return
	end if
!
	n21=0
	n22=0
	n23=0
	n24=0
	n25=0
	n31=0
	n32=0
	n33=0
	n34=0
	n35=0
!
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY EXPAND_HYDROROTAMS   #'
	PRINT*, '################################'
	PRINT*, '        '
!
	do i=2, mol_nmls
!
!DEBBUG
!	if (i==mol_nmls) then
!	  do j=1,mol(i)%res(1)%res_aas(1)%aa_nrt
!	     call dump_pdb(i,1,1,j)
!	  end do 
!	end if
!
          do j=1,mol(i)%mol_nrs
            do k=1,mol(i)%res(j)%res_naa
!
!	Looking up hydrogens to be expanded
!
	     nh2 = 0
	     nh3 = 0
 	     mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_flag2=.false. 
             do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_nat
                c_hb= mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_hb
		if (c_hb == h2_sp_name) then 
		   mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_flag2=.true.
		   nh2 = nh2 +1
		else if (c_hb == h3_sp_name) then
		   mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_flag2=.true.
		   nh3 = nh3 +1
		end if
             end do
!
	     nht = (2**nh2) * (3**nh3) 
!
	     if (mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_flag2) then
! 
	     set(1)=i
	     set(2)=j
	     set(3)=k
	     set(4)=0
!
	     call expand_rotamers(set,nht)
!
!	PRINT*, '  HYDROGN ROTAMER TO BE EXPANDED IN: ', i , j, k  
	PRINT*, '  HYDROGN ROTAMER EXPANDED IN: ', mol(i)%res(1)%res_chid, &  
     &              mol(i)%res(1)%res_nold, mol(i)%res(1)%res_aas(k)%aa_nam
	PRINT*, '  EXPANDED:', nht
	PRINT*, '        '
!
!	BUILDING NEW HYDROGENS FROM HERE
!
	do l=1, mol(i)%res(j)%res_aas(k)%aa_nrt, nht
	    nupper = nht 
	    nlower = 1
          do m =1, mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_nat
            c_hb = mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_hb
		if (c_hb == h2_sp_name) then
	           nmult = 2
		else if (c_hb == h3_sp_name) then 
		   nmult = 3
		else
		   cycle
		end if 
!
!	TRYING TO FIGURE OUT ATOM CONNECTIONS, NAMELY H , D , D-1
!
		n11 = i
		n12 = j
		n13 = k
		n14 = l
		n15 = m
!
	      flag = .false.
	      n4 = mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_n14
	      do n = 1, n4 
!
		  if (mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(n,5)==2) then 
		    n21 = mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(n,1)
		    n22 = mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(n,2)
		    n23 = mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(n,3)
		    n25 = mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(n,4)
		    if (n21==n11) then
			n24 = l 
		    else if (n21==1) then 
                        n24 = 1
		    else 
			PRINT*, 'BUMMER: UNEXPECTED ERROR IN EXPAND_HYDROROTAMS MUST STOP'
			stop
		    end if 
!
	          do o = 1, n4 
		  if (mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(o,5)==3) then 
		        n31 = mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(o,1)
		        n32 = mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(o,2)
		        n33 = mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(o,3)
		        n35 = mol(i)%res(j)%res_aas(k)%aa_rots(1)%rot_ats(m)%at_c14(o,4)
		        if (n31==n11) then
			    n34 = l 
		        else if (n31==1) then 
                            n34 = 1
		        else 
			    PRINT*, 'BUMMER: UNEXPECTED ERROR IN EXPAND_HYDROROTAMS MUST STOP'
			    stop
		        end if
!
		      n5 = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n25)%at_n14
		      do p = 1, n5
!
		   n51 = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n25)%at_c14(p,1)
		   n52 = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n25)%at_c14(p,2)
		   n53 = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n25)%at_c14(p,3)
		   n55 = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n25)%at_c14(p,4)
		   n56 = mol(n21)%res(n22)%res_aas(n23)%aa_rots(1)%rot_ats(n25)%at_c14(p,5)
		         if (n51==n11) then
			     n54 = l 
		         else if (n51 == 1) then 
                             n54 = 1
		         else 
			     PRINT*, 'BUMMER: UNEXPECTED ERROR IN EXPAND_HYDROROTAMS MUST STOP'
			     stop
		         end if
		         if (n56 == 2) then
			       if (n51==n31 .and. n52==n32 .and. n53==n33 .and. n55==n35) then  
			          flag = .true. 
!	PRINT*,'***TRIPLET-FOUND***'
			          exit
			       end if
			 end if 
		      end do        ! loop p
		      end if
			if (flag) then
			  exit
		      end if
		   end do           ! loop o 
	      end if
		if (flag) then
		   exit
		end if
	     end do               ! loop n 
!
	  if (flag) then  
!
!	PRINT*,'DEBUG4a',n11,n12,n13,n14,n15
!
		yat3(1)= mol(n11)%res(n12)%res_aas(n13)%aa_rots(n14)%rot_ats(n15)%at_xyz(1)
		yat3(2)= mol(n11)%res(n12)%res_aas(n13)%aa_rots(n14)%rot_ats(n15)%at_xyz(2)
		yat3(3)= mol(n11)%res(n12)%res_aas(n13)%aa_rots(n14)%rot_ats(n15)%at_xyz(3)
!
!	PRINT*,'DEBUG4b',n21,n22,n23,n24,n25
!
		yat1(1)= mol(n21)%res(n22)%res_aas(n23)%aa_rots(n24)%rot_ats(n25)%at_xyz(1)
		yat1(2)= mol(n21)%res(n22)%res_aas(n23)%aa_rots(n24)%rot_ats(n25)%at_xyz(2)
		yat1(3)= mol(n21)%res(n22)%res_aas(n23)%aa_rots(n24)%rot_ats(n25)%at_xyz(3)
!
!	PRINT*,'DEBUG4c',n31,n32,n33,n34,n35
!
		yat2(1)= mol(n31)%res(n32)%res_aas(n33)%aa_rots(n34)%rot_ats(n35)%at_xyz(1)
		yat2(2)= mol(n31)%res(n32)%res_aas(n33)%aa_rots(n34)%rot_ats(n35)%at_xyz(2)
		yat2(3)= mol(n31)%res(n32)%res_aas(n33)%aa_rots(n34)%rot_ats(n35)%at_xyz(3)
!
	  else
		PRINT*, 'BUMMER: UNEXPECTED ERROR IN EXPAND_HYDROROTAMS MUST STOP'
		stop
	  end if
!
!
	  call align_plane(yat1,yat2,yat3,rmat,tra)
	  call inimat(unit)
!	 
	  call applytransform(yat3,unit,tra)
	  call applytransform(yat3,rmat,notra)
	  call invomat(rmat)             
	  do n=1,3
          tra(n)=-tra(n) 
	  end do 
!
	  chi = (2* pi / nmult)
!
	  nupper = nupper / nmult
	  ncnt = 0
!
	  do n = 1, nupper
	   do o = 1, nmult
	    cho = chi * o
	    xat3(1) = yat3(1)
	    xat3(2) = yat3(3) * sin(cho) 
	    xat3(3) = yat3(3) * cos(cho)	    
	    call applytransform(xat3,rmat,tra)
	    do p = 1, nlower
		ncnt= ncnt+1
		ncrt= (l - 1) + ncnt
!
!	PRINT*,'NUPPER, NLOWER, NMULT, NCNT, NCRT'
!	PRINT*, NUPPER, NLOWER, NMULT, NCNT, ncrt 
!	PRINT*, xat3(1), xat3(2), xat3(3)
!
	mol(i)%res(j)%res_aas(k)%aa_rots(ncrt)%rot_ats(m)%at_xyz(1) = xat3(1)
	mol(i)%res(j)%res_aas(k)%aa_rots(ncrt)%rot_ats(m)%at_xyz(2) = xat3(2)
	mol(i)%res(j)%res_aas(k)%aa_rots(ncrt)%rot_ats(m)%at_xyz(3) = xat3(3)
!
	     end do 
	    end do
	  end do
!
	  nlower = nlower * nmult
	end do 
	end do 
!
!     DONE EXPANDING HYDROGENS 
!
	     end if  
	    end do
	  end do
	end do
!
!
	END SUBROUTINE EXPAND_HYDROROTAMS
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE EXPAND_ROTAMERS(set,nexp)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	TYPE (ROTAMER_T), DIMENSION(:), ALLOCATABLE :: a_sa
	INTEGER :: a_nrt
!
	integer:: i,j,k,l,m,n,o,p
	integer:: nht, nexp, nrt
	integer:: n12, n14, nc, nat
	integer, dimension(4) :: set
!
	i=set(1)
	j=set(2)
	k=set(3)
	nht=nexp

!	First backup rotamers into a_sa
!
	if (allocated(a_sa)) then
		   a_nrt=size(a_sa)
	           do l=1,a_nrt 
		      deallocate(a_sa(l)%rot_ats)
		   end do
		   deallocate (a_sa)
	end if
!
	a_nrt = mol(i)%res(j)%res_aas(k)%aa_nrt
	allocate(a_sa(a_nrt))
!
	do l=1,a_nrt 
	         a_sa(l)%rot_nat=   mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat 
	         a_sa(l)%rot_prob=  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_prob
	         a_sa(l)%rot_en=    mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_en
	         a_sa(l)%rot_flag=  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_flag
	         a_sa(l)%rot_flag2= mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_flag2
!
	         allocate(a_sa(l)%rot_ats(a_sa(l)%rot_nat))
	         do m=1,a_sa(l)%rot_nat
!
	a_sa(l)%rot_ats(m)%at_nam = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam
	a_sa(l)%rot_ats(m)%at_typ = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_typ
	a_sa(l)%rot_ats(m)%at_ab = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_ab
	a_sa(l)%rot_ats(m)%at_hb = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_hb
	a_sa(l)%rot_ats(m)%at_xyz(1) = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1)
	a_sa(l)%rot_ats(m)%at_xyz(2) = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2)
	a_sa(l)%rot_ats(m)%at_xyz(3) = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3)
	a_sa(l)%rot_ats(m)%at_bfa  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
	a_sa(l)%rot_ats(m)%at_occ  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_occ
	a_sa(l)%rot_ats(m)%at_cha  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_cha
	a_sa(l)%rot_ats(m)%at_sig  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_sig
	a_sa(l)%rot_ats(m)%at_eps  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_eps
	a_sa(l)%rot_ats(m)%at_svo  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_svo
	a_sa(l)%rot_ats(m)%at_sla  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_sla
	a_sa(l)%rot_ats(m)%at_sgr  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_sgr
	a_sa(l)%rot_ats(m)%at_sgf  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_sgf
	a_sa(l)%rot_ats(m)%at_ede  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_ede
	a_sa(l)%rot_ats(m)%at_noz  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_noz
	a_sa(l)%rot_ats(m)%at_ntyp  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_ntyp
	a_sa(l)%rot_ats(m)%at_num  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_num
	a_sa(l)%rot_ats(m)%at_n12  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_n12
	a_sa(l)%rot_ats(m)%at_n14  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_n14
	a_sa(l)%rot_ats(m)%at_flag  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_flag
!
	if (a_sa(l)%rot_ats(m)%at_n12 /=0 ) then
	   n12= a_sa(l)%rot_ats(m)%at_n12
	   allocate(a_sa(l)%rot_ats(m)%at_c12(n12,5))
	   do n=1,n12
	     do o=1,5
	a_sa(l)%rot_ats(m)%at_c12(n,o)= mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_c12(n,o) 
	     end do
           end do
	end if
!
	if (a_sa(l)%rot_ats(m)%at_n14 /=0 ) then
	   n14= a_sa(l)%rot_ats(m)%at_n14
	   allocate(a_sa(l)%rot_ats(m)%at_c14(n14,5))
	   do n=1,n14
	     do o=1,5
	a_sa(l)%rot_ats(m)%at_c14(n,o)= mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_c14(n,o) 
	     end do
           end do
	end if
!
                 end do
	        end do
!
!	NOW MAKE SPACE FOR EXPANDED ROTAMERS ....
!	AND COPY DATA BACK
!
        do l=1, mol(i)%res(j)%res_aas(k)%aa_nrt
           deallocate(mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats)
        end do
        deallocate(mol(i)%res(j)%res_aas(k)%aa_rots)	
!	    
	nrt = a_nrt * nht
	mol(i)%res(j)%res_aas(k)%aa_nrt = nrt
	allocate(mol(i)%res(j)%res_aas(k)%aa_rots(nrt))
	nc = 0
	do l=1, a_nrt 
	  nat = a_sa(l)%rot_nat
	  do m= 1,  nht
	    nc = nc + 1 
!
	   mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_nat =  a_sa(l)%rot_nat   
	   mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_prob = a_sa(l)%rot_prob  
	   mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_en =   a_sa(l)%rot_en
	   mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_flag = a_sa(l)%rot_flag  
	   mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_flag2 = a_sa(l)%rot_flag2
!
	    allocate(mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(nat))
	    do n= 1, nat 
!
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_nam = a_sa(l)%rot_ats(n)%at_nam
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_typ = a_sa(l)%rot_ats(n)%at_typ  
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_ab = a_sa(l)%rot_ats(n)%at_ab  
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_hb = a_sa(l)%rot_ats(n)%at_hb  
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_xyz(1) = a_sa(l)%rot_ats(n)%at_xyz(1)  
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_xyz(2) = a_sa(l)%rot_ats(n)%at_xyz(2)  
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_xyz(3) = a_sa(l)%rot_ats(n)%at_xyz(3)
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_bfa = a_sa(l)%rot_ats(n)%at_bfa  
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_occ = a_sa(l)%rot_ats(n)%at_occ 
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_cha = a_sa(l)%rot_ats(n)%at_cha 
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_sig= a_sa(l)%rot_ats(n)%at_sig  
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_eps= a_sa(l)%rot_ats(n)%at_eps 
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_svo= a_sa(l)%rot_ats(n)%at_svo 
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_sla= a_sa(l)%rot_ats(n)%at_sla 
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_sgr= a_sa(l)%rot_ats(n)%at_sgr 
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_sgf= a_sa(l)%rot_ats(n)%at_sgf 
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_ede= a_sa(l)%rot_ats(n)%at_ede 
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_noz= a_sa(l)%rot_ats(n)%at_noz 
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_ntyp= a_sa(l)%rot_ats(n)%at_ntyp  
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_num= a_sa(l)%rot_ats(n)%at_num
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_n12= a_sa(l)%rot_ats(n)%at_n12 
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_n14= a_sa(l)%rot_ats(n)%at_n14 
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_flag= a_sa(l)%rot_ats(n)%at_flag
!
	if (a_sa(l)%rot_ats(n)%at_n12 /=0 ) then
	   n12= a_sa(l)%rot_ats(n)%at_n12
	   allocate(mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_c12(n12,5))
	   do o=1,n12
	     do p=1,5
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_c12(o,p) = a_sa(l)%rot_ats(n)%at_c12(o,p)
	     end do
           end do
	end if
!
	if (a_sa(l)%rot_ats(n)%at_n14 /=0 ) then
	   n14= a_sa(l)%rot_ats(n)%at_n14
	   allocate(mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_c14(n14,5))
	   do o=1,n14
	     do p=1,5
	mol(i)%res(j)%res_aas(k)%aa_rots(nc)%rot_ats(n)%at_c14(o,p) = a_sa(l)%rot_ats(n)%at_c14(o,p)
	     end do
           end do
	end if
!
	end do
	end do 
	end do
!
!
	END SUBROUTINE EXPAND_ROTAMERS
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE INIT_LIGAND 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
     USE MOLEC_M
     USE AA_DATA_M
     USE MUMBO_DATA_M
     USE MAX_DATA_M
     USE MOLECULE_SIMP_DATA_M
!     
     IMPLICIT NONE
!     
     INTEGER, DIMENSION(:), allocatable :: nstart
     INTEGER  :: i, j, k, l
     INTEGER  :: nl, nx, ny, nz, ns, nt
!     
     CHARACTER (LEN=4) :: old, atnm1, atnm2, aanm, atnm
!     
     logical :: first, flag 
     CHARACTER (LEN=1) :: cid
!     
     REAL :: xx, xy, xz, xocc, xbfa, xprob
!
     LOGICAL            :: cif_flag, pdb_flag
     CHARACTER(LEN=4)   :: ext
     CHARACTER(LEN=132) :: infile
     INTEGER            :: nn
!  
     cif_flag = .false.
     pdb_flag = .false.
!
     do i=1,132
        infile(i:i) = ' '
     end do
     infile(1:132) = mumbo_lig_in(1:132)
!     
     ext = infile((len_trim(infile)-3):len_trim(infile))
     if ((ext.eq.'.pdb').or.(ext.eq.'.PDB')) then  
         print*,'  LIGAND INPUT FILE IS ASSUMED TO BE A ** PDB ** FILE '
         print*,'  ',infile(1:len_trim(infile))
         print*,'  '
         cif_flag = .false.
         pdb_flag = .true.
     else if ((ext.eq.'.cif').or.(ext.eq.'.CIF')) then
         print*,'  LIGAND INPUT FILE IS ASSUMED TO BE A ** CIF ** FILE '
         print*,'  ',infile(1:len_trim(infile))
         print*,'  '
         cif_flag = .true.
         pdb_flag = .false.
     else
        print*, '  NOT CLEAR WHETHER LIGAND INPUT FILE = PDB OR CIF-FILE '
        print*, '  FILE EXTENSION NOT RECOGNIZED: ', ext
        print*, '  MUST QUIT'
        print*,  infile(1:len_trim(infile)) !
        stop    
     end if   
!
!    reading in structure files
!
     nn =1                       ! everything should be read into sim(1) 
     if (cif_flag) then 
         call cif_read_coords(infile,nn)
     else if (pdb_flag) then 
         call pdb_read_coords(infile,nn)
     end if
!
     nl   = size(mol)
     allocate(mol(nl)%res(1))
     mol(nl)%mol_nrs=1
     mol(nl)%res(1)%res_lig=.true.
     mol(nl)%res(1)%res_pps=.false.
     mol(nl)%res(1)%res_bbr=.false.
     mol(nl)%res(1)%res_num=   0
     mol(nl)%res(1)%res_nold= mumbo_lig_nold
     mol(nl)%res(1)%res_chid= mumbo_lig_chid
!
     nx = size(sim(nn)%mol_res)
!     
!                        now trying to find out how many different types of ligands are present
     old='    '
     nz=0
     first=.true.
     do i=1, nx
        if (sim(nn)%mol_res(i)%res_name/=old) then
             if (first) then
                first=.false.
                nz=1
                old=sim(nn)%mol_res(i)%res_name
             else
                nz=nz+1
                old=sim(nn)%mol_res(i)%res_name
             end if
        end if
     end do
!
     allocate(mol(nl)%res(1)%res_aas(nz))
     if (allocated(nstart)) deallocate (nstart)
     allocate(nstart(nz))
     mol(nl)%res(1)%res_naa=nz
!     
     old='    '
     ny=0
     nz=0
     first=.true.
     do i=1, nx
         if  (sim(nn)%mol_res(i)%res_name==old) then
              ny=ny+1
         else if (sim(nn)%mol_res(i)%res_name/=old) then
           if (first) then
              first=.false.
              nz=1
              ny=1
              old = sim(nn)%mol_res(i)%res_name
           else
              allocate(mol(nl)%res(1)%res_aas(nz)%aa_rots(ny))
              mol(nl)%res(1)%res_aas(nz)%aa_nrt = ny
              mol(nl)%res(1)%res_aas(nz)%aa_nam = sim(nn)%mol_res(i-1)%res_name
              nz=nz+1
              ny=1
              old = sim(nn)%mol_res(i)%res_name
           end if
           nstart(nz)=i
         end if
     end do
!
     allocate(mol(nl)%res(1)%res_aas(nz)%aa_rots(ny))
     mol(nl)%res(1)%res_aas(nz)%aa_nrt = ny
     mol(nl)%res(1)%res_aas(nz)%aa_nam = sim(nn)%mol_res(nx)%res_name
!
!
!    Now the diffferent aas and rotamers have to be initiated
!
     if (mumbo_lig_chid==' ') then
        mol(nl)%res(1)%res_chid = sim(nn)%mol_res(1)%res_chid
     end if   
     if (mumbo_lig_nold==0) then
        mol(nl)%res(1)%res_nold = sim(nn)%mol_res(1)%res_nold             
     end if
!  
     nx = mol(nl)%res(1)%res_naa
!     print*, nx
     do i=1,nx                      
          aanm=  mol(nl)%res(1)%res_aas(i)%aa_nam 
          ny=    mol(nl)%res(1)%res_aas(i)%aa_nrt 
!          print*, ny, aanm
          do j=1,ny
             mol(nl)%res(1)%res_aas(i)%aa_rots(j)%rot_flag=.true.
             mol(nl)%res(1)%res_aas(i)%aa_rots(j)%rot_flag2=.false.
             mol(nl)%res(1)%res_aas(i)%aa_rots(j)%rot_prob= 1.0
          end do
          call init_aminoacid(nl,1,i,ny,aanm)
     end do
!
!!DEBUG
!        print*, nstart
!        nt=0
!        nx= mol(nl)%res(1)%res_naa
!        do i= 1, nx        
!          ny=  mol(nl)%res(1)%res_aas(i)%aa_nrt 
!          do j=1, ny
!             nt=nt+1
!             print*, 'BEEN HERE', nt, i, j 
!             call dump_pdb(nl,1,i,j)          
!          end do
!        end do
!!DEBUG
!
        ns = 1
        do i=1, nx
!           print*, nstart(i), ns 
           do j=1, mol(nl)%res(1)%res_aas(i)%aa_nrt
              do k=1, mol(nl)%res(1)%res_aas(i)%aa_rots(j)%rot_nat
                 atnm1 = mol(nl)%res(1)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_nam
                 do l=1,size(sim(nn)%mol_res(ns)%res_ats) 
                   atnm2 = sim(nn)%mol_res(ns)%res_ats(l)%at_name
!             
                   if (atnm1.eq.atnm2) then
!            
                    atnm = sim(nn)%mol_res(ns)%res_ats(l)%at_name
                    xx   = sim(nn)%mol_res(ns)%res_ats(l)%at_xyz(1)
                    xy   = sim(nn)%mol_res(ns)%res_ats(l)%at_xyz(2)
                    xz   = sim(nn)%mol_res(ns)%res_ats(l)%at_xyz(3)
                    xocc = sim(nn)%mol_res(ns)%res_ats(l)%at_occ
                    xbfa = sim(nn)%mol_res(ns)%res_ats(l)%at_bfa
!              
                    mol(nl)%res(1)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_nam=atnm
                    mol(nl)%res(1)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_xyz(1)=xx
                    mol(nl)%res(1)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_xyz(2)=xy
                    mol(nl)%res(1)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_xyz(3)=xz
                    mol(nl)%res(1)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_occ=xocc
                    mol(nl)%res(1)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_bfa=xbfa
                    mol(nl)%res(1)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_flag=.true.
!                  
                    exit
!                  
                   end if
!                
                 end do                 
!           
              end do
              ns = ns + 1
           end do
        end do   
!           
!       Now checking whether a residue with identical name and chain id is present in the main chain molecule
!       this causes significant trouble later 
!
         flag=.false.
         nl=(size(mol))
         do i=1,mol(1)%mol_nrs
              if ((mol(1)%res(i)%res_chid.eq.mol(nl)%res(1)%res_chid)) then 
                  if ((mol(1)%res(i)%res_nold.eq.mol(nl)%res(1)%res_nold)) then 
                     flag=.true.              
                  end if
              end if 
         end do
!
         if (flag) then                               
                PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >> '
                PRINT*, '>>                                                                   >> '
                PRINT*, '>> LIGAND/RESIDUE WITH IDENTICAL RESNUM AND CHAIN_ID                 >> '
                PRINT*, '>> IS ALREADY PRESENT IN ORIGINAL STRUCTURE                          >> '
                PRINT*, '>> THIS WILL CAUSE SERIOUS TROUBLE                                   >> '
                PRINT*, '>>                                                                   >> '
                PRINT*, '>> ',mol(nl)%res(1)%res_chid,'  ',mol(nl)%res(1)%res_nold,'          >> '
                PRINT*, '>>                                                                   >> '
                PRINT*, '>> REMEDY: SPECIFY A DIFFERENT LIGAND NAME IN MUMBO.INP:             >> '
                PRINT*, '>> LIGAND_TUNING= LIGNAME= X 901                                     >> '
                PRINT*, '>>                                                                   >> '
                PRINT*, '>> MUST EXIT                                                         >> '
                PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >> '
                stop
         end if
!
        PRINT*, '                            ..... DONE'
!
!
!!DEBUG
!        nt=0
!        nx=mol(nl)%res(1)%res_naa
!        do i=1,nx        
!          ny=  mol(nl)%res(1)%res_aas(i)%aa_nrt 
!          do j=1,ny
!             nt= nt+1
!             print*, 'BEEN HERE ', nt, i, j 
!            call dump_pdb(nl,1,i,j)          
!          end do
!        end do
!!DEBUG
!
        return
!
	END SUBROUTINE INIT_LIGAND 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE EXPAND_LIGAND
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE AA_DATA_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	INTEGER, DIMENSION(4) :: set
	INTEGER :: nrold, nexp, nml, nat, ncnt, nht, naa
	INTEGER :: i, j, k, m
	INTEGER, DIMENSION(:), ALLOCATABLE :: iseed
	INTEGER :: seed_size
	REAL, DIMENSION(:,:), ALLOCATABLE :: cent
	REAL, DIMENSION(:,:), ALLOCATABLE :: mean
	REAL, dimension(3,3) :: rot, unit
	REAL, dimension(3) :: tra, trb, trc
	REAL, dimension(3) :: xat
	REAL, dimension(6) :: shift
	REAL, parameter :: pi = 3.1415927 
	REAL :: xboff, dis(4), xrms, ang(3)
	REAL :: cosa, cosb, cosg, sina, sinb, sing
	REAL :: sino, coso, sinp, cosp, sink, cosk
	REAL :: ld , md, nd, ld2, md2, nd2
	CHARACTER (LEN=4) :: xxnam 
	LOGICAL ::	xxlog
!
!
	if (.not.mumbo_lig_flag) then 
	  return
	end if
!
	if (mumbo_lig_ngen==0) then
	  return
	end if
!
!	Now expanding ligand orientations
!
	nml= size(mol)
!	
!       HERE LOOP OVER ALL DIFFERENT LIGAND PRESENT IN mol(nml) NOW
!       in order to achieve this
!       mol(nml)%res(1)%res_aas(1)%aa_nrt from n older version had to be replaced by 
!       mol(nml)%res(1)%res_aas(naa)%aa_nrt all over the place
!
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY EXPAND_LIGAND        #'
	PRINT*, '################################'
	PRINT*, '        '
!
        do m=1,mol(nml)%res(1)%res_naa
!	
	naa = m
	nrold = mol(nml)%res(1)%res_aas(naa)%aa_nrt
	nexp  = mumbo_lig_ngen
	nht =   nrold * nexp
!	
	CALL random_seed(size=seed_size)
	ALLOCATE(iseed(seed_size))
	iseed(1) = mumbo_lig_seed
!
	set(1)= nml
	set(2)= 1
	set(3)= naa
	set(4)= 1
!
	CALL EXPAND_ROTAMERS(set,nexp)
!
!       CALCULATE CENTRE OF 'MASS' OF EACH ROTAMER OR ASSIGN FIX COORDINATES TO CENTRE OF MASS
!	IN CASE LIGCENTRE WAS SPECIFIED
!
	if (allocated(cent)) deallocate(cent)
	allocate(cent(nrold,3))
!
	ncnt=0
	do i=1, nht, nexp
	   ncnt= ncnt+1
	   cent(ncnt,1) = 0 
	   cent(ncnt,2) = 0 
	   cent(ncnt,3) = 0
	   nat = mol(nml)%res(1)%res_aas(naa)%aa_rots(i)%rot_nat
	   if (mumbo_lig_centre) then 
	      xxlog=.false.
	      do j = 1, nat
		xxnam= mol(nml)%res(1)%res_aas(naa)%aa_rots(i)%rot_ats(j)%at_nam
		if (xxnam==mumbo_lig_ctan) then 
	cent(ncnt,1) = mol(nml)%res(1)%res_aas(naa)%aa_rots(i)%rot_ats(j)%at_xyz(1) 
	cent(ncnt,2) = mol(nml)%res(1)%res_aas(naa)%aa_rots(i)%rot_ats(j)%at_xyz(2) 
	cent(ncnt,3) = mol(nml)%res(1)%res_aas(naa)%aa_rots(i)%rot_ats(j)%at_xyz(3) 
	xxlog=.true.
		end if		
	      end do
	      if (.not.xxlog) then 
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >> '
		PRINT*, '>>                                                                   >> '
		PRINT*, '>> ATOM: ', mumbo_lig_ctan, ' NOT FOUND TO CENTRE LIGAND ON          >> '
		PRINT*, '>>                                                                   >> '
		PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >> '
		STOP
	      end if
	   else
	      do j = 1, nat
	xat(1) = mol(nml)%res(1)%res_aas(naa)%aa_rots(i)%rot_ats(j)%at_xyz(1) 
	xat(2) = mol(nml)%res(1)%res_aas(naa)%aa_rots(i)%rot_ats(j)%at_xyz(2) 
	xat(3) = mol(nml)%res(1)%res_aas(naa)%aa_rots(i)%rot_ats(j)%at_xyz(3) 
	         cent(ncnt,1) = cent(ncnt,1) + xat(1) 
	         cent(ncnt,2) = cent(ncnt,2) + xat(2) 
	         cent(ncnt,3) = cent(ncnt,3) + xat(3)
	      end do
!
	      cent(ncnt,1) = cent(ncnt,1) / nat 
	      cent(ncnt,2) = cent(ncnt,2) / nat 
	      cent(ncnt,3) = cent(ncnt,3) / nat
	   end if 
	end do
!
	call inimat(unit)
	call random_seed(put=iseed)
	deallocate(iseed)
!
!	FIRST ROTAMER KEPT AS IN INPUT
!
	do i= 2, nexp
!
	   do j=1,3
	      tra(j) = 0.0
	      ang(j) = 0.0
	   end do
!
	   do j=1,6
	      shift(j) = 0.0
	   end do
!
	   do j=1, 6
	      call random_number(xboff)
	      shift(j) = xboff - 0.5
	   end do
!
	   if (.not.mumbo_lig_rot_flag) then 
		do j=1,3
		  shift(j) = 0.0
		end do 
	   end if
!
	   if (.not.mumbo_lig_shift_flag) then 
		do j=4,6
		  shift(j) = 0.0
		end do 
	   end if
!
	if (mumbo_lig_fullrot_flag) then 	   
!
	! uniform sampling of euler angles: sampling over cos(beta)
!
	   ang(1) =  (shift(1) + 0.5) * 360
	   ang(3) =  (shift(3) + 0.5) * 360

	   cosb   =  2*shift(2)
           
	   ang(2) =  ACOS(cosb)   
	   ang(1) = (ang(1) * pi) / 180 
	   ang(3) = (ang(3) * pi) / 180	
	
	   sina = sin(ang(1))
	   cosa = cos(ang(1))
	   sinb = sin(ang(2))
 	   sing = sin(ang(3))
 	   cosg = cos(ang(3))
  
	   rot(1,1) = - sina*cosb*sing + cosa*cosg
 	   rot(1,2) =   cosa*cosb*sing + sina*cosg
 	   rot(1,3) =   sinb*sing
	   rot(2,1) = - sina*cosb*cosg - cosa*sing
	   rot(2,2) =   cosa*cosb*cosg - sina*sing
 	   rot(2,3) =   sinb*cosg
  	   rot(3,1) =   sina*sinb
  	   rot(3,2) = - cosa*sinb
  	   rot(3,3) =   cosb
!
	else 
!
	   tra(1) = mumbo_lig_rms * shift(4) * 2
	   tra(2) = mumbo_lig_rms * shift(5) * 2
	   tra(3) = mumbo_lig_rms * shift(6) * 2
!
	   ang(1) =  (shift(1) + 0.5) * 180 
	   ang(2) =  (shift(2) + 0.5) * 360  
	   ang(3) =   mumbo_lig_rms * shift(3) * 90 
!
	   ang(1) = (ang(1) * pi) / 180 
	   ang(2) = (ang(2) * pi) / 180
	   ang(3) = (ang(3) * pi) / 180
!
!	POLAR ANGLE CODE
!
!	ang(1)=omega, ang(2)=phi, ang(3)=kappa
!
!               the rotation KAPPA takes place.
!
! (abbreviating cos(OMEGA) = CO; sin(PHI) = SP etc)
!         ( l )             ( SO*CP )
!         ( m )     =       ( SO*SP )
!         ( n )             (    CO )
!
! and   [ R11 R12 R13 ]
!       [ R21 R22 R23 ]   =
!       [ R31 R32 R33 ]
!
!     ( ll+(mm+nn)CK     lm(1-CK)-nSK     nl(1-CK)+mSK )
!     ( lm(1-CK)+nSK     mm+(ll+nn)CK     mn(1-CK)-lSK )
!     ( nl(1-CK)-mSK     mn(1-CK)+lSK     nn+(ll+mm)CK )
!
	 sino = sin(ang(1))
	 coso = cos(ang(1))
	 sinp = sin(ang(2))
	 cosp = cos(ang(2))
	 sink = sin(ang(3))
	 cosk = cos(ang(3))
!
	 ld = sino * cosp
	 md = sino * sinp
	 nd = coso
	 ld2 = ld * ld
	 md2 = md * md 
	 nd2 = nd * nd
!
         rot(1,1) =   ld2 + (md2 + nd2)  * cosk 
         rot(1,2) =   ld * md * (1-cosk) - nd * sink
         rot(1,3) =   nd * ld * (1-cosk) + md * sink
         rot(2,1) =   ld * md * (1-cosk) + nd * sink
         rot(2,2) =   md2 + (ld2 + nd2)  * cosk 
         rot(2,3) =   md * nd * (1-cosk) - ld * sink
         rot(3,1) =   nd * ld * (1-cosk) - md * sink
         rot(3,2) =   md * nd * (1-cosk) + ld * sink
         rot(3,3) =   nd2 + (ld2 + md2)  * cosk 
!
	end if
!
!
!	now apply matrix to all atoms in one rotamer........
!
	      do j = 1, nrold 
	         ncnt = ((j-1) * nexp) + i  
!
		   trb(1) = - cent(j,1)
		   trb(2) = - cent(j,2)
		   trb(3) = - cent(j,3)
!
		   trc(1) = cent(j,1)
		   trc(2) = cent(j,2)
		   trc(3) = cent(j,3)
!	  

		   do k = 1, mol(nml)%res(1)%res_aas(naa)%aa_rots(ncnt)%rot_nat
!
	xat(1) = mol(nml)%res(1)%res_aas(naa)%aa_rots(ncnt)%rot_ats(k)%at_xyz(1) 
	xat(2) = mol(nml)%res(1)%res_aas(naa)%aa_rots(ncnt)%rot_ats(k)%at_xyz(2) 
	xat(3) = mol(nml)%res(1)%res_aas(naa)%aa_rots(ncnt)%rot_ats(k)%at_xyz(3) 
!
	  	   call applytransform(xat,unit,trb)
	  	   call applytransform(xat,rot,tra)
	  	   call applytransform(xat,unit,trc)
!	         
	mol(nml)%res(1)%res_aas(naa)%aa_rots(ncnt)%rot_ats(k)%at_xyz(1) = xat(1)
	mol(nml)%res(1)%res_aas(naa)%aa_rots(ncnt)%rot_ats(k)%at_xyz(2) = xat(2)
	mol(nml)%res(1)%res_aas(naa)%aa_rots(ncnt)%rot_ats(k)%at_xyz(3) = xat(3)
!
		   end do
 		end do 
	end do
!
!  now calculate rms deviation between  all the rotamers generated
!  needs only to be calculated over 
!
	nat = mol(nml)%res(1)%res_aas(naa)%aa_rots(1)%rot_nat
	if (allocated(mean)) deallocate(mean)
	allocate(mean(nat,3))
!
	do i = 1, nat
	   mean(i,1) = 0 
	   mean(i,2) = 0
	   mean(i,3) = 0
	   do j = 1, nexp
	xat(1) = mol(nml)%res(1)%res_aas(naa)%aa_rots(j)%rot_ats(i)%at_xyz(1) 
	xat(2) = mol(nml)%res(1)%res_aas(naa)%aa_rots(j)%rot_ats(i)%at_xyz(2) 
	xat(3) = mol(nml)%res(1)%res_aas(naa)%aa_rots(j)%rot_ats(i)%at_xyz(3)
	     mean(i,1) = mean(i,1) + xat(1) 
	     mean(i,2) = mean(i,2) + xat(2) 
	     mean(i,3) = mean(i,3) + xat(3) 
	   end do
	   mean(i,1) = mean(i,1) / nexp 
	   mean(i,2) = mean(i,2) / nexp 
	   mean(i,3) = mean(i,3) / nexp 
	end do 
!
!  now calculate the root mean square deviation
!
	xrms = 0
	do i = 1, nat
	   do j = 1, nexp
	xat(1) = mol(nml)%res(1)%res_aas(naa)%aa_rots(j)%rot_ats(i)%at_xyz(1) 
	xat(2) = mol(nml)%res(1)%res_aas(naa)%aa_rots(j)%rot_ats(i)%at_xyz(2) 
	xat(3) = mol(nml)%res(1)%res_aas(naa)%aa_rots(j)%rot_ats(i)%at_xyz(3)
	     dis(1) = (xat(1) - mean(i,1))**2
	     dis(2) = (xat(2) - mean(i,2))**2
	     dis(3) = (xat(3) - mean(i,3))**2
	     dis(4) = (dis(1) + dis(2) + dis(3))
            xrms = xrms + dis(4)
	   end do
	end do
	xrms = xrms / (nat * nexp)
	xrms = sqrt(xrms) 
!
	PRINT*, '  NUMBER OF LIGANDS GENERATED:  ', nexp
	PRINT*, '  FOR EACH CONFORMER OF:                ', mol(nml)%res(1)%res_aas(naa)%aa_nam
	PRINT*, '  R.M.S.D. OF EXPANDED LIGANDS: ', xrms     
	PRINT*, '        '
!
!       here end over loop m, number of ligands present
        end do	
!
	END SUBROUTINE EXPAND_LIGAND
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE EXPAND_SINGLE_AA(set1,set2,anam)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Subroutine to expand a mol/residue/aa set by a single amino acid
! First check whether single amino acid is already available.
! If anam already present then the corresponding entry is only expanded by a single rotamer 
! in subroutine expand_single_rt 
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
!
	TYPE (AA_T), DIMENSION(:), ALLOCATABLE :: aa_ba
	INTEGER, dimension(4), intent(in) :: set1
	INTEGER, dimension(4), intent(out):: set2
	CHARACTER(LEN=4), intent(in)  :: anam
!	
	INTEGER :: n1, n2, n3, n4, m1, m2, m3, m4 
	INTEGER :: res_naa, res_nold, aa_nrt, rot_nat
	INTEGER :: i, j, k, l, m, n12, n14, newpos
	LOGICAL :: flag
	CHARACTER(LEN=4)   :: nam
	REAL    :: xx, xy, xz, xocc, xbfa
!	
        n1=set1(1)
        n2=set1(2)
        n3=set1(3)
        n4=set1(4)
        flag=.false.
        newpos=0
!
!       first check whether amino acid is already present or not within a specific mol/residue/aa set
!
        do i=1,mol(n1)%res(n2)%res_naa
           if (mol(n1)%res(n2)%res_aas(i)%aa_nam.eq.anam)then
           flag=.true.
           newpos=i
           end if
        end do
!
        if (flag) then 
!         only expansion of existing aa-entry by a single rotamer is required
          call expand_single_rotamer(set1,set2,anam)
!
        else
!        
!	First backup aas into res_sa
!
        if (allocated(aa_ba)) then
            res_naa=size(aa_ba)
            do i=1,res_naa
                 aa_nrt=size(aa_ba(i)%aa_rots)
                 do j=1,aa_nrt
                   deallocate(aa_ba(i)%aa_rots(j)%rot_ats)	         
                 end do 
                 deallocate(aa_ba(i)%aa_rots)     
            end do  
            deallocate(aa_ba)
        end if
!
!
        res_naa = mol(n1)%res(n2)%res_naa
        allocate (aa_ba(res_naa))
        do i = 1,res_naa
!        
             aa_nrt= mol(n1)%res(n2)%res_aas(i)%aa_nrt
             allocate (aa_ba(i)%aa_rots(aa_nrt))
             aa_ba(i)%aa_nrt= mol(n1)%res(n2)%res_aas(i)%aa_nrt
             aa_ba(i)%aa_nam= mol(n1)%res(n2)%res_aas(i)%aa_nam
!
!
!		REAL ::    rot_prob           ! Rotamer_probability
!		REAL ::    rot_qrot           ! Rotamer_backbone_backrub_rotation
!
!
             do j= 1, aa_nrt
! 
               rot_nat=mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_nat
               allocate (aa_ba(i)%aa_rots(j)%rot_ats(rot_nat))
               aa_ba(i)%aa_rots(j)%rot_nat=   mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_nat 
	       aa_ba(i)%aa_rots(j)%rot_prob=  mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_prob
	       aa_ba(i)%aa_rots(j)%rot_qrot=  mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_qrot
	       aa_ba(i)%aa_rots(j)%rot_en=    mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_en
	       aa_ba(i)%aa_rots(j)%rot_flag=  mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_flag
	       aa_ba(i)%aa_rots(j)%rot_flag2= mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_flag2
!
	       do k=1,aa_ba(i)%aa_rots(j)%rot_nat
!
!	       
        aa_ba(i)%aa_rots(j)%rot_ats(k)%at_nam     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_nam
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_typ     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_typ
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_ab      = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_ab
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_hb      = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_hb
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_xyz(1)  = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_xyz(1)
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_xyz(2)  = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_xyz(2)
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_xyz(3)  = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_xyz(3)
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_bfa     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_bfa
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_occ     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_occ
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_cha     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_cha
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_sig     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_sig
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_eps     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_eps
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_svo     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_svo
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_sla     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_sla
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_sgr     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_sgr
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_sgf     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_sgf
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_ede     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_ede
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_noz     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_noz
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_ntyp    = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_ntyp
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_num     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_num
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_n12     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_n12
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_n14     = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_n14
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_flag    = mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_flag
!               
!
	if (aa_ba(i)%aa_rots(j)%rot_ats(k)%at_n12 /=0 ) then
	   n12= aa_ba(i)%aa_rots(j)%rot_ats(k)%at_n12
	   allocate(aa_ba(i)%aa_rots(j)%rot_ats(k)%at_c12(n12,5))
	   do l=1,n12
	     do m=1,5
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_c12(l,m)=mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_c12(l,m) 
	     end do
           end do
	end if
!
	if (aa_ba(i)%aa_rots(j)%rot_ats(k)%at_n14 /=0 ) then
	   n14= aa_ba(i)%aa_rots(j)%rot_ats(k)%at_n14
	   allocate(aa_ba(i)%aa_rots(j)%rot_ats(k)%at_c14(n14,5))
	   do l=1,n14
	     do m=1,5
	aa_ba(i)%aa_rots(j)%rot_ats(k)%at_c14(l,m)=mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_c14(l,m) 
	     end do
           end do
	end if
!
               end do
            end do
         end do    
!
!       now clean up 
!
!
        do i = 1,mol(n1)%res(n2)%res_naa
            do j= 1, mol(n1)%res(n2)%res_aas(i)%aa_nrt
               deallocate(mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats)
	    end do
	    deallocate(mol(n1)%res(n2)%res_aas(i)%aa_rots)
        end do
        deallocate(mol(n1)%res(n2)%res_aas)
!
!       now add new amino acid and copy everything back..
!
        res_nold = res_naa 
        res_naa =  res_naa + 1
!
        allocate(mol(n1)%res(n2)%res_aas(res_naa))
        mol(n1)%res(n2)%res_naa=res_naa
!        
        do i = 1,res_nold
             mol(n1)%res(n2)%res_aas(i)%aa_nrt = aa_ba(i)%aa_nrt 
             mol(n1)%res(n2)%res_aas(i)%aa_nam = aa_ba(i)%aa_nam 
             aa_nrt = mol(n1)%res(n2)%res_aas(i)%aa_nrt
             allocate(mol(n1)%res(n2)%res_aas(i)%aa_rots(aa_nrt))
!
             do j= 1, aa_nrt
               rot_nat=aa_ba(i)%aa_rots(j)%rot_nat
               allocate (mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(rot_nat)) 
               mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_nat   =   aa_ba(i)%aa_rots(j)%rot_nat   
	       mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_prob  =   aa_ba(i)%aa_rots(j)%rot_prob  
	       mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_qrot  =   aa_ba(i)%aa_rots(j)%rot_qrot  
	       mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_en    =   aa_ba(i)%aa_rots(j)%rot_en    
	       mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_flag  =   aa_ba(i)%aa_rots(j)%rot_flag  
	       mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_flag2 =   aa_ba(i)%aa_rots(j)%rot_flag2  
!
                do k=1,mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_nat
!
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_nam    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_nam    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_typ    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_typ    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_ab     = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_ab     	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_hb     = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_hb     	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_xyz(1) = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_xyz(1) 	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_xyz(2) = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_xyz(2) 	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_xyz(3) = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_xyz(3) 	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_bfa    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_bfa    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_occ    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_occ    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_cha    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_cha    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_sig    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_sig    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_eps    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_eps    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_svo    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_svo    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_sla    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_sla    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_sgr    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_sgr    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_sgf    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_sgf    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_ede    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_ede    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_noz    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_noz    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_ntyp   = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_ntyp   	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_num    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_num    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_n12    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_n12    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_n14    = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_n14    	       
        mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_flag   = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_flag   	       
!
	if (mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_n12 /=0 ) then
	   n12= mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_n12
	   allocate(mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_c12(n12,5))
	   do l=1,n12
	     do m=1,5
	mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_c12(l,m) = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_c12(l,m)
	     end do
           end do
	end if
!
	if (mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_n14 /=0 ) then
	   n14= mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_n14
	   allocate(mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_c14(n14,5))
	   do l=1,n14
	     do m=1,5
	mol(n1)%res(n2)%res_aas(i)%aa_rots(j)%rot_ats(k)%at_c14(l,m) = aa_ba(i)%aa_rots(j)%rot_ats(k)%at_c14(l,m) 
	     end do
           end do
	end if
!
               end do
            end do             
        end do 
!
!       now dealing with the new aa and rotamer
!
        allocate(mol(n1)%res(n2)%res_aas(res_naa)%aa_rots(1))
        mol(n1)%res(n2)%res_aas(res_naa)%aa_nrt = 1 
        mol(n1)%res(n2)%res_aas(res_naa)%aa_nam = anam       
        set2(1)=n1
        set2(2)=n2
        set2(3)=res_naa
        set2(4)=1
        call init_aminoacid(n1,n2,res_naa,1,anam)
!
!       Now trying to fill in available data from common atoms between set1 and set2
!       Not sure whether more information then x,y,z,occ,bfac should be extracted from starting rotamer.. 
!
!       making typing easier
!
        n1= set2(1)
        n2= set2(2)
        n3= set2(3)
        n4= set2(4)
        m1= set1(1)
        m2= set1(2)
        m3= set1(3)
        m4= set1(4)
	mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_prob= mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_prob
	mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_qrot= mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_qrot	
!
	do i=1,mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat
	  nam=mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_nam
	  flag=mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_flag
	  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_n12=0
	  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_n14=0
!	  
	  do j=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	    if (nam.eq.mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_nam) then
	       if (.not.flag) then
	  xx   =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(1) 
	  xy   =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(2) 
	  xz   =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_xyz(3) 
	  xocc =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_occ 
	  xbfa =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(j)%at_bfa 
!	  
	  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_flag=.true.
	  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_xyz(1)=xx
	  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_xyz(2)=xy
	  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_xyz(3)=xz
	  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_occ=xocc
	  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_bfa=xbfa
!	  
!	  
	       end if
	    end if
	  end do
        end do
!
!
	if (allocated(aa_ba)) then
	    res_naa=size(aa_ba)
	    do i=1,res_naa
	         aa_nrt=size(aa_ba(i)%aa_rots)
	         do j=1,aa_nrt
	           deallocate(aa_ba(i)%aa_rots(j)%rot_ats)	         
	         end do 
	         deallocate(aa_ba(i)%aa_rots)     
	    end do  
	    deallocate(aa_ba)
	end if
!
!        
        end if
!
!
	END SUBROUTINE EXPAND_SINGLE_AA
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
	SUBROUTINE EXPAND_SINGLE_ROTAMER(set1,set2,anam)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Subroutine to expand a mol/residue/aa by a single rotamer
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	TYPE (ROTAMER_T), DIMENSION(:), ALLOCATABLE :: rt_ba
	INTEGER, dimension(4), intent(in) :: set1
	INTEGER, dimension(4), intent(out):: set2
	CHARACTER(LEN=4), intent(in)  :: anam
!	
	INTEGER :: n1, n2, n3, n4, m1, m2, m3, m4 
	INTEGER :: n12, n14, nat
	INTEGER :: aa_nrt, aa_nold
	INTEGER :: i, j, k, l
	LOGICAL :: flag
	CHARACTER(LEN=4)   :: nam
	REAL    :: xx, xy, xz, xocc, xbfa
!
!       here n1,n2.. = old set and m1,m2... new set
!
        n1=set1(1)
        n2=set1(2)
        n3=set1(3)
        n4=set1(4)
        m1=n1
        m2=n2
        m3=0
        nat=0
!
!       first checking out to which amino acid rotamer has to be added
!
!
        do i=1,mol(m1)%res(m2)%res_naa
           if (mol(m1)%res(m2)%res_aas(i)%aa_nam.eq.anam)then
             m3 = i
           end if
        end do
!
!       now backing up rotamers from m1,m2,m3
!
	if (allocated(rt_ba)) then
	         aa_nrt=size(rt_ba)
	         do i=1,aa_nrt
	           deallocate(rt_ba(i)%rot_ats)	         
	         end do 
	         deallocate(rt_ba)
	end if
!
        aa_nrt=mol(m1)%res(m2)%res_aas(m3)%aa_nrt
!        
        allocate (rt_ba(aa_nrt))
        do i= 1, aa_nrt
               nat = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_nat
               allocate (rt_ba(i)%rot_ats(nat))
               rt_ba(i)%rot_nat=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_nat               
	       rt_ba(i)%rot_prob=  mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_prob
	       rt_ba(i)%rot_qrot=  mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_qrot
	       rt_ba(i)%rot_en=    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_en
	       rt_ba(i)%rot_flag=  mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_flag
	       rt_ba(i)%rot_flag2= mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_flag2
	       do j=1,rt_ba(i)%rot_nat
        rt_ba(i)%rot_ats(j)%at_nam     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_nam
	rt_ba(i)%rot_ats(j)%at_typ     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_typ
	rt_ba(i)%rot_ats(j)%at_ab      = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_ab
	rt_ba(i)%rot_ats(j)%at_hb      = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_hb
	rt_ba(i)%rot_ats(j)%at_xyz(1)  = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_xyz(1)
	rt_ba(i)%rot_ats(j)%at_xyz(2)  = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_xyz(2)
	rt_ba(i)%rot_ats(j)%at_xyz(3)  = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_xyz(3)
	rt_ba(i)%rot_ats(j)%at_bfa     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_bfa
	rt_ba(i)%rot_ats(j)%at_occ     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_occ
	rt_ba(i)%rot_ats(j)%at_cha     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_cha
	rt_ba(i)%rot_ats(j)%at_sig     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_sig
	rt_ba(i)%rot_ats(j)%at_eps     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_eps
	rt_ba(i)%rot_ats(j)%at_svo     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_svo
	rt_ba(i)%rot_ats(j)%at_sla     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_sla
	rt_ba(i)%rot_ats(j)%at_sgr     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_sgr
	rt_ba(i)%rot_ats(j)%at_sgf     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_sgf
	rt_ba(i)%rot_ats(j)%at_ede     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_ede
	rt_ba(i)%rot_ats(j)%at_noz     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_noz
	rt_ba(i)%rot_ats(j)%at_ntyp    = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_ntyp
	rt_ba(i)%rot_ats(j)%at_num     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_num
	rt_ba(i)%rot_ats(j)%at_n12     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_n12
	rt_ba(i)%rot_ats(j)%at_n14     = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_n14
	rt_ba(i)%rot_ats(j)%at_flag    = mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_flag
!
	if (rt_ba(i)%rot_ats(j)%at_n12 /=0 ) then
	   n12 = rt_ba(i)%rot_ats(j)%at_n12
	   allocate(rt_ba(i)%rot_ats(j)%at_c12(n12,5))
	   do k=1,n12
	     do l=1,5
	rt_ba(i)%rot_ats(j)%at_c12(k,l)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_c12(k,l) 
	     end do
           end do
	end if
!
	if (rt_ba(i)%rot_ats(j)%at_n14 /=0 ) then
	   n14 = rt_ba(i)%rot_ats(j)%at_n14
	   allocate(rt_ba(i)%rot_ats(j)%at_c14(n14,5))
	   do k=1,n14
	     do l=1,5
	rt_ba(i)%rot_ats(j)%at_c14(k,l)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_c14(k,l) 
	     end do
           end do
	end if
!
            end do
         end do    
!
!       now clean up 
!
        do i= 1, mol(m1)%res(m2)%res_aas(m3)%aa_nrt
           deallocate(mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats)
	end do
	deallocate(mol(m1)%res(m2)%res_aas(m3)%aa_rots)
!
!       now add new amino acid and copy everything back..
!
        aa_nold = aa_nrt 
        aa_nrt =  aa_nrt + 1
!
        allocate(mol(m1)%res(m2)%res_aas(m3)%aa_rots(aa_nrt))
        mol(m1)%res(m2)%res_aas(m3)%aa_nrt=aa_nrt
        do i= 1, aa_nold
               nat=rt_ba(i)%rot_nat
               allocate(mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(nat)) 
               mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_nat   =   rt_ba(i)%rot_nat   
	       mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_prob  =   rt_ba(i)%rot_prob  
	       mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_qrot  =   rt_ba(i)%rot_qrot  
	       mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_en    =   rt_ba(i)%rot_en    
	       mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_flag  =   rt_ba(i)%rot_flag  
	       mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_flag2 =   rt_ba(i)%rot_flag2  
!
	       do j=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_nat
!
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_nam    = rt_ba(i)%rot_ats(j)%at_nam   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_typ    = rt_ba(i)%rot_ats(j)%at_typ   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_ab     = rt_ba(i)%rot_ats(j)%at_ab    	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_hb     = rt_ba(i)%rot_ats(j)%at_hb    	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_xyz(1) = rt_ba(i)%rot_ats(j)%at_xyz(1)	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_xyz(2) = rt_ba(i)%rot_ats(j)%at_xyz(2)	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_xyz(3) = rt_ba(i)%rot_ats(j)%at_xyz(3)	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_bfa    = rt_ba(i)%rot_ats(j)%at_bfa   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_occ    = rt_ba(i)%rot_ats(j)%at_occ   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_cha    = rt_ba(i)%rot_ats(j)%at_cha   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_sig    = rt_ba(i)%rot_ats(j)%at_sig   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_eps    = rt_ba(i)%rot_ats(j)%at_eps   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_svo    = rt_ba(i)%rot_ats(j)%at_svo   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_sla    = rt_ba(i)%rot_ats(j)%at_sla   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_sgr    = rt_ba(i)%rot_ats(j)%at_sgr   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_sgf    = rt_ba(i)%rot_ats(j)%at_sgf   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_ede    = rt_ba(i)%rot_ats(j)%at_ede   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_noz    = rt_ba(i)%rot_ats(j)%at_noz   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_ntyp   = rt_ba(i)%rot_ats(j)%at_ntyp  	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_num    = rt_ba(i)%rot_ats(j)%at_num   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_n12    = rt_ba(i)%rot_ats(j)%at_n12   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_n14    = rt_ba(i)%rot_ats(j)%at_n14   	       
    mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_flag   = rt_ba(i)%rot_ats(j)%at_flag  	       
	              
	       !               
!
	if (mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_n12 /=0 ) then
	   n12= mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_n12
	   allocate(mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_c12(n12,5))
	   do k=1,n12
	     do l=1,5
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_c12(k,l) = rt_ba(i)%rot_ats(j)%at_c12(k,l)
	     end do
           end do
	end if
!
	if (mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_n14 /=0 ) then
	   n14= mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_n14
	   allocate(mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_c14(n14,5))
	   do k=1,n14
	     do l=1,5
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(i)%rot_ats(j)%at_c14(k,l) = rt_ba(i)%rot_ats(j)%at_c14(k,l) 
	     end do
           end do
	end if
!
               end do
            end do             
!
!       now dealing with the new rotamer
!
        m4=aa_nrt
!
!       same number of atoms as for other rotamers from the same aminoacid expected 
!
        set2(1)=m1
        set2(2)=m2
        set2(3)=m3
        set2(4)=m4
!        
        allocate(mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(nat)) 
        mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat = nat   
 !        
	do i=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
          mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_nam  = rt_ba(aa_nold)%rot_ats(i)%at_nam   	       
          mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_flag = .false.  	       
        end do
!
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_prob= mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_prob
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_qrot= mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_qrot
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_flag  = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_flag  
	mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_flag2 = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_flag2  
!
	do i=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
	  nam=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_nam
	  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_n12=0
	  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_n14=0
          flag=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_flag
	  do j=1,mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat
!	  
!	  
!	  
	    if (nam.eq.mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_nam) then
	       if (.not.flag) then
	  xx   =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_xyz(1) 
	  xy   =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_xyz(2) 
	  xz   =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_xyz(3) 
	  xocc =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_occ 
	  xbfa =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_bfa 
!	  
	  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_flag=.true.
	  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(1)=xx
	  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(2)=xy
	  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_xyz(3)=xz
	  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_occ=xocc
	  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(i)%at_bfa=xbfa
!	  
	       end if
	    end if
	  end do
        end do
!
	if (allocated(rt_ba)) then
	         aa_nrt=size(rt_ba)
	         do i=1,aa_nrt
	           deallocate(rt_ba(i)%rot_ats)	         
	         end do 
	         deallocate(rt_ba)
	end if
!
!
	END SUBROUTINE EXPAND_SINGLE_ROTAMER
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!@author  Yasemin Rudolph
!
	SUBROUTINE EXPAND_BACKRUB()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER (LEN=4) :: atnm, axis_atnm1, axis_atnm2
	LOGICAL :: flag, flag_point1, flag_point2
	LOGICAL, DIMENSION(2) :: present
	REAL, dimension(3) :: axis_point1, axis_point2, temp_vector, temp_vector_back
	REAL :: qrot, qrot_epsilon
	INTEGER, dimension(4) :: set
	INTEGER :: nexp, axis_nm1, axis_nm2
	INTEGER :: i,j,k,l, q, m, nns, nnt, nnq
	INTEGER :: m1, m2, m3, m4, nn
	INTEGER :: d
!
	if (.not.backrub_flag) then
	  return
	end if
!
!	FOR THE BACKBONE BACKRUB MOTION AT AA i IT IS IMPORTANT THAT AA i-1 and i+1 exist 
!	THIS AND THE POSSIBILITY OF CHAIN BREAKS IS CHECKED IN CALC_PHI_PSI AND CAN BE USED FOR THE
!	BACKRUB MOTION
!
!   THE CODE BELOW WORKS ALSO FOR NUCLEOTIDES
!
!   NOW FIGURING OUT WETHER BACKRUB MOTION CAN BE APPLIED OR NOT 
!
    do i=2,size(mol)
!
       if (mol(i)%res(1)%res_ntype.eq.2) then
!                                    for amino acids backrub can be calculated if phi and psi can be calculatd 
!                                    according to the current implementation
          mol(i)%res(1)%res_bbr=.false. 
          mol(i)%res(1)%res_pps=.false. 
          call calc_phi_psi(i)
          mol(i)%res(1)%res_bbr = mol(i)%res(1)%res_pps
!          
       else if (mol(i)%res(1)%res_ntype.eq.3) then
!
!                                     for nucleotides, check whether:
!                                       - the residues (i + br3_noff1) to (i + br3_noff2) are nucleotides
!                                       - nucleotides  (i + br3_noff1) to (i + br3_noff2) are connected 
!                                       - atom br3_atnm1 in residue (i + br3_noff1) exists
!                                       - atom br3_atnm2 in residue (i + br3_noff2) exists
!
         if (br3_noff1.gt.br3_noff2) then 
            PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
            PRINT*, '>>>>>>> br3_noff1 > br3_noff2 in  EXPAND_BACKRUB()     '
            PRINT*, '>>>>>>> ', br3_noff1, br3_noff2                  
            PRINT*, '>>>>>>> must stop                                      '
            PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
            PRINT*, '    '
            stop
         end if
!
         mol(i)%res(1)%res_bbr= .true.
         nn= mol(i)%res(1)%res_num
!         
         if ((nn + br3_noff1).lt.1) then 
            mol(i)%res(1)%res_bbr= .false.
            cycle
         end if
!         
         if ((nn + br3_noff2).gt.mol(1)%mol_nrs) then 
            mol(i)%res(1)%res_bbr= .false.
            cycle
         end if
!
         do j = (nn + br3_noff1), (nn + br3_noff2)
            if (mol(1)%res(j)%res_ntype.ne.3) then
              mol(i)%res(1)%res_bbr= .false.
              cycle
            end if
         end do
!
         if (br3_noff1.lt.br3_noff2) then  
            do j = (nn + br3_noff1), (nn + br3_noff2 -1)
               flag = .false.
               call check_nuc_break(j,j+1,flag)
               if (flag) then 
                  mol(i)%res(1)%res_bbr= .false.
                  cycle
               end if
            end do
         end if
!                      now checking whether the atoms atoms br3_atnm1 and br3_atnm2 exit 
!
         do q=1,2
           present(q)=.false.
           if (q==1) then 
              nn = mol(i)%res(1)%res_num  + br3_noff1
              atnm = br3_atnm1
           else     
              nn = mol(i)%res(1)%res_num  + br3_noff2
              atnm = br3_atnm2
           end if
!
           do j= 1,  mol(1)%res(nn)%res_aas(1)%aa_rots(1)%rot_nat
              if (mol(1)%res(nn)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag) then 
                 if (atnm == mol(1)%res(nn)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam) then
                    present(q)=.true.
                 end if
              end if
           end do   
         end do                
!
         if ((.not.present(1)).or.(.not.present(2))) then 
               mol(i)%res(1)%res_bbr= .false.
         end if      
!
!
       else                                  !  this covers all other mol(i)%res(1)%res_ntype...
            mol(i)%res(1)%res_bbr = .false.
	   end if
	end do
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY EXPAND_BACKRUB       #'
	PRINT*, '################################'
	PRINT*, '        '
!
	nexp = (2*mumbo_back_nstp)+1
!
! ROTAMERS ARE EXPANDED
!
	do i=2, mol_nmls
	 if (.not.mol(i)%res(1)%res_bbr) then
	 PRINT*,'   NO BACKRUB MOTION FOR RESIDUE',mol(i)%res(1)%res_nold, mol(i)%res(1)%res_chid 
	 else 
!       NOW GO OVER ALL RESIDUES (j=1) AND AMINOACIDS AND EXPAND THE OCCURING ROTAMERS
	    do j=1,mol(i)%mol_nrs
	     do k=1,mol(i)%res(j)%res_naa
	       set(1)=i  
	       set(2)=j
	       set(3)=k
	       set(4)=0
	       call expand_rotamers(set,nexp)
	     end do
	    end do
	 PRINT*,'   BACKRUB SUCCESSFUL',mol(i)%res(1)%res_nold, mol(i)%res(1)%res_chid 
	 end if
    end do 
!
!	NOW APPLY BACKRUB MOTION TO EXPANDED ROTAMERS
!
    DO i=2, mol_nmls
	    IF (mol(i)%res(1)%res_bbr) THEN
!
!  HERE IDENTIFY THE COORDS OF AXIS ROTATION POINTS
!
      m1=1
      m2=mol(i)%res(1)%res_num
      m3=1
      m4=1
!
      if (mol(i)%res(1)%res_ntype.eq.2) then
            axis_atnm1 =    br2_atnm1
            axis_nm1 = m2 + br2_noff1
            axis_atnm2 =    br2_atnm2
            axis_nm2 = m2 + br2_noff2
      else if (mol(i)%res(1)%res_ntype.eq.3) then
            axis_atnm1 =    br3_atnm1
            axis_nm1 = m2 + br3_noff1
            axis_atnm2 =    br3_atnm2
            axis_nm2 = m2 + br3_noff2 
      else 
           cycle       
      end if
!
      if ((axis_atnm1==axis_atnm2).and.(axis_nm1==axis_nm2)) then 
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
        PRINT*, '>>>>>>> axis_atnm1 and axis_atnm2 are identical in  EXPAND_BACKRUB() '
        PRINT*, '>>>>>>> ', axis_nm1, axis_atnm1, axis_nm2, axis_atnm2                  
        PRINT*, '>>>>>>> axis for applying backrub undefined, must stop '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>> '
        PRINT*, '    '
        stop
      end if
!
      axis_point1=0.0
      axis_point2=0.0
      flag_point1=.false.
      flag_point2=.false.
!
      loop_point1: DO q=1,mol(m1)%res(axis_nm1)%res_aas(m3)%aa_rots(m4)%rot_nat
      atnm=   mol(m1)%res(axis_nm1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam
      flag=   mol(m1)%res(axis_nm1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_flag
      IF (flag) THEN
        IF (atnm==axis_atnm1) THEN          ! axis_point1 located in residue (i + brx_noff1)
          axis_point1(1)=mol(m1)%res(axis_nm1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
          axis_point1(2)=mol(m1)%res(axis_nm1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
          axis_point1(3)=mol(m1)%res(axis_nm1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
          flag_point1=.true.
        END IF
      END IF
      END DO loop_point1
!
      loop_point2: DO q=1,mol(m1)%res(axis_nm2)%res_aas(m3)%aa_rots(m4)%rot_nat
      atnm=   mol(m1)%res(axis_nm2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam
      flag=   mol(m1)%res(axis_nm2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_flag
      IF (flag) THEN
        IF (atnm==axis_atnm2) THEN         ! axis_point2 located in residue (i + brx_noff2)
          axis_point2(1)=mol(m1)%res(axis_nm2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
          axis_point2(2)=mol(m1)%res(axis_nm2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
          axis_point2(3)=mol(m1)%res(axis_nm2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
          flag_point2=.true.
        END IF
      END IF
      END DO loop_point2
!
      if ((.not.flag_point1).or.(.not.flag_point2)) then 
      PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
      PRINT*, '>>>> COULD NOT LOCATE AXES POINTS IN                      '
      PRINT*, '>>>> EXPAND_BACKRUB(). IT IS UNCLEAR WHY                  '
      PRINT*, '>>>> MUST STOP                                            '
      PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
           stop
      end if
!
!
! INFO ABOUT THE ROTAMERS TO BE ROTATED
! GO OVER ALL AMINOACIDS AND ROTAMERS PRESENT AT THIS POSITION AND APPLY ROTATION TO ALL
! ATOMS
!
      DO j=1,mol(i)%mol_nrs
        DO k=1,mol(i)%res(j)%res_naa
          nns=(mol(i)%res(j)%res_aas(k)%aa_nrt)/nexp
          nnq=0
          DO l=1,nns
            DO m=1,nexp
              nnt=((l-1)*nexp)+m
              nnq=nnq+1
              qrot= (m-1)*(mumbo_back_ndeg) - (mumbo_back_nstp*mumbo_back_ndeg)
              mol(i)%res(1)%res_aas(k)%aa_rots(nnq)%rot_qrot = qrot
!
!              PRINT*, ' '
!              PRINT*,'LOOKING AT ROTAMER', nnt, nnq, 'WILL BE ROTATED BY', qrot
!
          DO d=1, mol(i)%res(1)%res_aas(k)%aa_rots(nnq)%rot_nat
                  temp_vector(1) = mol(i)%res(1)%res_aas(k)%aa_rots(nnq)%rot_ats(d)%at_xyz(1)
                  temp_vector(2) = mol(i)%res(1)%res_aas(k)%aa_rots(nnq)%rot_ats(d)%at_xyz(2)
                  temp_vector(3) = mol(i)%res(1)%res_aas(k)%aa_rots(nnq)%rot_ats(d)%at_xyz(3)
!
		  CALL backrub_calc(axis_point1, axis_point2, temp_vector, qrot, temp_vector_back)
!
             mol(i)%res(1)%res_aas(k)%aa_rots(nnq)%rot_ats(d)%at_xyz(1) = temp_vector_back(1)
             mol(i)%res(1)%res_aas(k)%aa_rots(nnq)%rot_ats(d)%at_xyz(2) = temp_vector_back(2)
             mol(i)%res(1)%res_aas(k)%aa_rots(nnq)%rot_ats(d)%at_xyz(3) = temp_vector_back(3)
          END DO
!
            END DO
          END DO
!
        END DO
      END DO
!
	    END IF
	  END DO
!
! DEBUG PURPOSES 
! DUMP ROTAMERS TO CHECK HOW THEY WERE ROTATED 
!	do i=2, mol_nmls
!	 if (mol(i)%res(1)%res_bbr) then 	 
!	   do j=1,mol(i)%mol_nrs
!	     do k=1,mol(i)%res(j)%res_naa
!	      do l=1,mol(i)%res(j)%res_aas(k)%aa_nrt 
!	     
!	  call dump_pdb(i,j,k,l)
!
!	      end do
!	     end do
!	   end do
!	 end if
!	end do
! DEBUG
!
!
		PRINT*, '        '
		PRINT*, '        '
!
	END SUBROUTINE EXPAND_BACKRUB
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC!
