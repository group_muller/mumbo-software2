!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE WRITE_FINAL()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
      USE MOLEC_M
      USE MUMBO_DATA_M
      USE MAX_DATA_M
      USE WATER_DATA
      USE MOLECULE_SIMP_DATA_M
! 
      IMPLICIT NONE
       
      CHARACTER (LEN=1) :: chid, hc
      CHARACTER (LEN=4) :: anam, name
      REAL :: bfa, xboff
      INTEGER :: i, j, k, l, m, n
      INTEGER :: nnrs,  nat, nbfa, n1, n2 , n3, n4, nct, nres
      INTEGER :: m1, m2, m3, m4, m5, mo1, mo2, mo3, mo4
!
      CHARACTER (LEN=132) :: string
      CHARACTER (LEN=80) ::  word(40), baff
      INTEGER :: nnchar, nw
!
      LOGICAL :: water_present, solvent_present, first
!
      INTEGER :: ntotres, nmain, ntot, nlig, nn, nrs, nmls, noff, nprot, nsolv
!
      CHARACTER(LEN=4)   :: atnm
      REAL               :: xx, xy, xz, xbfa, xocc
      LOGICAL            :: flag
!
      LOGICAL            :: cif_flag, pdb_flag
      CHARACTER(LEN=4)   :: ext
      CHARACTER(LEN=132) :: filenm
!      
!
!      first read in the substitution tables in order to change for example HIS HID HIE back to HIS
!      in the final output-file (At present only the HIS HID HIE change back to HIS is implemented further 
!      below and makes sense (mumbosub(1) is checked). 
!      Further back-substitutions can be easily added if for example different protonations of ASP and GLU
!      are to be implemented)
!
      if (allocated(mumbosub)) deallocate(mumbosub)
      allocate(mumbosub(size(subs_instr)))
      do i=1,size(mumbosub)
            do j=1,132
               string(j:j)=' '
            end do
            nnchar=len(subs_instr(i))
            string(1:nnchar)=subs_instr(i)
            call parser(string, word, nw)
            call maxwords(word,nw)
!        
            baff=word(1)
            mumbosub(i)%mut_key=baff(1:3)
            allocate(mumbosub(i)%mut_rep(nw-2))
            do j=1,nw-2
                 baff=word(j+2)
                 mumbosub(i)%mut_rep(j)=baff(1:3)
            end do
      end do
!
!
!     now altering some properties in the final rotamers for later reporting:
!                                - substituting HID and HIE back to HIS
!                                - setting at_flag = .false. for some of the hydrogenes as requested 
!
      do i=1,  mol(1)%mol_nrs
        nnrs=  mol(1)%res(i)%res_nold
        chid=  mol(1)%res(i)%res_chid
!        
!
        if (mol(1)%res(i)%res_mut) then
!
!         identifying the amino acid and rotamer to be written out
!
          do j=2, size(mol)
!
            if ((nnrs==mol(j)%res(1)%res_nold).and.(chid == mol(j)%res(1)%res_chid)) then
!         
              do k= 1, mol(j)%res(1)%res_naa
                   do l = 1, mol(j)%res(1)%res_aas(k)%aa_nrt
                       if  (mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_flag2) then
                           anam = mol(j)%res(1)%res_aas(k)%aa_nam
!                          
!     substituting some of the amino acid names back. At present only HIS HID HIE substitution back to HIS makes sense..
!     checking mumbosub(1) only... 
!
                           do m=1,1
                              do n=1,size(mumbosub(m)%mut_rep)
                                if (anam(1:3).eq.mumbosub(m)%mut_rep(n)) then
                                    anam(1:3) = mumbosub(m)%mut_key
                                end if 
                              end do
                           end do
                           mol(j)%res(1)%res_aas(k)%aa_nam = '    '
                           mol(j)%res(1)%res_aas(k)%aa_nam = anam(1:3)
!
!
!     deleting some of the hydrogenes if required: mumbo_out_hyd2=.true. ==> only polar hydrogens are reported
!                                                  mumbo_out_hyd3=.true. ==> no hydrogens reported at all
!
                           if (mumbo_out_hyd2) then  
                              do m=1,    mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_nat
                                 name =  mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam
                                 hc   =  mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_hb
                                 if (name(1:1)=='H'.and.hc=='-') then
                                    mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_flag = .false.
                                 end if
                              end do   
                           end if
!
                          if (mumbo_out_hyd3) then  
                              do m=1,    mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_nat
                                 name =  mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam
                                 hc   =  mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_hb
                                 if (name(1:1)=='H') then
                                    mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_flag = .false.
                                 end if
                              end do   
                           end if
!
!
                       end if       
                   end do        
               end do
            end if
          end do     
        end if       
      end do          
!
!
!     flagging hydrogens in the constant part of the molecule as requested (HALL, HPOLAR, HNO) in output
!
      if (mumbo_out_hyd2.or.mumbo_out_hyd3) then 
!
        do i=1,  mol(1)%mol_nrs
          nmain = size(mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats)
          do j=1, nmain
            name =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam   
            hc   =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_hb
!         
            if (mumbo_out_hyd2.and.(name(1:1)=='H').and.(hc=='-')) then
                mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag = .false.
            else if (mumbo_out_hyd3.and.(name(1:1)=='H')) then 
                mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag = .false.
            end if
!         
          end do
        end do 
!
      end if
!
!
!     generating the sim structure/object for subsequently saving the  final strucure 
!
!     first finding out how many residues are there in total 
!     in order to allocate 'sim(1)&mol_res'
!            (protein residues + ligands + water molecules) 
!
!
      first=.true.
      water_present = .false.
      solvent_present= .false.
!
      nnrs = 0
      chid = ' '
      n1 = 0
      n2 = 0
      n3 = 0
      n4 = 0     
!
      do i=1,  mol(1)%mol_nrs
        nnrs=  mol(1)%res(i)%res_nold
        chid=  mol(1)%res(i)%res_chid
!
        if (mol(1)%res(i)%res_mut) then
!
!       now locating the rotamer
!
          do j=2, size(mol)
!
            if ((nnrs==mol(j)%res(1)%res_nold).and.(chid == mol(j)%res(1)%res_chid)) then
!         
              do k= 1, mol(j)%res(1)%res_naa
                   do l = 1, mol(j)%res(1)%res_aas(k)%aa_nrt
                       if (mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_flag2) then
                          anam = mol(j)%res(1)%res_aas(k)%aa_nam
                          n1 = j
                          n2 = 1
                          n3 = k
                          n4 = l
                        end if
                   end do
              end do                    
!
!
!             looking up water molecules and backing these up
!
              water_present = .false.
              do k = 1,size(ASWNAMES)
               if (anam.eq.ASWNAMES(k)) then
                    anam = ASNAMES(k)
                    mol(n1)%res(n2)%res_aas(n3)%aa_nam  = '    '
                    mol(n1)%res(n2)%res_aas(n3)%aa_nam  = anam(1:3) 
                    water_present   = .true.
                    solvent_present = .true.
                    exit
               end if 
              end do
              if (water_present) then 
                do k= 1 , mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat
                   name = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(k)%at_nam
                   if (name.eq.WOx_name.or.name.eq.WH1_name.or.name.eq.WH2_name) then 
                     call backup_water(n1,n2,n3,n4,k,first)
                     first=.false.
                     mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(k)%at_flag = .false.
                     cycle
                   end if 
                end do
              end if   
!
            end if
         end do   
       end if
     end do
!
!    now looking any ligand that may be present      
!
     if (mumbo_lig_flag) then 
!     
        nmls = size(mol)
!     
!        nnrs=  mol(nmls)%res(1)%res_nold
!        chid=  mol(nmls)%res(1)%res_chid
!      
!       now checking whether a hydrated ligand is present and substitute name back to unsolvated ligand
!
!
        do i = 1, mol(nmls)%res(1)%res_naa
          do j = 1, mol(nmls)%res(1)%res_aas(i)%aa_nrt
            if (mol(nmls)%res(1)%res_aas(i)%aa_rots(j)%rot_flag2) then
              anam =  mol(nmls)%res(1)%res_aas(i)%aa_nam
              n1 = nmls
              n2 = 1
              n3 = i
              n4 = j
            end if
          end do
        end do
!
        water_present = .false.
        do i = 1,size(ASWNAMES)
           if (anam.eq.ASWNAMES(i)) then
                anam = ASNAMES(i)
                water_present = .true.
                solvent_present =.true.
                mol(n1)%res(n2)%res_aas(n3)%aa_nam = '    '
                mol(n1)%res(n2)%res_aas(n3)%aa_nam  = anam(1:3) 
                exit
           end if      
        end do
!
        if (water_present) then 
           do i= 1 , mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat
              name = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_nam
              if (name.eq.WOx_name.or.name.eq.WH1_name.or.name.eq.WH2_name) then 
                call backup_water(n1,n2,n3,n4,i,first)
                first=.false.
                mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_flag = .false.
                cycle
              end if 
           end do
         end if   
!
     end if
!
!    counting the number of solvent molecules to be added
!
     nsolv = 0
     if (solvent_present) then 
        do i= 1, size(DATAnamepos)
           if (DATAnamepos(i) == WOx_name) then
              nsolv = nsolv +1
           end if
        end do 
     end if
!
!
!    now creating sim-structure
!    ntotres = mol(1)%mol_nrs + ligand + number of water molecules....
!
     ntotres = mol(1)%mol_nrs
     nprot   = ntotres
     if (mumbo_lig_flag) then
        ntotres =  ntotres + 1
     end if
     if (solvent_present) then
        nprot   = ntotres
        ntotres = ntotres + nsolv
     end if
!
!
     if (allocated(sim)) deallocate(sim)
     allocate(sim(1))
     allocate(sim(1)%mol_res(ntotres))  
!     
     sim(1)%mol_nrs            = ntotres
     sim(1)%mol_flag           =.true.     
     sim(1)%mol_reference_flag =.false.     
!
     ntot = 0
     n1 = 0
     n2 = 0
     n3 = 0
     n4 = 0     
!
!    now feeding the protein part into the sim structure 
!
     do i=1, size(mol(1)%res)
       nnrs= mol(1)%res(i)%res_nold
       chid= mol(1)%res(i)%res_chid 
       sim(1)%mol_res(i)%res_nold = nnrs 
       sim(1)%mol_res(i)%res_chid = chid 
       sim(1)%mol_res(i)%res_flag = .true.                 
!                              
!      below: transferring the mumboed positions into the sim structure/object 
!
       if (mol(1)%res(i)%res_mut) then 
!               
         nmain = size(mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats)
         do j=2, size(mol) 
              if (nnrs  == mol(j)%res(1)%res_nold .and. chid == mol(j)%res(1)%res_chid ) then
                do k= 1,      mol(j)%res(1)%res_naa
                     do l = 1,mol(j)%res(1)%res_aas(k)%aa_nrt
                         if (mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_flag2) then
                           anam = mol(j)%res(1)%res_aas(k)%aa_nam
                           ntot = nmain + size(mol(j)%res(1)%res_aas(k)%aa_rots(l)%rot_ats)  
                           n1 = j
                           n2 = 1
                           n3 = k
                           n4 = l
                           exit
                         end if
                     end do
                end do                    
              end if
         end do 
!    
!              print*, nnrs, chid, ' SET= ', n1, n2, n3, n4
!
         allocate(sim(1)%mol_res(i)%res_ats(ntot))
         sim(1)%mol_res(i)%res_nats = ntot        
         sim(1)%mol_res(i)%res_name = anam
!         
!                copying the main chain atoms of mumboed residues into the sim structure 
!
         nmain = size(mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats)
         do j=1, nmain
           atnm =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam   
           xx   =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1)
           xy   =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2)
           xz   =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3)
           xocc =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ   
           xbfa =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa   
           flag =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag  
!          
           sim(1)%mol_res(i)%res_ats(j)%at_name   =  atnm   
           sim(1)%mol_res(i)%res_ats(j)%at_xyz(1) =  xx     
           sim(1)%mol_res(i)%res_ats(j)%at_xyz(2) =  xy     
           sim(1)%mol_res(i)%res_ats(j)%at_xyz(3) =  xz     
           sim(1)%mol_res(i)%res_ats(j)%at_occ    =  xocc   
           sim(1)%mol_res(i)%res_ats(j)%at_bfa    =  xbfa   
           sim(1)%mol_res(i)%res_ats(j)%at_flag   =  flag 
!           
           sim(1)%mol_res(i)%res_ats(j)%at_alt    =  ' '
!           
         end do
!                copying the rotamer atoms of mumboed residues into the sim structure 
!
         do j=1, size(mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats)
           atnm = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_nam   
           xx   = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_xyz(1)
           xy   = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_xyz(2)
           xz   = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_xyz(3)
           xocc = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_occ   
           xbfa = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_bfa   
           flag = mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(j)%at_flag   
!          
           sim(1)%mol_res(i)%res_ats(j+nmain)%at_name   = atnm   
           sim(1)%mol_res(i)%res_ats(j+nmain)%at_xyz(1) = xx     
           sim(1)%mol_res(i)%res_ats(j+nmain)%at_xyz(2) = xy     
           sim(1)%mol_res(i)%res_ats(j+nmain)%at_xyz(3) = xz     
           sim(1)%mol_res(i)%res_ats(j+nmain)%at_occ    = xocc   
           sim(1)%mol_res(i)%res_ats(j+nmain)%at_bfa    = xbfa   
           sim(1)%mol_res(i)%res_ats(j+nmain)%at_flag   = flag  
!           
           sim(1)%mol_res(i)%res_ats(j+nmain)%at_alt    = ' '
!           
         end do
!
!        below: transferring the unchanged/constant protein parts into the sim structure
!
       else
!         
         nmain = size(mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats)
         allocate(sim(1)%mol_res(i)%res_ats(nmain))
         sim(1)%mol_res(i)%res_nats = nmain       
         sim(1)%mol_res(i)%res_name = mol(1)%res(i)%res_aas(1)%aa_nam
!         
!        
         do j=1, nmain
           atnm =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_nam   
           xx   =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(1)
           xy   =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(2)
           xz   =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_xyz(3)
           xocc =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_occ   
           xbfa =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_bfa   
           flag =  mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(j)%at_flag
!          
           sim(1)%mol_res(i)%res_ats(j)%at_name   =  atnm   
           sim(1)%mol_res(i)%res_ats(j)%at_xyz(1) =  xx     
           sim(1)%mol_res(i)%res_ats(j)%at_xyz(2) =  xy     
           sim(1)%mol_res(i)%res_ats(j)%at_xyz(3) =  xz     
           sim(1)%mol_res(i)%res_ats(j)%at_occ    =  xocc   
           sim(1)%mol_res(i)%res_ats(j)%at_bfa    =  xbfa   
           sim(1)%mol_res(i)%res_ats(j)%at_flag   =  flag  
!           
           sim(1)%mol_res(i)%res_ats(j)%at_alt     = ' '
!           
         end do
!
       end if    
!         
     end do
!
!    ligand has still to be added
!
     if (mumbo_lig_flag) then
!
       n1 = 0
       n2 = 0
       n3 = 0
       n4 = 0     
!      
       noff = size(mol(1)%res)
       nmls = size(mol)
       noff = noff+1
       nnrs = mol(nmls)%res(1)%res_nold
       chid = mol(nmls)%res(1)%res_chid
!
       sim(1)%mol_res(noff)%res_nold = nnrs 
       sim(1)%mol_res(noff)%res_chid = chid 
       sim(1)%mol_res(noff)%res_flag = .true.                 
!
       do i = 1,     mol(nmls)%res(1)%res_naa
         do j = 1,   mol(nmls)%res(1)%res_aas(i)%aa_nrt
           if       (mol(nmls)%res(1)%res_aas(i)%aa_rots(j)%rot_flag2) then
             anam =  mol(nmls)%res(1)%res_aas(i)%aa_nam
             n1 = nmls
             n2 = 1
             n3 = i
             n4 = j
           end if
         end do
       end do
!
       nlig = size(mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats)
       allocate(sim(1)%mol_res(noff)%res_ats(nlig))
!       
       sim(1)%mol_res(noff)%res_nats = nlig        
       sim(1)%mol_res(noff)%res_name = anam 
!
       do i = 1, nlig
         atnm =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_nam   
         xx   =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_xyz(1)
         xy   =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_xyz(2)
         xz   =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_xyz(3)
         xocc =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_occ   
         xbfa =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_bfa   
         flag =  mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(i)%at_flag  
!        
         sim(1)%mol_res(noff)%res_ats(i)%at_name   =  atnm   
         sim(1)%mol_res(noff)%res_ats(i)%at_xyz(1) =  xx     
         sim(1)%mol_res(noff)%res_ats(i)%at_xyz(2) =  xy     
         sim(1)%mol_res(noff)%res_ats(i)%at_xyz(3) =  xz     
         sim(1)%mol_res(noff)%res_ats(i)%at_occ    =  xocc   
         sim(1)%mol_res(noff)%res_ats(i)%at_bfa    =  xbfa   
         sim(1)%mol_res(noff)%res_ats(i)%at_flag   =  flag  
!         
         sim(1)%mol_res(noff)%res_ats(i)%at_alt    =  ' '
!           
       end do
!
     end if
!
!    water molecules are now added to the sim structure/object  
!
     if (solvent_present) then
!
       mo1   = 0
       mo2   = 0
       mo3   = 0
       mo4   = 0
       nsolv = 0  
       nat   = 0
       first = .true.
!      
       do i=1, size(DATAnamepos)
            m1 = DATAintpos(i,1)
            m2 = DATAintpos(i,2)
            m3 = DATAintpos(i,3)
            m4 = DATAintpos(i,4)
            if (first) then 
              mo1 = m1
              mo2 = m2
              mo3 = m3
              mo4 = m4
              first = .false.
              nsolv = 1
              nat = nat + 1
              cycle
            end if
            if ((m1==mo1).and.(m2==mo2).and.(m3==mo3).and.(m4==mo4)) then 
               nat = nat + 1
            else 
              allocate (sim(1)%mol_res(nsolv+nprot)%res_ats(nat))
              sim(1)%mol_res(nsolv+nprot)%res_nats = nat
              mo1 = m1
              mo2 = m2
              mo3 = m3
              mo4 = m4
              nat = 1
              nsolv = nsolv +1
            end if 
       end do     
       allocate (sim(1)%mol_res(nsolv+nprot)%res_ats(nat))
       sim(1)%mol_res(nsolv+nprot)%res_nats = nat
!      
!          now adding the actual water coordinates 
!      
       nct =  0
       nres = 0
       do i = (nprot+1), sim(1)%mol_nrs
         nres = nres + 1
         sim(1)%mol_res(i)%res_name = WHOH_name_out
         sim(1)%mol_res(i)%res_nold = nres
         sim(1)%mol_res(i)%res_chid = WchainID_out
         sim(1)%mol_res(i)%res_flag = .true.
!         
         do j = 1, sim(1)%mol_res(i)%res_nats
           nct = nct + 1
           m1 = DATAintpos(nct,1)
           m2 = DATAintpos(nct,2)
           m3 = DATAintpos(nct,3)
           m4 = DATAintpos(nct,4)
           m5 = DATAintpos(nct,5)
!          
           atnm =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(m5)%at_nam   
           xx   =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(m5)%at_xyz(1)
           xy   =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(m5)%at_xyz(2)
           xz   =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(m5)%at_xyz(3)
           xocc =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(m5)%at_occ   
           xbfa =  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(m5)%at_bfa   
           flag =  DATAflagpos(nct)  
!
           if (atnm==WOx_name) then 
               atnm = WOx_name_out
           else if ((atnm==WH1_name).or.(atnm==WH2_name)) then       
               atnm = WH_name_out
           end if    
!
           sim(1)%mol_res(i)%res_ats(j)%at_name   =  atnm   
           sim(1)%mol_res(i)%res_ats(j)%at_xyz(1) =  xx     
           sim(1)%mol_res(i)%res_ats(j)%at_xyz(2) =  xy     
           sim(1)%mol_res(i)%res_ats(j)%at_xyz(3) =  xz     
           sim(1)%mol_res(i)%res_ats(j)%at_occ    =  xocc   
           sim(1)%mol_res(i)%res_ats(j)%at_bfa    =  xbfa   
           sim(1)%mol_res(i)%res_ats(j)%at_flag   =  flag  
!                         
           sim(1)%mol_res(i)%res_ats(j)%at_alt    =  ' '
         end do 
       end do   
!
     end if
!
!    Making a few final changes to the sim structure 
!
     do i=1, size(sim(1)%mol_res)
        anam = sim(1)%mol_res(i)%res_name
        do j=1, size(sim(1)%mol_res(i)%res_ats)
!
!       setting occupancies and bfactors to a constant value
!
           sim(1)%mol_res(i)%res_ats(j)%at_occ  =  1.00
           sim(1)%mol_res(i)%res_ats(j)%at_bfa  = 30.00
!           
!       here removing peptide 'H   ' when anam == pro_name
!
           name = sim(1)%mol_res(i)%res_ats(j)%at_name
           if ((name=='H   ').and.(anam==pro_name)) then
               sim(1)%mol_res(i)%res_ats(j)%at_flag = .false.
           end if
!           
        end do
     end do   
!
!!!    DEBUG
!     print*, size(sim(1)%mol_res), sim(1)%mol_nrs
!     do i=1, size(sim(1)%mol_res)
!        print*, sim(1)%mol_res(i)%res_nold, sim(1)%mol_res(i)%res_chid,' ',sim(1)%mol_res(i)%res_name
!        print*, size(sim(1)%mol_res(i)%res_ats), sim(1)%mol_res(i)%res_nats,'##',sim(1)%mol_res(i)%res_flag
!        do j=1, size(sim(1)%mol_res(i)%res_ats)
!!        
!            atnm  = sim(1)%mol_res(i)%res_ats(j)%at_name     
!            xx    = sim(1)%mol_res(i)%res_ats(j)%at_xyz(1)   
!            xy    = sim(1)%mol_res(i)%res_ats(j)%at_xyz(2)   
!            xz    = sim(1)%mol_res(i)%res_ats(j)%at_xyz(3)   
!            xocc  = sim(1)%mol_res(i)%res_ats(j)%at_occ      
!            xbfa  = sim(1)%mol_res(i)%res_ats(j)%at_bfa      
!            flag  = sim(1)%mol_res(i)%res_ats(j)%at_flag
!            chid  = sim(1)%mol_res(i)%res_ats(j)%at_alt
!!        
!            print*, i, j, atnm, xx, xy, xz, xocc, xbfa, flag, '###',chid,'###'
!!        
!        end do
!     end do
!!    END DEBUG
!
!    Writing everything out...
!
     nn = 1 
     do i=1,132
       filenm(i:i) = ' '
     end do
     filenm(1:len_trim(mumbo_outpdb)) = mumbo_outpdb(1:len_trim(mumbo_outpdb))
!    
     cif_flag = .false.
     pdb_flag = .false.
     ext = filenm((len_trim(filenm)-3):len_trim(filenm))
     if ((ext.eq.'.pdb').or.(ext.eq.'.PDB')) then  
!         print*,'  '
         print*,'  OUTPDB/OUTFILE IS ASSUMED TO BE A ** PDB ** FILE '
         print*,'  ', filenm(1:len_trim(filenm))
         print*,'  '
         cif_flag = .false.
         pdb_flag = .true.
     else if ((ext.eq.'.cif').or.(ext.eq.'.CIF')) then
!         print*,'  '
         print*,'  OUTPDB/OUTFILE IS ASSUMED TO BE A ** CIF ** FILE '
         print*,'  ', filenm(1:len_trim(filenm))
         print*,'  '
         cif_flag = .true.
         pdb_flag = .false.
     else
        print*, '  NOT CLEAR WHETHER OUTPDB/OUTFILE IS MEANT TO BE PDB OR CIF-FILE '
        print*, '  ', filenm(1:len_trim(filenm)) 
        print*, '  '  
        stop
     end if   
!    
     if (cif_flag) then 
         call cif_write_coords(filenm,nn)
     end if
!     
     if (pdb_flag) then 
         call write_simp_pdb(filenm,nn)
     end if
!     
     if (allocated(sim)) deallocate(sim)
!              
     PRINT*, '        '
     PRINT*, '################################'
     PRINT*, '# SUMMARY WRITE_FINAL_STRUCT   #'
     PRINT*, '################################'
     PRINT*, '        '
!    
        if (solvent_present) then
     PRINT*, '  NUMBER OF WATER MOLECULES MODELLED= ', nsolv
     PRINT*, '        '
        end if
!    
     PRINT*, '  GMEC - STRUCTURE WRITTEN TO FILE: ', mumbo_outpdb(1:len_trim(mumbo_outpdb))
     PRINT*, '                  '
!    
!    
     return
!
    END SUBROUTINE WRITE_FINAL
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE WRITE_FINAL_OLD()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE MAX_DATA_M
        USE WATER_DATA
!
	IMPLICIT NONE
!
	CHARACTER (LEN=1) :: achid
	CHARACTER (LEN=4) :: anam, name
	REAL :: bfa, xboff
	INTEGER :: j,m,n,o
	INTEGER :: nnrs,  nat, nbfa, n1, n2 , n3, n4
!
	CHARACTER (LEN=132) :: string
	CHARACTER (LEN=80) ::  word(40), baff
	INTEGER :: i, nnchar, nw
!
        LOGICAL :: water_present, solvent_present, first
!
!      first read in the substitution tables in order to change for example HIS HJS HKS back to HIS
!      in the final output-file (At present only the HIS HJS HKS change back to HIS is implemented further 
!      below and makes sense (mumbosub(1) is checked). 
!      Further back-substitutions can be easily added if for example different protonations of ASP and GLU
!      are to be implemented)
!
        if (allocated(mumbosub)) deallocate(mumbosub)
        allocate(mumbosub(size(subs_instr)))
        do i=1,size(mumbosub)
            do j=1,132
               string(j:j)=' '
            end do
            nnchar=len(subs_instr(i))
            string(1:nnchar)=subs_instr(i)
	    call parser(string, word, nw)
	    call maxwords(word,nw)
!	        
	    baff=word(1)
	    mumbosub(i)%mut_key=baff(1:3)
	    allocate(mumbosub(i)%mut_rep(nw-2))
            do j=1,nw-2
                    baff=word(j+2)
                    mumbosub(i)%mut_rep(j)=baff(1:3)
              end do
        end do
!
        first=.true.
        water_present = .false.
        solvent_present= .false.
!
!       now start dealing with the output
!
!       GFORTRAN
	n4=-1000
	n3=-1000
	n2=-1000
	n1=-1000
!       GFORTRAN	
!
	open(15,file=mumbo_outpdb,form='formatted',status='unknown')
!
	nat=0
	do j=1, mol(1)%mol_nrs
	    nnrs=  mol(1)%res(j)%res_nold
	    achid= mol(1)%res(j)%res_chid
!
	    if (mol(1)%res(j)%res_mut) then
!
!        now locating the rotamer
!
		do m=2, size(mol)
		 if (nnrs  == mol(m)%res(1)%res_nold) then
		   if (achid == mol(m)%res(1)%res_chid) then
!
		     do n= 1,mol(m)%res(1)%res_naa
		        do o= 1,mol(m)%res(1)%res_aas(n)%aa_nrt
			  if (mol(m)%res(1)%res_aas(n)%aa_rots(o)%rot_flag2) then
			     anam = mol(m)%res(1)%res_aas(n)%aa_nam
			     n1 = m
			     n2 = 1
			     n3 = n
			     n4 = o
			  end if
		        end do
		     end do
!
	           end if
		  end if
		end do
!
	        bfa = 0
	        nbfa = 0
!
!
!       now checking whether a hydrated amino acid is present and substitute name back to unsolvated AA
!
              water_present = .false.
!
                do i=1,size(ASWNAMES)
                   if (anam.eq.ASWNAMES(i)) then
                        anam = ASNAMES(i)
                        water_present = .true.
                        solvent_present =.true.
                        exit
                   end if 
                end do
!
!       substituting back the amino acid names.. at present only HIS HJS HKS substitution back to HIS makes sense..
!       checking mumbosub(1) only... 
!
                do i=1,1
                   do m=1,size(mumbosub(i)%mut_rep)
                     if (anam(1:3).eq.mumbosub(i)%mut_rep(m)) then
                         anam(1:3) = mumbosub(i)%mut_key
                     end if 
                   end do
                end do 
!
!
!	FIRST WRITE OUT MAIN CHAIN ATOMS (STORED IN mol(1)) BEFORE ADDING ROTAMER
!	
!
	  do m=1,mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_nat
          nat = nat+1
	      nbfa = nbfa+1
!	
	      xboff = mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_occ
	      name = mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam
!
	      if (name(1:1)=='H') then
	              if (.not.mumbo_out_hyd) then
		            nat = nat-1
		            nbfa = nbfa-1
		            cycle
	              end if
	      end if
!                                                          omitting NH hydrogen if amino acid 
!                                                          selected at this position is a proline       
	      if ((name=='H   ').and.(anam==pro_name)) then
		            nat = nat-1
		            nbfa = nbfa-1
		            cycle
	      end if
!
	      bfa = bfa + mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
!
	      if (name(4:4)==' ') then
!
                write(15,fmt=1010)                                     &
    &	'ATOM  ',                                                      &
    &      nat,                                                        &
    &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam,      &
    &      anam,                                                       &
    &      achid,                                                      &
    &      nnrs,                                                       &
    &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1),   &
    &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2),   &
    &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3),   &
    &      xboff,                                                      &
    &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
!
	      else if (name(4:4)/=' ') then
!
                write(15,fmt=1011)                                      &
     &	'ATOM  ',                                                       &
     &      nat,                                                        &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam,      &
     &      anam,                                                       &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1),   &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2),   &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                      &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
!
	      end if
	  end do
!
	  bfa = bfa/nbfa
!
!	NOW WRITE OUT ROTAMER PART OF AMINO ACID
!
	  do m=1,mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat
	    nat = nat+1
!	
	    xboff = 1.0 
	    name=mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam
!
!       check whether atom belongs to a water molecule. If yes, then back the water molecule up and cycle
!
            if (water_present) then 
             if (name.eq.WOx_name.or.name.eq.WH1_name.or.name.eq.WH2_name) then 
               call backup_water_old(n1,n2,n3,n4,m,xboff,bfa,first)
               first=.false.
               nat = nat-1
               cycle
             end if 
            end if 
!
	    if (name(1:1)=='H') then
	     if (.not.mumbo_out_hyd) then 
		nat = nat-1
		cycle
	     end if
	    end if
!
	    if (name(4:4)==' ') then
!
                  write(15,fmt=1010)                                        &
     &	'ATOM  ',                                                           &
     &      nat,                                                            &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam,      &
     &      anam,                                                           &
     &      achid,                                                          &
     &      nnrs,                                                           &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                          &
     &      bfa
!
	    else if (name(4:4)/=' ') then
!
                  write(15,fmt=1011)                                        &
     &	'ATOM  ',                                                           &
     &      nat,                                                            &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam,      &
     &      anam,                                                           &
     &      achid,                                                          &
     &      nnrs,                                                           &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                          &
     &      bfa
!
	    end if
	  end do
!
	  else
!
!	IF NOT MUTATED THEN KEEP ORIGINAL STRUCTURE (no need to check for attached waters)
!
	  do m=1, mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_nat
	       nat = nat+1
	       xboff = mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_occ
	       name=   mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam
!
	       if (name(1:1)=='H') then
	          if (.not.mumbo_out_hyd) then 
		       nat = nat-1
		       cycle
	          end if
	       end if
!
	       if (name(4:4)==' ') then
!
                  write(15,fmt=1010)                                    &
     &	'ATOM  ',                                                       &
     &      nat,                                                        &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam,      &
     &      mol(1)%res(j)%res_aas(1)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1),   &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2),   &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                      &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
!
	       else 
!
                  write(15,fmt=1011)                                    &
     &	'ATOM  ',                                                       &
     &      nat,                                                        &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_nam,      &
     &      mol(1)%res(j)%res_aas(1)%aa_nam,                            &
     &      achid,                                                      &
     &      nnrs,                                                       &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(1),   &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(2),   &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                      &
     &      mol(1)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(m)%at_bfa
!
	       end if
	   end do
!
	   end if
	end do
!
	if (mumbo_lig_flag) then 
!
	  nnrs=  mol(mol_nmls)%res(1)%res_nold
	  achid= mol(mol_nmls)%res(1)%res_chid
!
!       now checking whether a hydrated liagnd is present and substitute name back to unsolvated ligand
!
          water_present = .false.
          do i=1,size(ASWNAMES)
                if (anam.eq.ASWNAMES(i)) then
                     anam = ASNAMES(i)
                     water_present = .true.
                     solvent_present =.true.
                exit
                end if 
          end do
!
	  do o= 1, mol(mol_nmls)%res(1)%res_aas(1)%aa_nrt
	        if (mol(mol_nmls)%res(1)%res_aas(1)%aa_rots(o)%rot_flag2) then
		    anam = mol(mol_nmls)%res(1)%res_aas(1)%aa_nam
		    n1 = mol_nmls
		    n2 = 1
		    n3 = 1
		    n4 = o
		end if
	  end do
!
	  do m=1,mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_nat
	    nat = nat+1
!	
	    xboff = 1.0 
	    name=mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam
!
!       check whether atom belongs to a water molecule. If yes, then back the water molecule up and cycle
!
            if (water_present) then 
              if (name.eq.WOx_name.or.name.eq.WH1_name.or.name.eq.WH2_name) then 
               call backup_water_old(n1,n2,n3,n4,m,xboff,bfa,first)
               first=.false.
               nat = nat-1
               cycle
              end if 
            end if 
!
	    if (name(1:1)=='H') then
	      if (.not.mumbo_out_hyd) then
		nat = nat-1
		cycle
	      end if
	    end if
!
!       bfa is not really defined for the ligand and is somwhat taken over from the protein...
!       in order to avoid any confusion, bfa is overwritten here
        bfa = 20.0
!
	    if (name(4:4)==' ') then
!
               write(15,fmt=1010)                                           &
     &	'HETATM',                                                           &
     &      nat,                                                            &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam,      &
     &      anam,                                                           &
     &      achid,                                                          &
     &      nnrs,                                                           &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                          &
     &      bfa
!
	    else if (name(4:4)/=' ') then
!
               write(15,fmt=1011)                                           &
     &	'HETATM',                                                           &
     &      nat,                                                            &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_nam,      &
     &      anam,                                                           &
     &      achid,                                                          &
     &      nnrs,                                                           &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(1),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(2),   &
     &      mol(n1)%res(n2)%res_aas(n3)%aa_rots(n4)%rot_ats(m)%at_xyz(3),   &
     &      xboff,                                                          &
     &      bfa
!
	    end if
	  end do
!
	end if
!
!       now appending all water molecules to the output-file
!
        if (solvent_present) then
          nnrs =  0
          achid = WchainID_out
          anam =  WHOH_name_out
!
          do i=1,size(DATAnamepos) 
!
!         nnrs incremented assuming that water molecules are always encoded by 3 atoms... 
!
            if (MOD(i,3).eq.1) then
              nnrs = nnrs+1
            end if 
!
            nat = nat+1
            name = DATAnamepos(i)
!
	    if (name(1:1)=='H') then
	       if (.not.mumbo_out_hyd) then
		   nat = nat-1
		   cycle
	       end if
	    end if
!
            if (name.eq.WOx_name) then 
               name=WOx_name_out
            else if (name.eq.WH1_name.or.name.eq.WH2_name) then 
               name=WH_name_out  
            end if
!
!           in order to prevent any confusion regarding strange b-factors and occupancies..
!           DATAcordpos(i,4) and DATAcordpos(i,5) are overwritten..
!
            DATAcordpos(i,4)=  1.0
            DATAcordpos(i,5)= 20.0
!
	    if (name(4:4)==' ') then
!
               write(15,fmt=1010)                                           &
     &	'HETATM',                                                           &
     &      nat,                                                            &
     &      name,                                                           &
     &      anam,                                                           &
     &      achid,                                                          &
     &      nnrs,                                                           &
     &      DATAcordpos(i,1),                                               &
     &      DATAcordpos(i,2),                                               &
     &      DATAcordpos(i,3),                                               &
     &      DATAcordpos(i,4),                                               &
     &      DATAcordpos(i,5)
!
	    else if (name(4:4)/=' ') then
!
               write(15,fmt=1011)                                           &
     &	'HETATM',                                                           &
     &      nat,                                                            &
     &      name,                                                           &
     &      anam,                                                           &
     &      achid,                                                          &
     &      nnrs,                                                           &
     &      DATAcordpos(i,1),                                               &
     &      DATAcordpos(i,2),                                               &
     &      DATAcordpos(i,3),                                               &
     &      DATAcordpos(i,4),                                               &
     &      DATAcordpos(i,5)
!
	    end if
!
          end do
        end if 
!
!
        write(15,fmt=1012) 'END   '                                       
	close(15)
!
	PRINT*, '        '
	PRINT*, '################################'
	PRINT*, '# SUMMARY WRITE_FINAL_STRUCT   #'
	PRINT*, '################################'
	PRINT*, '        '
!
        if (solvent_present) then
	PRINT*, '  NUMBER OF WATER MOLECULES MODELLED= ', nnrs
	PRINT*, '        '
        end if
!
	PRINT*, '  GMEC - STRUCTURE WRITTEN TO FILE: ', mumbo_outpdb(1:len_trim(mumbo_outpdb))
	PRINT*, '                  '
!
!
1010	format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
1011	format(a6,i5,1x,a4,1x,a4,a,i4,4x,3f8.3,2f6.2)
1012	format(a6,/)
!
	END SUBROUTINE WRITE_FINAL_OLD
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE BACKUP_WATER(m1,m2,m3,m4,m5,first)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
        USE MOLEC_M
        USE WATER_DATA
!
        IMPLICIT NONE
!
        INTEGER :: m1,m2,m3,m4,m5, npos, i
        LOGICAL :: first 
!
       if (first) then 
          if (allocated(DATAintpos)) deallocate(DATAintpos)
          allocate(DATAintpos(1,5))
          if (allocated(DATAnamepos)) deallocate(DATAnamepos)
          allocate(DATAnamepos(1))
          if (allocated(DATAflagpos)) deallocate(DATAflagpos)
          allocate(DATAflagpos(1))
          npos=1
        else
          npos=size(DATAnamepos)
          if (allocated(BACKintpos)) deallocate(BACKintpos)
          allocate(BACKintpos(npos,5))
          if (allocated(BACKnamepos)) deallocate(BACKnamepos)
          allocate(BACKnamepos(npos))
          if (allocated(BACKflagpos)) deallocate(BACKflagpos)
          allocate(BACKflagpos(npos))
          do i=1,npos
            BACKnamepos(i)   = DATAnamepos(i)
            BACKflagpos(i)   = DATAflagpos(i)
            BACKintpos(i,1)  = DATAintpos(i,1)
            BACKintpos(i,2)  = DATAintpos(i,2) 
            BACKintpos(i,3)  = DATAintpos(i,3) 
            BACKintpos(i,4)  = DATAintpos(i,4) 
            BACKintpos(i,5)  = DATAintpos(i,5) 
          end do
          deallocate(DATAintpos)
          allocate(DATAintpos((npos+1),5))
          deallocate(DATAnamepos)
          allocate(DATAnamepos(npos+1))
          deallocate(DATAflagpos)
          allocate(DATAflagpos(npos+1))
          do i=1,npos
            DATAnamepos(i)   = BACKnamepos(i)
            DATAflagpos(i)   = BACKflagpos(i)
            DATAintpos(i,1)  = BACKintpos(i,1)
            DATAintpos(i,2)  = BACKintpos(i,2)  
            DATAintpos(i,3)  = BACKintpos(i,3)
            DATAintpos(i,4)  = BACKintpos(i,4)
            DATAintpos(i,5)  = BACKintpos(i,5)  
          end do
          deallocate(BACKintpos)
          deallocate(BACKnamepos)
          deallocate(BACKflagpos)
          npos=npos+1
        end if
!
        DATAnamepos(npos)   = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(m5)%at_nam
        DATAflagpos(npos)   = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(m5)%at_flag
        DATAintpos(npos,1)  = m1
        DATAintpos(npos,2)  = m2
        DATAintpos(npos,3)  = m3
        DATAintpos(npos,4)  = m4
        DATAintpos(npos,5)  = m5
!
	END SUBROUTINE BACKUP_WATER
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	SUBROUTINE BACKUP_WATER_OLD(m1,m2,m3,m4,m5,rocc,rbfa,first)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
        USE WATER_DATA
!
        INTEGER :: m1,m2,m3,m4,m5,npos
        REAL    :: rocc, rbfa
        LOGICAL :: first 
!
        if (first) then 
          if (allocated(DATAcordpos)) deallocate(DATAcordpos)
          allocate(DATAcordpos(1,5))
          if (allocated(DATAnamepos)) deallocate(DATAnamepos)
          allocate(DATAnamepos(1))
          npos=1
        else
          npos=size(DATAnamepos)
          if (allocated(BACKcordpos)) deallocate(BACKcordpos)
          allocate(BACKcordpos(npos,5))
          if (allocated(BACKnamepos)) deallocate(BACKnamepos)
          allocate(BACKnamepos(npos))
          do i=1,npos
            BACKnamepos(i) = DATAnamepos(i)
            BACKcordpos(i,1) = DATAcordpos(i,1)
            BACKcordpos(i,2) = DATAcordpos(i,2) 
            BACKcordpos(i,3) = DATAcordpos(i,3) 
            BACKcordpos(i,4) = DATAcordpos(i,4) 
            BACKcordpos(i,5) = DATAcordpos(i,5) 
          end do
          deallocate(DATAcordpos)
          allocate(DATAcordpos((npos+1),5))
          deallocate(DATAnamepos)
          allocate(DATAnamepos(npos+1))
          do i=1,npos
            DATAnamepos(i) = BACKnamepos(i)
            DATAcordpos(i,1) = BACKcordpos(i,1)
            DATAcordpos(i,2) = BACKcordpos(i,2)  
            DATAcordpos(i,3) = BACKcordpos(i,3)
            DATAcordpos(i,4) = BACKcordpos(i,4)
            DATAcordpos(i,5) = BACKcordpos(i,5)  
          end do
          deallocate(BACKcordpos)
          deallocate(BACKnamepos)
          npos=npos+1
        end if
!
          DATAnamepos(npos)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(m5)%at_nam
          DATAcordpos(npos,1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(m5)%at_xyz(1)
          DATAcordpos(npos,2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(m5)%at_xyz(2)
          DATAcordpos(npos,3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(m5)%at_xyz(3)
          DATAcordpos(npos,4)=rocc
          DATAcordpos(npos,5)=rbfa
!
	END SUBROUTINE BACKUP_WATER_OLD
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE WRITE_ENERGIES(toten,hkl)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MUMBO_DATA_M
	USE EP_DATA_M
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	integer, dimension(size(mol),4)     :: hkl      
	logical, dimension(size(mol))       :: hkl_flag    
	real :: toten, enp(15), enhalf(15)
	logical, save :: first = .true.
	integer, save :: ncnt, nlin, nlon
	character (len=1000) :: temp_file, timp_file, tomp_file
	integer :: i, j, k, l, n1, n2, n3, n4, n5, n6
!
	integer, dimension(4)  :: set1, set2
	integer   :: nmol
	logical :: first1, first2, flag1, flag2, skipflag
	real, dimension(15) :: ent       
	character (len=4) :: aanm
	real ::  envdwtot,  eneletot, enhbdtot, ensoltot, ensolrot 
	real ::  etot1, etot3, etot4, etot5, etot10, etot8, etot7, etot9
	real ::  etot11, etoi3, etoi4, etoi9
!
	if (first) then 
	    do i=1,1000
	       temp_file(i:i)=' '
	       timp_file(i:i)=' '
	       tomp_file(i:i)=' '
	    end do
	    first=.false.
	    ncnt = 0
	    nlin = 0
	    nlon = 0
	    call getcwd(mumbo_cwd)
	    mumbo_ncwd=len_trim(mumbo_cwd)
	    temp_file = mumbo_cwd(1:mumbo_ncwd) // '/mumbo.ene'
	    timp_file = mumbo_cwd(1:mumbo_ncwd) // '/mumbo.lis'
	    tomp_file = mumbo_cwd(1:mumbo_ncwd) // '/mumbo.ene2'
!
	    open(15,file=temp_file,form='formatted',status='unknown')
	    if (mumbo_log_ana_flag) then
	    open(16,file=timp_file,form='formatted',status='unknown')
	    open(17,file=tomp_file,form='formatted',status='unknown')
!	    
	    write(16,*) 'SUMMARY OF ROTAMERS PRESENT IN A GIVEN CONFIGURATION ' 
	    write(16,*) '-----------------------------------------------------'
	    write(17,*) '   '
	    write(17,*) 'SUMMARY OF RESID-RESID INTER FOR A GIVEN CONFIGURATION'
	    write(17,*) '   '
	    write(17,*) '   (RESID-RESID INTERACTIONS NOT REPORTED IF ALL ABSOLUTE...  '
	    write(17,'(A,F7.4,A)') '    ...VALUES OF INTERACTION ENERGIES <=        ',mumbo_log_ene2_ctoff, '     )'
	    write(17,*) '   '
	    write(17,*) '------------------------------------------------------'
	    end if
!
	    write(15,*) 'SUMMARY OF RESIDUE ENERGIES FOR A GIVEN CONFIGURATION ' 
	    write(15,*) '------------------------------------------------------'
!
	end if
!
	ncnt=ncnt+1
!
	   etot1=  0
	   etot3=  0
	   etot4=  0
	   etot5=  0
	   etot7=  0
	   etot8=  0
	   etot9=  0
	   etot10= 0
!
	   etoi3=  0
	   etoi4=  0
	   etoi9=  0
!
	write(15,'(a,i5,a,f10.3)') 'CONF#: ',ncnt,' TOTAL ENERGY=', toten  
	write(15,'(16x,a63)')'     ETOT     EVDW    EELEC    ERPROB   ESOLV    EXRAY   EHBOND'
	nlin=nlin+2
!
	if (mumbo_log_ana_flag) then
!GFORTRAN		write (16,'(F10.3,\)') toten 
		write (16,'(F10.3)') toten 
		do i= 2,(size(mol))
	    	 n1=hkl(i-1,1)
	    	 n2=hkl(i-1,3)
	     	 n3=hkl(i-1,4)
! GFORTRAN	     	 write (16,'(x,a4,i3,a1,\)')                              &
	     	 write (16,'(1x,a4,i3,a1)')                              &
     &                  mol(n1)%res(1)%res_aas(n2)%aa_nam,           &
     &                  n3,','
		end do
		write (16,'(/)')                             
	end if
!
!	NOW FIGURING OUT THE DETAILS, USING ENERGIES STORED IN EP-PAIRS
!
!	..%rot_en(1)= enp1 = TEMPLATE INTERCATION + SOLV + SOLVR
!	..%rot_en(2)= enp2 = TEMPLATE INTERACTION (VDW + ELEC + RPROB + XRAY + HBOND) 
!	..%rot_en(3)= enp3 = VDW  
!	..%rot_en(4)= enp4 = ELEC 
!	..%rot_en(5)= enp5 = RPROB - ROT PROBABILITY (only for i = j)
!	..%rot_en(6)= enp6 = SOLVATION 
!	..%rot_en(7)= enp7 = SOLVATION REF(only for i = j)
!	..%rot_en(8)= enp8 = XRAY  (only for i = j)
!	..%rot_en(9)= enp9 = HBOND
!	..%rot_en(10)= enp10 = INDIVIDUEL AND NOT PAIRWISE SOLVATION CONTRIBUTIONS
!
	do i = 2, (size(mol))
	   n1=hkl(i-1,1)
	   n2=hkl(i-1,3)
	   n3=hkl(i-1,4)
!
	   do j= 1, size(enp)
		enp (j) = 0
		enhalf(j) = 0
	   end do
!
	   do j = 2, (size(mol))
		n4 = hkl(j-1,1)
		n5 = hkl(j-1,3)
		n6 = hkl(j-1,4)
		if (i==j) then
		  enp(1) = enp(1) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(1)
		  enp(2) = enp(2) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(2)
		  enp(3) = enp(3) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(3)
	    enhalf(3) = enhalf(3) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(3)
		  enp(4) = enp(4) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(4)
	    enhalf(4) = enhalf(4) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(4)
		  enp(5) = enp(5) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(5)
		  enp(6) = enp(6) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(6)
		  enp(7) = enp(7) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(7)
		  enp(8) = enp(8) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(8)
		  enp(9) = enp(9) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(9)
	    enhalf(9) = enhalf(9) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(9)
		  enp(10) = enp(10) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(10)
		else
!
		  enp(1) = enp(1) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(1)
		  enp(2) = enp(2) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(2)
		  enp(3) = enp(3) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(3)
	    enhalf(3) = enhalf(3) + (ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(3)/2)
		  enp(4) = enp(4) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(4)
	    enhalf(4) = enhalf(4) + (ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(4)/2)
		  enp(5) = enp(5) 
		  enp(6) = enp(6) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(6)
		  enp(7) = enp(7) 
		  enp(8) = enp(8) 
		  enp(9) = enp(9) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(9)
	    enhalf(9) = enhalf(9) + (ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(9)/2)
		  enp(10) = enp(10) + ea(n1)%eb(n2)%ec(n3)%ed(n4)%ee(n5)%ef(n6)%ep_en(10)
		end if
	   end do
!
!  solvation energy = solvref + solv-energy
!
	   enp(10) =  enp(10) +  enp(7)
	   enp(2)  =  enp(2)  +  enp(10)
!!
	   write (15,'(1X,A1,I5,2X,A4,I3,7F9.3)')                             &
     &                      mol(n1)%res(1)%res_chid,                         &
     &                      mol(n1)%res(1)%res_nold,                         &
     &                      mol(n1)%res(1)%res_aas(n2)%aa_nam,               &
     &                      n3,                                              &
     &            enp(2), enp(3), enp(4), enp(5), enp(10), enp(8), enp(9)
!
	   nlin=nlin+1
!
	   etot1=   etot1  +  enp(2)
	   etoi3=   etoi3  +  enp(3)
	   etot3=   etot3  +  enhalf(3)
	   etoi4=   etoi4  +  enp(4)
	   etot4=   etot4  +  enhalf(4)
	   etot5=   etot5  +  enp(5)
	   etot7=   etot7  +  enp(7)
	   etot8=   etot8  +  enp(8)
	   etoi9=   etoi9  +  enp(9)
	   etot9=   etot9  +  enhalf(9)
	   etot10=  etot10 +  enp(10)
!
	end do
!
	etot11= etot3 + etot4 + etot5 + etot10 + etot8 + etot9
!
	write(15,'(2x,a14,7F9.3)')  	'E-SUM-RESIDUE ',				&
     &		   	etot1,   etoi3,  etoi4,  etot5,  etot10,  etot8,  etoi9
	write(15,'(2x,a14,7F9.3)')  	'E-SUM-OVERALL ',				&
     &		   	etot11,  etot3,  etot4,  etot5,  etot10,  etot8,  etot9
!
	write(15,'(a64)')'---------------------------------------------------------------'
	nlin=nlin+1
!
	if (nlin > max_lines_output) then
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '>>>>>>>  TO MANY LINES TO WRITE INTO FILE:           '
	PRINT*, '>>>>>>> ', temp_file(1:len_trim(temp_file)), nlin
	PRINT*, '>>>>>>>  NUMBER OF LINES IN ANY OUTPUT RESTRICTED TO:'
	PRINT*, '>>>>>>> ', max_lines_output
	PRINT*, '>>>>>>>  DURING COMPILATION (MAX_LINES_OUTPUT)       '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '    '
	STOP
	end if
!
!	NOW WRITING OUT INTERACTIONS IN GREAT DETAIL IF ASKED FOR
!
	if (mumbo_log_ana_flag) then
!
	  write(17,'(a,i5,a,f10.3)') ' CONF#: ',ncnt,' TOTAL ENERGY=', toten  
	  nlon=nlon+1
!
	  do i = 2, (size(mol))
!
	  envdwtot =  0.0
          eneletot =  0.0
	  enhbdtot =  0.0
	  ensoltot =  0.0
	  ensolrot =  0.0
!
	    n1=hkl(i-1,1)
	    n2=hkl(i-1,3)
	    n3=hkl(i-1,4)
	    set1(1)=n1
	    set1(2)=1
	    set1(3)=n2
	    set1(4)=n3
!
	    do j=1,4
	     set2(j)=1
	    end do
!
	    do j=1,size(mol)
		hkl_flag(j)  =.false.
	    end do
		hkl_flag(1)  =.true.
		hkl_flag(i-1)=.true.
!
	    do j=1,size(enp)
	      enp(j)=0
	    end do
!
!  FIRST DETERMINING ROTAMER SELF - ENERGIES ...
!
	    first1=.true.
	    first2=.true.
	    call energize(set1,set2,enp,first1,first2)
	    first1=.false.
	    first2=.false.
!
!
	write(17,'(a64)')'---------------------------------------------------------------'
	    write (17,'(a,i5,2x,a1,i5,2x,a4,i3,7F9.3)')                      & 
     &                      ' CONF#: ',ncnt,                                 & 
     &                      mol(n1)%res(1)%res_chid,                         & 
     &                      mol(n1)%res(1)%res_nold,                         & 
     &                      mol(n1)%res(1)%res_aas(n2)%aa_nam,               & 
     &                      n3                                               
	    write(17,'(16x,a69)')                                            &
     &    '    ETOT     EVDW    EELEC   ERPROB ESOLV-R   ESOLV*   EXRAY   EHBOND'
	    write(17,'(a16,a8,7F9.3)') 'SELF-ENERGY    ',                    &
     &                      '    ----',                                      &
     &                      enp(3), enp(4),  enp(5),                         &
     &                      enp(7), enp(10), enp(8), enp(9) 
	    nlon=nlon+4
!
			    envdwtot =  enp(3)
			    eneletot =  enp(4)
			    enhbdtot =  enp(9)
			    ensoltot =  enp(10)
			    ensolrot =  enp(7)
!!
!   NOW CALCULATING RESIDUE - RESIDUE INTERACTIONS, ALL OVER AGAIN.....
!
		  do j=1,mol(1)%mol_nrs
		          set2(1)=1
		          set2(2)=j
		          set2(3)=1
		          set2(4)=1
!
			  aanm = mol(1)%res(j)%res_aas(1)%aa_nam
!
			  do k=1,size(enp)
			    enp(k)=0
			    ent(k)=0
			  end do
			  call energize(set1,set2,enp,first1,first2)
			  do k=1,size(enp)
			    ent(k)= enp(k)
			  end do
!
	                  if (mol(1)%res(j)%res_mut) then
!
 		            flag1=.false.
			    flag2=.false.
			    nmol = 0
			    do k=2, size(mol)
!
		if(mol(k)%res(1)%res_num == mol(1)%res(j)%res_num .and. k /= n1) then 
		   nmol = k
		   flag1=.true.
		   exit
		end if
!
			    end do
!
			    do k=2,size(mol)
			      if (hkl(k-1,1) == nmol) then
				set2(1)=hkl(k-1,1)
				set2(2)=hkl(k-1,2)
				set2(3)=hkl(k-1,3)
				set2(4)=hkl(k-1,4)
                                aanm = mol(set2(1))%res(1)%res_aas(set2(3))%aa_nam
				hkl_flag(k-1) = .true.
		  		flag2=.true.
				exit
			      end if
			    end do
!
			    do k=1,size(enp)
			      enp(k)=0
			    end do
			    if (flag1.and.flag2) then 
			      first1=.false.
			      first2=.false.
			      call energize(set1,set2,enp,first1,first2)	
			    end if
			    do k=1,size(enp)
			      ent(k)= ent(k)+ enp(k)
			    end do
!
		          end if
!
!        don't write out lines if all energies are zero... or more precisely 
!        abs(en) < mumbo_log_ene2_ctoff
!
                          skipflag=.false.
                          if (abs(ent(3)).le.mumbo_log_ene2_ctoff) then 
                            if (abs(ent(4)).le.mumbo_log_ene2_ctoff) then
                              if (abs(ent(10)).le.mumbo_log_ene2_ctoff) then
                                if (abs(ent(9)).le.mumbo_log_ene2_ctoff) then
                                   skipflag=.true.
                                end if
                              end if
                            end if
                          end if
!
                          if (.not.skipflag) then 
!
!
	   write (17,'(1x,a1,i5,2x,a4,3x,a8,2F9.3,2a8,F9.3,a8,F9.3)')         &
     &                      mol(1)%res(j)%res_chid,                          &
     &                      mol(1)%res(j)%res_nold,                          &
     &                      aanm,                                            &
     &                      '    ----',                                      &
     &                      ent(3), ent(4),                                  &
     &                      '    ----',                                      &
     &                      '    ----',                                      &
     &                      ent(10),                                         &
     &                      '    ----',                                      &
     &                      ent(9)                                            
	                    nlon=nlon+1
!
                          end if
!
			  envdwtot =  envdwtot + ent(3)
			  eneletot =  eneletot + ent(4)
			  enhbdtot =  enhbdtot + ent(9)
			  ensoltot =  ensoltot + ent(10)
!
		  end do
!
!  STILL FIGURING OUT ADDITIONAL STUFF LIKE RESIDUE LIGAND INTERACTIONS ...
!
		  do k=2,size(mol)
			if (.not.hkl_flag(k-1)) then
			  set2(1)=hkl(k-1,1)
			  set2(2)=hkl(k-1,2)
			  set2(3)=hkl(k-1,3)
			  set2(4)=hkl(k-1,4)
			  aanm = mol(set2(1))%res(1)%res_aas(set2(3))%aa_nam
!
			  do l=1,size(enp)
			     enp(l)=0
			     ent(l)=0
			  end do
			  first1=.false.
			  first2=.false.
			  call energize(set1,set2,enp,first1,first2)
			  do l=1,size(enp)
			      ent(l)= enp(l)
			  end do
!
!        don't write out lines if all energies are zero... or more precisely 
!        abs(en) < mumbo_log_ene2_ctoff
!
                          skipflag=.false.
                          if (abs(ent(3)).le.mumbo_log_ene2_ctoff) then 
                            if (abs(ent(4)).le.mumbo_log_ene2_ctoff) then
                              if (abs(ent(10)).le.mumbo_log_ene2_ctoff) then
                                if (abs(ent(9)).le.mumbo_log_ene2_ctoff) then
                                   skipflag=.true.
                                end if
                              end if
                            end if
                          end if
!
                          if (.not.skipflag) then 
!
	   write (17,'(1x,a1,i5,2x,a4,3x,a8,2F9.3,2a8,F9.3,a8,F9.3)')         &
     &                      mol(set2(1))%res(1)%res_chid,                    &
     &                      mol(set2(1))%res(1)%res_nold,                    &
     &                      aanm,                                            &
     &                      '    ----',                                      &
     &                      ent(3), ent(4),                                  &
     &                      '    ----',                                      &
     &                      '    ----',                                      &
     &                      ent(10),                                         &
     &                      '    ----',                                      &
     &                      ent(9)                                            
	                    nlon=nlon+1
!
                          end if
!
			  envdwtot =  envdwtot + ent(3)
			  eneletot =  eneletot + ent(4)
			  enhbdtot =  enhbdtot + ent(9)
			  ensoltot =  ensoltot + ent(10)
!
!
			 end if
		   end do
!
	  if (nlon > max_lines_output) then
	    PRINT*, '    '
	    PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	    PRINT*, '>>>>>>>  TO MANY LINES TO WRITE INTO FILE:           '
	    PRINT*, '>>>>>>> ', tomp_file(1:len_trim(tomp_file)), nlon
	    PRINT*, '>>>>>>>  NUMBER OF LINES IN ANY OUTPUT RESTRICTED TO:'
	    PRINT*, '>>>>>>> ', max_lines_output
	    PRINT*, '>>>>>>>  DURING COMPILATION (MAX_LINES_OUTPUT)       '
	    PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	    PRINT*, '    '
	    STOP
	  end if
!
	  write(17,'(4(a,F9.3))')                                             &
     &                      ' ENVDWTOT = ',  envdwtot,                        &
     &                      ' ENELETOT = ',  eneletot,                        &
     &                      ' ENSOLTOT = ', (ensolrot + ensoltot) ,           &
     &                      ' ENHBDTOT = ',  enhbdtot 
!
	  end do
!
	end if
!
!
	END SUBROUTINE WRITE_ENERGIES
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE WRITE_STRUCT()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MOLEC_M
    USE MAX_DATA_M
    USE MUMBO_DATA_M
!
    IMPLICIT NONE
!
    CHARACTER(LEN=1000), dimension(5) :: filenm
    INTEGER :: i,j,k,l,m 
    INTEGER :: npos, natn, nres, nnrs, nskip, nkeep
!    
    CHARACTER(LEN=4) :: name 
!
    LOGICAL :: cflag, aaflag 
    integer :: taas, tncnt, tnres, tnkeep, tpos
    integer :: kaas, naas
!
    CHARACTER(LEN=4)   :: resnm, atnm, res_name
    CHARACTER(LEN=132) :: string, blank
!
    CHARACTER(LEN=6)   :: w_alab = 'ATOM  '  ! string(1:6)     ATOM or HETAM label 
    CHARACTER(LEN=5)   :: w_anum             ! string(7:11)    atom number
    CHARACTER(LEN=4)   :: w_anam             ! string(13:16)   atom name
!    CHARACTER(LEN=1)   :: w_altc = ' '       ! string(17:17)   alternative location/conformation indicator
    CHARACTER(LEN=4)   :: w_rnam             ! string(18:20)   residue name
    CHARACTER(LEN=1)   :: w_chid             ! string(22:22)   chain identifier
    CHARACTER(LEN=6)   :: w_rnum             ! string(23:26)   residue sequence number 
!                                        
    CHARACTER(LEN=1)   :: w_inse = ' '       ! string(27:27)   code for insertion of residues
    CHARACTER(LEN=8)   :: w_coox             ! string(31:38)   orthogonal x-coordinate
    CHARACTER(LEN=8)   :: w_cooy             ! string(39:46)   orthogonal y-coordinate
    CHARACTER(LEN=8)   :: w_cooz             ! string(47:54)   orthogonal z-coordinate
    CHARACTER(LEN=6)   :: w_occu             ! string(55:60)   occupancy
    CHARACTER(LEN=6)   :: w_bfac             ! string(61:66)   temperature factor
!    CHARACTER(LEN=2)   :: w_elem             ! string(77:78)   element symbol
!    CHARACTER(LEN=2)   :: w_char             ! string(79:80)   charge of symbol
!    
    CHARACTER(LEN=7)   :: w_backrup           ! string(73:79)   backrup degree....
!    
!
!'ATOM   5661 AOP1 NAP B1318      -8.357 -33.199  26.448  1.00 51.58           O            '
!'123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
!'         1         2         3         4         5         6         7         8         9'
!
    MUMBO_CONV_FLAG=.FALSE.
!
!   check for existing files and create backups
!
    PRINT*,mumbo_proc,  '        '
    PRINT*,mumbo_proc,  ' ################################'
    PRINT*,mumbo_proc,  ' # SUMMARY WRITE_STRUCT         #'
    PRINT*,mumbo_proc,  ' ################################'
    PRINT*,mumbo_proc,  '        '
!
    do i=1,132
       blank(i:i) = ' '
    end do   
!    
    blank(1:6) = w_alab
!
    cflag=.true.
    npos=size(mol)
!
! DUMP ROTAMERS 
!	do i=2, npos
!	   do j=1,mol(i)%mol_nrs
!	     do k=1,mol(i)%res(j)%res_naa
!	      do l=1,mol(i)%res(j)%res_aas(k)%aa_nrt 
!	     
!	  call dump_pdb(i,j,k,l)
!
!	      end do
!	     end do
!	   end do
!	end do
! DEBUG
!
!	BACK UP PREVIOUS FILES IF REQUESTED
!
    if (mumbo_log_dbase_flag) then
       call backup(npos)
    end if
!
    tncnt = 0
    tnres = 0
    tnkeep = 0
    tpos = (size(mol)) -1
    taas = 0
    kaas = 0
!
    call get_filnms (0,filenm)
!
    call write_0atom_struct(filenm(2))
    open(unit=15,file=filenm(1),status='unknown',form='formatted')
!
!   writing out 0_mch_sum (unit = 15) and 1_atm_sum, 2_atm_sum.... (unit = 14) 
!
    do i=2,size(mol)
!
        call get_filnms (i-1,filenm)
        open(unit=14,file=filenm(3),status='unknown',form='formatted')
!
        natn=    0
        nres=    0
        naas=    0
        nkeep=   0
        nskip=   0
!
        do j=1,mol(i)%mol_nrs
          do k=1,mol(i)%res(j)%res_naa
             resnm    = mol(i)%res(1)%res_aas(k)%aa_nam
             if (len_trim(resnm).gt.3) then
!             
         PRINT*, '    '
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>          '
         PRINT*, '>>>>>>>  EXPECTING RESIDUE NAMES TO HAVE NO MORE THAN 3 CHARC. '
         PRINT*, '>>>>>>>  IN SUBROUTINE WRITE_STRUCT() '
         PRINT*, '>>>>>>>  ', mol(i)%res(1)%res_aas(k)%aa_nam
         PRINT*, '>>>>>>>  MUST STOP '
         PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
         PRINT*, '    '
         STOP
!             
             else
               w_rnam = '    '
               w_rnam(1:len_trim(resnm)) = resnm(1:len_trim(resnm)) 
             end if 
!             
             aaflag=.false.
             naas = naas +1
             taas = taas +1 
!
             do l=1, mol(i)%res(j)%res_aas(k)%aa_nrt
               nres=nres+1
               cflag= mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_flag
               if(.not.cflag) then
                 nskip=nskip+1
                 cycle
               else
                 nkeep=nkeep+1
                 aaflag=.true.
               end if
!
               nnrs=nkeep
!
!                            here taking care of glycines with no rotamer atoms being present
!
!               if ((resnm=='GLY ').and.(size(mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats)==0)) then
               if ((resnm=='GLY ').and.(mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat==0)) then
!            
                   string = blank
                   string(18:20) = w_rnam                ! residue name
                   natn=natn+1                           ! atom number
                   if (natn > max_ntmp) then
                      natn = 1
                   end if
                   write(w_anum,'(i5)') natn
                   string(7:11) = w_anum 
                   w_anam= ' HB '
                   string(13:16) = w_anam
                   w_rnum = '      '
                   write(w_rnum,'(i6)') nnrs
                   if (nnrs.le.9999) then 
                     string(23:26) = w_rnum(3:6)
                   else   
                     string(22:27) = w_rnum
                   end if
                   w_coox = '   1.000'
                   string(31:38) = w_coox
                   w_cooy = '   1.000'
                   string(39:46) = w_cooy
                   w_cooz = '   1.000'
                   string(47:54) = w_cooz
                   w_occu = '  1.00'
                   string(55:60) = w_occu
                   w_bfac = '  1.00'
                   string(61:66) = w_bfac
                   w_backrup = '       '
                   if (backrub_flag.and.(mol(i)%res(1)%res_bbr)) then 
                     write(w_backrup,'(F7.2)') mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_qrot
                   end if  
                   string(73:79) = w_backrup 
!          
                   write(14,'(A)') string
                   cycle
!                   
               end if                             ! done taking care of 'special glycines'
!
!
               do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
               if (.not.mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_flag) then
                  cycle
               end if 
!               
!              preparing to write out each atom into a string
!
               string = blank
               string(18:20) = w_rnam                ! residue name
!                                                 
               natn=natn+1                           ! atom number
               if (natn > max_ntmp) then
                  natn = 1
               end if
               write(w_anum,'(i5)') natn
               string(7:11) = w_anum 
!                                                    ! atom name
               w_anam= '    '                       
               atnm  = mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_nam
               if (atnm(4:4).eq.' ') then 
                 w_anam(2:4) = atnm(1:3)
               else 
                 w_anam = atnm
               end if
               string(13:16) = w_anam 
!                                                        ! residue number
               w_rnum = '      '
               write(w_rnum,'(i6)') nnrs
               if (nnrs.le.9999) then 
                 string(23:26) = w_rnum(3:6)
               else   
                 string(22:27) = w_rnum
               end if
!                                                        ! coordinate x, y, z
               w_coox = '        '
               write(w_coox,'(f8.3)')  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(1)
               string(31:38) = w_coox
!
               w_cooy = '        '
               write(w_cooy,'(f8.3)')  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(2)
               string(39:46) = w_cooy
!
               w_cooz = '        '
               write(w_cooz,'(f8.3)')  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_xyz(3)
               string(47:54) = w_cooz
!
               w_occu = '      '
               write(w_occu,'(f6.2)')  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_prob
               string(55:60) = w_occu
!
               w_bfac = '      '
               write(w_bfac,'(f6.2)')  mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_bfa
               string(61:66) = w_bfac
!
               w_backrup = '       '
               if (backrub_flag.and.(mol(i)%res(1)%res_bbr)) then 
                 write(w_backrup,'(F7.2)') mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_qrot
               end if  
               string(73:79) = w_backrup 
!
               write(14,'(A)') string

!                          End loop over all atoms in rotamer
              end do
!                          End loop over all rotamers in aa
          end do

          if (aaflag) then
            kaas = kaas+1
          end if
!
!                          End loop over all amino acids at each residue position
         end do
!                          End loop over all residues in molecule
        end do 
!
           PRINT *,mumbo_proc, ' WRITING OUT DATA MOLECULE ', i-1
           PRINT *,mumbo_proc, '        RESIDUE NUMBER=    ', mol(i)%res(1)%res_chid, &
     &              mol(i)%res(1)%res_nold  
        if (mol(i)%res(1)%res_ntype.eq.1) then 
           PRINT *,mumbo_proc, '        FOUND LIGS=      ', naas
           PRINT *,mumbo_proc, '        FOUND LIGS*NROT= ', nres
        else if (mol(i)%res(1)%res_ntype.eq.2) then 
           PRINT *,mumbo_proc, '        FOUND AAS=       ', naas
           PRINT *,mumbo_proc, '        FOUND AAS*NROT=  ', nres
        else if (mol(i)%res(1)%res_ntype.eq.3) then 
           PRINT *,mumbo_proc, '        FOUND NUCS=      ', naas
           PRINT *,mumbo_proc, '        FOUND NUCS*NROT= ', nres
        end if    
           PRINT *,mumbo_proc, '        DELETED=         ', nskip
           PRINT *,mumbo_proc, '        KEPT=            ', nkeep 
!
        if (nkeep==0) then
    PRINT*, '    '
    PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
    PRINT*, '>>>>>>>  NO ROTAMER KEPT                             '
    PRINT*, '>>>>>>>  THIS WILL CERTAINLY CAUSE TROUBLE LATER     '
    PRINT*, '>>>>>>>                                              '
    PRINT*, '>>>>>>>  POSSIBLE REMEDY:                            '
    PRINT*, '>>>>>>>                                              '
    PRINT*, '>>>>>>>  IF ERROR OCCURS AT THE END OF MC_STEP ==>   '
    PRINT*, '>>>>>>>      INCREASE    ENERGY_CUTOFF= XX           '            
    PRINT*, '>>>>>>>                                              '
    PRINT*, '>>>>>>>  IF ERROR OCCURS AT THE END OF BRUT_STEP ==> '
    PRINT*, '>>>>>>>      INCREASE   ENERGY_BRUT=  KEEP= -XX      '            
    PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
    PRINT*, '    '
    STOP
       end if
!
       tncnt=tncnt+ nskip
       tnkeep=tnkeep + nkeep
       tnres=tnres + nres
!
    close(14)
!
    end do
!
    write(*,'(3a)')      ' ',mumbo_proc,'          '
    write(*,'(3a, I9)')  ' ',mumbo_proc,' NUM11 POS          (TOTAL)           :',tpos
    write(*,'(3a, 3I9)') ' ',mumbo_proc,' NUM11 AAS+NUCS+LIG (TOT,KEPT,DELETED):',taas,kaas,(taas-kaas)
    write(*,'(3a, 3I9)') ' ',mumbo_proc,' NUM12 (A+N+L)*ROTS (TOT,KEPT,DELETED):',tnres,tnkeep,tncnt
    write(*,'(3a)')      ' ',mumbo_proc,'          '
    PRINT*,'        '
!
    if (tncnt == 0) MUMBO_CONV_FLAG=.TRUE.
!
!
!   now write out main-chain information of positions to be mumboed into output unit (15)
!
    natn=0
    do i=2,size(mol)
!
      do k=1,132
        string(k:k)=' '
      end do 
!
      res_name='    '
      if (mol(i)%res(1)%res_ntype.eq.1) then
         res_name = 'LIG '
      else if (mol(i)%res(1)%res_ntype.eq.2) then 
          res_name = xaa_name
      else if (mol(i)%res(1)%res_ntype.eq.3) then 
          res_name = nuc_name
      end if
!
!     incorperate ligand_flag
!
      if (mumbo_lig_flag) then
        if (i==size(mol)) then 
           natn = natn + 1
           if (natn > max_ntmp) then
              natn = 1
           end if
!
           write (string,fmt=1010)                                &
     &        'ATOM  ',                                             &
     &        natn   ,                                              &
     &        'CA    ',lig_name,                                    &
     &        mol(i)%res(1)%res_chid,                               &
     &        mol(i)%res(1)%res_nold,                               &
     &        1.00,                     &
     &        1.00,                     &
     &        1.00,                     &
     &        1.00,                     &
     &        1.00
                write (15,fmt='(a)') string
           exit
        end if
      end if 
!
      do j=1,(size(mol(i)%res(1)%res_atom_mc))
        if (.not.mol(i)%res(1)%res_atom_mc(j)%at_flag) then
           cycle
        end if   
!
        do k=1,132
             string(k:k)=' '
        end do 
        natn=natn+1
        if (natn > max_ntmp) then
            natn = 1
        end if
!
        write (string,fmt=1015)                                         &
     &      'ATOM  ',                                                   &
     &      natn ,                                                      &
     &      mol(i)%res(1)%res_atom_mc(j)%at_nam ,                       &
     &      res_name,                                                   &
     &      mol(i)%res(1)%res_chid,                                     &
     &      mol(i)%res(1)%res_nold,                                     &
     &      mol(i)%res(1)%res_atom_mc(j)%at_xyz(1),                     &
     &      mol(i)%res(1)%res_atom_mc(j)%at_xyz(2),                     &
     &      mol(i)%res(1)%res_atom_mc(j)%at_xyz(3),                     &
     &      mol(i)%res(1)%res_atom_mc(j)%at_occ,                        &
     &      mol(i)%res(1)%res_atom_mc(j)%at_bfa
!     
        if (backrub_flag.and.(mol(i)%res(1)%res_bbr)) then 
          write (string(71:74),fmt='(a4)') bbr_name
        end if
!
!       if (water_flag.and.(mol(i)%res(1)%res_wtr)) then 
        if (water_flag) then 
          write (string(76:80),fmt='(a4)') wtr_name
        end if
!              
        write (15,fmt='(a)') string
!
       end do
    end do
!
    close(15)
!
!  PLEASE BE AWARE THAT THE 10** FORMATS DO NOT ALLOW FOR RESIDUE NUMBERS THAT EXCEED 9999
!
1010    format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
1015    format(a6,i5,2x,a4,a4,a,i4,4x,3f8.3,2f6.2)
!
	PRINT*, '  ... FINISHED WRITING STRUCTURE '
!
	END SUBROUTINE WRITE_STRUCT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE BACKUP(npos)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MAX_DATA_M
!
	IMPLICIT NONE
!
	CHARACTER(LEN=1000), dimension(5) :: filenm
	CHARACTER(LEN=1000) :: string
	INTEGER :: npos, na, ios
	LOGICAL :: flag
	CHARACTER(LEN=132) :: line
	INTEGER i,j,k,l,m
!	
	na=0
	do i= 1,npos
	   call get_filnms(i-1,filenm)
	   do j= 1,5
	      do k=1,1000
			string(k:k)=' '
		end do
		   na=len_trim(filenm(j))
		   string(1:na)=filenm(j)
!
	    do l= 1,8
		   flag=.false.
		   write (string(na+1:na+3),'(a2,i1)') '--',(9-l)
!		   
		   inquire (file=string(1:na+3),exist=flag)
!
		   if (flag) then
!
	open(14,file=string(1:na+3),status='unknown',form='formatted')
	write (string(na+1:na+3),'(a2,i1)') '--',((9-l)+1)
	open(15,file=string(1:na+3),status='unknown',form='formatted')
			  do m=1, max_lines_input
			      read(14,fmt='(a)',iostat=ios) line
			      if (ios==eo_file) then
!			         print*,'END-OF-FILE REACHED IN FILE: '
!			         print*, string(1:na+3) 
!			         print*,'NO HARM DONE                 '
			         exit
			      end if
			      if (m==max_lines_input) then
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
	PRINT*, '>>>>>>> ', string(1:na+3)
	PRINT*, '>>>>>>>  INCREASE MAX_LINES_INPUT                     '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '    '
	STOP
			      end if
			      write(15,fmt='(a)') line
			  end do
			  close(14)
			  close(15)
		   end if
!
		end do
!
		flag=.false.
		string(1:na)=filenm(j)
		inquire (file=string(1:na),exist=flag)
		if (flag) then
!
	open(14,file=string(1:na),status='unknown',form='formatted')
	write (string(na+1:na+3),'(a3)') '--1'              		
	open(15,file=string(1:na+3),status='unknown',form='formatted')
			do m=1, max_lines_input
			      read(14,fmt='(a)', iostat=ios) line
			      if (ios==eo_file) then
!			         print*,'END-OF-FILE REACHED IN FILE: '
!			         print*, string(1:na) 
!			         print*,'NO HARM DONE                 '
			         exit
			      end if
			      if (m==max_lines_input) then
	PRINT*, '    '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '>>>>>>>  END-OF-FILE NOT REACHED:                    '
	PRINT*, '>>>>>>> ', string(1:na)
	PRINT*, '>>>>>>>  INCREASE MAX_LINES_INPUT                     '
	PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
	PRINT*, '    '
	STOP
			      end if
			      write(15,fmt='(a)') line
			end do
			close(14)
			close(15)
		end if
	   end do
	end do
!
	END SUBROUTINE BACKUP
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE LOAD_PREV_STRUCT 
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MAX_DATA_M
!
    IMPLICIT NONE
!
    INTEGER :: natmc, npos, na, nb
!
    na=max_lines_input
    nb=max_npos
!
    CALL GET_MCINFO(na,nb,natmc,npos)     
!
    CALL ALLO_PREV_STRUCT(npos)
!
    CALL GET_PREV_STRUCT(npos)
!
    END SUBROUTINE LOAD_PREV_STRUCT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE ALLO_PREV_struct(npos)
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
	USE MOLEC_M
	USE MAX_DATA_M
	USE MUMBO_DATA_M
!
	IMPLICIT NONE
!
	integer :: npos
	character (LEN=1000), dimension(5):: filenam
	character (LEN=1000)              :: string
	character (LEN=132)               :: filenm
	integer :: nresid, ncnt, i, nx, ny, nz, j, k, na
	integer, dimension(:), allocatable :: natom
	character(LEN=4), dimension(:), allocatable ::cnam
	logical, dimension(:), allocatable :: lflag
	character(LEN=4) :: old, brname, wrname
	character(LEN=1) :: cid
	logical :: first 
!
    IF (ALLOCATED(mol)) DEALLOCATE (mol)
    ALLOCATE (mol(npos+1))
    mol_nmls=npos+1
!
    ncnt=0
    call get_filnms(ncnt,filenam)
!
!   0_atm_sum adheres to conventions regarding PDB format (or CIF format, if chosen)
!   reading in using new subroutines, alternatively a cif-file could also be read in
!
    string = filenam(2)
    do i=1,132
      filenm(i:i) = ' ' 
    end do
    filenm(1:132) = string(1:132)
    call pdb_get_nresid(filenm,nresid,max_lines_input)  
    if (allocated(natom)) deallocate(natom)
    allocate (natom(nresid))
    if (allocated(cnam)) deallocate(cnam)
    allocate (cnam(nresid))
    call pdb_get_natom(filenm,nresid,natom,cnam,max_lines_input)
!!
!    call get_nresid(filenam(2),nresid,max_lines_input,cid)
!	allocate(natom(nresid),cnam(nresid))
!!
!	call get_natom(filenam(2),nresid,natom,cnam,max_lines_input)
!
    allocate (mol(1)%res(nresid))
    mol(1)%mol_nrs=nresid
!
    do,i=1,nresid
       allocate(mol(1)%res(i)%res_aas(1))
       mol(1)%res(i)%res_naa=1
       mol(1)%res(i)%res_mut= .false.
       mol(1)%res(i)%res_lig= .false.
       mol(1)%res(i)%res_bbr= .false.
       mol(1)%res(i)%res_pps= .false.
       mol(1)%res(i)%res_endo_exo= .false.
!       
       allocate(mol(1)%res(i)%res_aas(1)%aa_rots(1))
       mol(1)%res(i)%res_aas(1)%aa_nrt=1
       allocate(mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_ats(natom(i)))
       mol(1)%res(i)%res_aas(1)%aa_rots(1)%rot_nat=natom(i)
    end do
!
    do, i=2,npos+1
           allocate(mol(i)%res(1))
           mol(i)%mol_nrs=1
           mol(i)%res(1)%res_mut= .false.
           mol(i)%res(1)%res_lig= .false.
           mol(i)%res(1)%res_bbr= .false.
           mol(i)%res(1)%res_pps= .false.
           mol(i)%res(1)%res_endo_exo= .false.
    end do
!
    deallocate(natom,cnam)
!
!   CHECKING  ----- 0_mch_sum ------- AND ALLOCATING STUFF
!   0_mch_sum should also adhere to PDB format with the exception of the additinal keywords BBR and WTR...  
!
    string = filenam(1)
    do i=1,132
      filenm(i:i) = ' ' 
    end do
    filenm(1:132) = string(1:132)
    call pdb_get_nresid(filenm,nresid,max_lines_input)  
    if (allocated(natom)) deallocate(natom)
    allocate (natom(nresid))
    if (allocated(cnam)) deallocate(cnam)
    allocate (cnam(nresid))
    call pdb_get_natom(filenm,nresid,natom,cnam,max_lines_input)
!!
!	call get_nresid(filenam(1),nresid,max_lines_input,cid)
!	allocate (natom(nresid),cnam(nresid),lflag(nresid))
!!
!	call get_natom(filenam(1),nresid,natom,cnam,max_lines_input)
!!
    mumbo_lig_flag=.false.
    do, i=1,nresid
        if (cnam(i) == lig_name) then
           mumbo_lig_flag=.true.
        end if
    end do
!
    if (mumbo_lig_flag) then
        mol(npos+1)%res(1)%res_lig= .true.
    end if
!
!   HERE FINDING OUT ABOUT BACKRUB_FLAG AND WHICH RESIDUES TO BE BACKRUB(B)ED
!
    backrub_flag=.false.
    brname=bbr_name
    if (allocated(lflag)) deallocate(lflag)
    allocate(lflag(nresid))
    lflag=.false.
!    
    call get_lflag(filenam(1),nresid,natom,lflag,brname)
!        
    do, i=2,npos+1
           if (lflag(i-1)) backrub_flag=.true.
           mol(i)%res(1)%res_bbr=lflag(i-1)
    end do
!
!DEBUG
!    PRINT*, 'BACKRUB_FLAG', backrub_flag
!    do, i=2,npos+1
!           PRINT*,i-1,mol(i)%res(1)%res_bbr
!    end do
!DEBUG
!
!  HERE FINDING OUT ABOUT WATER_FLAG BEING PRESENT IN 0_mch_sum
!       THIS makes only sense in the none WATER steps...if JOB = WATER specified then water_flag will always remain true
!
    if (allocated(lflag)) deallocate(lflag)
    allocate(lflag(nresid))
    lflag=.false.
!
    if (.NOT.water_flag) then 
       wrname=wtr_name
       call get_lflag(filenam(1),nresid,natom,lflag,wrname)
       do, i=2,npos+1
             if (lflag(i-1)) water_flag=.true.
       end do
    end if 
!
    do, i=2,npos+1
        allocate(mol(i)%res(1)%res_atom_mc(natom(i-1)))
    end do
!
    deallocate(natom,cnam)
!
!   NOW ALLOCATING RESIDUES TO BE MUMBOED (THIS SHOULD WORK FINE FOR 1_atm_sum... SINCE ALL 
!   ITEMS ARE WELL SEPARATED IN FILES)
!
    do, i=2,npos+1
!
     call get_filnms((i-1),filenam)
     call get_nresid(filenam(3),nx,max_lines_input,cid)
     allocate (natom(nx),cnam(nx))
     call get_natom(filenam(3),nx,natom,cnam,max_lines_input)
!
     old='    '
     nz=0
     first=.true.
     do j=1, nx
       if (cnam(j)/=old) then
          if (first) then
             first=.false.
             nz=1
             old=cnam(j)
          else
             nz=nz+1
             old=cnam(j)
          end if
       end if
     end do
!
     allocate(mol(i)%res(1)%res_aas(nz))
     mol(i)%res(1)%res_naa=nz
!
     old='    '
     ny=0
     nz=0
     first=.true.
     do j=1, nx
         if (cnam(j)==old) then
            ny=ny+1
         else if (cnam(j)/=old) then
           if (first) then
              first=.false.
              nz=1
              ny=1
              old=cnam(j)
           else
             allocate(mol(i)%res(1)%res_aas(nz)%aa_rots(ny))
             mol(i)%res(1)%res_aas(nz)%aa_nrt=ny
!    
             do k=1,ny
                na=natom(j-1)
                allocate(mol(i)%res(1)%res_aas(nz)%aa_rots(k)%rot_ats(na))
                mol(i)%res(1)%res_aas(nz)%aa_rots(k)%rot_nat=na
             end do
!    
             nz=nz+1
             ny=1
             old=cnam(j)
           end if
         end if
     end do
!    
     allocate(mol(i)%res(1)%res_aas(nz)%aa_rots(ny))
     mol(i)%res(1)%res_aas(nz)%aa_nrt=ny
!    
     do k=1,ny
         na=natom(nx)
!    
!        line above corrected from na=natom(j-1) on March 29, 2016
!    
         allocate(mol(i)%res(1)%res_aas(nz)%aa_rots(k)%rot_ats(na))
         mol(i)%res(1)%res_aas(nz)%aa_rots(k)%rot_nat=na
     end do
!    
     deallocate (natom,cnam)
    end do
!
	END SUBROUTINE ALLO_PREV_STRUCT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    SUBROUTINE GET_PREV_STRUCT(npos)
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
    USE MOLEC_M
    USE MAX_DATA_M
!    
    IMPLICIT NONE
!    
    integer:: npos,na,nb,nc,nd,i,j,k,l, m
    integer:: nw, nn, ios, ntype
    real:: xbfa, xx, xy, xz, xocc, xprob, xrot
    character(LEN=1000), dimension(5):: filenam
    character(LEN=132) :: string
    character(LEN=80) :: boff, baff
    character(LEN=80), dimension(40) :: word
    character(LEN=1) :: chid
    character(LEN=4) :: aanam, ctype	
    logical :: chainid_flag
!    
    PRINT*, '        '
    PRINT*, '################################'
    PRINT*, '# SUMMARY  GET_PREV_STRUCT     #'
    PRINT*, '################################'
    PRINT*, '        '
    PRINT*, '  STARTING READING IN PREVIOUS STRUCTURE ....'
!
    do i=1,npos+1
       na=0
       nb=0
       nc=0
       nd=0
!
!   READ IN CONSTANT PART FIRST (THE FORMAT FULLY ADHERES TO PDB)
!
    if (i==1) then
      call get_filnms((i-1),filenam)
      open(14,file=filenam(2),status='old',form='formatted')
      na=mol(i)%mol_nrs
      do j=1,na
        mol(i)%res(j)%res_num= j
        nb=mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_nat
        mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_flag=.true.
        mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_flag2=.false.
!
        do k=1,nb
            read(14,fmt='(a)',iostat=ios) string
            if (ios==eo_file) then
        PRINT*, '    '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
        PRINT*, '>>>>>>>  UNEXPECTED END-OF-FILE REACHED IN           '
        PRINT*, '>>>>>>> ', filenam(2)(1:len_trim(filenam(2)))
        PRINT*, '>>>>>>>  MUST EXIT IN SUBROUTINE GET_PREV_STRUCT     '
        PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
        PRINT*, '    '
        STOP
            end if
!
            call pdb_parser_explicit(string,word,nw)
!
! word(1)  =  ATOM OR HETATM RECORD              
! word(2)  =  ATOM SERIAL NUMBER                 
! word(3)  =  ATOM NAME                          
! word(4)  =  ALTERNATIVE CONFORMATION INDICATOR 
! word(5)  =  RESIDUE NAME                       
! word(6)  =  CHAIN IDENTIFIER                   
! word(7)  =  RESIDUE SEQUENCE NUMBER            
! word(8)  =  CODE FOR INSERTION OF RESIDUES     
! word(9)  =  ORTHOGONAL X-COORDINATE            
! word(10) =  ORTHOGONAL Y-COORDINATE            
! word(11) =  ORTHOGONAL Z-COORDINATE            
! word(12) =  OCCUPANCY                          
! word(13) =  TEMPERATURE FACTOR                 
! word(14) =  ELEMENT SYMBOL                     
! word(15) =  CHARGE ON THE ATOM                 
!
            if (k==1) then
                boff=word(5)
                read(word(7),*) nn
                mol(i)%res(j)%res_aas(1)%aa_nam=boff(1:4)
                boff=word(6)
                mol(i)%res(j)%res_chid = boff(1:1)
                mol(i)%res(j)%res_nold = nn
                mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_prob= 1.0
            end if
            boff=word(3)
            read(word(9),*)   xx
            read(word(10),*)  xy
            read(word(11),*)  xz
            read(word(12),*)  xocc
            read(word(13),*)  xbfa
            mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_nam=boff(1:4)
            mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(1)=xx
            mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(2)=xy
            mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_xyz(3)=xz
            mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_occ=xocc
            mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_bfa=xbfa
            mol(i)%res(j)%res_aas(1)%aa_rots(1)%rot_ats(k)%at_flag=.true.
!
        end do
      end do
      close(14)
!
!!
!     STILL HAVE TO READ IN FILE   ----- 0_mch_sum -------
!     (THE FORMAT ADHERES TO PDB UNTIL TEMP FACTORS, THIS SHOULD BE FINE)  
!
      open(14,file=filenam(1),status='old',form='formatted')
!
      na=0
!
      do j=1,npos
        do k=1,(size(mol(j+1)%res(1)%res_atom_mc))
          read(14,fmt='(a)',iostat=ios) string
          if (ios==eo_file) then
            PRINT*, '    '
            PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
            PRINT*, '>>>>>>>  UNEXPECTED END-OF-FILE REACHED IN           '
            PRINT*, '>>>>>>> ', filenam(1)(1:len_trim(filenam(1)))
            PRINT*, '>>>>>>>  MUST EXIT IN SUBROUTINE GET_PREV_STRUCT     '
            PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
            PRINT*, '    '
            STOP
          end if
!          
            call pdb_parser_explicit(string,word,nw)
!
! word(1)  =  ATOM OR HETATM RECORD              
! word(2)  =  ATOM SERIAL NUMBER                 
! word(3)  =  ATOM NAME                          
! word(4)  =  ALTERNATIVE CONFORMATION INDICATOR 
! word(5)  =  RESIDUE NAME                       
! word(6)  =  CHAIN IDENTIFIER                   
! word(7)  =  RESIDUE SEQUENCE NUMBER            
! word(8)  =  CODE FOR INSERTION OF RESIDUES     
! word(9)  =  ORTHOGONAL X-COORDINATE            
! word(10) =  ORTHOGONAL Y-COORDINATE            
! word(11) =  ORTHOGONAL Z-COORDINATE            
! word(12) =  OCCUPANCY                          
! word(13) =  TEMPERATURE FACTOR                 
! word(14) =  ELEMENT SYMBOL                     
! word(15) =  CHARGE ON THE ATOM                 
!
            if (k==1) then
                read(word(7),*) na
                boff=word(6)
                mol(j+1)%res(1)%res_chid = boff(1:1)
                mol(j+1)%res(1)%res_nold = na 
            end if    
!
            boff=word(3)
            read(word(9),*)   xx
            read(word(10),*)  xy
            read(word(11),*)  xz
            read(word(12),*)  xocc
            read(word(13),*)  xbfa
!
            mol(j+1)%res(1)%res_atom_mc(k)%at_nam=boff(1:4)
            mol(j+1)%res(1)%res_atom_mc(k)%at_xyz(1)=xx
            mol(j+1)%res(1)%res_atom_mc(k)%at_xyz(2)=xy
            mol(j+1)%res(1)%res_atom_mc(k)%at_xyz(3)=xz
            mol(j+1)%res(1)%res_atom_mc(k)%at_occ=xocc
            mol(j+1)%res(1)%res_atom_mc(k)%at_bfa=xbfa
            mol(j+1)%res(1)%res_atom_mc(k)%at_flag=.true.
!
        end do
      end do
!
      close (14)
!
!
!     NOW READ IN POSITIONS TO BE MUMBOED
!
	else
		call get_filnms((i-1),filenam)
		open(14,file=filenam(3),status='old',form='formatted')
		na=mol(i)%res(1)%res_naa
!
		do j=1,na
		nb=mol(i)%res(1)%res_aas(j)%aa_nrt
		do k=1,nb
		nc=mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_nat
		mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag=.true.
		mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_flag2=.false.
		do l=1,nc
			read(14,fmt='(a)',iostat=ios) string
			if (ios==eo_file) then
		  PRINT*, '    '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '>>>>>>>  UNEXPECTED END-OF-FILE REACHED IN           '
		  PRINT*, '>>>>>>> ', filenam(3)(1:len_trim(filenam(3)))
		  PRINT*, '>>>>>>>  MUST EXIT IN SUBROUTINE GET_PREV_STRUCT     '
		  PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
		  PRINT*, '    '
		  STOP
			end if
!
			call parser(string, word, nw)     ! this should work fine with 1_atm_sum... files 
!
			call chainid_present(word(5),chainid_flag)
			if (chainid_flag) then
				do m=6,40
				   word(m-1)=word(m)
				end do
			end if
!
			if (l==1 .and. k==1) then
				boff=word(4)
				mol(i)%res(1)%res_aas(j)%aa_nam=boff(1:4)
			end if
			if (l==1) then
				read(word(9),*) xprob
				mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_prob=xprob
				if (mol(i)%res(1)%res_bbr) then 
				    read(word(11),*) xrot
				    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_qrot=xrot
				else
				    mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_qrot=0.00
				end if
			end if
!
			boff=word(3)
			read(word(6),*)  xx
			read(word(7),*)  xy
			read(word(8),*)  xz
			read(word(9),*)  xocc
			read(word(10),*) xbfa
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_nam=boff(1:4)
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(1)=xx
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(2)=xy
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_xyz(3)=xz
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_occ=xocc
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_bfa=xbfa
	mol(i)%res(1)%res_aas(j)%aa_rots(k)%rot_ats(l)%at_flag=.true.
!
		end do
		end do
		end do
!
		close(14)
!
!	CHECKING FOR GLYCINES AT POSITIONS TO BE MUMBOED 
!    PLACE HOLDER ATOMS ARE IDENTIFIED BY THE LINE
!    'ATOM      1  HB  GLY     1       1.000   1.000   1.000  1.00  1.00'    
!
       do j=1, mol(i)%res(1)%res_naa 
         if (mol(i)%res(1)%res_aas(j)%aa_nam == gly_name) then
            mol(i)%res(1)%res_aas(j)%aa_nrt=1
            xx   =  mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_ats(1)%at_xyz(1)
            xy   =  mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_ats(1)%at_xyz(2)
            xz   =  mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_ats(1)%at_xyz(3)
            xocc =  mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_ats(1)%at_occ
            xbfa =  mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_ats(1)%at_bfa
!            
            if (abs(xx-1.0).lt.0001) then 
               if (abs(xy-1.0).lt.0001) then 
                 if (abs(xz-1.0).lt.0001) then 
                   if (abs(xocc-1.0).lt.0001) then 
                     if (abs(xbfa-1.0).lt.0001) then 
!            
            mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_nat = 0 
            mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_prob= 1.0
            mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_ats(1)%at_flag = .false.
!
                    end if
                  end if
                end if
              end if
            end if
!!
!            print*, 'HERE GLY', i, j, mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_nat
!            print*, 'HERE GLY', i, j, mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_prob
!            print*, 'HERE GLY', mol(i)%res(1)%res_aas(j)%aa_rots(1)%rot_ats(1)%at_flag
!            print*, '    '
!!
         end if 
       end do
!
	end if
	end do
!
	do i=2,npos+1
	   do j=1, mol(1)%mol_nrs
		if (mol(1)%res(j)%res_nold == mol(i)%res(1)%res_nold) then
		   if (mol(1)%res(j)%res_chid == mol(i)%res(1)%res_chid) then
!
			    mol(i)%res(1)%res_num = mol(1)%res(j)%res_num
			    mol(1)%res(j)%res_mut = .true.
!
		   end if
		end if
	   end do
	end do
!
! 	Filling up a few things
!
    do i=1,size(mol)
         do j=1,mol(i)%mol_nrs
           do k=1,mol(i)%res(j)%res_naa
              do l=1, mol(i)%res(j)%res_aas(k)%aa_nrt
                 do m=1, mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_nat
             mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_n12 = 0
             mol(i)%res(j)%res_aas(k)%aa_rots(l)%rot_ats(m)%at_n14 = 0
                 end do
              end do 
           end do
         end do
    end do
!
!   BEFORE CONTINUING, IT IS IMPORTANT TO KNOW THE TYPE OF RESIDUES PRESENT IN EACH CHAIN
!
    do i=1,size(mol)
      do j=1,mol(i)%mol_nrs
        do k=1,1            ! it should be perfectly fine to check just one aa/nucleotide
             aanam = mol(i)%res(j)%res_aas(k)%aa_nam
             call check_residue_type_byname(aanam,ntype,ctype)                 
             mol(i)%res(j)%res_ntype = ntype
!            print*, 'GET_PREV: ', i, j, k, ' ntype= ', ntype, ctype, mol(i)%res(j)%res_nold, mol(i)%res(j)%res_chid
!        
        end do
      end do
    end do
!
!
	PRINT*, '                                         .... DONE READING'
	PRINT*, '        '
!
    return
!
	END SUBROUTINE GET_PREV_STRUCT
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!@author  Yasemin Rudolph
!
    SUBROUTINE APPLY_BACKRUB_MAINCHAIN()
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
    USE MOLEC_M
    USE MUMBO_DATA_M
    USE MAX_DATA_M
!
    IMPLICIT NONE

    CHARACTER (LEN=4) :: atnm, axis_atnm1, axis_atnm2
    LOGICAL :: flag, flag_point1, flag_point2
    REAL, DIMENSION(3) :: temp_vector, temp_vector_back                             ! temporary vectors for rotation only
    REAL, DIMENSION(3) :: axis_point1, axis_point2, axis_point3                     ! CA atoms set up the axis
    REAL, DIMENSION(3) :: point1, point2, point3, point4, point5, point6, point7    ! atoms between the CA-axis
    REAL :: qrot, qrot2                                 ! qrot2 = -qrot, needed for second rotation
    INTEGER :: i, q, l
    INTEGER :: m1, m2, m3, m4, nstart, nend, nn
    INTEGER :: axis_nm1, axis_nm2
    INTEGER :: io_error
!    
    REAL, PARAMETER :: err3 = 0.001
    CHARACTER (LEN=4), DIMENSION(8) :: br3_all =(/"P   ","OP1 ","OP2 ","O5' ","C5' ","C4' ","C3' ","O3' "/) 
    CHARACTER (LEN=4), DIMENSION(:), allocatable :: br3_ats  
!    
    PRINT*, '        '
    PRINT*, '###################################'
    PRINT*, '# START APPLY_BACKRUB_MAINCHAIN   #'
    PRINT*, '###################################'
    PRINT*, '        '
!
    IF (.NOT.backrub_flag) THEN
      RETURN
    END IF
!
!   trying to figure out, which main chain atoms to reposition when applying the backrup motion
!   to the nucleotide backbone
!
    if (allocated(br3_ats)) deallocate(br3_ats)
! 
    nstart = 0
    if (br3_noff1.lt.0) then 
          nstart = 0
    else if (br3_noff1==0) then 
          do i=1,size(br3_all)
            if (br3_all(i).eq.br3_atnm1) then 
               nstart = i
               exit
            end if
          end do
    else if (br3_noff1.gt.0) then 
          nstart=size(br3_all)
    end if
!
    nend = 0
    if (br3_noff2.lt.0) then 
          nend = 0
    else if (br3_noff2==0) then 
          do i=1,size(br3_all)
            if (br3_all(i).eq.br3_atnm2) then 
               nend = i
               exit
            end if
          end do
    else if (br3_noff2.gt.0) then 
          nend=size(br3_all)
    end if
! 
    if  (nstart.lt.nend) then
           nstart = nstart-1
!          nend   = nend
    else if (nstart.gt.nend) then      
          nn = nstart
          nstart = nend-1
          nend = nn
    end if      
!
    allocate(br3_ats(nend - nstart))
    if ((nstart+1).le.nend) then
       do i = nstart+1, nend
           br3_ats(i-nstart) = br3_all(i)
       end do
    end if
!    
!    print*, 'rotation point 1 offset: ', br3_noff1, ' selected atom ', br3_atnm1 
!    print*, 'rotation point 2 offset: ', br3_noff2, ' selected atom ', br3_atnm2
!    print*, 'atoms to be rotated: ', br3_ats
!
! HERE IDENTIFY THE CA COORDS OF RESIDUE_i-1, CA COORDS OF RESIDUE_i, and CA COORDS OF RESIDUE_i+1 TO DEFINE THE 
! ROTATION AXIS FOR BACKRUB
!
! All atoms between res_i-1 and res_i+1 will be rotated around the axis between CA_i-1 and CA_i+1
! In the following two steps new rotation axis between CA_i-1 and CA_i  and  CA_i + CA_i+1 are set
! to rotate atoms counterwise qrot to make backbone torsions as little as possible (--> position of CA_i is altered)
!
    Loop1: DO i=2, mol_nmls     
      Loop2: IF (mol(i)%res(1)%res_bbr) THEN
!
        m1=1
        m2=mol(i)%res(1)%res_num
        m3=1
        m4=1
!
! chain has to be changed according to qrot only if qrot /= 0.
        Loop3: IF ( abs(mol(i)%res(1)%res_aas(1)%aa_rots(1)%rot_qrot).gt.err3) THEN     
!
          qrot = mol(i)%res(1)%res_aas(1)%aa_rots(1)%rot_qrot
          qrot2 = -qrot                                        ! qrot2 can be changed to any value needed, e.g. 0.5*qrot
!
!
!     BELOW THE IMPLEMENTATION OF THE RICHARDSON ALGORITHM FOR PROTEIN CHAINS
!
          if (mol(i)%res(1)%res_ntype.eq.2) then
!
!          PRINT*, ' '
!          PRINT*, ' --- NEXT RESIDUE ---'
!          PRINT*, ' '
!          PRINT*, ' We are now looking at residue i = ', mol(i)%res(1)%res_nold
!
! Axis point 1 = residue_i-1
          Loop4a: DO q=1,mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam

              IF (atnm==ca_name) THEN         ! axis_point1 is CA of residue_i-1
                axis_point1(1) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                axis_point1(2) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                axis_point1(3) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
              END IF
          END DO Loop4a
!
! Axis point 3 = residue_i+1
          Loop4b: DO q=1,mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam

              IF (atnm==ca_name) THEN         ! axis_point3 is CA of residue_i+1
                axis_point3(1) = mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                axis_point3(2) = mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                axis_point3(3) = mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
              END IF
          END DO Loop4b
!             
! Axis point 2 = residue_i
          Loop4c: DO q=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam

              IF (atnm==ca_name) THEN         ! axis_point2 is CA of residue_i
                temp_vector(1) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                temp_vector(2) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                temp_vector(3) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)

                CALL BACKRUB_CALC(axis_point1,axis_point3,temp_vector,qrot,temp_vector_back ) 

                axis_point2(1) = temp_vector_back(1)
                axis_point2(2) = temp_vector_back(2)
                axis_point2(3) = temp_vector_back(3)
!
!                PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                PRINT*, 'axis_point1 = ', axis_point1
!                PRINT*, 'axis_point3 = ', axis_point3
!                PRINT*, ' Residue ', mol(m1)%res(m2)%res_num
!                PRINT*, 'temp_vector = CA before rotation = ', temp_vector
!                PRINT*, 'axis_point2 = CA after rotation = ', axis_point2
!                                
              END IF
          END DO Loop4c
!
! NOW IDENTIFY ALL MAINCHAIN ATOMS AND ROTATE THEM qrot DEGREES AND THEN BACK BY -qrot DEGREES
!
          Loop5: DO q=1,mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam

              Loop5a: IF (atnm==c_name) THEN             ! C atom of res_i-1
                point1(1) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point1(2) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point1(3) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
!
                  CALL BACKRUB_CALC(axis_point1,axis_point3,point1,qrot,temp_vector )
                  CALL BACKRUB_CALC(axis_point1,axis_point2,temp_vector,qrot2,temp_vector_back)
!
!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'point1 = C atom of res_i-1   ', point1
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3                  
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '

                  mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
                  mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
                  mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)

              ELSE IF (atnm==o_name) THEN        ! O atom of res_i-1
                point2(1) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point2(2) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point2(3) = mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
!
                  CALL BACKRUB_CALC(axis_point1,axis_point3,point2,qrot,temp_vector)
                  CALL BACKRUB_CALC(axis_point1,axis_point2,temp_vector,qrot2,temp_vector_back)
!
!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'point2 = O atom of res_i-1   ', point2
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3  
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '
!
                  mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
                  mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
                  mol(m1)%res(m2-1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)
!
            END IF Loop5a
          END DO Loop5
!
!
          Loop6: DO q=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam
!
              Loop6a: IF (atnm==n_name) THEN              ! N atom of res_i
                point3(1) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point3(2) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point3(3) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
!
                  CALL BACKRUB_CALC( axis_point1, axis_point3, point3, qrot, temp_vector )
                  CALL BACKRUB_CALC( axis_point1, axis_point2, temp_vector, qrot2, temp_vector_back )
!
!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'point3 = N atom of res_i     ', point3
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3  
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '
!
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)
!
              ELSE IF (atnm==ca_name) THEN              ! CA atom of res_i  --> only rotated once!
                point4(1)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point4(2)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point4(3)=mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
!
                  CALL BACKRUB_CALC(axis_point1,axis_point3,point4,qrot,temp_vector)
!
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector(1)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector(2)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector(3)

!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'point4 = CA atom of res_i    ', point4
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3  
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot.AND. 
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '

              ELSE IF (atnm==c_name) THEN                 ! C atom of res_i
                point5(1) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point5(2) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point5(3) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)

                  CALL BACKRUB_CALC( axis_point1,axis_point3,point5, qrot, temp_vector )
                  CALL BACKRUB_CALC( axis_point2,axis_point3,temp_vector, qrot2, temp_vector_back )
!
!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3  
!                  PRINT*, 'point5 = C atom of res_i     ', point5
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '
!
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)

              ELSE IF (atnm==o_name) THEN                 ! O atom of res_i
                point6(1) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point6(2) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point6(3) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
!
                  CALL BACKRUB_CALC( axis_point1, axis_point3, point6, qrot, temp_vector )
                  CALL BACKRUB_CALC( axis_point2, axis_point3, temp_vector, qrot2, temp_vector_back )
!
!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3  
!                  PRINT*, 'point6 = O atom of res_i     ' , point6
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '

                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
                  mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)

            END IF Loop6a
          END DO Loop6


          Loop7: DO q=1,mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam

              Loop7a: IF (atnm==n_name) THEN             ! C atom of res_i+1
                point7(1) = mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                point7(2) = mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                point7(3) = mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)

                  CALL BACKRUB_CALC( axis_point1, axis_point3, point7, qrot, temp_vector )

                  CALL BACKRUB_CALC( axis_point2, axis_point3, temp_vector, qrot2, temp_vector_back )

                  mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
                  mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
                  mol(m1)%res(m2+1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)

!                  PRINT*, ' '
!                  PRINT*, '--- TEST DEBUG TEST DEBUG TEST DEBUG ---'
!                  PRINT*, 'point7 = N atom of res_i+1   ', point7
!                  PRINT*, 'axes points are '
!                  PRINT*, 'axis_point1 = ', axis_point1
!                  PRINT*, 'axis_point2 = ', axis_point2
!                  PRINT*, 'axis_point3 = ', axis_point3  
!                  PRINT*, 'temp_vector & qrot =         ', temp_vector, ' & ', qrot
!                  PRINT*, 'temp_vector_back & qrot2 =   ', temp_vector_back, ' & ', qrot2
!                  PRINT*, ' '
!
            END IF Loop7a
          END DO Loop7
!
!       HERE NUCLEOTIDS ARE BEING DEALT WITH. 
!
          else if (mol(i)%res(1)%res_ntype.eq.3) then
!
            axis_atnm1 =    br3_atnm1
            axis_nm1 = m2 + br3_noff1
            axis_atnm2 =    br3_atnm2
            axis_nm2 = m2 + br3_noff2              
!
            axis_point1=0.0
            axis_point2=0.0
            flag_point1=.false.
            flag_point2=.false.            
!
            DO q=1,mol(m1)%res(axis_nm1)%res_aas(m3)%aa_rots(m4)%rot_nat
              atnm=   mol(m1)%res(axis_nm1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam
              IF (atnm==axis_atnm1) THEN         ! axis_point1 is residue (i + br3_noff1)
                axis_point1(1)=mol(m1)%res(axis_nm1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                axis_point1(2)=mol(m1)%res(axis_nm1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                axis_point1(3)=mol(m1)%res(axis_nm1)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
                flag_point1=.true.
              END IF
            END DO 
!
            DO q=1,mol(m1)%res(axis_nm2)%res_aas(m3)%aa_rots(m4)%rot_nat
              atnm=   mol(m1)%res(axis_nm2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam
              IF (atnm==axis_atnm2) THEN         ! axis_point2 is residue (i + br3_noff2)
                axis_point2(1)=mol(m1)%res(axis_nm2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
                axis_point2(2)=mol(m1)%res(axis_nm2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
                axis_point2(3)=mol(m1)%res(axis_nm2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
                flag_point2=.true.
              END IF
            END DO 
!
            if ((.not.flag_point1).or.(.not.flag_point2)) then 
            PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
            PRINT*, '>>>> COULD NOT LOCATE AXES POINTS IN                      '
            PRINT*, '>>>> APPLY_BACKRUB_MAINCHAIN. IT IS UNCLEAR WHY           '
            PRINT*, '>>>> MUST STOP                                            '
            PRINT*, '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BUMMER >>'
                 stop
            end if
!
!           NOW APPLYING THE ROTATION TO THE BACKBONE ATOMS LOCATED BETWEEN THESE AXES POINTS
!
!
            DO q=1,mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_nat
            atnm=   mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_nam
!
               do l= 1,size(br3_ats)
!              
            IF (atnm==br3_ats(l)) THEN 
              temp_vector(2) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2)
              temp_vector(1) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1)
              temp_vector(3) = mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3)
!
              CALL BACKRUB_CALC(axis_point1,axis_point2,temp_vector,qrot,temp_vector_back)
!
              mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(1) = temp_vector_back(1)
              mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(2) = temp_vector_back(2)
              mol(m1)%res(m2)%res_aas(m3)%aa_rots(m4)%rot_ats(q)%at_xyz(3) = temp_vector_back(3)
            END IF
!            
                end do
!            
            END DO
!
            end if     ! finished looking at either aminoacids or nucleotides
!  
        END IF Loop3     
      END IF Loop2
    END DO Loop1
!
!   
   OPEN( UNIT = 19, FILE = '0_bck_sum', STATUS = 'UNKNOWN', ACTION = 'WRITE', IOSTAT = io_error )
     IF ( io_error /= 0 ) THEN
       WRITE (*,*) 'Sorry, problems writing backrubbed residues into 0_bck_sum'
     ELSE
       DO i=2, mol_nmls
         IF (mol(i)%res(1)%res_bbr) then
         IF (abs(mol(i)%res(1)%res_aas(1)%aa_rots(1)%rot_qrot).gt.err3) THEN
         WRITE (19,*) mol(i)%res(1)%res_chid, mol(i)%res(1)%res_nold, mol(i)%res(1)%res_aas(1)%aa_rots(1)%rot_qrot
         END IF
         END IF
       END DO
     END IF
   CLOSE( UNIT = 19 )
!
!
    PRINT*, '         ... DONE    '
    PRINT*, '        '
!
!
     END SUBROUTINE APPLY_BACKRUB_MAINCHAIN
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!
